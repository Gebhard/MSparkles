%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AnnotationManager < com.common.Cloneable
    properties(Access=private)
        dataMap;        
    end
    
    properties(Transient, Dependent)
        Values;
        Count;
    end    

    methods
        function obj = AnnotationManager()
            obj.dataMap = containers.Map();
        end
    end
    
    methods
        function value = get.Values(obj)
            try
                value = obj.dataMap.values;
            catch
                value = [];
            end
        end
        
        function value = get.Count(obj)
            try 
                value = obj.dataMap.Count;
            catch
                value = 0;
            end
        end
        
        function set.dataMap(obj, value)
            obj.dataMap = value;
            
            values = obj.dataMap.values;
            
            for i=1:numel(values)
                values{i}.Parent = obj;
            end
        end
        
        function AddAnnotation(obj, hAnnotation)
            obj.dataMap(hAnnotation.ID) = hAnnotation;
            hAnnotation.Parent = obj;
        end
        
        function DeleteAnnotation(obj, annotation)
            hAffectedDataset = [];
            
            if ischar(annotation) || isstring(annotation)
                hAnnotation = obj.dataMap(annotation);
            else
                hAnnotation = annotation;
            end
            
            hAnnotation.Selected = false;
            
            try
                hProjectManager = com.Management.MProjectManager.GetInstance();
                
                try
                    if ~isempty(hProjectManager)
                        hAffectedDataset = hProjectManager.DeleteAnnotationNode(hAnnotation);                
                    end
                catch
                end
                
                obj.dataMap.remove(hAnnotation.ID);
                hAnnotation.delete();
                   
                if ~isempty(hProjectManager)
                    if ~isempty(hAffectedDataset)
                        hAffectedDataset.Save();
                    end
                end
            catch
            end                                                                      
        end
         
        function SelectAnnotation(obj, hAnnotation, isSelected)
            values = obj.dataMap.values;
            for i=1:obj.dataMap.Count
                values{i}.Selected = false ;
            end
            
            if ~isempty(hAnnotation)
                hAnnotation.Selected = isSelected;
            end
        end
        
        function RenderAnnotations(obj, hStackScroller)
            values = obj.dataMap.values;
            
            for i=1:obj.dataMap.Count
                values{i}.Render(hStackScroller);
            end
        end
        
        function DeleteShapes(obj)
            values = obj.dataMap.values;
            
            for i=1:obj.dataMap.Count
                values{i}.DeleteShape();
            end
        end
        
        function hAnnotation = GetAnnotation(obj, ID)
            try
                hAnnotation = obj.dataMap(ID);
            catch
                hAnnotation = [];
            end
        end
        
        function Clear(obj)
            values = obj.dataMap.values;
            
            for i=1:obj.dataMap.Count
                values{i}.DeleteShape();
            end
        end
        
        function HideAll(obj)
            obj.SetVisibility('off');
        end
        
        function ShowAll(obj)
            obj.SetVisibility('on');
        end
    end    
    
    %Delete in 1.9
    methods(Static)
        function obj = UpgradeFrom(hMannotationMngr)
            obj = hMannotationMngr;
%             obj = com.Annotations.AnnotationManager();
% 
%             vals = hMannotationMngr.Values;
%             if ~isempty(vals)
%                 for i=1:hMannotationMngr.Count
%                     obj.AddAnnotation( com.Annotations.MAnnotation.Convert( vals(i) ) );
%                 end
%             end
%             
        end
    end
    
    methods(Access=private)
        function SetVisibility(obj, visibility)
             values = obj.dataMap.values;
            
            for i=1:obj.dataMap.Count
                values{i}.Visible = visibility;
            end
        end
    end
end

