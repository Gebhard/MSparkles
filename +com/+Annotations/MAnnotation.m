%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MAnnotation < com.common.Data.MSparklesObject
    %MANNOTATION Annotation data class
    %   This class stores all parameters of an annotation
    %   Visual representation of annotations is handled by clases derived
    %   from 'Graphics.MAnnotationShape'. This uncouples data and
    %   visualization and further allows to dispaly an annotation on
    %   multiple instances of type MStackScroller, e.g. in the Video export
    %   dialog.   
    
    properties(SetAccess=?com.Annotations.MAnnotationShape, Transient)
        hShape;
    end
    
    properties(SetAccess=protected)
        Style;   % type of this annotation, set by derived classes   
    end    
    
    properties(Transient, SetAccess = ?com.Annotations.AnnotationManager)
        Parent;
    end
    
    properties(Transient, SetObservable, AbortSet)
        Selected;
        Visible;
    end
    
    properties(Transient, Dependent)
        ShapeProps;
        FontProps;
        Length;
        AnnotationID;
    end
    
    properties(SetObservable, AbortSet)        
        String;                     % Text to be displayed
        ChannelID;                  % 1xn vector, specifying in which channel(s) the annotations is visible      
        Frames;                     % nx2 Array with ranges if frame numbers in which the annotation is visible     
        Position;                   % 1x4 Vector [X,Y,W,H]
        
        %Shape settings
        FaceColor;                  %Face color of the shape
        FaceAlpha;                  %Transparency of the face color
        EdgeColor;                  %Color of the edge, surrounding the shape
        EdgeAlpha;                  %Transparency of the edge
        LineWidth;                  %Thickness of the edge
        
        %Font settings
        FontSize;                   %Size of the font in points (1/72 inch), >0
        FontWeight;                 %'bold' -OR- 'normal'
        FontAngle;                  %'italic' -OR- 'normal'
        FontName;                   %Name of a supported font
        FontColor;                  %Font Color
        HorizontalAlignment;        %'left', 'center', 'right'
        VerticalAlignment;          %'middle', 'top', 'bottom', 'baseline', 'cap'                
    end
    
    %% Construction & destruction
    methods
        function obj = MAnnotation(style, position)
            obj = obj@com.common.Data.MSparklesObject();
            
            if nargin < 2 || isempty(position)
                position = [100,100,50,50];
            end
            
            obj.Style = style;
            obj.Position = position;
            
            obj.SetDefaultParams();                        
        end     
        
        function delete(obj)
            obj.BeingDeleted = true;  
            obj.DeleteShape();
        end
    end   
    
    %% Public interface
    methods         
        function set.Visible(obj, value)
            obj.Visible = value;
        end
        
        function set.AnnotationID(obj, value)
            obj.ID = value;
        end
        
        function value = get.String(obj)
            if isempty(obj.String)
                value = char('');
            else
                value = obj.String;
            end
        end
        
        function set.Selected(obj, value)
            obj.Selected = value;
        end
        
        function value = get.Selected(obj)
            if isempty(obj.Selected)
                value = false;
            else
                value = obj.Selected;
            end
        end
        
        function value = get.Length(obj)
            value = obj.Position(3);
        end
        
        function set.Position(obj, value)
            obj.Position = value;
        end
        
        function props = get.ShapeProps(obj)
            props = {'FaceColor', obj.FaceColor, 'FaceAlpha', obj.FaceAlpha, ...
                'EdgeColor', obj.EdgeColor, 'EdgeAlpha', obj.EdgeAlpha, ...
                'LineWidth', obj.LineWidth};
        end
        
        function props = get.FontProps(obj)
            props = {'FontSize', obj.FontSize, 'FontWeight', obj.FontWeight, ...
                'FontAngle', obj.FontAngle, 'FontName', obj.FontName, ...
                'Color', obj.FontColor, 'HorizontalAlignment', obj.HorizontalAlignment, ...
                'VerticalAlignment', obj.VerticalAlignment};
        end
        
        function DeleteShape(obj)
            if ~isempty(obj.hShape)
                try
                    delete( obj.hShape );
                    obj.hShape = [];
                catch
                end
            end
        end
        
        function hAnnotationShape = Render(obj, hStackScroller)
            if isempty(obj.hShape) || ~isvalid(obj.hShape)
                switch(obj.Style)
                    case com.Enum.AnnotationType.Rect
                        obj.hShape = com.Annotations.MRectAnnotation(hStackScroller, obj);
                    case com.Enum.AnnotationType.Ellipse
                        obj.hShape = com.Annotations.MEllipseAnnotation(hStackScroller, obj);
                    case com.Enum.AnnotationType.Arrow
                        obj.hShape = com.Annotations.MArrowAnnotation(hStackScroller, obj);
                    case com.Enum.AnnotationType.Timestamp
                        obj.hShape = com.Annotations.MTimeStampAnnotation(hStackScroller, obj);
                    case com.Enum.AnnotationType.Scalebar
                        obj.hShape = com.Annotations.MScaleBarAnnotation(hStackScroller, obj);
                    otherwise
                        obj.hShape = [];
                end  
            else
                obj.hShape.StackScroller = hStackScroller;
                obj.hShape.RegisterPropListeners()
            end
            
            hAnnotationShape = obj.hShape;
        end
        
        function Select(obj, isSelected)
            obj.Parent.SelectAnnotation(obj, isSelected);
        end      
        
        function Edit(obj, hDataset)
            if nargin < 2
                hDataset = [];
            end
            
            com.CalciumAnalysis.UI.dlgEditAnnotation(obj, hDataset);
        end
    end
    
    %% Helper functions
    methods(Access=private)
        function value = GetDefaultAnnotationName(obj)
            try
                value = char(obj.Style);
            catch
                value = '';
            end
        end
        
        function SetDefaultParams(obj)
            hMSparkles = MSparklesSingleton.GetInstance();
            hCaMainWnd = hMSparkles.hCaMainWnd;
            
            obj.Name = obj.GetDefaultAnnotationName();
            obj.String = char('');
            if ~isempty(hCaMainWnd)
                obj.ChannelID = hCaMainWnd.CurrentChannel;
            end
            
            obj.Frames = [];                       
            
            obj.FaceColor   = [0.3010 0.7450 0.9330];
            obj.FaceAlpha   = 0.5;
            
            obj.Visible = 'on';
                        
            switch obj.Style
                case com.Enum.AnnotationType.Scalebar
                    obj.LineWidth   = 2;
                    obj.HorizontalAlignment = 'center';    
                    obj.VerticalAlignment = 'cap';
                    obj.EdgeColor   = [1 1 1];
                    obj.EdgeAlpha   = 1;
                    
                    hDataset = hCaMainWnd.hDataset;
                    if ~isempty(hDataset)
                        metadata = hDataset.MetaData;
                        obj.Position(3) = metadata.ScalebarSize;
                    end
                otherwise
                    obj.LineWidth   = 1;
                    obj.HorizontalAlignment = 'left';
                    obj.VerticalAlignment = 'middle';
                    obj.EdgeColor   = [0 0.4470 0.7410];
                    obj.EdgeAlpha   = 0.9;
            end
            
            
            obj.FontSize    = 14;
            obj.FontWeight  = 'normal';
            obj.FontAngle   = 'normal';
            obj.FontName    = 'Calibri';
            obj.FontColor   = [1, 1, 1];
        end
    end
end

