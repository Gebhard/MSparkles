%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MAnnotationShape < handle
    %MANNOTATION Summary of this class goes here
    %   Detailed explanation goes here       
    
    properties(Access=protected)
        hShape;                     % Handle to the patch, representing the shape of the annotation
        hAxis;                      % Handle to the parent axis on which this annotation is displayed
        hRotatable;                 % Handle to a manipulation object
        hCtxMenu;                   % Handle to the context menu
        MouseDownPos;

        hStackScroller;             % Handle to the stackscroller, displaying this annotation
        hAnnotation com.Annotations.MAnnotation;    % Handle to the underlying MAnnotation object
    end
    
    %Listeners
    properties(Access=protected)
        arrPropListeners;       
    end
          
    properties(Dependent)
        StackScroller;              
        Selected;
        Position;
        Width;
        Height;
                
        PointCenter;
        PointNorth;
        PointNorthEast;
        PointEast;
        PointSouthEast;
        PointSouth;
        PointSouthWest;
        PointWest;
        PointNorthWest;
        
        Visible;
    end
    
    %% Construction & destruction
    methods
        function obj = MAnnotationShape(hStackScroller, hAnnotation) 
            obj.StackScroller = hStackScroller;            
            obj.hAnnotation = hAnnotation;
            obj.hAnnotation.hShape = obj;
            obj.InitAnnotation();    
        end
        
        function delete(obj) 
            obj.hAnnotation.hShape = [];
            obj.DeletePropListeners();
            obj.DeleteDragRect();
            obj.DeleteShape();
            obj.DeleteContextMenu();
        end
    end  
    
    %% Getters and Setters
    methods
        function value = get.Visible(obj)
            if obj.IsVisibleOnFrame(obj.hStackScroller.CurrentFrame) && ...
                obj.IsVisibleInChannel(obj.hStackScroller.CurrentChannel) && ...
                strcmpi(obj.hAnnotation.Visible, 'on')
                
                value = 'on';
            else
                value = 'off'; 
            end
        end
        
        function value = get.Position(obj)
            value = obj.hAnnotation.Position;
        end
        
        function set.Position(obj, value)
            obj.hAnnotation.Position = value;
        end
        
        function value = get.Width(obj)
            value = obj.Position(3);
        end
        
        function set.Width(obj, value)
            obj.Position(3) = value;
        end
        
        function value = get.Height(obj)
            value = obj.Position(4);
        end
        
        function value = get.Selected(obj)            
            value = obj.hAnnotation.Selected;
        end
        
        function set.Selected(obj, value)
            obj.hAnnotation.Select(value);                        
        end
        
        function set.StackScroller(obj, value)
            if isa(value, 'com.CalciumAnalysis.Rendering.MStackPlayer')
                obj.hStackScroller = value;
                obj.hAxis = obj.hStackScroller.mAxes;                                                
            else
                error('Invalid type: value must be an object of type MStackScroller.');
            end
        end
        
        function value = get.StackScroller(obj)
            value = obj.hStackScroller;
        end
        
        function value = get.PointCenter(obj)
            value = obj.PointNorthWest + [obj.Width/2, obj.Height/2];
        end
        
        function value = get.PointNorth(obj)
            value = obj.PointNorthWest + [obj.Width/2, 0];
        end
        
        function value = get.PointNorthEast(obj)
            value = obj.PointNorthWest + [obj.Width, 0];
        end
                
        function value = get.PointEast(obj)
            value = obj.PointNorthWest + [obj.Width, obj.Height/2];
        end
        
        function value = get.PointSouthEast(obj)
            value = obj.PointNorthWest + [obj.Width, obj.Height];
        end
                
        function value = get.PointSouth(obj)
            value = obj.PointNorthWest + [obj.Width/2, obj.Height];
        end
        
        function value = get.PointSouthWest(obj)
            value = obj.PointNorthWest + [0, obj.Height];
        end        
        
        function value = get.PointWest(obj)
            value = obj.PointNorthWest + [0, obj.Height/2];
        end
        
        function value = get.PointNorthWest(obj)
            value = [obj.Position(1), obj.Position(2)];
        end
    end
    
    %% Interaction
    methods
        function ButtonDownFcn(obj, ~, hEvent)
            if obj.IsVisible() && hEvent.Button == 1
                if ~obj.Selected
                    obj.Selected = true;
                end
            end
        end
        
        function ButtonUpFcn(obj, ~, ~)
            if all(obj.hRotatable.MouseDownPos == obj.hRotatable.LastMousePos)
                if obj.Selected                
                    obj.Selected = false;
                else
                    obj.Selected = true;
                end
            end
        end
        
        function MouseMoveFcn(~, ~, ~)
        end
    end
    
    methods
        function RegisterPropListeners(obj)
            obj.DeletePropListeners();
            
            obj.arrPropListeners{end+1} = addlistener(obj.hStackScroller, 'CurrentFrame', 'PostSet', @obj.OnFrameChanged);
            obj.arrPropListeners{end+1} = addlistener(obj.hStackScroller, 'CurrentChannel', 'PostSet', @obj.OnChannelChanged);            
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'Position', 'PostSet', @obj.OnPositionChanged);                
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'FaceColor', 'PostSet', @obj.OnFaceColorChanged);                
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'FaceAlpha', 'PostSet', @obj.OnFaceAlphaChanged);                
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'EdgeColor', 'PostSet', @obj.OnEdgeColorChanged);                
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'EdgeAlpha', 'PostSet', @obj.OnEdgeAlphaChanged);                
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'LineWidth', 'PostSet', @obj.OnLineWidthChanged);                                          
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'Selected', 'PostSet', @obj.OnSelectionChanged);
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'BeingDeleted', 'PostSet', @obj.OnDeleteAnnotation);
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'Visible', 'PostSet', @obj.OnVisibilityChanged);
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'Frames', 'PostSet', @obj.OnFrameChanged);
            obj.arrPropListeners{end+1} = addlistener(obj.hAnnotation, 'ChannelID', 'PostSet', @obj.OnChannelChanged);
        end
    end
    
    methods(Access = protected)
        function OnPositionChanged(obj, ~, ~)
            obj.UpdateShape(obj.hAnnotation.Position);
        end
        
        function OnFaceColorChanged(obj, ~, ~)
            obj.hShape.FaceColor = obj.hAnnotation.FaceColor;
        end
        
        function OnFaceAlphaChanged(obj, ~, ~)
            obj.hShape.FaceAlpha = obj.hAnnotation.FaceAlpha;
        end
        
        function OnEdgeColorChanged(obj, ~, ~)
            obj.hShape.EdgeColor = obj.hAnnotation.EdgeColor;
        end
        
        function OnEdgeAlphaChanged(obj, ~, ~)
            obj.hShape.EdgeAlpha = obj.hAnnotation.EdgeAlpha;
        end
        
        function OnLineWidthChanged(obj, ~, ~)
            obj.hShape.LineWidth = obj.hAnnotation.LineWidth;
        end
        
        function OnFrameChanged(obj, ~, ~)
            %If no range is specified, the annotation is allways visible
            if obj.IsVisibleOnFrame(obj.hStackScroller.CurrentFrame)
                obj.Show();
            else
                obj.Hide();
            end
        end
        
        function OnChannelChanged(obj, ~, ~)                         
            if obj.IsVisibleInChannel(obj.hStackScroller.CurrentChannel)
                obj.Show();
            else
                obj.Hide();
            end
        end
        
        function OnSelectionChanged(obj, ~, ~)
            if obj.hAnnotation.Selected == true
                if isempty(obj.hRotatable)
                    obj.CreateDragRect();
                end
            else
                obj.DeleteDragRect();
            end
        end
        
        function OnDeleteAnnotation(obj, ~, ~)
            obj.delete();
        end
        
        function OnVisibilityChanged(obj, ~, ~)
            try
                obj.hShape.Visible = obj.Visible;
            catch
            end
        end
        
        function OnCreateContextMenu(obj, hCtxMenu)
            uimenu(hCtxMenu,'Text','Edit','MenuSelectedFcn', @obj.OnCtxMenuEdit);
            uimenu(hCtxMenu,'Text','Delete','MenuSelectedFcn', @obj.OnCtxMenuDelete);
        end  
        
        function OnCtxMenuEdit(obj, ~, ~)  
            obj.hAnnotation.Edit();
        end
        
        function OnCtxMenuDelete(obj, ~, ~)
            obj.hAnnotation.Parent.DeleteAnnotation(obj.hAnnotation);
        end
        
        function DeletePropListeners(obj)
            for i=1:numel(obj.arrPropListeners)
                try
                    delete( obj.arrPropListeners{i} );
                catch
                end
            end
            
            obj.arrPropListeners = {};
        end
        
        function visible = IsVisibleOnFrame(obj, frame)
            %If no range is specified, the annotation is allways visible
            if ~isempty(obj.hAnnotation.Frames)                
                visible = ~(frame < obj.hAnnotation.Frames(1) || frame > obj.hAnnotation.Frames(2));
            else
                visible = true;
            end
        end
        
        function visible = IsVisibleInChannel(obj, channel)
            %If no range is specified, the annotation is allways visible
            if ~isempty(obj.hAnnotation.ChannelID)                
                visible = any(channel == obj.hAnnotation.ChannelID);
            else
                visible = true;
            end
        end
    end    
     
    methods(Access = private)                
        function CreateContextMenu(obj, varargin)
            try
                obj.DeleteContextMenu();
                
                obj.hCtxMenu = uicontextmenu('Parent', obj.hAxis.Parent.Parent);            
                obj.OnCreateContextMenu(obj.hCtxMenu);
            catch
                obj.hCtxMenu = [];
            end
        end
        
        function CreateDragRect(obj)
            try
                if isempty(obj.hRotatable)
                    obj.hRotatable = com.Annotations.MRotatable(obj.hAxis, obj.Position);
                    obj.hRotatable.Child = obj;
                    %obj.hRotatable.UIContextMenu = obj.hCtxMenu;
                end
            catch 
            end
        end
    end
    
    %% Public interface
    methods
        function InitAnnotation(obj)
            obj.CreateShape();
            obj.RegisterPropListeners();            
            obj.CreateContextMenu();
            
            %Handle visibility 
            obj.OnFrameChanged();
            obj.OnChannelChanged();
            obj.OnVisibilityChanged();                        
            
            if ~isempty(obj.hShape)
                obj.hShape.PickableParts = 'all';
                obj.hShape.ButtonDownFcn = @obj.ButtonDownFcn;
                if ~isempty(obj.hCtxMenu)
                    obj.hShape.UIContextMenu = obj.hCtxMenu;                
                end
            end      
        end
        
        function DeleteShape(obj)
            try
                obj.hShape.delete();                
            catch
            end
            obj.hShape = [];
        end
        
        function DeleteDragRect(obj)
            try
                obj.hRotatable.Child = [];
                obj.hRotatable.delete();
            catch
            end
            
            obj.hRotatable = [];
        end
        
        function DeleteContextMenu(obj)
            try
                obj.hCtxMenu.delete();
            catch                
            end
            
            obj.hCtxMenu = [];
        end
        
        function Show(obj)
            obj.hAnnotation.Visible = 'on';
        end
        
        function Hide(obj)
            obj.hAnnotation.Visible = 'off';
        end
        
        function visible = IsVisible(obj)
            visible = strcmpi(obj.hAnnotation.Visible, 'on');
        end
    end
    
    %% Abstract methods
    methods(Abstract, Access = protected)
        CreateShape(obj, varargin);
        UpdateShape(obj, position);
    end
end

