%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MArrowAnnotation < com.Annotations.MAnnotationShape
    %MELLIPSEANNOTATION Summary of this class goes here
    %   Detailed explanation goes here

    methods
        function obj = MArrowAnnotation(hStackScroller, hAnnotation)
            %MELLIPSEANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MAnnotationShape(hStackScroller, hAnnotation);
        end               
    end
    
     methods(Access = protected)
        function CreateShape(obj)
            f = [1 2 3 4];            
            
            v = [obj.PointWest; obj.PointNorthEast; obj.PointSouth; obj.PointCenter];
            obj.hShape = patch(obj.hAxis, 'Faces', f, 'Vertices', v, ...
                obj.hAnnotation.ShapeProps{:});
        end
         
        function UpdateShape(obj, position)             
            obj.hShape.Vertices = [position(1) + position(3), position(2);  %Arrow tip
                 position(1) + position(3)/2, position(2) + position(4);    %Bottom corner
                 position(1) + position(3)/2, position(2) + position(4)/2;  %Center corner
                 position(1), position(2) + position(4)/2];                 %bottomLeft
        end
     end
end

