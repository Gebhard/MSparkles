%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MDraggable < handle
    %MANNOTATION Summary of this class goes here
    %   Detailed explanation goes here        
    
    properties (Access=protected)
        hRect matlab.graphics.primitive.Rectangle;
    end
    
    properties(Access=private)
        OldWindowButtonMotionFcn; 
        OldWindowButtonUpFcn;        
        DragDirection com.CalciumAnalysis.Enum.DragDirectionEnum;
        UpdatePositionFcn;
        hPosListener;        
    end
    
    properties(SetAccess=private)
        MouseDownPos;
        LastMousePos;
    end
    
    properties        
        Child;
        ButtonDownHandler;
        ButtonUpHandler;         
    end
    
    properties(SetObservable, AbortSet)
        Center;
    end
    
    properties(Dependent)
        CornerNorth;
        CornerNorthEast;
        CornerEast;
        CornerSouthEast;
        CornerSouth;
        CornerSouthWest;
        CornerWest;
        CornerNorthWest;        
        Width;
        Height;
        Visible;
        UIContextMenu;
    end
    
    methods
        function obj = MDraggable(ax, varargin)
            API_version = '1.0.0';
            Modify_date = '28.03.2019';
            
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding            
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['MDraggable v.' API_version ' (' Modify_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addOptional(p,'Position', [10,10,25,25] );
            addOptional(p,'CenterPos', []);
            addOptional(p,'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirXY);
            addOptional(p,'FaceColor', 'none');
            addOptional(p,'EdgeColor', [0 0.4470 0.7410]);
            addOptional(p,'LineWidth', 0.5);

            % input parsing
            parse(p,varargin{:});
            par = p.Results;
           
            if ~isempty(par.CenterPos)
                position = [par.CenterPos(1) - par.CenterPos(3)/2, ...
                    par.CenterPos(2) - par.CenterPos(4)/2,...
                    par.CenterPos(3), par.CenterPos(4)];
            else
                position = par.Position;
            end
            
            obj.hRect = rectangle(ax, 'Position', position, 'EdgeColor', par.EdgeColor, ...
                'FaceColor', par.FaceColor, 'LineWidth', par.LineWidth);  
            
            obj.hRect.ButtonDownFcn = @obj.OnBtnDwn; 
            obj.hRect.PickableParts = 'all';  
            
            obj.hPosListener = addlistener(obj.hRect, 'Position', 'PostSet', @obj.OnPositionChanged);
            
            obj.DragDirection = par.DragDirection;            
        end

        function delete(obj)
            if ~isempty(obj.hRect)
                obj.hRect.delete();                             
            end
            
            delete( obj.hPosListener );
        end
    end
    
    methods 
        function SetCenterPos(obj, center)
            if isempty(obj.Center) || any(center ~= obj.Center)
                obj.Center = center;
                obj.hRect.Position(1:2) = center - [obj.Width/2, obj.Height/2];
            end
        end
        
        function set.DragDirection(obj, value)
            obj.DragDirection = value;
            switch (obj.DragDirection)
                case com.CalciumAnalysis.Enum.DragDirectionEnum.DirX
                    obj.UpdatePositionFcn = @obj.OnHorzDrag;
                case com.CalciumAnalysis.Enum.DragDirectionEnum.DirY
                    obj.UpdatePositionFcn = @obj.OnVertDrag;
                otherwise
                    obj.UpdatePositionFcn = @obj.OnBiDirDrag;
            end
        end
        
        function value = get.CornerNorth(obj)
            value = obj.CornerNorthWest + [obj.Width / 2, 0];
        end
        
        function value = get.CornerNorthEast(obj)
            value = obj.CornerNorthWest + [obj.Width, 0];
        end
        
        function value = get.CornerEast(obj)
            value = obj.CornerNorthWest + [obj.Width, obj.Height / 2];
        end
        
        function value = get.CornerSouthEast(obj)
            value = obj.CornerNorthWest + [obj.Width, obj.Height];
        end
        
        function value = get.CornerSouth(obj)
            value = obj.CornerNorthWest + [obj.Width / 2, obj.Height];
        end
        
        function value = get.CornerSouthWest(obj)
            value = obj.CornerNorthWest + [0, obj.Height];            
        end
        
        function value = get.CornerWest(obj)
            value = obj.CornerNorthWest + [0, obj.Height / 2];
        end
        
        function value = get.CornerNorthWest(obj)
            value = [obj.hRect.Position(1), obj.hRect.Position(2)];
        end
        
        function value = get.Width(obj)
            value = obj.hRect.Position(3);
        end
        
        function value = get.Height(obj)
            value = obj.hRect.Position(4);
        end
        
        function set.CornerNorth(obj, value)
            sw = obj.CornerSouthWest;
            wh = abs(value - sw);
            
            obj.hRect.Position = [sw(1), value(2), obj.Width, wh(2)];
        end
        
        function set.CornerNorthEast(obj, value)
            sw = obj.CornerSouthWest;            
            wh = abs(value - sw);
            
            obj.hRect.Position = [sw(1), value(2), wh(1), wh(2)];
        end
        
        function set.CornerEast(obj, value)
            nw = obj.CornerNorthWest;            
            wh = abs(value - nw);
            
            obj.hRect.Position = [nw(1), nw(2), wh(1), obj.Height];
        end
        
        function set.CornerSouthEast(obj, value)
            nw = obj.CornerNorthWest;
            wh = abs(value - nw);
            
            obj.hRect.Position = [nw(1), nw(2), wh(1), wh(2)]; 
        end
        
        function set.CornerSouth(obj, value)
            nw = obj.CornerNorthWest;
            wh = abs(value - nw);
            
            obj.hRect.Position = [ nw(1), nw(2), obj.Width, wh(2)];
        end
        
        function set.CornerSouthWest(obj, value)
            ne = obj.CornerNorthEast;
            wh = abs(value - ne);
            
            obj.hRect.Position = [ value(1), ne(2), wh(1), wh(2)];
        end
        
        function set.CornerWest(obj, value)
            ne = obj.CornerNorthEast;
            wh = abs(value - ne);
            
            obj.hRect.Position = [value(1), ne(2), wh(1), obj.Height];
        end
        
        function set.CornerNorthWest(obj, value)
            se = obj.CornerSouthEast;
            wh = abs(value - se);
            
            obj.hRect.Position = [value(1), value(2), wh(1), wh(2)];
        end
        
        function set.Width(obj, value)
            obj.hRect.Position(3) = value;
        end
        
        function set.Height(obj, value)
            obj.hRect.Position(4) = value;
        end   
        
        function set.Visible(obj, value)
            obj.hRect.Visible = value;
            obj.hRect.HitTest = value; 
            obj.OnVisibilityChanged();
        end
        
        function value = get.Visible(obj)
            value = obj.hRect.Visible;
        end
        
        function Hide(obj)
            obj.Visible = 'off';
        end
        
        function Show(obj)
            obj.Visible = 'on';
        end
        
        function set.UIContextMenu(obj, value)
            try
                obj.hRect.UIContextMenu = value;
            catch 
            end
        end
        
        function value = get.UIContextMenu(obj)
            try
                value = obj.hRect.UIContextMenu;
            catch 
            end
        end
    end
    
    methods(Access = protected)
        function OnVisibilityChanged(obj)
            
        end        
    end
    
    methods(Access = private) 
        function OnPositionChanged(obj, ~, ~)
            obj.Center = obj.CornerNorthWest + [obj.Width/2, obj.Height/2];
            obj.Child.Position = obj.hRect.Position;
        end                
        
        function OnBtnDwn(obj, btnSrc, btnDwnEvent)
            parentFig = gcbf();
            obj.LastMousePos = btnDwnEvent.IntersectionPoint;             
            obj.MouseDownPos = obj.LastMousePos;
            obj.OldWindowButtonMotionFcn = parentFig.WindowButtonMotionFcn;
            obj.OldWindowButtonUpFcn = parentFig.WindowButtonUpFcn;
            
            parentFig.WindowButtonMotionFcn = @obj.OnMouseMove;
            parentFig.WindowButtonUpFcn = @obj.OnButtonUp;                                    
            
            if ~isempty(obj.ButtonDownHandler)
                obj.ButtonDownHandler();
            end        
            
            if ~isempty(obj.Child)
                try
                    obj.Child.ButtonDownFcn(btnSrc, btnDwnEvent);
                catch
                end
            end
        end
        
        function OnMouseMove(obj, moveSrc, moveEvent)
            if ~isempty(obj.LastMousePos)
                motion = moveEvent.IntersectionPoint - obj.LastMousePos;
                obj.LastMousePos = moveEvent.IntersectionPoint;
  
                obj.UpdatePositionFcn(motion);                      
                
                if ~isempty(obj.Child)
                    try
                        obj.Child.MouseMoveFcn(moveSrc, btnDwnEvent);
                    catch
                    end
                end                            
            end
        end
        
        function OnButtonUp(obj, btnSrc, btnUpEvent)
            parentFig = gcbf();
            parentFig.WindowButtonMotionFcn = obj.OldWindowButtonMotionFcn;
            parentFig.WindowButtonUpFcn = obj.OldWindowButtonUpFcn;
            
            if ~isempty(obj.ButtonUpHandler)
                obj.ButtonUpHandler();
            end
            
            if ~isempty(obj.Child)
                try
                    obj.Child.ButtonUpFcn(btnSrc, btnUpEvent);
                catch
                end
            end
                    
            try
                obj.LastMousePos = [];    
                obj.MouseDownPos = [];
            catch
            end
        end 
        
        function OnHorzDrag(obj, motion)
            obj.hRect.Position(1) = obj.hRect.Position(1) + motion(1); 
        end
        
        function OnVertDrag(obj, motion)
            obj.hRect.Position(2) = obj.hRect.Position(2) + motion(2); 
        end
        
        function OnBiDirDrag(obj, motion)
            obj.hRect.Position(1:2) = obj.hRect.Position(1:2) + motion(1:2);   
        end
    end       
end

