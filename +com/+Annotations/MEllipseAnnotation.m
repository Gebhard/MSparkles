%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MEllipseAnnotation < com.Annotations.MTextAnnotation
    %MELLIPSEANNOTATION Summary of this class goes here
    %   Detailed explanation goes here

    methods
        function obj = MEllipseAnnotation(hStackScroller, hAnnotation)
            %MELLIPSEANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MTextAnnotation(hStackScroller, hAnnotation);
        end               
    end
    
     methods(Access = protected)
         function CreateShape(obj)
             nw = obj.PointNorthWest;
             r1 = obj.Width/2;
             r2 = obj.Height/2;
             t = linspace(0, 2*pi);
             x = nw(1) + r1 + r1*cos(t);
             y = nw(2) + r2 + r2*sin(t);
             obj.hShape = patch(obj.hAxis, 'XData', x, 'YData', y, obj.hAnnotation.ShapeProps{:});
             
             CreateShape@com.Annotations.MTextAnnotation(obj);
         end
         
         function UpdateShape(obj, position)   
             UpdateShape@com.Annotations.MTextAnnotation(obj, position);
             r1 = obj.Width/2;
             r2 = obj.Height/2;             
             t = linspace(0, 2*pi);
             obj.hShape.XData = position(1) + r1 + r1*cos(t);
             obj.hShape.YData = position(2) + r2 + r2*sin(t);
         end
     end
end

