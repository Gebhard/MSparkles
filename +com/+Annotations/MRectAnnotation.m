%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MRectAnnotation < com.Annotations.MTextAnnotation
    %MRECTANNOTATION Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = MRectAnnotation(hStackScroller, hAnnotation)
            %MRECTANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MTextAnnotation(hStackScroller, hAnnotation);
        end
    end
    
     methods(Access = protected)                
         function CreateShape(obj)             
             f = [1 2 3 4];
             v = [obj.PointNorthWest; obj.PointNorthEast; obj.PointSouthEast; obj.PointSouthWest];
             obj.hShape = patch(obj.hAxis, 'Faces', f, 'Vertices', v, obj.hAnnotation.ShapeProps{:});
             
             CreateShape@com.Annotations.MTextAnnotation(obj);
         end
         
         function UpdateShape(obj, position)
             UpdateShape@com.Annotations.MTextAnnotation(obj, position);
             obj.hShape.Vertices = [position(1), position(2);...        %topLeft
                 position(1) + position(3), position(2); ...            %topRight
                 position(1) + position(3), position(2) + position(4);  %bottomRight
                 position(1), position(2) + position(4)];               %bottomLeft
         end
    end
end

