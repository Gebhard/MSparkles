%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MResizable < com.Annotations.MDraggable
    %MRESIZABLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=private)
        AchorPoints = {};
        hListener;
    end
    
    methods
        function obj = MResizable(ax, position)
            obj = obj@com.Annotations.MDraggable(ax, 'Position', position);            
            try
                obj.CreateResizeHandles(ax);
            catch
            end
        end
        
        function delete(obj)
            obj.DeleteListener();            
            cellfun( @(x) x.delete(), obj.AchorPoints);
        end
    end
    
    methods(Access=private)
        function CreateResizeHandles(obj, ax)
            oldUnit = ax.Units;
            ax.Units = "pixels";
            
            %a bit rough, but works for now...
            ptSize = 10 / (512 / ax.YLim(2));
            
            
            AnchorN = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerNorth, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirY, 'LineWidth', 1);            

            AnchorNE = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerNorthEast, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirXY);

            AnchorE = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerEast, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirX);

            AnchorSE = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerSouthEast, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirXY);

            AnchorS = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerSouth, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirY);

            AnchorSW = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerSouthWest, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirXY);

            AnchorW = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerWest, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirX);

            AnchorNW = com.Annotations.MAnchorPoint(ax, 'CenterPos', [obj.CornerNorthWest, ptSize, ptSize], ...
                'DragDirection', com.CalciumAnalysis.Enum.DragDirectionEnum.DirXY);                        

            AnchorN.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorN, 'Center', 'PostSet', @obj.OnAnchorNChanged) );
            AnchorN.ButtonUpHandler = @() obj.DeleteListener();            
            AnchorNE.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorNE, 'Center', 'PostSet', @obj.OnAnchorNEChanged) );
            AnchorNE.ButtonUpHandler = @() obj.DeleteListener();
            AnchorE.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorE, 'Center', 'PostSet', @obj.OnAnchorEChanged) );
            AnchorE.ButtonUpHandler = @() obj.DeleteListener();
            AnchorSE.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorSE, 'Center', 'PostSet', @obj.OnAnchorSEChanged) );
            AnchorSE.ButtonUpHandler = @() obj.DeleteListener();
            AnchorS.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorS, 'Center', 'PostSet', @obj.OnAnchorSChanged) );
            AnchorS.ButtonUpHandler = @() obj.DeleteListener();            
            AnchorSW.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorSW, 'Center', 'PostSet', @obj.OnAnchorSWChanged) );
            AnchorSW.ButtonUpHandler = @() obj.DeleteListener();
            AnchorW.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorW, 'Center', 'PostSet', @obj.OnAnchorWChanged) );
            AnchorW.ButtonUpHandler = @() obj.DeleteListener();
            AnchorNW.ButtonDownHandler = @() obj.SetListener ( addlistener(AnchorNW, 'Center', 'PostSet', @obj.OnAnchorNWChanged) );
            AnchorNW.ButtonUpHandler = @() obj.DeleteListener();            
            obj.ButtonDownHandler = @() obj.SetListener ( addlistener(obj, 'Center', 'PostSet', @obj.OnMyCenterChanged) );
            obj.ButtonUpHandler = @() obj.DeleteListener();                                                                                                          

            obj.AchorPoints = {AnchorN;AnchorNE;AnchorE;AnchorSE;AnchorS;AnchorSW;AnchorW;AnchorNW};  
            
            ax.Units = oldUnit;
        end
        
        function SetListener(obj, hListener)            
            obj.DeleteListener();
            obj.hListener = hListener;
        end
        
        function DeleteListener(obj)
            if ~isempty(obj.hListener)
                try
                    obj.hListener.delete();
                catch
                end
            end
        end
        
        function OnAnchorNChanged(obj, ~, arg2)
            obj.CornerNorth = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorNEChanged(obj, ~, arg2)
            obj.CornerNorthEast = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorEChanged(obj, ~, arg2)
            obj.CornerEast = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorSEChanged(obj, ~, arg2)
            obj.CornerSouthEast = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorSChanged(obj, ~, arg2)
            obj.CornerSouth = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorSWChanged(obj, ~, arg2)
            obj.CornerSouthWest = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorWChanged(obj, ~, arg2)
            obj.CornerWest = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnAnchorNWChanged(obj, ~, arg2)
            obj.CornerNorthWest = arg2.AffectedObject.Center;
            obj.UpdateAnchorPoints();
        end
        
        function OnMyCenterChanged(obj, ~, ~)                        
            obj.UpdateAnchorPoints();
        end
        
        function UpdateAnchorPoints(obj)
            obj.AchorPoints{1}.SetCenterPos(obj.CornerNorth);
            obj.AchorPoints{2}.SetCenterPos(obj.CornerNorthEast);
            obj.AchorPoints{3}.SetCenterPos(obj.CornerEast);
            obj.AchorPoints{4}.SetCenterPos(obj.CornerSouthEast);
            obj.AchorPoints{5}.SetCenterPos(obj.CornerSouth);
            obj.AchorPoints{6}.SetCenterPos(obj.CornerSouthWest);
            obj.AchorPoints{7}.SetCenterPos(obj.CornerWest);
            obj.AchorPoints{8}.SetCenterPos(obj.CornerNorthWest);
        end
    end

    methods(Access=protected)
       function OnVisibilityChanged(obj)            
            if strcmpi(obj.Visible, 'off')
                cellfun(@(x)x.Hide(), obj.AchorPoints);
            else
                cellfun(@(x)x.Show(), obj.AchorPoints);
            end
        end 
    end    
end

