%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MScaleBarAnnotation < com.Annotations.MTextAnnotation
    %MELLIPSEANNOTATION Summary of this class goes here
    %   Detailed explanation goes here    
    
    properties(Access=private)
        hLine;
    end
    
    properties(Dependent)
        ScaleString;
    end
    
    methods
        function obj = MScaleBarAnnotation(hStackScroller, hAnnotation)
            %MELLIPSEANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MTextAnnotation(hStackScroller, hAnnotation);                           
        end               
    end
    
    methods
        function RegisterPropListeners(obj)
            RegisterPropListeners@com.Annotations.MTextAnnotation(obj);
        end
    end
    
    methods(Access = protected)        
        function CreateShape(obj)
            obj.RenderText(obj.ScaleString);
            
            ptWest = obj.PointWest;
            ptEast = obj.PointEast;
            
            obj.hShape = patch(obj.hAxis, 'XData',[ptWest(1), ptEast(1)], 'YData',[ptWest(2), ptEast(2)], ...
                obj.hAnnotation.ShapeProps{:});
        end         
        
        function OnScalebarChanged(obj, ~, ~)
            obj.String = obj.ScaleString;
        end
         
        function UpdateShape(obj, position)
            UpdateShape@com.Annotations.MTextAnnotation(obj, position);              
            
            ptWest = obj.PointWest;
            ptEast = obj.PointEast;
            
            obj.hShape.XData = [ptWest(1), ptEast(1)];
            obj.hShape.YData = [ptWest(2), ptEast(2)];
            
            obj.String = obj.ScaleString;
        end
     end
     
    methods
        function RenderText(obj, string)
            if isempty(obj.hText)
                pos = obj.Position;            
                obj.hText = text(obj.hAxis, pos(1), pos(2), '', obj.hAnnotation.FontProps{:});
                obj.hText.String = string;
                
                if obj.Height < obj.hText.Extent(4)
                    obj.Position(4) = obj.hText.Extent(4);
                end
                
            else
                obj.hText.String = string;
            end
                        
            obj.UpdateTextPos();
        end
         
        function UpdateTextPos(obj)
             
            switch(obj.HorizontalAlignment)                
                case 'right'
                    x = obj.Position(1) + obj.Width;
                case 'center'
                    x = obj.Position(1) + obj.Width / 2;
                otherwise %default to 'left'
                    x = obj.Position(1);
            end
            
            y = obj.Position(2) + (obj.Height / 2) + obj.hAnnotation.LineWidth;            
             
            obj.hText.Position(1:2) = [x, y];
        end                 
         
        function value = get.ScaleString(obj) 
            metadata = obj.StackScroller.hDataset.MetaData;            
            value = sprintf('%.1f %s', obj.Width, metadata.DomainUnit) ;
        end
    end    
end

