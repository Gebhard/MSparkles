%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef (Abstract) MTextAnnotation < com.Annotations.MAnnotationShape
    %MRECTANNOTATION Summary of this class goes here
    %   Detailed explanation goes here
        
    properties(Dependent)
        String;
        VerticalAlignment;
        HorizontalAlignment;
    end
    
    properties (Access=protected)
        hText;        
    end
    
    %% Construction & destruction
    methods
        function obj = MTextAnnotation(hStackScroller, hAnnotation)
            %MRECTANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MAnnotationShape(hStackScroller, hAnnotation);            
        end        
    end
    
    %% Getters & setters
    methods
        function value = get.String(obj)
            value = obj.hText.String;
        end
        
        function set.String(obj, value)
            if isempty(obj.hText) || ~isvalid(obj.hText)
                obj.CreateText(value)
            else
                obj.hText.String = value;
            end
        end
        
        function value = get.VerticalAlignment(obj)
            value = obj.hText.VerticalAlignment;
        end
        
        function set.VerticalAlignment(obj, value)
            obj.hText.VerticalAlignment = value;
            obj.UpdateTextPos();
        end
        
        function value = get.HorizontalAlignment(obj)
            value = obj.hText.HorizontalAlignment;
        end
        
        function set.HorizontalAlignment(obj, value)
            obj.hText.HorizontalAlignment = value;
            obj.UpdateTextPos();
        end
    end
    
    %% Public interface
    methods                
        function DeleteShape(obj)
            DeleteShape@com.Annotations.MAnnotationShape(obj);            
            try
                obj.hText.delete();                
            catch
            end
            obj.hText = [];
        end
        
        function InitAnnotation(obj)
            InitAnnotation@com.Annotations.MAnnotationShape(obj);             
            
            if ~isempty(obj.hText)
                obj.hText.PickableParts = 'all';
                obj.hText.ButtonDownFcn = @obj.ButtonDownFcn;
                if ~isempty(obj.hCtxMenu)
                    obj.hText.UIContextMenu = obj.hCtxMenu;
                end
            end   
        end
    end
    
    %% Methods overwrites
    methods
        function RegisterPropListeners(obj)
            RegisterPropListeners@com.Annotations.MAnnotationShape(obj);
            
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'String', 'PostSet', @obj.OnStringChanged);
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'FontSize', 'PostSet', @obj.OnFontSizeChanged);
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'FontWeight', 'PostSet', @obj.OnFontWeightChanged); 
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'FontAngle', 'PostSet', @obj.OnFontAngleChanged);                
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'FontName', 'PostSet', @obj.OnFontNameChanged);                
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'FontColor', 'PostSet', @obj.OnFontColorChanged);                
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'HorizontalAlignment', 'PostSet', @obj.OnHorizontalAlignmentChanged);                
            obj.arrPropListeners{end + 1} = addlistener(obj.hAnnotation, 'VerticalAlignment', 'PostSet', @obj.OnVerticalAlignmentChanged);                            
        end
    end
    
    methods(Access = protected)
        function OnVisibilityChanged(obj, ~, ~)
            OnVisibilityChanged@com.Annotations.MAnnotationShape(obj);
            try
                obj.hText.Visible = obj.Visible;
            catch
            end
        end
        
        function OnStringChanged(obj, ~, ~)
            obj.RenderText(obj.hAnnotation.String);
        end
        
        function OnFontSizeChanged(obj,~,~)
            obj.hText.FontSize = obj.hAnnotation.FontSize;
        end
        
        function OnFontWeightChanged(obj,~,~)
            obj.hText.FontWeight = obj.hAnnotation.FontWeight;
        end
         
        function OnFontAngleChanged(obj,~,~)
            obj.hText.FontAngle = obj.hAnnotation.FontAngle;
        end
                        
        function OnFontNameChanged(obj,~,~)
            obj.hText.FontName = obj.hAnnotation.FontName;
        end
                        
        function OnFontColorChanged(obj,~,~)
            obj.hText.Color = obj.hAnnotation.FontColor;
        end
                  
        function OnHorizontalAlignmentChanged(obj,~,~)
            obj.hText.HorizontalAlignment = obj.hAnnotation.HorizontalAlignment;
            obj.UpdateTextPos();
        end        
                       
        function OnVerticalAlignmentChanged(obj,~,~)
            obj.hText.VerticalAlignment = obj.hAnnotation.VerticalAlignment;
            obj.UpdateTextPos();
        end
        
        function CreateShape(obj)
            obj.RenderText(obj.hAnnotation.String);
         end
         
        function UpdateShape(obj, position)
             obj.hShape.Vertices = [position(1), position(2);... %topLeft
                 position(1) + position(3), position(2); ... %topRight
                 position(1) + position(3), position(2) + position(4); %bottomRight
                 position(1), position(2) + position(4)]; %bottomLeft
             obj.UpdateTextPos();
        end   
        
        function CreateText(obj, string)
            pos = obj.Position;            
            obj.hText = text(obj.hAxis, pos(1), pos(2), '', obj.hAnnotation.FontProps{:});
            obj.hText.String = string;
            obj.hText.Visible = obj.Visible;

            if obj.Width < obj.hText.Extent(3)
                obj.Position(3) = obj.hText.Extent(3);
            end

            if obj.Height < obj.hText.Extent(4)
                obj.Position(4) = obj.hText.Extent(4);
            end
        end
    end
    
    %% Text rendering
    methods
        function RenderText(obj, string)
            if isempty(obj.hText)
                obj.CreateText(string);
            else
                obj.hText.String = string;
            end
                        
            obj.UpdateTextPos();
         end
         
         function UpdateTextPos(obj)
             
            switch(obj.HorizontalAlignment)                
                case 'right'
                    x = obj.Position(1) + obj.Width;
                case 'center'
                    x = obj.Position(1) + obj.Width / 2;
                otherwise %default to 'left'
                    x = obj.Position(1);
            end
            
            switch (obj.VerticalAlignment)
                case {'bottom', 'baseline'}
                    y = obj.Position(2) + obj.Height;
                case 'middle'
                    y = obj.Position(2) + obj.Height / 2;                                    
                otherwise %default to {'top', 'cap'}
                    y = obj.Position(2);
            end
             
            obj.hText.Position(1:2) = [x, y];
         end                 
    end
end

