%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MTimeStampAnnotation < com.Annotations.MTextAnnotation
    %MELLIPSEANNOTATION Summary of this class goes here
    %   Detailed explanation goes here    
    
    properties(Dependent)
        TimeString;
    end
    
    methods
        function obj = MTimeStampAnnotation(hStackScroller, hAnnotation)
            %MELLIPSEANNOTATION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.Annotations.MTextAnnotation(hStackScroller,hAnnotation);             
        end               
    end
    
     methods(Access = protected)
              
         function CreateShape(obj, varargin)
            obj.RenderText(obj.TimeString);
         end
         
        function OnFrameChanged(obj, ~, ~)
            try
                OnFrameChanged@com.Annotations.MTextAnnotation(obj);            
                obj.String = obj.TimeString;
            catch
            end
        end                         
     end
     
    methods
        function value = get.TimeString(obj)
            hDataset = obj.StackScroller.hDataset;            
            metadata = hDataset.MetaData;
            frameNo = obj.StackScroller.CurrentFrame;
                        
            if metadata.UseExactTimingInformation == true
                try
                    frameTime = metadata.PerFrameTimestamps(frameNo);
                catch
                    %This should actually never happen, since ExactTimestamp should
                    %not be allowed to be true if no per-frame timestamps are
                    %available. But, you know... the devil is a squirrel...
                    frameTime = (frameNo-1) / metadata.VolumeRate;            
                end
            else        
                frameTime =  (frameNo-1) / metadata.VolumeRate;
            end

            tSec = floor(frameTime);
            mSec = (frameTime - tSec)* 1000;
            dt = datetime(0,0,0,0,0, tSec, mSec);
    
            value = [datestr(dt, metadata.TimeFormat), metadata.TimeUnit];
        end
    end         
end

