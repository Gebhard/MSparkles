%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef (Abstract) MAnalysisResult < com.common.Analysis.MResultRoot
    %MANALYSISRESULT Assiciates a ROI set and corresponding results to a
    %channel in the dataset
    
    properties
        DetectionTH;
        SynchronicityIndex;
        SynchronicityPeaks;
        EegSignalOffset;
        EegMaxCorrelation;
    end    
    
    properties(Transient, Access=protected)
        lblMatrix = [];
        numLabels = 0;
        morphLut = [];
        morphLutName = [];
    end
       
    methods
        function obj = MAnalysisResult(channelID)
            obj = obj@com.common.Analysis.MResultRoot(channelID);            
        end
    end
    
    methods        
        function value = get.lblMatrix(obj)
            if ~isempty(obj.ROIs)
                if isempty(obj.lblMatrix) || obj.ROIs.NumObjects ~= obj.numLabels
                    obj.lblMatrix = labelmatrix(obj.ROIs); 
                    obj.numLabels = obj.ROIs.NumObjects;
                end
            end
            
            value = obj.lblMatrix;
        end  
    end
    
    methods
        function [peakTimes, sigStartEnd, peakValues] = GetClassifiedSignals(obj)
            try
                if ~isempty(obj.Results)       
                    numTh = numel(obj.Results.Thresholds);
                    numTimesteps = obj.hParent.hDataset.MetaData.SamplesT;

                    peakTimes = nan(obj.RoiCount, numTimesteps) ;
                    sigStartEnd = nan(obj.RoiCount, numTimesteps, 2) ;
                    peakValues = nan(obj.RoiCount, numTimesteps) ;

                    for t=1:numTh
                        pt = obj.Results.PeakFrame(:,t);
                        sf = obj.Results.StartFrame(:,t);
                        dur = obj.Results.DurationF(:,t);

                        %Make one big array out of cell array
                        for r=1:obj.RoiCount
                            peakTimes(r, pt{r}) = pt{r};
                            sigStartEnd(r, pt{r}, 1) = sf{r};
                            sigStartEnd(r, pt{r}, 2) = sf{r} + dur{r};
                            peakValues(r, pt{r}) = t;
                        end            
                    end
                else
                    peakTimes = [];
                    sigStartEnd = [];
                    peakValues = [];
                end
            catch ex
                peakTimes = [];
                sigStartEnd = [];
                peakValues = [];
            end
        end
        
        function AnalyzeSynchronicity(obj)
            try
                [syncMatrix, obj.SynchronicityIndex] = obj.GetSynchronicityMatrix();
                
                if ~isempty( syncMatrix )
                    %Find ROIs within periods of high synchronicity and save them in   
                    %MSynchronicityPeak objects
                    hAnalysis = obj.hParent;
                    hDataset = hAnalysis.hDataset;

                    [pks,locs, ~, ~, intersect] = findpeaks2(obj.SynchronicityIndex,...
                        hDataset.MetaData.VolumeRate,...
                        'MinPeakDistance',  2*(1/hDataset.MetaData.VolumeRate),...
                        'MinPeakHeight', hDataset.AnalysisSettings.Analysis.SyncClassificationTH, ...
                        'MinPeakProminence',  hDataset.AnalysisSettings.Analysis.SyncMinPeakProm, ...
                        'WidthReference', 'halfprom',...
                        'DurFrac', hDataset.AnalysisSettings.Analysis.SyncWidthRef);
                    
                    if hDataset.AnalysisSettings.Analysis.SyncExcludeSubSignals
                        realPeaks   = com.lib.SigProc.RemoveSubSignals(pks, locs, intersect);
                        pks         = pks(realPeaks);
                        locs        = locs(realPeaks);
                        intersect   = intersect(realPeaks,:);
                    end
                    
                    SyncPeaks = [pks', locs', round(intersect)];
                    sz = size(SyncPeaks);
                    obj.SynchronicityPeaks = com.CalciumAnalysis.Analysis.MSynchronicityPeak.empty(sz(1),0);

                    for ii = 1:sz(1)
                        [~, idxMin] = min(abs( hDataset.TimingSignal - SyncPeaks(ii,3) ));
                        [~, idxMax] = min(abs( hDataset.TimingSignal - SyncPeaks(ii,4) ));

                        allROIs  = syncMatrix(:, idxMin:idxMax);
                        activeRois = [];
                        activationIdx = [];

                        for jj=1:size(allROIs, 2)
                            rois = find( ~isnan(allROIs(:,jj)) );
                            activeRois = cat(1, activeRois, rois);
                            activationIdx = cat(1, activationIdx, ones(numel(rois),1)*jj);
                        end

                        %get the indices of the first occurance of a ROI 
                        [activeRois, ia] = unique(activeRois, "first");

                        synkPeak = com.CalciumAnalysis.Analysis.MSynchronicityPeak;
                        synkPeak.Synchronicity      = SyncPeaks(ii,1);
                        synkPeak.PeakTime           = SyncPeaks(ii,2);
                        synkPeak.PeakStartEnd       = SyncPeaks(ii,3:4);
                        synkPeak.AffectedROIs       = activeRois;
                        synkPeak.ActivationOrder    = activationIdx(ia);

                        obj.SynchronicityPeaks(ii) = synkPeak;
                    end
                else
                    obj.SynchronicityPeaks = [];
                    obj.SynchronicityIndex = [];
                end
            catch ex
                obj.SynchronicityIndex = [];
                obj.SynchronicityPeaks = [];
                com.common.Logging.MLogManager.Error("Synchronicity analysis failed.");
                com.common.Logging.MLogManager.Exception(ex);
            end      
        end
        
        function DeleteROI(obj, roiID)
            try
                if (~isempty(obj.Results))
                    obj.Results.DeleteRoi(roiID);                    
                end          
            catch
            end
            
            obj.UpdateStats();
            
            if (roiID > 0)
                obj.ROIs.PixelIdxList(roiID) = [];
                obj.ROIs.NumObjects = numel(obj.ROIs.PixelIdxList);  
                
                obj.morphLut = [];
                obj.lblMatrix = [];
            end            
        end
        
        function CC = AddROI(obj, ~, ~)
            CC = obj.ROIs;
        end
        
        function CC = MergeROI(obj, ~, ~)
            CC = obj.ROIs;
        end
        
        function CC = CutROI(obj, ~, ~)
            CC = obj.ROIs;
        end     
        
        function OnExportExcel(obj, hXlsFile)
            if ~isempty(obj.Results)
                hDataset = obj.hParent.hDataset;                 
                s = size(obj.Results.SigTAvg);
                numRows = s(1);
                numCols = s(2);

                obj.UpdateExportWaitbar(); 
                
                %Get time stamps
                tstamps = hDataset.TimingSignal;  

                %% Write raw ROI traces      
                ColumnNames = strings(2, numCols + 1);
                ColumnNames(1,1) = "Frame No.";
                ColumnNames(2,1) = "Time (sec)";                    
                ColumnNames(1, 2:end) = string(1:numCols);
                ColumnNames(2, 2:end) = num2str(tstamps(:), "%.3f");
                
                RowNames = "ROI " + (1:numRows)';

                obj.UpdateExportWaitbar();

                traces = obj.Results.SigTAvg;
                if isempty(traces)
                    traces = nan(1,1);  %Empty table if we have no traces
                else
                    traces(isnan(traces)) = 0;  %Ensure we have valid values
                end

                sheetName = "Traces";
                hXlsFile.Write(traces, sheetName, "B3");
                hXlsFile.Write(ColumnNames, sheetName, "A1");
                hXlsFile.Write(RowNames, sheetName, "A3");

                %% Prepare signals for Excel
                obj.UpdateExportWaitbar(); 
                [ sig, sigRowHdr, sigCounts, sigCountHdr, sigComp, sigCompHdr,...
                    graphPad, graphPadHdr] = com.CalciumAnalysis.Reporting.PrepareSignalTables(obj.Results, tstamps);

                %% Write signal counts sheet      
                obj.UpdateExportWaitbar(); 
                sheetName = "Signal Counts";
                hXlsFile.Write(sigCounts, sheetName, "B2");
                hXlsFile.Write(RowNames, sheetName, "A2");    
                hXlsFile.Write(sigCountHdr, sheetName, "B1");

                %% Write signal composition sheet
                obj.UpdateExportWaitbar();
                sheetName = "Signal composition";
                hXlsFile.Write(sigCompHdr, sheetName, "A1");
                hXlsFile.Write(sigComp, sheetName, "B2");  

                %% Write GraphPad sheet
                obj.UpdateExportWaitbar();
                sheetName = "GraphPad";
                hXlsFile.Write(graphPadHdr, sheetName, "A1");
                hXlsFile.Write(graphPad, sheetName, "A2"); 

                %% Write Synchronicty analysis
                obj.UpdateExportWaitbar();
                sheetName = "Synchronicity";
                if ~isempty(obj.SynchronicityIndex)
                    cn = ColumnNames';
                    cn(1,3) = "Sync";
                    hXlsFile.Write(cn, sheetName, "A1");
                    hXlsFile.Write(obj.SynchronicityIndex', sheetName, "C2");
                end
                
                rowHeader = ["Synchronicity"; "Sync peak time"; "Start & End"; "Affected ROIs"; "Activation Order";];
                sheetName = "Synchronicity Peaks";

                if ~isempty(obj.SynchronicityPeaks)
                    for i=1:numel(obj.SynchronicityPeaks)
                        syncPeak = obj.SynchronicityPeaks(i);
                        idx = ((i-1) * 6) + 1;
                        
                        hXlsFile.Write(rowHeader, sheetName, "A"+idx);
                        hXlsFile.Write(syncPeak.Synchronicity, sheetName, "B" + idx);
                        hXlsFile.Write(syncPeak.PeakTime, sheetName, "B" + (idx+1));
                        hXlsFile.Write(syncPeak.PeakStartEnd(:)', sheetName, "B" + (idx+2));
                        hXlsFile.Write(syncPeak.AffectedROIs(:)', sheetName, "B" + (idx+3));
                        hXlsFile.Write(syncPeak.ActivationOrder(:)', sheetName, "B" + (idx+4));
                    end
                end
                
                obj.UpdateExportWaitbar();
                
                sheetName = "Signals";
                hXlsFile.Write(sigRowHdr, sheetName, "A1");
                hXlsFile.Write(sig, sheetName, "A2");
                
                try
                    sheetName = "Dataset stats";
                    sheetHdr = ["Mean peak", "Peak std. dev.", "Mean duration", "Duration std. dev", "Mean integrated sig.", "Integrated sig. std. dev.", ...
                                "Median peak", "Peak iqr.",...
                                "Median duration", "Duration iqr.",...
                                "Median integrated sig.", "Integrated sig iqr."];  

                    stats = zeros(1, 12);
                    stats(1) = mean(sig(:,6), "omitnan");
                    stats(2) = std(sig(:,6), "omitnan");
                    stats(3) = mean(sig(:,9), "omitnan");
                    stats(4) = std(sig(:,9), "omitnan");
                    stats(5) = mean(sig(:,7), "omitnan");
                    stats(6) = std(sig(:,7), "omitnan");

                    stats(7) = median(sig(:,6), "omitnan");
                    stats(8) = iqr(sig(:,6));
                    stats(9) = median(sig(:,9), "omitnan");
                    stats(10) = iqr(sig(:,9));
                    stats(11) = median(sig(:,7), "omitnan");
                    stats(12) = iqr(sig(:,7));

                    hXlsFile.Write(sheetHdr, sheetName, "A1");
                    hXlsFile.Write(stats, sheetName, "A2");
                catch
                end
                
                obj.WriteSpecialTables(hXlsFile, RowNames);                                
                obj.UpdateExportWaitbar();
            end
        end        
    end
    
    methods(Access=protected)
        function count = CountRois(obj)
            try
                count = obj.ROIs.NumObjects;
            catch
                count = 0;
            end
        end
        
        function m = getMatrix(~, c)
            numCols = max(cellfun(@numel, c));
            numRows = numel(c);
            
            m=nan(numRows, numCols);
            
            for i=1:numRows
                data = c{i};
                m(i, 1:numel(data)) = data;
            end
        end
    end
    
    methods(Sealed)
        function UpdateColorLUT(obj)
            obj.morphLut = obj.hParent.AnalysisSettings.GetColormap(obj.ROIs.NumObjects);
        end
        
        function overlay = RenderRoiMap(obj, analysisSettings, zPos, frame, normFac, RoiIDs,...
                tProject, showNumbers, Colormap, ColorIdx)
            arguments
                obj (1,1) {mustBeNonempty};
                analysisSettings (1,1) {mustBeNonempty};
                zPos {mustBeNumeric} = 1;
                frame {mustBePositive, mustBeInteger} = 1;
                normFac {mustBeNumeric} = 1;
                RoiIDs {mustBeNonnegative, mustBeNonzero} = [];
                tProject {mustBeNumericOrLogical} = false;
                showNumbers {mustBeNumericOrLogical} = false;
                Colormap = [];
                ColorIdx = [];
            end
            
            if isempty(Colormap)
                Colormap = analysisSettings.ColormapName;
                updateColormap = ~strcmpi(obj.morphLutName, Colormap);
                tempColormap = false;
            else
                tempColormap = true;
                updateColormap = true;
            end

            if isempty(obj.morphLut) || updateColormap
                morphLUT = analysisSettings.GetColormap(obj.RoiCount, Colormap, ColorIdx);
                if ~tempColormap
                    obj.morphLut = morphLUT;
                end
            end                         
            
            lbl = obj.GetLabelMatrix(obj.ChannelID, zPos, frame, RoiIDs);
            
            try 
                if (~isempty(lbl))  
                    
                    if tProject && ndims(lbl) > 2
                        lbl = max(lbl, [], 6);
                    end
                    
                    overlay = single( label2rgb(lbl, morphLUT, 'k') ) ./ normFac;
                    
                    if showNumbers
                        try
                            if isempty(RoiIDs)
                               RoiIDs = 1:obj.ROIs.NumObjects;
                            end

                            centroids = regionprops(obj.ROIs, 'Centroid');
                            textPositions = arrayfun(@(x) [x.Centroid(1), x.Centroid(2)], centroids, 'UniformOutput', false);
                            textPositions = cell2mat(textPositions);

                            %%Make upper left corner to origin
                            textPositions(:, 2) = obj.ROIs.ImageSize(1) - textPositions(:,2);
                            overlay = com.CalciumAnalysis.Rendering.renderText(overlay, textPositions,...
                                num2str( RoiIDs' ), [1,1,1], 12, normFac);
                        catch
                        end
                    end
                    
                else
                    overlay = zeros(512, 512, 3, 'single');   %return default empty image
                end
            
            catch  
                overlay = zeros(512, 512, 3, 'single');   %return default empty image
            end
         end    
         
        function hBmp = RenderTrajectories(obj, hDataset, analysisSettings, frame, hBmp, hAxes)
            % RENDEROVERLAY return the ROI image of the specified channel
            % at the specified zLayer for a given frame.
                % channelID     Channel number
                % zPos          Z-Position
                % frame         The time-step
                % overlay       The resulting overlay image 
                % hBmp          Target Bitmap. If not specified, this
                %               function will create its own bitmap as render target.
            
            if nargin < 3
                frame = 1;
            end            
            
            %Render traces of all ROIs present in the current frame.
            %Each trace is rendered from the respective start up to the
            %current frame.
            %Current position is highlighted with circle.
            
            try
                %Get coordinates of centers of all ROIs, visible in frame
                imgSz = obj.ROIs.ImageSize;

                %init black RGB image
                if nargin < 5 || isempty(hBmp)
                    hBmp = zeros(imgSz(1), imgSz(2), 3, 'single');
                end
                
                if ~isempty(obj.Results)

                    %Get coordinates of visible ROIs
                    coords = cellfun(@(x) GetVisibleTraces(x) ,obj.Results.AreaCenter, 'UniformOutput', false);
                    coords = coords(~cellfun(@(x) isempty(x) || numel(x) < 4,coords));
                    %maxLen = max(cellfun('length',coords));
                    
                    %Draw traces as lines
                    numTraces = numel(coords);

                    traces = cell(numTraces, 1);
                    posns = nan(numTraces, 3);
                    posns(:,3) = 5;
                    
                    for i=1:numTraces
                        currTrace = coords{i};
                        sz = size(currTrace);
                        len = 2*sz(2);
                        x = currTrace(3,:) ./ hDataset.MetaData.SampleSizeX;
                        y = currTrace(2,:) ./ hDataset.MetaData.SampleSizeY;
                        
                        vertices = reshape([(x-1) fliplr(x) (x+1);(y-1) fliplr(y) (y+1)], 1, 3 * len, []) ;
                            
                        if ~isempty(vertices)
                            posns(i,1:2) = [x(end), y(end)];
                        end

                        if numel(vertices) >= 4
                            traces{i} = vertices(:);
                        end                    
                    end
                    
                    traces = traces(~cellfun(@(x) isempty(x),traces));
                    
                    hBmp = insertShape(hBmp, 'FilledCircle', posns, 'Opacity', 1);
                    hBmp = insertShape(hBmp, 'line', traces, 'LineWidth', 1, 'Color', 'red',...
                        'Opacity', 1);
                    
                    %line(hAxes, y, x, 'color',[0.3010 0.7450 0.9330], 'LineWidth', 3);
                end
            catch ex
            end
            
            function coords = GetVisibleTraces(centerCoords)
            %Check if any vertices are visible at all, if so, return
            %all up to current frame. Otherwise, return empty set
                if any( frame >= centerCoords(1,:) & frame <= centerCoords(1,:) )
                    idx = centerCoords(1,:) <= frame;
                    coords = centerCoords( :, idx );
                else
                    coords = [];
                end
            end
            
        end
        
        function hFig = RenderEventTrace(obj, RoiID, varargin)
            fun_date = '22.11.2019';
            fun_version = '2.0.000';
            
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding
            p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['MCaSignalsRoot.ExportRoiTraces v.' fun_version ' (' fun_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.
                        
            addParameter(p,'Color',     [0 0.4470 0.7410] );
            addParameter(p,'Colormap',  [] );
            addParameter(p,'ColorIdx',  [] );
            addParameter(p,'RoiColorBar', false);
            
            addParameter(p,'LineWidth',     1,                  @(x) x>0 && x <= 10 );
            addParameter(p,'GraphXLabel',   [],                 @(x) ischar(x));
            addParameter(p,'GraphYLabel',   [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'GraphTitle',    [],                 @(x) isempty(x) || ischar(x));            
            addParameter(p,'ImgXLabel',     [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'ImgYLabel',     [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'ImgTitle',      [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'FigTitle',      [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'Legend',        [],                 @(x) isempty(x) || ischar(x));
            addParameter(p,'ShowLegend',    true,               @(x) islogical(x));
            addParameter(p,'ShowPeaks',     true,               @(x) islogical(x));
            addParameter(p,'ShowStdDev',    true,               @(x) islogical(x));
            addParameter(p,'ShowRiseAndDecay',    true,         @(x) islogical(x));
            addParameter(p,'Channel',       1,                  @(x) ~isempty(x) && isnumeric(x));
            addParameter(p,'ShowBBox',      true,               @(x) islogical(x));
            addParameter(p,'Show3DGraph',   false,              @(x) islogical(x));
            addParameter(p,'Rotation3D',    30,                 @(x) x >= -90 && x <= 90);
            addParameter(p,'Pitch3D',       30,                 @(x) x >= -90 && x <= 90);
            addParameter(p,'UseRoiColors',  true,               @(x) islogical(x));
            addParameter(p,'Figure',        []);
            addParameter(p,'GraphStyle',    'Traces',           @(x) ismember(x, ["Traces", "Heatmap"]));
            addParameter(p,'HighlightSpan', []);
            addParameter(p,'HighlightColor',[]);
            addParameter(p,'XLim',[]);
            addParameter(p,'YLim',[]);

            parse(p,varargin{:});
            params = p.Results; 
            
            hDataset = obj.hParent.hParent.hParent;
                       
            RoiID = sort(RoiID);
            
            if isempty(params.ImgTitle)
                params.ImgTitle = "ROI " + strjoin(string(RoiID), ",");
            end
            
            if isempty(params.ImgXLabel)
            	params.ImgXLabel = sprintf('%s', hDataset.MetaData.DomainUnit);
            end
            
            if isempty(params.GraphTitle)
                params.GraphTitle = 'Trace';
            end
            
            if isempty(params.FigTitle)
                params.FigTitle = hDataset.DatasetName;
            end
            
            if isempty(params.GraphYLabel) || params.GraphYLabel == ""
                if obj.hParent.AnalysisType == com.Enum.AnalysisType.Dynamic
                   params.GraphYLabel = sprintf('Area (%s²)', hDataset.MetaData.DomainUnit);
                else
                    params.GraphYLabel = hDataset.NormalizationName;
                end
            end
            
            if isempty(params.GraphXLabel) || params.GraphXLabel == ""
                params.GraphXLabel = 'Time (sec)';
            end
            
            if isempty(params.Figure)
                hFig = CreateTraceExportFigure();
            else
                hFig = params.Figure;
                if isa(hFig, "matlab.ui.Figure")
                    clf(hFig);
                elseif isa(hFig, "matlab.graphics.axis.Axes")
                    cla(hFig)
                end
            end
                       
            if params.Show3DGraph
                hFig.UserData = "3D";
            else
                hFig.UserData = string(params.GraphStyle);
            end

            %% Render ROI-map image
            bkgnd = hDataset.F(:,:,1,1,1,1);
            bkgnd = bkgnd / max(bkgnd, [], 'all');
            bkgnd = repmat(bkgnd, 1, 1, 3);
            
            if obj.hParent.AnalysisType == com.Enum.AnalysisType.Dynamic
                sf = horzcat(obj.Results.StartFrame(RoiID, :));
                sf = floor(vertcat(sf{:}));
                dur = vertcat(obj.Results.DurationF(RoiID, :));
                dur = ceil(vertcat(dur{:}));
                ef = sf + dur;
                minSf = min(sf);
                maxEf = max(ef);
                frames =minSf:maxEf;
            else
                frames = 1; 
            end
            
            frames = min(frames, hDataset.MetaData.SamplesT);
            
            hRoiMap = obj.RenderRoiMap(obj.hParent.AnalysisSettings, [], frames, 255, RoiID,...
                true, true, params.Colormap, params.ColorIdx);
            
            idx = hRoiMap ~= 0;
            
            bkgnd(idx) = hRoiMap(idx);
            if params.RoiColorBar && ~isempty(params.ColorIdx)
                sp = subplot('Position', [0.05 0.2 0.2585 0.7], 'Parent', hFig);
                sp.TitleFontSizeMultiplier = 1;
            else
                sp = subplot('Position', [0.05 0.2 0.21 0.7], 'Parent', hFig);
                sp.TitleFontSizeMultiplier = 1;
            end
            
            x = [0, hDataset.MetaData.ExtentX];
            y = [0, hDataset.MetaData.ExtentY];
            imagesc(sp, x, y, bkgnd);
           
            try
                if params.RoiColorBar && ~isempty(params.ColorIdx)
                    cIdx = unique(params.ColorIdx(:,2));
                    cMap = feval(params.Colormap, numel(cIdx));
                    colormap(sp, cMap);
                    colorbar(sp);
                    sp.CLim = [min(cIdx), max(cIdx)];
                end
            catch
            end
            
            %ToDo: Render Bounding Box
%             try
%                 bb = regionprops(obj.ROIs, 'BoundingBox');
%                 box = bb(RoiID);
% 
%                 hold(sp, 'on');
%                 for i=1:numel(box)
%                     box(i).BoundingBox(1) = (box(i).BoundingBox(1) -7.5) *  hDataset.MetaData.SampleSizeX;
%                     box(i).BoundingBox(2) = (box(i).BoundingBox(2) -7.5) *  hDataset.MetaData.SampleSizeY;
%                     box(i).BoundingBox(3) = (box(i).BoundingBox(3) +7.5) *  hDataset.MetaData.SampleSizeX;
%                     box(i).BoundingBox(4) = (box(i).BoundingBox(4) +7.5) *  hDataset.MetaData.SampleSizeY;
% 
%                     rectangle('Position', box(i).BoundingBox, 'LineWidth', 0.75, 'EdgeColor', 'yellow', 'LineStyle', '-', 'Parent', sp); 
%                 end
%                 hold(sp, 'off');
%             catch
%                 %currently fails in 3D case
%             end
            
            title(sp, params.ImgTitle, 'FontSize', 10);
            xlabel(sp, params.ImgXLabel, 'FontSize', 8);
            ylabel(sp, params.ImgYLabel, 'FontSize', 8);
            
            %% Render ROI trace
            sp2 = subplot('Position', [0.34 0.2 0.65 0.7], 'Parent', hFig);
            sp2.Units = 'centimeters';
            sp2.TitleFontSizeMultiplier = 1;
            
            isHeatmap  = strcmpi(params.GraphStyle, "heatmap");
            
            if ~isHeatmap
                hTraces = obj.RenderRoiTrace(hDataset, sp2, RoiID, params.LineWidth,...
                    'ShowPeaks', params.ShowPeaks,...
                    'ShowStdDev', params.ShowStdDev,...
                    'ShowRiseAndDecay', params.ShowRiseAndDecay,...
                    'Show3DTraces', params.Show3DGraph,...
                    'Rotation3D', params.Rotation3D,...
                    'Pitch3D', params.Pitch3D,...
                    'UseRoiColors', params.UseRoiColors,...
                    'RoiColormap', params.Colormap,...
                    'RoiColorIdx', params.ColorIdx,...
                    'HighlightSpan', params.HighlightSpan,...
                    'HighlightColor', params.HighlightColor,...
                    'XLim', params.XLim,...
                    'YLim', params.YLim);
                    
            else
                hTraces = com.CalciumAnalysis.Rendering.Graph.MHeatMap('Parent', sp2,...
                    'Dataset', hDataset,...
                    'Analysis', obj.hParent,...
                    'Resultset', obj,...
                    'Rois', RoiID,...
                    'DataLabel', params.GraphYLabel,...
                    'ColorMap', com.Enum.ColorMapEnum(params.Colormap));
            end
            
            title(sp2, params.GraphTitle, 'FontSize', 10);
            set(sp2, 'FontSize', 8);
            
            if params.ShowLegend && ~isHeatmap
                if obj.hParent.AnalysisType ~= com.Enum.AnalysisType.Dynamic
                    if isempty(params.Legend)  
                        params.Legend = "ROI " + RoiID;
                    end
                    legend(sp2,hTraces, params.Legend);
                end
            else
                legend(sp2, 'off');
            end
            
            xlabel(sp2, params.GraphXLabel, 'FontSize', 10);
            if params.Show3DGraph && ~isHeatmap
                zlabel(sp2, params.GraphYLabel, 'FontSize', 10);
                ylabel(sp2, 'ROI #', 'FontSize', 10 );
                numrois = numel(RoiID);
                if numrois == 1
                    yticks(sp2, 1);
                    yticklabels(sp2, RoiID);
                else
                    ylim(sp2, [1,numrois]);
                    
                     if numrois < 6
                         yticks(sp2, 1:numrois);
                         yticklabels(sp2, RoiID)
%                      else
%                          ytick = sp2.YTick;
%                          ytick(2,:) = RoiID(end);
%                          ytick = min(ytick);
%                          yticklabels(sp2, RoiID(ytick));
                     end
                end
                
            else
                if isHeatmap
                    yticklabels(sp2, RoiID(sp2.YTick));
                else
                    ylabel(sp2, params.GraphYLabel, 'FontSize', 10);
                end
                zlabel(sp2, []);
            end
            sgtitle(hFig, params.FigTitle, 'FontSize', 10);
        end
    end
     
    methods(Abstract)
        lbl = GetLabelMatrix(obj, channel, zPos, frame, RoiIDs);
    end
    
    methods(Abstract, Access=protected)
        WriteSpecialTables(obj, hXlsFile);
        Traces = RenderRoiTrace(obj, hDataset, hRenderTarget, roiID, lineWidth, varargin);
        [syncIndex, syncMatrix] = GetSynchronicityMatrix(obj);
    end
end

