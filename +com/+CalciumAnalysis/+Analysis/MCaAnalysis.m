%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaAnalysis < com.common.Analysis.MAnalysisRoot
    %MCAANALYSIS Summary of this class goes here
    %   Detailed explanation goes here   
    
    properties
        arrIntersectIDs = {};
    end
    
    methods
        %delete in 1.9
        function UpdateSettings(obj)
            
            try
                settings = obj.CreateAnalysisSettings();
                
                if ~isempty(settings)
                    settings.CopyFrom(obj.AnalysisSettings);
                    obj.AnalysisSettings = settings;
                end
                
            catch ex
                com.common.Logging.MLogManager.Error(ex);
            end                        
        end                
        
        function result = DoAllRoisExist(obj)
            try
                if ~isempty(obj.AnalysisResults)
                    for i=obj.Channels
                        hResults = obj.GetResultset(i);
                        
                        if isempty(hResults) || isempty(hResults.ROIs)
                            result = false;
                            return;
                        end
                    end
                    
                    result = true;
                else
                    result = false;
                end
            catch
                result = false;
            end
        end
        
        function SaveRoiImage(obj, outpath, hResultset)
            try
                SaveGroupImage(obj, "OutPath", outpath, "Resultset", hResultset, "Dataset", obj.hParent.hParent); 
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function SaveCorrelationGraph(obj, outpath, hResultset)
            try
                hPlot = com.CalciumAnalysis.Rendering.Graph.MPeakDurationScatterGraph("Dataset", obj.hDataset,...
                    "Analysis", obj, "Resultset", hResultset);                
                obj.SaveFigure(hPlot.hFigure, outpath, "pdf");                
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function SaveSummaryGraphs(obj, outpath, hResultset)
            try
                f = figure("Visible", "off");
                sp1 = subplot(2,1,1, "Visible", "off", "Parent", f);
                sp2 = subplot(2,1,2, "Visible", "off", "Parent", f);
                
                com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph("Parent", sp1,...
                    "Dataset", obj.hDataset, "Analysis", obj, "Resultset", hResultset,...
                    "Type", "peak");

                com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph('Parent', sp2, ...
                    "Dataset", obj.hDataset, "Analysis", obj, "Resultset", hResultset,...
                    "Type", "duration", "Title", "");

                obj.SaveFigure(f, outpath, "pdf"); 
                delete(f);
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function SaveHeatmaps(obj, outpath, hResultset)
            try
                hGlobalSettings = MGlobalSettings.GetInstance();
                
                if hGlobalSettings.HeatampsIncludeSyncPlot
                    hChildRenderFunc = @RenderSyncPlot;
                else
                    hChildRenderFunc = [];
                end
                
                hHeatMap = com.CalciumAnalysis.Rendering.Graph.MHeatMap("Dataset", obj.hDataset, "Resultset", hResultset,...
                        "Analysis", obj, "YLabel", "ROI #", ...
                        "DataLabel", obj.hDataset.NormalizationName, "Title", strrep(obj.hDataset.DatasetName,"_","\_"), ...
                        "ChildRenderFunc", hChildRenderFunc );  
                
                hSdHeatMap = com.CalciumAnalysis.Rendering.Graph.MSignalDurationHeatMap("Dataset", obj.hDataset,...
                    "Analysis", obj, "Resultset", hResultset, ...
                    "YLabel", "ROI #", ...
                    "ChildRenderFunc", hChildRenderFunc);

                obj.SaveFigure(hHeatMap.hFigure, outpath{1}, "pdf");
                obj.SaveFigure(hSdHeatMap.hFigure, outpath{2}, "pdf");  
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function SaveEegCorrelationPlot(obj, outpath, hResultset, hEegResultset) 
             try
                f = figure("Visible", "off");
                hPlot = com.CalciumAnalysis.Rendering.Graph.EegCorrPlot(...
                    "Parent", f,...
                    "Dataset",obj.hDataset,... 
                    "Resultset", hResultset,...
                    "EegResultset", hEegResultset); 
                
                obj.SaveFigure(hPlot.hFigure, outpath, "pdf");   
                delete(f);
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
             end
        end
        
        function ExportGraphs(obj, outPath)
            hGlobalSettings = MGlobalSettings.GetInstance();
            
            fName = obj.hDataset.DatasetName;
            pfx = obj.Name;  
            basePath = fullfile(outPath, sprintf("%s-%s", fName, pfx));
            
            for c=obj.Channels
                hResultset = obj.GetResultset(c);
                channelName = obj.hDataset.MetaData.GetChannelName(c);
                channelPath = sprintf("%s-%s", basePath, channelName);
                
                if hGlobalSettings.GenerateHeatmapFiles
                    obj.SaveHeatmaps( {channelPath+ "-Heatmap", channelPath + "-SDHeatmap"},...
                        hResultset);
                end
                
                if hGlobalSettings.GenerateSummaryGraphs
                    obj.SaveSummaryGraphs(channelPath + "-Summary", hResultset);
                end
                
                if hGlobalSettings.GenerateCorrelationGraph
                    obj.SaveCorrelationGraph( channelPath + "-Correlation", hResultset);
                end
                
                if ~isempty(hResultset.ROIs)
                    obj.SaveRoiImage( channelPath + "-ROI.png", hResultset);
                end
                
                if ~isempty(obj.hDataset.EEGDataset)
                    hEegResultSet = obj.hDataset.EEGDataset.Analyses.CurrentAnalysis.GetFirstResultset;
                    
                    obj.SaveEegCorrelationPlot(channelPath + "-EEG_corr.pdf", hResultset, hEegResultSet);
                end
                
                try
                    if (hGlobalSettings.GenerateRoiTraces == true)
                        hFig = CreateTraceExportFigure();
                        obj.ExportEventTraces(outPath, hResultset.ChannelID,[], 'Figure', hFig);                
                    end
                catch ex
                    com.common.Logging.MLogManager.Exception(ex);
                end
            end
        end
        
        function GenerateOutputFiles(obj)
            hGlobalSettings = MGlobalSettings.GetInstance();
            fPath = obj.GetResultsFolder();

            if isfolder(fPath)
                try
                    %Export automatically as excel
                    if hGlobalSettings.GenerateExcelFiles
                        obj.ExportExcel(fPath);
                    end
                catch ex
                    com.common.Logging.MLogManager.Critical("Failed to generate Excel files...!");
                    com.common.Logging.MLogManager.Exception(ex);
                end

                try
                    obj.ExportGraphs(fPath);
                catch ex
                    com.common.Logging.MLogManager.Critical("Failed to generate graphs...!");
                    com.common.Logging.MLogManager.Exception(ex);
                end

                try
                    if hGlobalSettings.GeneratePDFReport
                        obj.ExportPdfReport();
                    end
                catch ex
                    com.common.Logging.MLogManager.Critical("Failed to generate PDF report...!");
                    com.common.Logging.MLogManager.Exception(ex);
                end
            else
                com.common.Logging.MLogManager.Critical("Output folder for saving results does not exist or is unreachable!");
            end
        end
        
        function ExportExcel(obj, outPath)
            multiWaitbar(obj.SaveWBName, 'Value', 0, 'Color', 'g');            
            fName = obj.hDataset.DatasetName;
            pfx = obj.Name; 
            if isempty(outPath)
                outPath = obj.GetResultsFolder();
            end
    
            for c=obj.Channels
                try
                    hResultset = obj.GetResultset(c);
                    if ~isempty(hResultset.Results)
                        channelName = obj.hDataset.MetaData.GetChannelName(c);
                        path = fullfile(outPath, sprintf("%s-%s-%s.xlsx", fName, pfx, channelName));
                        
                        hResultset.ExportExcel(path);
                    end
                catch ex
                    com.common.Logging.MLogManager.Exception(ex);
                end

            end
            multiWaitbar(obj.SaveWBName, 'Close');
        end
        
        function ExportEventTraces(obj, outPath, channelID, RoiID, varargin)
            % 1) If no outPath, show SelectFolder dialog
            %
            % 2) Call new function RenderPlot. This function renders either
            % one or more speciffic plots, if "PlotID" is specified in
            % varargin, or all plots, if PlotID is not specified.
            % RenderPlot returns a list of graphicsobjects (figures). Based
            % on plot-settings of DynamicRoiAnalysis, the returned plots
            % can either be wave- or trace-plots
            %
            % 3) Create a subplot for each plot returned by RenderPlot, and
            % call new Function RenderRoiMap. If RenderRoiMap gets a RoiId
            % or a list of ROI IDs, it renders only the specified ROIs
            % (similar to what is now renderOverview...). RoiIds can be a
            % M-by-N matrix. For each row in this matrix an image is
            % created which contails all ROIs (their projections). This
            % function must replace the current renderOverview stuff to go
            % one step into multi-roi-selection-
            %
            % 4) The title of each figure has to centered w.r.t. to the
            % entire figure, not a single subplot. Otherwise, long names
            % might be cut.
            
            szR = size(RoiID, 1);
            
            pfx = obj.Name;
            fName = obj.hDataset.DatasetName;
            channelName = obj.hDataset.MetaData.GetChannelName(channelID);
            fBaseName = sprintf('%s-%s-%s', fName, pfx, channelName);
            fileName = [];
            
            if isempty(outPath)
                if szR == 1
                    fName = fullfile(obj.GetResultsFolder(), fBaseName);
                    [outPath, fileName] = SelectFile({'*.png'}, "Select file name", fName, true);
                else
                    outPath = SelectFolder(obj.GetResultsFolder(), "Select destination");
                end
            end 
            
            if ~isempty(outPath) && (isstring(outPath) || ischar(outPath))
                hResultset = obj.GetResultset(channelID);
                
                com.common.Logging.MLogManager.Info("Exporting ROI traces.")
                if ~isempty(hResultset)
                    if nargin < 4 || isempty(RoiID)
                        RoiID = (1:hResultset.RoiCount)';
                    end

                    multiWaitbar('Saving ROI traces', 'Color', 'g');
                    roiNOs = join(string(RoiID), "_", 2);                   
                    szR = size(RoiID, 1);
                    
                    for i=1:szR
                        hPlot = hResultset.RenderEventTrace( RoiID(i,:),  varargin{:});
                        
                        if isempty(fileName)
                            hPlot.FileName = fullfile(outPath, sprintf('%s-ROI_%s - %s', fBaseName, roiNOs(i), hPlot.UserData )); 
                        else
                            hPlot.FileName = fullfile(outPath,fileName);
                        end

                        saveas(hPlot, hPlot.FileName, "png");       
                        
                        multiWaitbar('Saving ROI traces', 'Value', i/szR);
                    end
                    
                    if ~isempty(hPlot)
                        delete(hPlot);
                    end
                    
                    multiWaitbar('Saving ROI traces', 'close');
                end
            end
        end
        
        function ExportPdfReport(obj, showSaveAsDialog)
            arguments
                obj (1,1) com.CalciumAnalysis.Analysis.MCaAnalysis;
                showSaveAsDialog (1,1) logical = false;
            end
            
            try
                path = obj.hDataset.GetResultsFolder();
                reportFile = fullfile(path,...
                        sprintf("Report - %s - %s.pdf", obj.Name, obj.hDataset.DatasetName));

                if showSaveAsDialog
                    [fName, fPath] = uiputfile({'*.pdf';}, 'Save PDF report', reportFile); 

                    if fName == 0
                        reportFile = [];
                    else
                        reportFile = fullfile(fPath, fName); 
                    end
                end

                if ~isempty(reportFile)
                    multiWaitbar('Saving report', 'Color', 'g', 'busy');
                    settings = MGlobalSettings.GetInstance();

                    rpt = com.CalciumAnalysis.Reporting.PDF.MAnalysisReport(reportFile, obj.hDataset, obj);

                    rpt.add(com.CalciumAnalysis.Reporting.PDF.TitleSection(obj.hDataset, obj) );
                    rpt.add(com.CalciumAnalysis.Reporting.PDF.TableOfContents() );
                    rpt.add(com.CalciumAnalysis.Reporting.PDF.PreProcSection(obj.hDataset, obj) );
                    
                    for c=obj.Channels
                        rpt.add( com.CalciumAnalysis.Reporting.PDF.MStaticAnalysisChapter(obj.hDataset, obj, c) );
                    
                        %Append EEG correlation graph, if possible
                        if ~isempty(obj.hDataset.EEGDataset)
                            hEegResultSet = obj.hDataset.EEGDataset.Analyses.CurrentAnalysis.GetFirstResultset;
                            rpt.add( com.CalciumAnalysis.Reporting.PDF.EegCorrelationChapter(obj, hEegResultSet, c) );
                        end
                    
                        try
                            if (settings.reportIncludeRoiTraces == true)
                                if obj.includeTraces()
                                    rpt.add( com.CalciumAnalysis.Reporting.PDF.RoiTraceSection(obj, obj.GetResultset(c)) )
                                end                                        
                            end
                        catch
                        end
                    end
                    
                    rpt.fill();
                end
 
            catch ex  
                com.common.Logging.MLogManager.Exception(ex); 
            end

            try
                rpt.close();
            catch 
            end

            multiWaitbar('Saving report', 'Close');
        end
        
        function hPlots = RenderEventTraces(obj, channelID, RoiID, varargin)
            hResultset = obj.GetResultset(channelID);

            if ~isempty(hResultset)
                if nargin < 3 || isempty(RoiID)
                    RoiID = (1:hResultset.RoiCount)';
                end

                szR = size(RoiID);
                hPlots = cell(szR(1),1);
                multiWaitbar('Rendering ROI traces', 'Color', 'g');

                for i=1:szR(1)
                    hPlots{i} = hResultset.RenderEventTrace( RoiID(i,:),  varargin{:});
                    multiWaitbar('Rendering ROI traces', 'Value', i/szR(1));
                end

                multiWaitbar('Rendering ROI traces', 'close');
            end
        end
    end
    
    methods(Sealed)
        function ResetColorLUT(obj, channelID)
            hResultset = obj.GetResultset(channelID);
            if ~isempty(hResultset)
                hResultset.UpdateColorLUT();
            end
        end
        
        function overlay = RenderRoiMap(obj, channelID, zPos, frame, normFac)
            % RENDEROVERLAY return the ROI image of the specified channel
            % at the specified zLayer for a given frame.
                % channelID     Channel number
                % zPos          Z-Position
                % frame         The time-step
                % normFac       Normalization factor by which to divide 
                %               overlay
                % overlay       The resulting overlay image
            if nargin < 5
                normFac = 1;
            end
            
            if nargin < 4
                frame = 1;
            end
            
            if nargin < 3
               zPos = 1; 
            end
            
            hResultset = obj.GetResultset(channelID);
            overlay = hResultset.RenderRoiMap(obj.AnalysisSettings, zPos, frame, normFac);
        end
        
        function overlay = RenderTrajectories(obj, hDataset, channelID, frame, hBmp, hAxis)
            % RENDEROVERLAY return the ROI image of the specified channel
            % at the specified zLayer for a given frame.
                % channelID     Channel number
                % frame         The time-step
                % overlay       The resulting overlay image                
            
            if nargin < 4
                frame = 1;
            end
            
            if nargin < 5
                hBmp = [];
            end
            
            hResultset = obj.GetResultset(channelID);
            overlay = hResultset.RenderTrajectories(hDataset, obj.AnalysisSettings, frame, hBmp, hAxis);
        end
    end
    
    methods(Access=protected)
        function SaveFigure(~, hFigure, outpath, fmt)
            saveas(hFigure, outpath, fmt);
        end
        
        function result = includeTraces(obj)
            settings = MGlobalSettings.GetInstance();

            switch(obj.AnalysisType)
                case com.Enum.AnalysisType.Activity
                    result = settings.reportIncludeRoiTracesAuto;
                case com.Enum.AnalysisType.Grid
                    result = settings.reportIncludeRoiTracesGrid;
                case com.Enum.AnalysisType.Global
                    result = settings.reportIncludeRoiTracesGlobal;                    
                case com.Enum.AnalysisType.Manual
                    result = settings.reportIncludeRoiTracesManual ;                  
                case com.Enum.AnalysisType.Dynamic
                    result = settings.reportIncludeRoiTracesAuto;
            end
        end
    end
    
    methods(Abstract)
        result = IsIntersectable(obj, hAnalysis);
        DeleteROI(obj, channelID, roiID);
        CC = AddROI(obj, channelID, roi);
        CC = MergeROI(obj, channelID, roi);
        CC = CutROI(obj, channelID, roi);
    end
end

