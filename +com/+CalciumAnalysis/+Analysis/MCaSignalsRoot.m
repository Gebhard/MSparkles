%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MCaSignalsRoot < com.common.Data.MSparklesObject
    %MCAANALYSISRESULT Base class for analyzed signals and derived
    %quantities, extractet from ROIs of a time-series
    %   Durations, contains the signal durations w.r.t. PxIntersectPoints
    
    properties
        NumRois;
        Thresholds; % Thresholds used to determine and classify signals [Nx1], where N is the number of classification thresholds
        
        %Signal Ca
        SigTAvg;        % Average value per ROI/event per timestep.         [NxM], where N is the number of ROIs, M is the number of timesteps
        SigTStd;        % Standard deviation per ROI/event per timestep.    [NxM], where N is the number of ROIs, M is the number of timesteps
        SigTP95;        % 95%ile  value per ROI/event per timestep.         [NxM], where N is the number of ROIs, M is the number of timesteps
        SigTMax;        % The Maximum Ca per event                          [Nx1], where N is the number of ROIs
        SigTSum;        % Sum of Ca per event per time point
        SigMax;         % Peak value per event.                             [NxM], where N is the number of ROIs, M is the maximum number of signals        
        SigAvg;         % The average Ca per signal.                
        SigSum;         % Intergral under signal. From StartTime to StartTime+Duration. [NxM], where N is the number of ROIs, M is the maximum number of signals        
        
        Prominence;         %Signal prominence                         [NxM], where N is the number of ROIs, M is the maximum number of signals        
        PxIntersectPoints;  %Intersection points at x% of peak value with df/f0. Cell array of Size Nx1, where each cell contains a Mx2 matrix of [X,Y] coordinates, and M is the number if sognals per ROI
        P90IntersectPoints; %Intersection points at 90% of peak value with df/f0. Cell array of Size Nx1, where each cell contains a Mx2 matrix of [X,Y] coordinates, and M is the number if sognals per ROI
        
        %Area
        Areas;      % Signal area per ROI per timestep.         [Nx1] cell array, where N is the number of ROIs       
        AreaCenter; % The center of area per event per timepoint
        
        %Timing
        StartTime;  % Signal start times per ROI per signal.    [NxM], where N is the number of ROIs, M is the maximum number of signals
        StartFrame; % Signal start frame per ROI per signal.    [NxM], where N is the number of ROIs, M is the maximum number of signals
        PeakTime;   % Time in seconds of peak                   [NxM], where N is the number of ROIs, M is the maximum number of signals      
        PeakFrame;  % Frame numbers of peak                     [NxM], where N is the number of ROIs, M is the maximum number of signals
        RiseTime;   % Signal rise time (sec) per ROI per signal.[NxM], where N is the number of ROIs, M is the maximum number of signals
        DecayTime;  % Signal decay time (sec) per ROI per signal.[NxM], where N is the number of ROIs, M is the maximum number of signals        
        Duration;   % Signal duration in seconds.               [NxM], where N is the number of ROIs, M is the maximum number of signals
        DurationF;  % Signal duration in frames.                [NxM], where N is the number of ROIs, M is the maximum number of signals                 
    end    
    
    properties(Transient, Dependent)
        SignalCount;
    end
    
    properties(Access=protected)
        thClassIdx;    %Indices of threshold classes to corresponding signals   [NxM], where N is the number of ROIs, M is the maximum number of signals
        IsMultiPeakSignal;
    end    
    
    properties(SetAccess=protected)
        SigBounds;
    end
    
    properties(Access=private)
        pbSteps;    %Total number of steps of progressbar
        pbCurr;     %Current step# 
    end
    
    methods
        function value = get.SignalCount(obj)
            value = obj.GetSignalCount();
        end
        
        function value = get.IsMultiPeakSignal(obj)
            if isempty(obj.IsMultiPeakSignal)
                obj.IsMultiPeakSignal = false;
            end
            
            value = obj.IsMultiPeakSignal;
        end                               
    end
    
    methods
        function DeleteRoi(obj, roiID)
            try
                if roiID > 0
                    meta = metaclass(obj);
                    props = arrayfun(@(x) getNonDependentPropertyNames(x), meta.PropertyList, 'UniformOutput', false);
                    fn = props( ~cellfun('isempty', props) );
                    
                    fn(strcmp(fn, 'NumRois')) = [];
                    fn(strcmp(fn, 'Thresholds')) = [];
                    fn(strcmp(fn, 'ID')) = [];
                    fn(strcmp(fn, 'Name')) = [];
                    fn(strcmp(fn, 'hParent')) = [];
                    fn(strcmp(fn, 'BeingDeleted')) = [];
                    fn(strcmp(fn, 'SigBounds')) = [];
                    
                    numFields = numel(fn);

                    for f=1:numFields    
                        try
                            obj.(fn{f})(roiID, :) = [];
                        catch
                        end
                    end
                    
                    obj.NumRois = obj.NumRois - numel(roiID);
                    obj.updateSigBounds();
                end
            catch
                com.common.Logging.MLogManager.Error("Failed to delete ROI# " + roiId);
            end
            
            function prop = getNonDependentPropertyNames(x)
                if x.Dependent || ~strcmpi("public", x.GetAccess)
                    prop = [];
                else
                    prop = x.Name;
                end
            end
            
        end
        
        function trace = GetRoiTrace(obj, roiID)
            %Returns the trace of the ROI with id roiID as a row vector.
            %If the trace has less elements than SamplesT in the metadata,
            %the trace should be padded with 0's. In this case, the signal
            %should laso be embedded at its true location.
            %
            %This function must be overwritten in derived classes.
            %
            %Default behaviour is for the default, static case.
            
            trace = obj.SigTAvg(roiID, :);
            trace(isnan(trace)) = 0;
        end
        
        function trace = GetRoiTraceStd(obj, roiID)
            %Returns the trace of the ROI with id roiID as a row vector.
            %If the trace has less elements than SamplesT in the metadata,
            %the trace should be padded with 0's. In this case, the signal
            %should laso be embedded at its true location.
            %
            %This function must be overwritten in derived classes.
            %
            %Default behaviour is for the default, static case.
            
            trace = obj.SigTStd(roiID, :);
            trace(isnan(trace)) = 0;
        end
        
        function AnalyzeSignals(obj, hDataset, rois)
            obj.NumRois = rois.NumObjects;
            obj.InitProgressBar();
            
            obj.SigTAvg             = nan(obj.NumRois, hDataset.MetaData.SamplesT, 'single');  %Average fluorescence per time step
            obj.SigTStd             = nan(obj.NumRois, hDataset.MetaData.SamplesT, 'single');  %Standard deviation of SigTAvg per time step            
            obj.SigTP95             = nan(obj.NumRois, hDataset.MetaData.SamplesT, 'single');  %95%ile of fluorescence per time step
            obj.SigTSum             = nan(obj.NumRois, hDataset.MetaData.SamplesT, 'single');  %Summed fluorescence per time step
            obj.SigTMax             = nan(obj.NumRois, hDataset.MetaData.SamplesT, 'single');  %Maximum fluorescence value per time step
            obj.SigMax              = cell(obj.NumRois,1);  %Maximum fluorescence value of signal/event 
            obj.SigAvg              = cell(obj.NumRois,1);  %Average fluorescence per signal/event 
            obj.SigSum              = cell(obj.NumRois,1);  %Summed fluorescence per signal/event
            obj.Areas               = cell(obj.NumRois,1);  %Signal/event area per time step
            obj.AreaCenter          = cell(obj.NumRois,1);  %Geometric center of signal/event in [X,Y,Z] coordinates
                        
            obj.StartFrame          = cell(obj.NumRois,1);  %Frame number, marking the start of a signal/event
            obj.PeakFrame           = cell(obj.NumRois,1);  %Frame number, of when the peak occurs 
            obj.StartTime           = cell(obj.NumRois,1);  %Start time of a signal in seconds
            obj.PeakTime            = cell(obj.NumRois,1);  %Time of when the peak accurs, in seconds
            obj.RiseTime            = cell(obj.NumRois,1);  %Signal rise time, from ith% percentile to 90% peak value
            obj.DecayTime           = cell(obj.NumRois,1);  %Signal decay time, from 90% peak value to ith% percentile
            obj.Duration            = cell(obj.NumRois,1);  %Duration of the signal/event in seconds
            obj.DurationF           = cell(obj.NumRois,1);  %Duration of signal/event in frames
            obj.Prominence          = cell(obj.NumRois,1);  %Peak prominence
            obj.PxIntersectPoints   = cell(obj.NumRois,1);  %Intersection points of start and end times with signal trace
            obj.P90IntersectPoints  = cell(obj.NumRois,1);  %Intersection points of 90% peak with signal trace
            
            obj.InitCustomFields();
            
               
            idxShift = obj.GetIndexShift(hDataset);
            hAnalysisSettings = hDataset.Settings.Analysis;
            
            F = hDataset.F(:,:,obj.hParent.ChannelID,:,:,:);
            F0 = hDataset.F0(:,:,obj.hParent.ChannelID,:,:,:);
            szF = size(F);
            
            try
                for k=1:obj.NumRois
                    idx = obj.GetEventIndices(hDataset, rois, k) + idxShift;
                    idx = idx(:);

                    [x,y,~,z,~,t] = ind2sub(szF, idx);
                    [xMin, xMax] = bounds(x);
                    [yMin, yMax] = bounds(y);
                    [zMin, zMax] = bounds(z);
                    [tMin, tMax] = bounds(t);

                    szsub = [xMax-xMin+1 yMax-yMin+1 zMax-zMin+1 tMax-tMin+1];
                    subInd = sub2ind(szsub,x-xMin+1,y-yMin+1,z-zMin+1, t-tMin+1);
                    [X,Y,Z] = meshgrid(1:szsub(1),1:szsub(2), 1:szsub(3)); 

                    dd  = nan([prod(szsub(1:3)) szsub(4)], 'single');
                    dd0 = dd;

                    %% F & F0: calc vector (over time or over xy) parameters
                    dd(subInd)              = F(idx);
                    dd0(subInd)             = F0(idx);

                    sigT                    = hDataset.GetNormalizedData(dd, dd0);
                    sigTAvg                 = mean(sigT, 1, "omitnan");
                    sigTStd                 = std(sigT, 0, 1, "omitnan");                                   
                    sigTP95                 = prctile(sigT, 95, 1);
                    
                    sigTAvg(isinf(sigTAvg)) = 0;
                    sigTStd(isinf(sigTStd)) = 0;
                    
                    if any(isnan(sigTAvg))
                        sigTAvg = fillmissing(sigTAvg, "linear");
                    end
                    
                    if any(isnan(sigTStd))
                        sigTStd = fillmissing(sigTStd, "linear");
                    end
                    
                    if any(isnan(sigTStd))
                        sigTP95 = fillmissing(sigTP95, "linear");
                    end
                    
                    if hAnalysisSettings.EnableSgolayFilt && size(sigT,2) >= hAnalysisSettings.SGolaySize
                        sigTAvg = single(sgolayfilt(double(sigTAvg), hAnalysisSettings.SGolayOrder, hAnalysisSettings.SGolaySize));
                        sigTStd = single(sgolayfilt(double(sigTStd), hAnalysisSettings.SGolayOrder, hAnalysisSettings.SGolaySize));
                        sigTP95 = single(sgolayfilt(double(sigTP95), hAnalysisSettings.SGolayOrder, hAnalysisSettings.SGolaySize));
                    end

                    obj.SigTAvg(k,tMin:tMax)        = sigTAvg;
                    obj.SigTStd(k,tMin:tMax)        = sigTStd;
                    obj.SigTP95(k,tMin:tMax)        = sigTP95;
                    obj.SigTSum(k,tMin:tMax)        = sum(sigT, 1, 'omitnan');
                    [obj.SigTMax(k,tMin:tMax), I]   = max(sigT, [], 1, 'omitnan');                  
                    obj.Areas{k}            = sum(~isnan(dd0),1);
                    obj.AreaComputations(hDataset, X',Y',Z,xMin,yMin,zMin,tMin,tMax,k,sigT, I);
                    obj.Areas{k}            = obj.Areas{k} .* hDataset.MetaData.SampleVolume;

                    obj.StartFrame{k} = tMin;
                    obj.DurationF{k} = tMax-tMin+1;

                    obj.UpdateProgressBar();
                end
            catch 
            end
            
            obj.ComputeSignalProperties(hDataset);                     
            obj.updateSigBounds();            
            obj.CloseProgressbar();
        end
        
        function value = GetAvgArea(obj, roiID)
            if nargin == 2
                areas = obj.Areas(roiID,:);
            else
                areas = obj.Areas;
            end
            
            value = cellfun(@(x) mean(x, 'all', 'omitnan'), areas);
        end
        
        function value = GetMaxArea(obj, roiID)
            if nargin == 2
                areas = obj.Areas(roiID,:);
            else
                areas = obj.Areas;
            end
            
            value = cellfun(@(x) max(x, [],'all', 'omitnan'), areas);
        end
        
        function value = GetAvgDuration(obj, roiID)
            if nargin == 2
                durs = obj.Duration(roiID,:);
            else
                durs = obj.Duration;
            end
            
            pc = sum(cellfun(@(x) numel(x), durs), 2, 'omitnan');
            value = sum(cellfun(@(x) sum(x, 'all', 'omitnan'), durs), 2, 'omitnan') ./ pc;
        end
        
        function value = GetAvgDurationStd(obj, roiID)
            if nargin == 2
                durs = obj.Duration(roiID,:);
            else
                durs = obj.Duration;
            end
            %ToDo
            sz = size(durs);
            value = zeros(sz(1),1);

            for i=1:sz(1)
                value(i) = std(vertcat(durs{i,:}), 'omitnan');
            end
            
         end
        
        function value = GetSignalCount(obj, roiID)
            if nargin == 2
                peaks = obj.SigMax(roiID,:);
            else
                peaks = obj.SigMax;
            end
            
            value = sum(cellfun(@(x) numel(x), peaks), 2, 'omitnan');            
        end
    end
    
    methods(Access=protected) 
        function InitProgressBar(obj)
            obj.pbSteps = obj.NumRois*2;
            obj.pbCurr = 0;
            multiWaitbar('Analyze ROIs', 'Color', 'g');
        end
        
        function UpdateProgressBar(obj)
            obj.pbCurr = obj.pbCurr+1;
            multiWaitbar('Analyze ROIs', 'Value', obj.pbCurr/obj.pbSteps);
        end
        
        function CloseProgressbar(~)
            multiWaitbar('Analyze ROIs', 'Close');
        end
        
        function [peak, loc, fwhm, prom, intersect, intersectP90] = ExcludeSubSignals(~, peak, loc, fwhm, prom, intersect, intersectP90)
            realPeaks = com.lib.SigProc.RemoveSubSignals(peak, loc, intersect);

            peak = peak(realPeaks);
            loc = loc(realPeaks);
            fwhm = fwhm(realPeaks);
            prom = prom(realPeaks);
            intersect = intersect(realPeaks, :);
            intersectP90 = intersectP90(realPeaks, :);
        end
        
        function [TH, numTH, minProm] = GetSignalDetectionParams(obj, hDataset, traces)
            if hDataset.Settings.Analysis.UseManualTh            
                TH = hDataset.Settings.Analysis.ClassificationTh;
            else
                rm = mean(traces(:), 'omitnan');
                rsd = std(traces(:), 'omitnan');
                TH = [rm + rsd , rm + 2*rsd, rm + 3*rsd];
            end
            
            minProm = hDataset.Settings.Analysis.MinPeakProminence;
            numTH = numel(TH);
            obj.Thresholds = TH;
            TH(end+1) = inf;
        end
        
        function [peak, loc, fwhm, prom, intersect, intersectP90] = FindSignalPeaks(~, trace, minTH, minProm, maxPeakWidth, DurFrac)
            [peak, loc, fwhm, prom, intersect] = findpeaks2(trace, ...
                        'MinPeakHeight', minTH,...
                        'MinPeakProminence', minProm, ...
                        'WidthReference', 'halfprom',...                        
                        'DurFrac', DurFrac);
                    
            [~, ~, ~, ~, intersectP90] = findpeaks2(trace, ...
                'MinPeakHeight', minTH,...
                'MinPeakProminence', minProm, ...
                'WidthReference', 'halfprom',...
                'DurFrac', 0.9);

            peak = peak(:);
            loc = loc(:);
            fwhm = fwhm(:);
            prom = prom(:);    
            
            if maxPeakWidth > 0
                idxTooLong = fwhm > maxPeakWidth;

                peak(idxTooLong) = [];
                loc(idxTooLong) = [];
                fwhm(idxTooLong) = [];
                prom(idxTooLong) = [];
                intersect(idxTooLong, :) = [];
                intersectP90(idxTooLong, :) = [];
            end
        end
        
        function ComputeTraces(obj, hDataset, traces)
            %Computes standard properties for signal traces, such as
            %Peak values, promonences, duration, rise and decay times. based on 50%, 25%,
            %or 10% of peak, as well as corresponding frames and times in
            %seconds.
            
            [TH, numTH, minProm] = obj.GetSignalDetectionParams(hDataset, traces);
            
            if min(max(traces, [],2, 'omitnan')) < TH(1)
                com.common.Logging.MLogManager.Warning("Minimum classification threshold too large. Peak detection may fail for some signals.");
            end
            
            minTh = TH(1);
            falsePositives = [];
            
            durationHeight  = hDataset.Settings.Analysis.DurationHeight;
            
            %Get maximum peakwidth in samples
            maxPeakWidth    = hDataset.Settings.Analysis.MaxPeakWidth * hDataset.MetaData.VolumeRate;
            
            for i=1:obj.NumRois
                if any( traces(i,:) > TH(1) )                                                                      
                    [peak, loc, fwhm, prom, intersect, intersectP90] = obj.FindSignalPeaks( ...
                        obj.GetRoiTrace(i), minTh,  minProm, maxPeakWidth, durationHeight);
                    
                    if ~isempty(peak)
                        %Exclude subsignals
                        if hDataset.Settings.Analysis.ExcludeSubSignals
                            [peak, loc, fwhm, prom, intersect, intersectP90] = ...
                                obj.ExcludeSubSignals(peak, loc, fwhm, prom, intersect, intersectP90);                       
                        end

                        %Specific to signals that may have multiple peaks, e.g.
                        %from static ROIs
                        if ~isempty(loc) && obj.IsMultiPeakSignal                                                
                            obj.OnIsMultipeakSignal(hDataset, i, loc, intersect);
                        end

                        durs = fwhm ./ hDataset.MetaData.VolumeRate;
                        riseTimes = (intersectP90(:, 1) - intersect(:, 1)) ./ hDataset.MetaData.VolumeRate;
                        decayTimes = (intersect(:, 2) - intersectP90(:, 2)) ./ hDataset.MetaData.VolumeRate;

                        %iterate over THs and sort into respective bins
                        for t=1:numTH
                            lTh = TH(t);
                            uTh = TH(t+1);

                            idx = (peak >= lTh & peak < uTh);
                            
                            obj.SigMax{i, t}            = single(peak(idx));
                            obj.Prominence{i, t}        = prom(idx);
                            obj.PeakFrame{i, t}         = int32(loc(idx));
                            obj.PeakTime{i, t}          = single((loc(idx) - 1) ./ hDataset.MetaData.VolumeRate);
                            obj.Duration{i, t}          = durs(idx);
                            obj.DurationF{i, t}         = fwhm(idx);
                            obj.PxIntersectPoints{i, t} = single(intersect(idx, :));
                            obj.P90IntersectPoints{i, t}= single(intersectP90(idx, :));
                            obj.RiseTime{i, t}          = riseTimes(idx);
                            obj.DecayTime{i, t}         = decayTimes(idx);
                            obj.StartFrame{i, t}        = single(intersect(idx, 1));
                            obj.StartTime{i, t}         = single((intersect(idx, 1) - 1) ./ hDataset.MetaData.VolumeRate); 
                            
                            numSig = sum(idx);
                            sigAvg = [];
                            sigSum = [];
                            
                            sFrames = round(obj.StartFrame{i, t});
                            eFrames = round(sFrames + (obj.DurationF{i, t}-1) );
                            
                            for s=1:numSig
                                sig = obj.SigTAvg(i, sFrames(s):eFrames(s));
                                sigAvg = cat(1, sigAvg, mean(sig(:), "omitnan"));
                                sigSum = cat(1, sigSum, sum(sig(:), "omitnan"));
                            end
                            
                            obj.SigAvg{i, t} = sigAvg;
                            obj.SigSum{i, t} = sigSum;
                        end
                    else
                        falsePositives = cat(1, falsePositives, i);
                    end
                else
                    falsePositives = cat(1, falsePositives, i);
                end                                                
                obj.UpdateProgressBar();
            end
            
            if ~isempty(falsePositives) && obj.hParent.hParent.AnalysisType ~= com.Enum.AnalysisType.Manual
                com.common.Logging.MLogManager.Info("Deleting " + numel(falsePositives) + ...
                    " ROIs, because they did not reach the minimum classification threshold (" + ...
                    minTh +")");
                obj.hParent.DeleteROI(falsePositives);
            end
        end
        
        function OnIsMultipeakSignal(~, ~, ~, ~, ~)            
        end
    end
    
    methods
        function updateSigBounds(obj)
            [lB, uB] = bounds(obj.SigTAvg(:), "omitnan");
            
            if (abs(uB-lB) < 1)
                obj.SigBounds = [lB, uB] * 1.5;
            else
                obj.SigBounds = [floor(lB), ceil(uB)];
            end
        end
    end
    
    methods(Abstract, Access=protected)
        InitCustomFields(obj);
        idxShift = GetIndexShift(obj, hDataset);
        idx = GetEventIndices(obj, hDataset, rois, eventNo);
        AreaComputations(obj, X,Y,Z,eventNo,sigT,sigTMaxIdx);
        ComputeSignalProperties(obj, hdataset);
    end
end

