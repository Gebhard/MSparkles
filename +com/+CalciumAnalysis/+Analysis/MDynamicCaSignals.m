%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDynamicCaSignals < com.CalciumAnalysis.Analysis.MCaSignalsRoot
    %MSTATICCAANALYSISRESULT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        AvgArea;
        MaxArea;
        TotalArea;
        SigTCenter;     % The center of mass (area * Ca) per event per timepoint
        SigTMaxLoc;     % Spatial coordinates of the per-frame maximum
        
        %Parameters for complexity analysis of dynamic Ca waves
        Complexity;     % Complexity metric for each Signal
        ComplexityE;    % Complexity of size changes metric for each Signal
        ComplexityC;    % Complexity of Ca2+ changes metric for each Signal
        distMax;        % max distance for centers [area, mass]
        distEnd;        % final distance for centers [area, mass]
        distTotal;      % total distance for centers [area, mass]
    end    
    
    properties(Dependent)
       AvgAreaStd;
       AvgPropagationSpeed;
    end
    
    methods
        function value = get.AvgAreaStd(obj)
            value = cellfun(@(x) std(x, 'omitnan'), obj.Areas);
        end
        
        function value = get.AvgPropagationSpeed(obj)
            value = obj.distTotal(:,1) ./ vertcat(obj.Duration{:});
        end
    end
    
    methods         
        function ComputeSignalComplexity(obj)
            AreaMeanDists = cell(obj.NumRois,1 );
            CaAcgMeanDists = cell(obj.NumRois,1 );
            maxADists = zeros(obj.NumRois,1);
            maxCaDists = zeros(obj.NumRois,1);

            for k=1:obj.NumRois
                sigTAvg = obj.GetRoiTrace(k);
                sigArea = obj.Areas{k}(:);
                
                meanADist = mean(sigArea, 'omitnan');
                meanCaDist = mean(sigTAvg, 'omitnan');

                AreaMeanDists{k} = abs(sigArea - meanADist);
                CaAcgMeanDists{k} = abs(sigTAvg - meanCaDist);

                maxADists(k) = max( AreaMeanDists{k}(:) );
                maxCaDists(k) = max( CaAcgMeanDists{k}(:) );
            end

            maxADist = max(maxADists);
            maxCaDist = max(maxCaDists);
            maxDur = round(max(cat(2,obj.DurationF{:})));
            
            %Compute complexity
            for k=1:obj.NumRois
                obj.ComplexityE{k} = CalcEntropy(AreaMeanDists{k}(:) / maxADist, maxDur);
                obj.ComplexityC{k} = CalcEntropy(CaAcgMeanDists{k}(:) / maxCaDist, maxDur);

                obj.Complexity{k} = obj.ComplexityE{k} + obj.ComplexityC{k};
            end
        end  
    end    
    
     methods(Access=protected) 
         function ComputeSignalProperties(obj, hDataset)
            %obj.ComputeTraces(hDataset, obj.SigTP95);
            obj.ComputeTraces(hDataset, obj.SigTAvg);
            obj.ComputeSignalComplexity();
         end
         
%          function [peak, loc, fwhm, prom, intersect, intersectP90] = FindSignalPeaks(obj, trace, minTH, minProm, DurFrac, tMin, tMax)
% %             [peak, loc, fwhm, prom, intersect, intersectP90] = FindSignalPeaks@com.CalciumAnalysis.Analysis.MCaSignalsRoot(...
% %                 obj, trace, minTH, minProm, DurFrac);
% % 
% %             %Get only the highest peak
% %             if ~isempty(peak)
% %                 [~, idx] = max(peak);
% %                 peak = peak(idx);
% %                 loc = loc(idx);
% %                 fwhm = fwhm(idx);
% %                 prom = prom(idx);   
% %                 intersect = intersect(idx,:);
% %                 intersectP90 = intersectP90(idx,:);
% %             end
% 
%             [peak, loc] = max(trace, [], 'omitnan');
%             prom = peak;
%             
%          end

        function ComputeTraces(obj, hDataset, traces)
            %Computes standard properties for signal traces, such as
            %Peak values, promonences, duration, rise and decay times. based on 50%, 25%,
            %or 10% of peak, as well as corresponding frames and times in
            %seconds.
            
            [TH, numTH, minProm] = obj.GetSignalDetectionParams(hDataset, traces);
            
            if min(max(traces, [],2, 'omitnan')) < TH(1)
                com.common.Logging.MLogManager.Warning("Minimum classification threshold too large. Peak detection may fail for some signals.");
            end
            
            minTh = TH(1);
            
            falsePositives = [];
            
            n = single([]);
            
            startFrames = obj.StartFrame;
            durationsF = obj.DurationF;
            
            obj.StartFrame = {};
            
            for i=1:obj.NumRois
                if any( traces(i,:) > TH(1) )                                                                                                                                  
                    [peak, loc] = max(traces(i,:), [], 'omitnan');
                    prom = peak;
                    
                    if ~isempty(peak) && peak >= minTh  
                        startFrame = round(startFrames{i});
                        durationF = durationsF{i};
                        
                        endFrame = startFrame + durationF - 1;
                        
                        durs = single(durationF ./ hDataset.MetaData.VolumeRate);
                        riseTimes = single((loc - startFrame) ./ hDataset.MetaData.VolumeRate);
                        decayTimes = single((endFrame - loc) ./ hDataset.MetaData.VolumeRate);
                        
                        intersect = single([startFrame, loc ]);
                        intersectP90 = single([ loc, endFrame ]);
                         
                        %iterate over THs and sort into respective bins
                        for t=1:numTH
                            lTh = TH(t);
                            uTh = TH(t+1);

                            if(peak >= lTh && peak < uTh)
                                obj.SigMax{i, t}            = single(peak);
                                obj.Prominence{i, t}        = prom;
                                obj.PeakFrame{i, t}         = single(loc);
                                obj.PeakTime{i, t}          = single((loc - 1) ./ hDataset.MetaData.VolumeRate);
                                obj.Duration{i, t}          = durs;
                                obj.PxIntersectPoints{i, t} = intersect;
                                obj.P90IntersectPoints{i, t}= intersectP90;
                                obj.RiseTime{i, t}          = riseTimes;
                                obj.DecayTime{i, t}         = decayTimes;
                                obj.StartFrame{i, t}        = startFrame;
                                obj.StartTime{i, t}         = single(startFrame - 1) ./ hDataset.MetaData.VolumeRate; 
                                obj.DurationF{i, t}         = durationF;
                                sigAvg = [];
                                sigSum = [];
                                
                                sig = obj.SigTAvg(i, startFrame:endFrame);
                                sigAvg = cat(1, sigAvg, mean(sig(:), "omitnan"));
                                sigSum = cat(1, sigSum, sum(sig(:) , "omitnan"));

                                obj.SigAvg{i, t} = sigAvg;
                                obj.SigSum{i, t} = sigSum;
                            else
                                obj.SigMax{i, t}            = n;
                                obj.Prominence{i, t}        = n;
                                obj.PeakFrame{i, t}         = n;
                                obj.PeakTime{i, t}          = n;
                                obj.Duration{i, t}          = n;
                                obj.PxIntersectPoints{i, t} = n;
                                obj.P90IntersectPoints{i, t}= n;
                                obj.RiseTime{i, t}          = n;
                                obj.DecayTime{i, t}         = n;
                                obj.StartTime{i, t}         = n;
                                obj.StartFrame{i, t}        = n;
                                obj.DurationF{i, t}         = n;
                                obj.SigAvg{i, t}            = n;
                                obj.SigSum{i, t}            = n;
                            end
                        end
                    else
                        falsePositives = cat(1,falsePositives, i);
                    end
                else
                    falsePositives = cat(1,falsePositives, i);
                end                                                
                obj.UpdateProgressBar();
            end
            
            if ~isempty(falsePositives)
                com.common.Logging.MLogManager.Info("Deleting " + numel(falsePositives) + ...
                    " ROIs, because they did not reach the minimum classification threshold (" + ...
                    minTh +")");
                obj.hParent.DeleteROI(falsePositives);
            end
        end
        
        function InitCustomFields(obj)
            obj.SigTCenter          = cell(obj.NumRois,1);
            obj.AvgArea             = zeros(obj.NumRois, 1, 'single'); 
            obj.MaxArea             = zeros(obj.NumRois, 1, 'single');
            obj.TotalArea           = zeros(obj.NumRois, 1, 'single');
            obj.Complexity          = cell(obj.NumRois,1);
            obj.ComplexityE         = cell(obj.NumRois,1);
            obj.ComplexityC         = cell(obj.NumRois,1);
            obj.distMax             = zeros(obj.NumRois, 2, 'single');
            obj.distEnd             = zeros(obj.NumRois, 2, 'single');
            obj.distTotal           = zeros(obj.NumRois, 2, 'single');
            obj.SigTMaxLoc          = cell(obj.NumRois,1);
        end
         
        function idxShift = GetIndexShift(~, ~)
            idxShift = 0; 
        end
        
        function idx = GetEventIndices(~, ~, rois, eventNo)
            idx = rois.PixelIdxList{eventNo}(:);
        end
                 
        function AreaComputations(obj, hDataset, X,Y,Z, xMin,yMin,zMin,tMin,tMax, eventNo, sigT, sigTMaxIdx)
            %% center of area
            xx = sum(~isnan(sigT).*X(:),1,'omitnan') ./ obj.Areas{eventNo};
            yy = sum(~isnan(sigT).*Y(:),1,'omitnan') ./ obj.Areas{eventNo};
            zz = sum(~isnan(sigT).*Z(:),1,'omitnan') ./ obj.Areas{eventNo};
             
            tl = tMin:tMax;
            areaCenter = zeros(4,numel(tl));
            areaCenter(1,:) = tl;
            areaCenter(2:4,:) = [   (xx+xMin-1) .* hDataset.MetaData.SampleSizeX;...
                                    (yy+yMin-1) .* hDataset.MetaData.SampleSizeY;...
                                    (zz+zMin-1) .* hDataset.MetaData.SampleSizeZ];
            
            obj.AreaCenter{eventNo} = areaCenter;
            obj.AvgArea(eventNo)    = mean( obj.Areas{eventNo}(:) .* hDataset.MetaData.SampleVolume );
            obj.MaxArea(eventNo)    = max( obj.Areas{eventNo}(:)  .* hDataset.MetaData.SampleVolume);
            obj.TotalArea(eventNo)  = sum( obj.Areas{eventNo}(:)  .* hDataset.MetaData.SampleVolume);
            
            %%Per-frame peak location
            xx = (X(sigTMaxIdx) + xMin - 1) .* hDataset.MetaData.SampleSizeX;
            yy = (Y(sigTMaxIdx) + yMin - 1) .* hDataset.MetaData.SampleSizeY;
            zz = (Z(sigTMaxIdx) + zMin - 1) .* hDataset.MetaData.SampleSizeZ;
            sigTLoc = [tl; xx; yy; zz];
            obj.SigTMaxLoc{eventNo} = sigTLoc;
            
            dist = sqrt(((xx-xx(1)) .* hDataset.MetaData.SampleSizeX) .^2 + ...
                        ((yy-yy(1)) .* hDataset.MetaData.SampleSizeY) .^2 + ...
                        ((zz-zz(1)) .* hDataset.MetaData.SampleSizeZ) .^2);
                    
            obj.distMax(eventNo,1) = max(dist);
            obj.distEnd(eventNo,1) = dist(end);
            obj.distTotal(eventNo,1) = sum(sqrt(sum(diff(sigTLoc(2:end, :), 1, 2) .^2,1)));
            
            
            
            %% center of mass (area * Ca)
            sigTSum = obj.SigTSum(eventNo, :);
            sigTSum = sigTSum(~isnan(sigTSum));
            
            xx = sum(sigT.*X(:),1,'omitnan')./sigTSum;
            yy = sum(sigT.*Y(:),1,'omitnan')./sigTSum;
            zz = sum(sigT.*Z(:),1,'omitnan')./sigTSum;

            dist = sqrt(((xx-xx(1)) .* hDataset.MetaData.SampleSizeX) .^2 + ...
                        ((yy-yy(1)) .* hDataset.MetaData.SampleSizeY) .^2 + ...
                        ((zz-zz(1)) .* hDataset.MetaData.SampleSizeZ) .^2);
                    
            obj.distMax(eventNo,2) = max(dist);
            obj.distEnd(eventNo,2) = dist(end);            
            
            %1st row = tim eindex
            %2nd - 4th row are X,Y,Z, coordinates, respectively
            tl = tMin:tMax;
            sigTCenter = zeros(4,numel(tl));
            sigTCenter(1,:) = tl;
            sigTCenter(2:4,:) = [   (xx+xMin-1) .* hDataset.MetaData.SampleSizeX;...
                                    (yy+yMin-1) .* hDataset.MetaData.SampleSizeY;...
                                    (zz+zMin-1) .* hDataset.MetaData.SampleSizeZ];
                                
            obj.SigTCenter{eventNo}= sigTCenter;            
            obj.distTotal(eventNo,2) = sum(sqrt(sum(diff(sigTCenter(2:end, :), 1, 2) .^2,1)));
         end         
    end
end

