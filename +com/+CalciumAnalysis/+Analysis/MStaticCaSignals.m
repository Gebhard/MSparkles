%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStaticCaSignals < com.CalciumAnalysis.Analysis.MCaSignalsRoot
    %MSTATICCAANALYSISRESULT At this point, all properties needed are
    %already implemented by the base class. However, this specialized
    %derived class ensures to be future proof, in case there are changes or
    %additions, that are not needed or solved differnetly for dynamic
    %signals.
    
    properties
        %Peaks
        StartToStartTime;
        PeakToPeakTime;
        InterSignalTime;
        SigMeanFreq;    % Mean frequency inbetween first and last peak. If a ROI has only one peak, the respective value will be NaN
    end
    
    properties(Transient, Dependent)
        AvgArea;
        MaxArea;
        AvgDuration;
        AvgDurationStd;
    end
    
    methods
        function obj = MStaticCaSignals()
            obj = obj@com.CalciumAnalysis.Analysis.MCaSignalsRoot;
            obj.IsMultiPeakSignal = true;
        end
    end
    
    methods
        function value = get.AvgArea(obj)
            value = obj.GetAvgArea();
        end
        
        function value = get.MaxArea(obj)
            value = obj.GetMaxArea();
        end
        
        function value = get.AvgDuration(obj)
            value = obj.GetAvgDuration();
        end
        
        function value = get.AvgDurationStd(obj)
            value = obj.GetAvgDurationStd();
        end
    end
        
    methods(Access=protected)
        function ComputeSignalProperties(obj, hDataset)
             obj.ComputeTraces(hDataset, obj.SigTAvg);
        end
        
        function InitCustomFields(obj)
            obj.StartToStartTime    = cell(obj.NumRois,1);
            obj.PeakToPeakTime      = cell(obj.NumRois,1);
            obj.InterSignalTime     = cell(obj.NumRois,1);
            obj.SigMeanFreq         = zeros(obj.NumRois, 2, 'single');
        end
         
        function idxShift = GetIndexShift(~, hDataset)
            idxShift = (0:hDataset.MetaData.SamplesT-1) * (hDataset.MetaData.SamplesY * hDataset.MetaData.SamplesX...
                * hDataset.MetaData.SamplesZ); 
        end
        
        function idx = GetEventIndices(~, hDataset, rois, eventNo)
            %Project ROIs along entire time axis
            idx = repmat(rois.PixelIdxList{eventNo}(:), 1, hDataset.MetaData.SamplesT);             
        end
         
        function AreaComputations(obj, hDataset, X,Y,Z, xMin,yMin,zMin,tMin,tMax, eventNo, sigT, sigTMaxIdx)
            %% center of area
            xx = sum(~isnan(sigT).*X(:),1,'omitnan')./obj.Areas{eventNo};
            yy = sum(~isnan(sigT).*Y(:),1,'omitnan')./obj.Areas{eventNo};
            zz = sum(~isnan(sigT).*Z(:),1,'omitnan')./obj.Areas{eventNo};
             
            obj.AreaCenter{eventNo} = [ (xx+xMin-1) .* hDataset.MetaData.SampleSizeX;...
                                        (yy+yMin-1) .* hDataset.MetaData.SampleSizeY;...
                                        (zz+zMin-1) .* hDataset.MetaData.SampleSizeZ;...
                                        tMin:tMax];
        end
        
        function OnIsMultipeakSignal(obj, hDataset, roiId, loc, intersect)
            %Compute peak-peak time (delta-t from peak-to-peak of
            %two consecutive signals)
            locShift = circshift(loc, -1);
            locShift(end) = nan;%loc(end);
            obj.PeakToPeakTime{roiId} = (locShift - loc) ./ hDataset.MetaData.VolumeRate;

            %Compute signal-signal time (delta-t from
            %start-to-start of two consecutive signals)
            sigStart = intersect(:,1);
            sigStartShift = circshift(sigStart, -1);
            sigStartShift(end) = nan;%sigStart(end);
            obj.StartToStartTime{roiId} = (sigStartShift - sigStart) ./ hDataset.MetaData.VolumeRate;

            %Compute inter-signal time (delta-t from end-to-start
            %of two consecutive signals)
            sigEnd = intersect(:,2);
            sigStartShift(end) = nan;%sigEnd(end);
            obj.InterSignalTime{roiId} = (sigStartShift - sigEnd) ./ hDataset.MetaData.VolumeRate;
                        
            %Compute mean frequency
            if numel(loc) > 1
                locD = diff(loc ./ hDataset.MetaData.VolumeRate);
                obj.SigMeanFreq(roiId,1) = 1 ./ mean(locD);
                obj.SigMeanFreq(roiId,2) = 1 ./ std(locD);
                
                obj.SigMeanFreq(isinf(obj.SigMeanFreq)) = nan; 
            else
                obj.SigMeanFreq(roiId,:) = nan;
            end
        end
    end
end

