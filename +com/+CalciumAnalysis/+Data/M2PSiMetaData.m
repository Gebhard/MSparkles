%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef M2PSiMetaData < com.common.Data.MMetadataRoot
    %M2PSIMETADATA Extended metadata for microscopes acquired with
    %ScanImage 5.x
    
    properties
        ScanImageVersion;
        HasResonantMirror;
        SampleRate;
        PixelBinFactor;
        ScanMode;
        ScanPixelTimeMaxMinRatio;
        ScanPixelTimeMean;
        ScannerFrequency;
        ChannelsAvailable;
        LogAverageFactor;
        
        ScanZoomFactor;
        ScanVolumeRate;
        ScanRotation;
        
        BeamPowers;
        BeamstackEndPower;
        BeamStackStartPower;
        
        PmtAutoPower;
        PmtBandwidths;
        PmtGains;
        PmtNames;
        PmtOffsets;
        PmtPowersOn;
    end
    
    methods
        function obj = M2PSiMetaData(hSiHeader)
            obj = obj@com.common.Data.MMetadataRoot();
            
            if nargin == 1
                try
                    obj.HasResonantMirror = hSiHeader.SI.hScan2D.hasResonantMirror;
                    obj.SampleRate = hSiHeader.SI.hScan2D.sampleRate;
                    obj.PixelBinFactor = hSiHeader.SI.hScan2D.pixelBinFactor;
                    obj.ScanMode = hSiHeader.SI.hScan2D.scanMode;
                    obj.ScanPixelTimeMaxMinRatio = hSiHeader.SI.hScan2D.scanPixelTimeMaxMinRatio;
                    obj.ScanPixelTimeMean = hSiHeader.SI.hScan2D.scanPixelTimeMean;
                    obj.ScannerFrequency = hSiHeader.SI.hScan2D.scannerFrequency;
                    obj.ChannelsAvailable = hSiHeader.SI.hScan2D.channelsAvailable;
                    obj.LogAverageFactor = hSiHeader.SI.hScan2D.logAverageFactor;

                    obj.ScanZoomFactor = hSiHeader.SI.hRoiManager.scanZoomFactor;
                    obj.ScanVolumeRate = hSiHeader.SI.hRoiManager.scanVolumeRate;
                    obj.ScanRotation = hSiHeader.SI.hRoiManager.scanRotation;

                    obj.BeamPowers = hSiHeader.SI.hBeams.powers;
                    obj.BeamstackEndPower = hSiHeader.SI.hBeams.stackEndPower;
                    obj.BeamStackStartPower = hSiHeader.SI.hBeams.stackStartPower;

                    obj.PmtAutoPower = hSiHeader.SI.hPmts.autoPower;
                    obj.PmtBandwidths = hSiHeader.SI.hPmts.bandwidths;
                    obj.PmtGains = hSiHeader.SI.hPmts.gains;
                    obj.PmtNames = hSiHeader.SI.hPmts.names;
                    obj.PmtOffsets = hSiHeader.SI.hPmts.offsets;
                    obj.PmtPowersOn = hSiHeader.SI.hPmts.powersOn;
                catch
                end
            end
            
            fn = fieldnames(obj);                   
            numFields = numel(fn);

            for f=1:numFields                        
                if isempty(obj.(fn{f}))
                    obj.(fn{f}) = "N/A";
                end
            end
        end
    end
    
    methods
        function value = ToString(obj)
            fn = fieldnames(obj);                   
            numFields = numel(fn);

            value = strings(numFields,2);
            
            for f=1:numFields   
                value(f,1) = fn{f};
                value(f,2) = obj.(fn{f});
            end
        end
    end
end

