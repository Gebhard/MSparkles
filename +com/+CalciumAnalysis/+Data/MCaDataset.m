%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaDataset < com.common.Data.MImageDataset
    %CADATASET Summary of this class goes here
    %   Detailed explanation goes here
    properties (Constant)
        VERSION = 1.89;
    end
    
    properties
        Version;
        Settings;                   % Detector parameters as set by user (or automatically found by algorithm)
        SanityBounds;
    end
    
    properties(AbortSet=true) 
        NormalizationMode;             
        F0Path;                     % Path to F0 file
        ExternalF0;                 % Flag, indicating wether F0 was computed or imported
        EEGDataset;                 % EEG Dataset        
        Annotations;
        F0Config;
    end
    
    properties(Transient)
        F0;                         % Basal background
        bRequiresReload;
        AnalysisSettings;           %Deprecated, delete in 1.9
    end
    
    properties(Transient, Dependent)
        CurrentAnalysis com.CalciumAnalysis.Analysis.MCaAnalysis;
        NormalizationName;
    end
    
    properties(SetAccess=private, Transient)
        F0min;
        F0max;
        normFMin;
        normFMax;
        propListener = {};
     end   
    
    properties(Access=private, Transient, Dependent)
        NormalizedF;
    end
    
    methods(Static)
        function obj = Convert(input)
            obj = com.CalciumAnalysis.Data.MCaDataset(input.OrgDatasetPath);
            
            C = metaclass(input);
            P = C.Properties;

            for k = 1:length(P)
                try
                    if ~P{k}.Dependent && ~P{k}.Constant
                        obj.(P{k}.Name) = input.(P{k}.Name);
                    end
                catch
                    
                end
            end
            
            obj.Version = [];
        end
    end
    
    % Construction & destruction
    methods
        function obj = MCaDataset(filePath)
            obj = obj@com.common.Data.MImageDataset(com.Enum.DatasetType.CalciumData, filePath);
            
            globalSettings = MGlobalSettings.GetInstance();
            obj.NormalizationMode = globalSettings.DefaultDetrendingType;
            obj.ExternalF0 = false;
            obj.Version = com.CalciumAnalysis.Data.MCaDataset.VERSION;            
            obj.Clear();                                                                       
            obj.F0Path = [];
            obj.Settings = com.CalciumAnalysis.Settings.MCaDatasetSettings();
            
            obj.AddPreProcConfig(com.CalciumAnalysis.Processing.PreProc.PureLetDenoiseCfg());
            obj.AddPreProcConfig(com.CalciumAnalysis.Processing.PreProc.TemporalMedianFilterCfg());
            
            obj.InitDatastore(obj.DatasetPath);
            obj.ResetMetadata();

            if isempty( obj.F0Config )
                obj.F0Config = com.CalciumAnalysis.Processing.Config.F0EstimatorCfg.empty(...
                obj.MetaData.NumChannels, 0);
            
                for i=1:obj.MetaData.NumChannels
                    obj.F0Config(i) = com.CalciumAnalysis.Processing.Config.F0EstimatorCfg(i);
                end
            end
            
            obj.SanityBounds = [-1, 20];
        end
        
        function delete(obj)
            obj.clearPropListener();
        end
    end
    
    % getters and setters
    methods
        function set.EEGDataset(obj, value)
            obj.EEGDataset = value;
            obj.EEGDataset.hParent = obj;
        end
        
        function value = get.Settings(obj)
            if isempty(obj.Settings)
                obj.Settings = com.CalciumAnalysis.Settings.MCaDatasetSettings();
            end
            
            value = obj.Settings;
        end
        
        function value = get.NormalizedF(obj)
            switch (obj.NormalizationMode)
                case com.CalciumAnalysis.Data.NormalizationMode.dF
                    value = obj.F - obj.F0;
                case com.CalciumAnalysis.Data.NormalizationMode.dFF
                    value = 1 - (obj.F0 ./ obj.F);
                case com.CalciumAnalysis.Data.NormalizationMode.dFF0
                    value = (obj.F ./ obj.F0) - 1;
                case com.CalciumAnalysis.Data.NormalizationMode.ratio
                    value = obj.F ./ obj.F0;
            end
            
            value = min(max( value, obj.SanityBounds(1) ), obj.SanityBounds(2));
        end
        
        function set.F0(obj, value)
            obj.F0 = value;
            if (~isempty(obj.F0))
                [obj.F0min, obj.F0max] = bounds(obj.F0(:));            
                obj.UpdateNormalizedMinMaxValues();
            else
               obj.F0min = 0;
               obj.F0max = 0;
            end
        end
        
        function set.F0Path(obj, value)
            obj.F0Path = value;
        end
        
        function value = get.F0Path(obj)
            if iscell(obj.F0Path)
                obj.F0Path = obj.F0Path{1};
            end
            
            value = obj.F0Path;
        end
        
        function set.AnalysisSettings(obj, value)
            obj.Settings = value;
        end  
        
        function value = get.AnalysisSettings(obj)
            %com.common.Logging.MLogManager.Warning("Deprecation warning. AnalysisSettings will be removed in the next version.");
            value = obj.Settings;
        end 
        
        function value = get.CurrentAnalysis(obj)
            value = obj.Analyses.CurrentAnalysis;
        end                                                
        
        function value = get.Annotations(obj)
            if isempty(obj.Annotations)
                obj.Annotations = com.Annotations.AnnotationManager();
            end
            
            value = obj.Annotations;
        end
        
        function set.Annotations(obj, value)
            obj.Annotations = value;
        end
        
        function value = get.NormalizationName(obj)
            switch (obj.NormalizationMode)
                case com.CalciumAnalysis.Data.NormalizationMode.dF
                    value = "F - F_{0}";
                case com.CalciumAnalysis.Data.NormalizationMode.dFF
                    value = "(F - F_{0})/F";
                case com.CalciumAnalysis.Data.NormalizationMode.ratio
                    value = "F / F_{0}";
                otherwise
                    value = "(F - F_{0})/F_{0}";
            end
        end
    end
    
    methods
        function value = GetNormalizedData(obj, fData, F0Data, method)
            
            if nargin < 4
                method = obj.NormalizationMode;
            end
            
            switch (method)
                case com.CalciumAnalysis.Data.NormalizationMode.dF
                    value = fData - F0Data;
                case com.CalciumAnalysis.Data.NormalizationMode.dFF
                    value = 1 - (F0Data ./ fData);
                case com.CalciumAnalysis.Data.NormalizationMode.dFF0
                    value = (fData ./ F0Data) - 1;
                case com.CalciumAnalysis.Data.NormalizationMode.ratio
                    value = fData ./ F0Data;
            end
            
            value = min(max( value, obj.SanityBounds(1), 'includenan' ), obj.SanityBounds(2), 'includenan');
        end
        
        function upgraded = Upgrade(obj)
            %Check if metadata are present
            if isempty(obj.MetaData)
                try
                    com.common.Logging.MLogManager.Warning(sprintf("Metadata are empty... Restoring original Metadata from source file. (Dataset: %s)", obj.DatasetPath));
                    obj.ResetMetadata();
                    obj.Save();
                    com.common.Logging.MLogManager.Info(sprintf("Metadata restored"));
                catch ex
                    com.common.Logging.MLogManager.Error("Restoring metadata failed!");
                    com.common.Logging.MLogManager.Exception(ex);
                end
            end
            
            if isempty(obj.Version) || obj.Version < com.CalciumAnalysis.Data.MCaDataset.VERSION
                try
                    obj.UpgradeToCurrentVersion();
                    obj.Version = com.CalciumAnalysis.Data.MCaDataset.VERSION;
                    upgraded = true;
                catch ex
                    upgraded = false;
                    com.common.Logging.MLogManager.Exception(ex);
                    %rethrow(ex);                    
                end
            else
                upgraded = false;
            end
        end
        
        function UpgradeToCurrentVersion(obj)
            %Ensure, that each version is only upgraded from its direct
            %predecessor                        
            obj.UpgradeTo189();
        end              
        
        function UpgradeTo189(obj)
            if obj.Version < 1.89
                obj.UpgradeTo188();
                
                obj.UpgradeF0Config();
            end
        end
        
        function UpgradeTo188(obj)
            if obj.Version < 1.88
                obj.UpgradeTo187();

                %Upgrade dataset settings
                settings = com.CalciumAnalysis.Settings.MCaDatasetSettings();

                if ~isempty(obj.Settings)
                    settings.Analysis.CopyFrom(obj.Settings.Analysis);
                    settings.Display.CopyFrom(obj.Settings.Display);
                    settings.EegSync.CopyFrom(obj.Settings.EegSync);
                end

                obj.Settings = settings;
                
                obj.Analyses.UpgradeSettings();
            end
        end
        
        function UpgradeTo187(obj)
            if obj.Version < 1.87
                %obj.UpgradeTo186()
                
                obj.Annotations = com.Annotations.AnnotationManager.UpgradeFrom( obj.Annotations );
                
                %Create new analysis manager and copy its values
                hMngr = obj.CreateAnalysisManager();
                hMngr.Convert(obj.Analyses);
                obj.Analyses = hMngr;
            end
        end
        
        function UpgradeTo186(obj)
            if isempty(obj.Version) || obj.Version < 1.862
                error('Cannot upgrade from version, older than 1.8.6.2 Please install MSparkles 1.8.8 and execute it once first.');
            end
        end
        
        function success = SavePreProcResult(obj)
            try
                [fPath,fName,~] = fileparts(obj.OrgDatasetPath);
                
                preprocPath = fullfile(fPath, sprintf('PreProc-%s%s', fName, '.tif'));

                hTiffFile = com.common.FileIO.MSparklesTiffFile(preprocPath);
                hTiffFile.MetaData = obj.MetaData;
                hTiffFile.FileData = obj.F;
                hTiffFile.SaveBigTiff();

                obj.PreProcDatasetPath = preprocPath;
                success = true;
            catch
                success = false;
            end
        end
        
        function Clear(obj)
            Clear@com.common.Data.MImageDataset(obj);
          
            obj.F0              = [];                             
            obj.F0min           = 0;
            obj.F0max           = 0;            
            obj.normFMin        = 0;
            obj.normFMax        = 0;
            obj.bRequiresReload = false;
            
            obj.Analyses.CurrentAnalysis = [];                       
            
            if ~isempty(obj.EEGDataset)
                obj.EEGDataset.Clear();
            end
            
            obj.Annotations.Clear();
        end

        function value = FrameTimes(obj, frameNo)
            if nargin < 2
                frameNo = 1:obj.MetaData.SamplesT;
            end

            if obj.MetaData.UseExactTimingInformation&& ...
                ~isempty(obj.MetaData.PerFrameTimestamps)

                frameNo(frameNo < 1) = 1;
                value = obj.MetaData.PerFrameTimestamps( frameNo );
            else
                value = frameNo ./ obj.MetaData.VolumeRate;
            end
        end
        
        function value = IsReloadRequired(obj)
            try
                value = ~obj.IsLoaded || isempty(obj.F) || ...
                    (~isempty(obj.F0Path) && isempty(obj.F0));
            catch
                %com.common.Logging.MLogManager.Warning("Error in 'MCaDataset.IsReloadRequired()'. Defaulting to TRUE.");
                value = true;
            end
        end
        
        function UpdateNormalizedMinMaxValues(obj)
            if (~isempty(obj.F) && ~isempty(obj.F0))
                [obj.normFMin, obj.normFMax] = bounds(obj.NormalizedF, 6);
            else
                obj.normFMin = 0;
                obj.normFMax = 0;
            end
        end
        
        function ImportF0(obj, filePath)
            obj.ExternalF0 = true;
            obj.F0Path = filePath;
            
            obj.LoadF0();
        end
        
        function DeleteF0(obj)
            com.common.Logging.MLogManager.Info("Resetting F0 for dataset " + obj.DatasetName + ".");                        
            
            obj.ExternalF0 = false;
            obj.F0 = [];
            obj.normFMin = 0;
            obj.normFMax = 0;
            
            if (~isempty(obj.F0Path) && isfile(obj.F0Path))
                delete(obj.F0Path);   
                obj.F0Path = [];
                
                %Update Project manager
                hProjMan = com.Management.MProjectManager.GetInstance();

                if ~isempty(hProjMan)
                    hProjMan.DeleteF0Node(obj, obj.TreeNode);
                end
                
                obj.Save();
            end
        end        
        
        function ResetF0(obj)
            %This function is a wrapper for completeness of Reset-functions
            obj.DeleteF0();
        end
        
        function success = SaveF0(obj)
            try
                multiWaitbar('Saving F0', 'Busy', 'Color', 'g');
                [fPath, fName, ~] = fileparts(obj.OrgDatasetPath);
                f0Path = fullfile(fPath, sprintf('F0_%s.mat', fName));

                obj.F0Path = f0Path;
                obj.Save();      

                F0 = obj.F0;
                save(f0Path, 'F0', '-v7.3', '-nocompression');
                success = true;
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                success = false;
            end
            multiWaitbar('Saving F0', 'Close' );
        end
        
        function varargout = subsref(obj, S)
            if length(S) == 2
                switch S(1).subs
                    case "NormalizedF"
                        switch (obj.NormalizationMode)
                            case com.CalciumAnalysis.Data.NormalizationMode.dF
                                [varargout{1:nargout}] = min(max(subsref(obj.F, S(2)) - subsref(obj.F0, S(2)),...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                            case com.CalciumAnalysis.Data.NormalizationMode.dFF
                                [varargout{1:nargout}] = min(max(1 - (subsref(obj.F0, S(2)) ./ subsref(obj.F, S(2))),...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                            case com.CalciumAnalysis.Data.NormalizationMode.ratio
                                [varargout{1:nargout}] = min(max(subsref(obj.F, S(2)) ./ subsref(obj.F0, S(2)),...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                            otherwise  %case com.CalciumAnalysis.Data.NormalizationMode.dFF0
                                [varargout{1:nargout}] = min(max((subsref(obj.F, S(2)) ./ subsref(obj.F0, S(2))) -1,...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                        end
                        return;
                        
                    case "deltaF"
                        [varargout{1:nargout}] = (subsref(obj.F, S(2)) - subsref(obj.F0, S(2)));
                        return;
                        
                    case "dFF"
                        [varargout{1:nargout}] = min(max(1 - (subsref(obj.F0, S(2)) ./ subsref(obj.F, S(2))),...
                                    obj.SanityBounds(1)), obj.SanityBounds(2)); 
                        return;
                        
                    case "Ratio"
                        [varargout{1:nargout}] = min(max(subsref(obj.F, S(2)) ./ subsref(obj.F0, S(2)),...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                        return;
                        
                    case "dFF0"
                        [varargout{1:nargout}] = min(max((subsref(obj.F, S(2)) ./ subsref(obj.F0, S(2))) -1,...
                                    obj.SanityBounds(1)), obj.SanityBounds(2));
                        return;
                        
                    case "Dynamic Range"
                        [varargout{1:nargout}] = com.lib.ImageProc.ComputeDynRangeImage(...
                            obj.dFF0( S(2) ), [obj.MetaData.SamplesY, obj.SamplesX, obj.SamplesZ] , 0, 0);
                        return;
                end                                                     
            end

            [varargout{1:nargout}] = builtin('subsref',obj,S);
        end
    end
    
    methods(Access=protected)
        function value = GetTimingSignal(obj)
            if obj.MetaData.UseExactTimingInformation&& ...
                ~isempty(obj.MetaData.PerFrameTimestamps)
                value = obj.MetaData.PerFrameTimestamps;
            else
                value = (0:obj.MetaData.SamplesT - 1) ./ obj.MetaData.VolumeRate;
            end
            
            if ~isrow(value)
                value = value';
            end
        end
        
        function success = OnLoadData(obj, filePath, fileName, fileExt)
            success = OnLoadData@com.common.Data.MImageDataset(obj, filePath, fileName, fileExt);            
            
            if success == true
                success = obj.LoadF0();

                try
                    if ~isempty(obj.EEGDataset)
                        obj.EEGDataset.LoadData();
                    end
                catch
                    com.common.Logging.MLogManager.Error('Failed to load EEG data');
                    obj.EEGDataset = [];
                    success = false;
                end
            end   
        end
        
        function success = LoadF0(obj)
            try
                com.common.Logging.MLogManager.Info('Loading F0 ...');
                if ~isempty(obj.F0Path)
                    [~, ~, fExt] = fileparts( obj.F0Path );
                    switch string(lower(fExt))
                        case ".mat"
                            obj.F0 = single(struct2array(load(obj.F0Path, "F0")));
                        case {".tif", ".tiff"}
                            [obj.F0, ~] = single(com.common.FileIO.MSparklesTiffFile.LoadFile(obj.F0Path));
                            obj.F0 = reshape(obj.F0, obj.MetaData.NumSamples);
                    end
                    
                    %obj.F0 = reshape(obj.F0, obj.MetaData.NumSamples);
                    com.common.Logging.MLogManager.Info('Loading F0 ... Done!');
                end
                success = true;
            catch
                com.common.Logging.MLogManager.Error('Failed to load F0');
                %obj.F0Path = [];
                obj.F0 = [];
                success = false;
            end
        end
        
        function hAnalysisManager = CreateAnalysisManager(obj)
            hAnalysisManager = com.CalciumAnalysis.Analysis.AnalysisManager(obj);
        end
    end
    
    methods (Access=private)
        function UpgradeF0Config(obj)
            %Save original cfg
            if numel(obj.F0Config) == 1
                orgCfg = obj.F0Config;

                %Create new ones
                obj.F0Config = com.CalciumAnalysis.Processing.Config.F0EstimatorCfg.empty(...
                    obj.MetaData.NumChannels, 0);

                %Set original cfg as first entry in list
                obj.F0Config(1) = orgCfg;

                %Create cfgs for other channels
                for i=2:obj.MetaData.NumChannels
                    obj.F0Config(i) = com.CalciumAnalysis.Processing.Config.F0EstimatorCfg(i);
                end
            else
                 for i=1:obj.MetaData.NumChannels
                    if isempty(obj.F0Config(i))
                        obj.F0Config(i) = com.CalciumAnalysis.Processing.Config.F0EstimatorCfg(i);
                    else
                        obj.F0Config(i).ChannelID = i;
                    end
                end
            end
        end
        
        function clearPropListener(obj)
            if ~isempty(obj.propListener)
                for i=1:numel(obj.propListener)
                    delete (obj.propListener{i});
                end
            end 
        end
        
        function UpdatePropListener(obj)
            obj.clearPropListener();
            obj.propListener{1} = addlistener(obj, 'NormalizationMode', 'PostSet', @obj.handlePropEvents);                                         
        end
               
        function handlePropEvents(obj, src, ~)
            switch src.Name 
                case 'NormalizationMode'
                    obj.UpdateNormalizedMinMaxValues();
            end
        end
    end
end