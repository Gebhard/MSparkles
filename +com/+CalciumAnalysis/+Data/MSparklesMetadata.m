%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSparklesMetadata < com.common.Data.MMetadataRoot
    %MSPARKLESMETADATA manages metadata, independent of file formats.
    %   It serves the purpose to provide and abstract (w.r.t different file
    %   formats) and generalized mechanism to access the most important
    %   metadata.
    
    properties(SetAccess={?com.CalciumAnalysis.Data.MSparklesMetadata, ?com.common.Cloneable})
        NumSamples = [];            % Holds the number of samples for all dimensions in the following order: [X Y Z C T]
        PerFrameTimestamps = [];    % Per-frame timing information (exact). numel(PerFrameTimestamps) == NumSamples.T
        IsLegacyTiff = false;
    end
    
    properties
        DomainUnit = [];            % Char array with the name of the measurement unit.
        FrameRate = [];             % The averaged/idealized frame rate.
        VolumeRate = [];            % The averaged/idealized volume rate. If NumSamples.Z==1, FrameRate equals VolumeRate.
        FramesPerVolume = [];       % Number of frames/slices per volume
        BitDepth = [];              % Per-channel bit-depth [C1 C2 ... Cn]. numel(BitDepth) == NumSamples.C
        ChannelNames = [];          % Cell-array with the names for each channel {'Ch1' 'Ch2' ... 'Chn'} 
        
        SampleSizeX = 1;           % Physical sample size along x-axis.
        SampleSizeY = 1;           % Physical sample size along y-axis.
        SampleSizeZ = 1;           % Physical sample size along z-axis.
        
        ScalebarSize;
        TimeUnit;
        TimeFormat;
        UseExactTimingInformation;
        
        %Extented metadata
        ExtendedMetaData;
    end
    
    properties(Transient, Dependent)
        Domain;         % Physical extent to the domain (typically microns)
        SamplesX;       % Number of samples along image x-axis.
        SamplesY;       % Number of samples along image y-axis.
        SamplesZ;       % Number of samples along image z-axis.
        SamplesT;       % Number of samples along time axis (how many 2D frames -or- 3D volumes).
        SamplesS;       % Number of slices per frame.
        NumChannels;    % Number of recorded color channels.
        
        SampleVolume;   % The area (in 2D) or the volume (in 3D) of the field of view.
        
        ExtentX;        % Physical extent of the field of view in x-direction (SamplesX * SampleSizeX).
        ExtentY;        % Physical extent of the field of view in y-direction (SamplesY * SampleSizeY).
        ExtentZ;        % Physical extent of the field of view in z-direction (SamplesZ * SampleSizeZ).
        TotalFrameCount;% Total number of frames. (SamplesZ * SamplesT * SamplesS * NumChannels).
        
        DefaultScalebarSize;
        DefaultTimeUnit;
        DefaultTimeFormat;
        
        NumSpatialDims;
    end
    
    methods
        function obj = MSparklesMetadata(hTiff)
            if nargin == 1 && ~isempty(hTiff.hFile) && hTiff.IsOpen()  
                try
                    if ~obj.ReadSIMetadata(hTiff)
                        obj.ReadLegacyMetadata(hTiff);
                    end
                       
                    %Default scalebar size 
                    obj.ScalebarSize = obj.DefaultScalebarSize;
                    obj.TimeUnit = obj.DefaultTimeUnit;
                    obj.TimeFormat = obj.DefaultTimeFormat;
                    obj.UseExactTimingInformation = true;
                catch
                    %ToDO, proper error handling
                end
            end
        end
    end
        
    methods
        function value = get.NumSpatialDims(obj)
            if obj.SamplesZ > 1
                value = 3;
            else
                value = 2;
            end
        end
        
        function value = get.SampleSizeX(obj)
            if isempty(obj.SampleSizeX)
                obj.SampleSizeX = 1;
            end
            
            value = obj.SampleSizeX;
        end
        
        function value = get.SampleSizeY(obj)
            if isempty(obj.SampleSizeY)
                obj.SampleSizeY = 1;
            end
            
            value = obj.SampleSizeY;
        end
        
        function value = get.SampleSizeZ(obj)
            if isempty(obj.SampleSizeZ)
                obj.SampleSizeZ = 1;
            end
            
            value = obj.SampleSizeZ;
        end
        
        function value = get.ExtendedMetaData(obj)
            if isempty(obj.ExtendedMetaData)
                obj.ExtendedMetaData = com.CalciumAnalysis.Data.M2PSiMetaData();
            end
            
            value = obj.ExtendedMetaData;
        end
        
        function value = get.SamplesX(obj)
            try
                value = obj.NumSamples(2);
            catch
                value = 1;
            end
        end
        
        function set.SamplesX(obj, value)
            obj.NumSamples(2) = value;
        end
        
        function value = get.SamplesY(obj)
            try
                value = obj.NumSamples(1);
            catch
                value = 1;
            end
        end
        
        function set.SamplesY(obj, value)
            obj.NumSamples(1) = value;
        end
        
        function value = get.SamplesZ(obj)
            try
                value = obj.NumSamples(4);
            catch
                value = 1;
            end
        end
        
        function set.SamplesZ(obj, value)
            obj.NumSamples(4) = value;
        end
        
        function value = get.SamplesT(obj)
            try
                value = obj.NumSamples(6);
            catch
                value = 1;
            end
        end
        
        function set.SamplesT(obj, value)
            obj.NumSamples(6) = value;
        end
        
        function value = get.SamplesS(obj)
            try
                value = obj.NumSamples(5);
            catch
                value = 1;
            end
        end
        
        function set.SamplesS(obj, value)
            obj.NumSamples(5) = value;
        end
        
        function set.Domain(obj, value)
            if numel(value) >= 2
                obj.ExtentX = value(1);
                obj.ExtentY = value(2);
                
                if numel(value) == 3
                    obj.ExtentZ = value(3);
                end
            end
        end

        function value = get.Domain(obj)
            value = [obj.ExtentX, obj.ExtentY, obj.ExtentZ];
        end
        
        function value = get.SampleVolume(obj)
            value = obj.SampleSizeX * obj.SampleSizeY; %area
            if obj.SamplesZ > 0 && ~isempty(obj.ExtentZ) && obj.ExtentZ > 0
                value = value * obj.SampleSizeZ; %Volume
            end
        end
        
        function value = get.NumChannels(obj)
            try
                value = obj.NumSamples(3);
            catch
                value = 1;
            end
        end
        
        function set.NumChannels(obj, value)
            obj.NumSamples(3) = value;
        end        
        
        function value = get.ExtentX(obj)
            try
                value = obj.SampleSizeX * obj.SamplesX;
            catch
                value = 1;
            end
        end
        
        function value = get.ExtentY(obj)
            try
                value = obj.SampleSizeY * obj.SamplesY;
            catch
                value = 1;
            end
        end
        
        function value = get.ExtentZ(obj)
            try
                value = obj.SampleSizeZ * obj.SamplesZ;
            catch
                value = 1;
            end
        end
        
        function value = get.TotalFrameCount(obj)
            try
                value = obj.SamplesZ * obj.SamplesT * obj.SamplesS * obj.NumChannels;
            catch
                value = 0;
            end
        end       
        
        function set.ExtentX(obj, value)
            obj.SampleSizeX = value / obj.SamplesX;
        end
        
        function set.ExtentY(obj, value)
            obj.SampleSizeY = value / obj.SamplesY;
        end
        
        function set.ExtentZ(obj, value)
            obj.SampleSizeZ = value / obj.SamplesZ;
        end     
        
        function value = get.DefaultScalebarSize(obj)
            % 10% of the extent, rounded to the next multiple of 100
            value = round(obj.ExtentX, -2) / 10;
        end
        
        function value = get.DefaultTimeUnit(~)
            value = 'min';
        end
        
        function value = get.DefaultTimeFormat(~)
            value = 'MM:SS.FFF';
        end
    end
    
    methods (Static, Access = private)
        function value = ParseValue(inStr, pattern)
            if iscell(inStr)
                value  = zeros(numel(inStr), 1);
                
                for i=1:numel(inStr) 
                    lines = splitlines(inStr{i});
                    idx = contains(lines, pattern);
                    res = split(lines(idx), '=');
                    
                    if numel(res) == 2
                       value(i) = str2double(res{2});
                    else
                        value =[];
                    end
                end            
            else
                value = [];
            end                        
        end
    end
    
    methods
        function ReadExtendedMetadata(obj, hTiff)
            if nargin == 1 && ~isempty(hTiff.hFile) && hTiff.hFile.isOpen()  
                try
                    [header, ~] = ScanImageTiffReader.getHeaderData(hTiff.hFile); 
                    obj.ExtendedMetaData = com.CalciumAnalysis.Data.M2PSiMetaData(header);
                    
%                     if ~isempty(fieldnames(header))
%                         verInfo = ScanImageTiffReader.getSITiffVersionInfo(header);
%                         %obj.ExtendedMetaDataScanImageVersion
%                     end                                        
                catch                    
                end
            end
        end
        
        function channelName = GetChannelName(obj, idx)
            try
                if iscell(obj.ChannelNames)
                    channelName = obj.ChannelNames{idx};
                else
                    if isempty(obj.ChannelNames)
                        channelName = "Channel " + idx;
                    else
                        channelName = obj.ChannelNames(1:end);
                    end
                end
            catch
                if idx <= obj.NumChannels
                    channelName = "Channel " + idx;
                else
                    channelName = "";
                end
            end
        end
        
        function channelNames = GetAllChannelNames(obj)
            channelNames = strings(obj.NumChannels, 1);
            
            for i=1:obj.NumChannels
                channelNames(i) = obj.GetChannelName(i);
            end
        end
    end
    
    methods(Access=private)
        function [C, Z, T] = ReadImageJMetaData(~, ImageDesc)
            %Read basic meta data from ImageJ file.
            C = 1; Z = 1; T = 1; %Init default values
            
            isHyperstack = any(cell2mat(cellfun(@(x) strcmp('hyperstack', x), [ImageDesc{:}], 'Uniformoutput', false)));
            
            for i=1:numel(ImageDesc)
                s = ImageDesc{i};
                switch ( lower(s{1}) )
                    case 'channels'
                        C = str2double( s{2} );
                    case 'slices'
                        if isHyperstack
                            Z = str2double( s{2} );
                        else
                            T = str2double( s{2} );
                        end
                    case 'frames'
                        T = str2double( s{2} );
                end
            end        
        end
        
        function success = ReadSIMetadata(obj, hTiff)
            try
                warning("off","imageio:tiffmexutils:libtiffWarning");
                [header, frameDescs] = ScanImageTiffReader.getHeaderData(hTiff.hFile); 
                warning("on","imageio:tiffmexutils:libtiffWarning");
                
                if ~isempty(fieldnames(header))
                    try
                        verInfo = ScanImageTiffReader.getSITiffVersionInfo(header);
                    catch
                    end

                    obj.IsLegacyTiff = false;
                    hdr = ScanImageTiffReader.extractHeaderData(header, verInfo);
                    obj.ExtendedMetaData = com.CalciumAnalysis.Data.M2PSiMetaData(header);                   

                    %Get image dimensions and number of channels
                    X = hdr.numPixels;
                    Y = hdr.numLines;
                    C = numel(hdr.savedChans);                        
                    S = 1;  %This seems only to be supported since SI 2019b... WE ignore it for now
                    
                    %Ensure we have a valid frame averaging factor
                    logAvgFactor = header.SI.hScan2D.logAverageFactor;
                    
                    if isempty(logAvgFactor) || isnan(logAvgFactor) || logAvgFactor == 0
                        logAvgFactor = 1;
                    end

                    if hdr.numVolumes == 1 && hdr.numSlices == 1
                        %In this case, the stored numbers in the stack
                        %manager are not reliable and it is safer to count
                        %the occurances of 'frameNumberAcquisition' and
                        %divide by the frame averaging factor.
                        Z = 1;       
                        T = max(com.CalciumAnalysis.Data.MSparklesMetadata.ParseValue(frameDescs, 'frameNumberAcquisition')) / logAvgFactor;                           
                    else
                        Z = header.SI.hStackManager.numSlices;
                        T = header.SI.hFastZ.numVolumes;
                    end
                    
                    if isinf(Z)
                        Z = 1;
                    end

                    %Store the number of Samples for all six dimensions.
                    obj.NumSamples = [Y,X,C,Z,S,T];

                    %Compute extents
                    obj.ExtentX = max(header.SI.hRoiManager.imagingFovUm(:,1)) - min(header.SI.hRoiManager.imagingFovUm(:,1));
                    obj.ExtentY = max(header.SI.hRoiManager.imagingFovUm(:,2)) - min(header.SI.hRoiManager.imagingFovUm(:,2));
                    if header.SI.hStackManager.numSlices > 1
                        obj.ExtentZ = header.SI.hStackManager.stackZEndPos - header.SI.hStackManager.stackZStartPos;
                    else
                        obj.ExtentZ = 1;
                    end
                    
                    obj.DomainUnit = 'micron';
                    obj.FrameRate = header.SI.hRoiManager.scanFrameRate;
                    obj.VolumeRate = header.SI.hRoiManager.scanVolumeRate / logAvgFactor;
                    obj.FramesPerVolume = header.SI.hFastZ.numFramesPerVolume;
                    obj.BitDepth = header.SI.hChannels.channelAdcResolution{:};
                    obj.ChannelNames = header.SI.hChannels.channelName;
                    obj.PerFrameTimestamps = unique(com.CalciumAnalysis.Data.MSparklesMetadata.ParseValue(frameDescs, 'frameTimestamps_sec'));

                    success = true;
                else
                    success = false;
                end
            catch
                success = false;
            end
        end
        
        function ReadLegacyMetadata(obj, hTiff)
            %Fallback. No ScanImage metadata found. Assuming defaulat values, based on File props.                          
            obj.IsLegacyTiff = true;
            warning('off', 'all');
            metaData = imfinfo(hTiff.FilePath); 
            warning('on', 'all');

            %init dummy values
            S = 1;

            try
                %Try to read real values from metadata, if they
                %exist
                desc = strsplit(metaData(1).ImageDescription);
                res = cellfun(@(x) strsplit(x, '='), desc, 'UniformOutput', false);

                switch ( lower(res{1}{1}) )
                    case 'imagej'
                        [C, Z, T] = obj.ReadImageJMetaData(res); 
                    otherwise
                        T = numel(metaData);
                        C = 1;                        
                        Z = 1;
                end                                                                                                            
            catch
                T = numel(metaData);
                C = 1;                        
                Z = 1;
            end

            % X & Y are swapped
            X = metaData(1).Width;
            Y = metaData(1).Height;                                                                        

            if isempty(metaData(1).XResolution)
                metaData(1).XResolution = 1;
            end

            if isempty(metaData(1).YResolution)
                metaData(1).YResolution = 1;
            end
            
            try
                if isempty(metaData(1).ZResolution)
                    metaData(1).ZResolution = 1;
                end
            catch
                metaData(1).ZResolution = 1;
            end

            obj.SampleSizeX = metaData(1).XResolution;
            obj.SampleSizeY = metaData(1).YResolution;
            obj.SampleSizeZ = metaData(1).ZResolution; 

            if isempty(obj.SampleSizeX)
                obj.SampleSizeX = 1;
            end
            
            if isempty(obj.SampleSizeY)
                obj.SampleSizeY = 1;
            end
            
            if isempty(obj.SampleSizeZ)
                obj.SampleSizeZ = 1;
            end
            
            obj.NumSamples = [Y,X,C,Z,S,T];
            obj.DomainUnit = metaData(1).ResolutionUnit;
            if isempty(obj.DomainUnit)
                obj.DomainUnit = 'micron';
            end

            obj.FrameRate = 1;
            obj.VolumeRate =1;
            obj.FramesPerVolume = 1;
            obj.BitDepth = metaData(1).BitDepth;

            obj.ChannelNames = cell(C,1);

            for c=1:C
                obj.ChannelNames{c} = sprintf('Channel %d', c);
            end

            obj.PerFrameTimestamps = [];                   
        end
    end
end

