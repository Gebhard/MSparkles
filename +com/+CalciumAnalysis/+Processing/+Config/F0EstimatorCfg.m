%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef F0EstimatorCfg < com.common.Proc.PStageConfig 
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        DEF_CLEANUPMETHOD       = com.CalciumAnalysis.Enum.F0CleanupMethod.MeanSD;
        DEF_CLEANUPITERATIONS   = 3;
        DEF_CLEANUPSIGMAFACTOR  = 2;
        DEF_GUIDANCETYPE        = com.CalciumAnalysis.Enum.F0GuidanceSignal.IterativeMean;
        DEF_GUIDANCEOPTIMIZER   = com.CalciumAnalysis.Enum.F0Optimizer.Error;
        DEF_GUIDANCEORDER       = 6;
        DEF_F0FITORDER          = 5;
        DEF_CLAMPLBOUND         = -inf;
        DEF_CLAMPUBOUND         = inf;
        DEF_USECLAMPLBOUND      = false;
        DEF_USECLAMPUBOUND      = false;
            
        DEF_USEGAUSSFILTER      = true;
        DEF_GAUSSKERNELHALFSIZE = 5;
        DEF_GAUSSSIGMA          = 2;
        
        DEF_USEF0MASK            = false;
        DEF_F0MASKTHRESHOLD      = [];
        DEF_DYNRNGSIGMA          = 3;
        DEF_DYNRNGKRNLSIZE       = 7;
    end
    
    properties
        UseGaussFilter;
        GaussKernelHalfSize;
        GaussSigma;
        
        CleanupMethod;
        CleanupIterations;
        CleanupSigmaFactor;
        GuidanceType;
        GuidanceOptimizer;
        GuidanceOrder;
        F0FitOrder;
        ClampLBound;
        ClampUBound;
        UseClampLBound;
        UseClampUBound;
        
        UseF0Mask;
        F0MaskThreshold;
        DynRngSigma;        %Sigma parameter to smooth (dynamic) range image.
        DynRngKrnlSize;     %Kernel size for symmetric median filter if (dynamic) range image
    end
    
    properties
        ChannelID; 
    end
    
    methods
        function obj = F0EstimatorCfg(chanID)
            obj = obj@com.common.Proc.PStageConfig("F0 estimation");
        
            if nargin < 1 || isempty(chanID)
                chanID = 1;
            end
            
            obj.ChannelID           = chanID;
            obj.CleanupMethod       = obj.DEF_CLEANUPMETHOD;
            obj.CleanupIterations   = obj.DEF_CLEANUPITERATIONS;
            obj.CleanupSigmaFactor  = obj.DEF_CLEANUPSIGMAFACTOR;
            obj.GuidanceType        = obj.DEF_GUIDANCETYPE;
            obj.GuidanceOptimizer   = obj.DEF_GUIDANCEOPTIMIZER;
            obj.GuidanceOrder       = obj.DEF_GUIDANCEORDER;
            obj.F0FitOrder          = obj.DEF_F0FITORDER;
            obj.ClampLBound         = obj.DEF_CLAMPLBOUND;
            obj.ClampUBound         = obj.DEF_CLAMPUBOUND;
            obj.UseClampLBound      = obj.DEF_USECLAMPLBOUND;
            obj.UseClampUBound      = obj.DEF_USECLAMPUBOUND;
            
            obj.UseGaussFilter      = obj.DEF_USEGAUSSFILTER;
            obj.GaussKernelHalfSize = obj.DEF_GAUSSKERNELHALFSIZE;
            obj.GaussSigma          = obj.DEF_GAUSSSIGMA;
            
            obj.UseF0Mask           = obj.DEF_USEF0MASK;
            obj.F0MaskThreshold     = obj.DEF_F0MASKTHRESHOLD;
            obj.DynRngSigma         = obj.DEF_DYNRNGSIGMA;
            obj.DynRngKrnlSize = obj.DEF_DYNRNGKRNLSIZE;
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(obj, varargin)
            hStage = com.CalciumAnalysis.Processing.Stages.PStageCalculateF0();
            hStage.StageConfig = obj;
        end
    end
    
    methods
        function value = get.DynRngKrnlSize(obj)
            if isempty(obj.DynRngKrnlSize)
                obj.DynRngKrnlSize = obj.DEF_DYNRNGKRNLSIZE;
            end
            
            value = obj.DynRngKrnlSize;
        end
        
        function value = get.DynRngSigma(obj)
            if isempty(obj.DynRngSigma)
                obj.DynRngSigma = obj.DEF_DYNRNGSIGMA;
            end
            
            value = obj.DynRngSigma;
        end
        
        function value = get.UseF0Mask(obj)
            if isempty(obj.UseF0Mask)
                value = obj.DEF_USEF0MASK;
            else
                value = obj.UseF0Mask;
            end
        end
        
        function value = get.F0MaskThreshold(obj)
            if isempty(obj.F0MaskThreshold)
                value = obj.DEF_F0MASKTHRESHOLD;
            else
                value = obj.F0MaskThreshold;
            end
        end
        
        function value = get.UseGaussFilter(obj)
            if isempty(obj.UseGaussFilter)
                value = obj.DEF_USEGAUSSFILTER;
            else
                value = obj.UseGaussFilter;
            end
        end
        
        function value = get.GaussKernelHalfSize(obj)
            if isempty(obj.GaussKernelHalfSize)
                value = obj.DEF_GAUSSKERNELHALFSIZE;
            else
                value = obj.GaussKernelHalfSize;
            end
        end
        
        function value = get.GaussSigma(obj)
            if isempty(obj.GaussSigma)
                value = obj.DEF_GAUSSSIGMA;
            else
                value = obj.GaussSigma;
            end
        end
        
    end
end

