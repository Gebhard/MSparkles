%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef DenoiseStack < com.common.Proc.PipelineStage
    %PSTAGEDENOISESTACK Summary of this class goes here
    %   Detailed explanation goes here

    methods
        function obj = DenoiseStack()
            obj = obj@com.common.Proc.PipelineStage('Denoising', "1.2.0", "24.07.2017");                
        end  
    end
    
    methods
        function data = ApplyFilter(~, data)
            data = single(OWT_SURELET_denoise(data));
        end
    end
    
    methods (Access=protected)        
        function success = OnExecuteStage(obj, hDataset)             
            %sigma = 2;             % Noise standard deviation
            %wtype = 'sym8';        % Orthonormal Wavelet filter
                        
            %hDataset.F = single(hDataset.F);   %ensure its float format                      
            numT = hDataset.MetaData.SamplesT;
            numZ = hDataset.MetaData.SamplesZ;
            numC = hDataset.MetaData.NumChannels;
            
            totalIter = numT * numZ * numC;
            
            p=1;
            d = parallel.pool.DataQueue;
            afterEach(d, @updateProgress);
            S = 1;            
            
            try
%                 F = hDataset.F;
                for c=1:numC %for all channels
                    for z=1:numZ % for all z-layers
                        dummy = hDataset.F(:,:,c,z,S,:);
                        parfor j=1:numT %for all time-steps 
                            dummy(:,:,1,1,S,j) = OWT_SURELET_denoise(dummy(:,:,1,1,S,j));           
                            send(d,j);
                        end
                        
                        hDataset.F(:,:,c,z,S,:) = single(dummy);
                    end
                end 
                
%                 hDataset.F = single(F);
            catch ex
                rethrow (ex);
            end                        
            
            success = true;
            
            function updateProgress(~)
                obj.UpdateStatus(p/totalIter);
                p = p+1;
            end
        end
    end
end

