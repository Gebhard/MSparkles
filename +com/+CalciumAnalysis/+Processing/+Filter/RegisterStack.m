%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef RegisterStack < com.common.Proc.PipelineStage
    %PSTAGEREGISTERSTACK Summary of this class goes here
    %   Detailed explanation goes here 
    
     methods
        function obj = RegisterStack()
            obj = obj@com.common.Proc.PipelineStage('Image registration', '1.1.0', '24.07.2019'); 
        end                
     end
    
     methods (Access=protected)
        function OnInitializeDefaultParameterSet(~)
        end
        
        function success = OnExecuteStage(obj, hDataset, ~)                                                           
            refFrame    = obj.getReferenceFrame();
            refChan     = obj.getReferenceChannel();
            totalSteps  = hDataset.MetaData.SamplesZ * hDataset.MetaData.SamplesT * 2;
            hResult     = zeros(size(hDataset.F), 'single');
            numFrames   = hDataset.MetaData.SamplesT;
            numC        = hDataset.MetaData.NumChannels;
            inStack     = hDataset.F;
            S = 1;
            p = 1;
            d = parallel.pool.DataQueue;
            afterEach(d, @updateProgress);

            warning ('off','all');
            sampZ   = hDataset.MetaData.SamplesZ;
            sampX   = hDataset.MetaData.SamplesX;
            sampY   = hDataset.MetaData.SamplesY;
            xyzLims = [];%zeros(sampZ*numFrames, 4);

            %Get the reference image/volume
            template_image = com.lib.ImageProc.Registration.NormalizeImg( inStack(:,:,refChan,:,S,refFrame) );

            imgTforms = cell(numFrames, sampZ);

            %register reference frame
            hResult(:,:,:,1,S,refFrame) = inStack(:,:,:,1,S,refFrame);
            for z=2:sampZ 
                imgRef  = hResult(:,:,refChan,z - 1,S,refFrame);
                img     = inStack(:,:,refChan,z,S,refFrame);
                imgTforms{refFrame, z} = com.lib.ImageProc.Registration.RegisterImages(imgRef, img);
            end

            for z=1:sampZ                                                 
                parfor t=1:numFrames 
                    if t ~= refFrame
                        img = inStack(:,:,refChan,z,S,t); 
                        MOVINGREG = com.lib.ImageProc.Registration.RegisterImages(...
                            template_image(:,:,refChan,z), img);
                        hResult(:,:,refChan,z,S,t) = single(MOVINGREG.RegisteredImage);
                        MOVINGREG.RegisteredImage = []; %release some memory
                        imgTforms{t, z} = MOVINGREG;
                    end
                    send(d,t);                                               
                end
            end                                
            
            for t=1:numFrames
                for z=1:sampZ
                    MOVINGREG = imgTforms{t, z};
                    if ~isempty(MOVINGREG)
                        [xlim, ylim] = outputLimits(MOVINGREG.Transformation,[1 sampX],[1 sampY]);
                        xyzLims = cat(1,xyzLims, [xlim, ylim] );

                        for c=1:numC
                            currImage = inStack(:,:,c,z,S,t);
                            hResult(:,:,c,z,S,t) = ...
                                single( imwarp(currImage, MOVINGREG.MovingRefObj, ...
                                    MOVINGREG.Transformation, 'OutputView',...
                                    MOVINGREG.SpatialRefObj, 'SmoothEdges', true));
                        end
                    end
                    send(d,t);
                end
            end

            x1 = xyzLims(:,1);
            x2 = xyzLims(:,2);
            y1 = xyzLims(:,3);
            y2 = xyzLims(:,4);

            X1 = ceil(max(x1)); %lower X
            X2 = floor(min(x2(x2 > 0))); % upper X
            Y1 = ceil(max(y1)); %lower Y
            Y2 = floor(min(y2(y2 > 0))); % upper Y

            hDataset.F = hResult;
            try
                hDataset.Crop( [X1, Y1, X2, Y2]);
            catch
            end               
            
            success = true;
            warning ('on','all');           
            
            function updateProgress(~)
                obj.UpdateStatus(p/totalSteps);
                p = p+1;
            end 
        end      
     end  
     
    methods(Access = private)
        function refFrame = getReferenceFrame(obj)
            try
                refFrame = obj.StageConfig.ReferenceFrame;
            catch
                refFrame = 1;
            end
        end
        
        function refChan = getReferenceChannel(obj)
            try
                refChan = obj.StageConfig.ReferenceChannel;
            catch
                refChan = 1;
            end
        end
    end
end

