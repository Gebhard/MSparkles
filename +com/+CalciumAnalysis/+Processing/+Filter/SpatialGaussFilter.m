%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef SpatialGaussFilter < com.CalciumAnalysis.Processing.Filter.FilterStage
    %PTEMPORALMEDIANFILTER Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = SpatialGaussFilter()
            %PTEMPORALMEDIANFILTER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.CalciumAnalysis.Processing.Filter.FilterStage('Spatial Gauss filter', "1.0.0", "24-07-2019");
        end
    end
    
    methods
        function data = ApplyFilter(obj, data, ~)
            %1) Dataset is 6-D, then we assume [X,Y,C,Z,S,T]
            %2) Dataset is 3-D, then we assume [X,Y,Z]
            %3) Dataset is 2-D, then we assume [X,Y]
            %4) Else: unknown format, return original data
            
            sz = size(data);
            switch numel(sz)
                case 6
                    %Iterate over all channels (C) and time-steps (T)                    
                    for c=1:sz(3)
                        for t=1:sz(6)
                            if (sz(4) > 1) %really 3-D?
                                dummy = reshape( data(:,:,c,:,1,t), sz(1), sz(2), sz(4) );
                            else %2-D
                                dummy = reshape( data(:,:,c,:,1,t), sz(1), sz(2));
                            end
                            
                            data(:,:,c,:,:,t) = obj.ApplyFilter(dummy);
                        end
                    end
                case 3
                    data = imgaussfilt3(data, obj.StageConfig.Sigma, "FilterSize", obj.FilterSize, "Padding", "symmetric");
                case 2
                    data = imgaussfilt(data, obj.StageConfig.Sigma, "FilterSize", obj.FilterSize, "Padding", "symmetric");                    
                otherwise
                    data = data;
            end           
        end
    end
end