%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef TemporalGaussFilter < com.CalciumAnalysis.Processing.Filter.FilterStage
    %PTEMPORALMEDIANFILTER Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = TemporalGaussFilter()
            %PTEMPORALMEDIANFILTER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.CalciumAnalysis.Processing.Filter.FilterStage('Temporal Gauss filter', "1.0.0", "24-07-2019");
        end
    end
    
    methods
        function data = ApplyFilter(obj, data, ~)
            %Build Gauss kernel
            fltrKrnl = GetGaussKernel(obj.StageConfig.Sigma, obj.FilterHalfSize); 
            
            %Reshape kernel
            %Assumption: Last dimension is time
            szFltr = ones(1, numel( size(data) ));
            szFltr(end) = numel(fltrKrnl);
            fltrKrnl = reshape(fltrKrnl(:), szFltr);
            
            %Apply filter
            data = imfilter(data, fltrKrnl, "conv", "replicate");                 
        end
    end
end



