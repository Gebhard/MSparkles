%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef TemporalKuwaharaFilter < com.CalciumAnalysis.Processing.Filter.FilterStage
    %PTEMPORALMEDIANFILTER Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = TemporalKuwaharaFilter()
            %PTEMPORALMEDIANFILTER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.CalciumAnalysis.Processing.Filter.FilterStage('Temporal Kuwahara filter', "1.0.0", "25-07-2019");
        end
    end
    
    methods
        function data = ApplyFilter(obj, data)
            %Assumption: Time is always in the last dimension
            data = obj.Kuwahara1D(data, obj.StageConfig.FilterWidth);
        end
    end
    
    methods(Access=private)
        function S = Kuwahara1D(obj, src, khSize)
        %CALCENTROPY Summary of this function goes here
        %   Detailed explanation goes here    

            sz = size(src);
            S = zeros(sz, 'single');   

            obj.CreateStatusBar();

            k = zeros(sz(1), sz(2), sz(3), sz(4), sz(5), 2, 'single');
            s = zeros(sz(1), sz(2), sz(3), sz(4), sz(5), 2, 'single');

            sk = size(k);
            sk = sk(1) * sk(2) * sk(3) * sk(4) * sk(5) * 1;

            mIdx = (0:sz(4)-1) * (sz(1) * sz(2)) + (0:sz(2)-1)*sz(1) + (1:sz(1))';

            try  
                tMax = sz(6);
                for t=1:tMax

                    %Add values of highest index to hiso
                    tMinIdx = max(t-khSize, 1); 
                    tMaxIdx = min(t+khSize, tMax);

                    k(:,:,:,:,:,1) = mean(src(:,:,:,:,:,tMinIdx:t), 6);
                    k(:,:,:,:,:,2) = mean(src(:,:,:,:,:,t:tMaxIdx), 6);

                    s(:,:,:,:,:,1) = std(src(:,:,:,:,:,tMinIdx:t), 0, 6);
                    s(:,:,:,:,:,2) = std(src(:,:,:,:,:,t:tMaxIdx), 0, 6);

                    [~, idx] = min(s, [], 6);
                    S(:,:,:,:,:,t) = k( (idx-1)*sk  + mIdx);                        

                    obj.UpdateStatus(t/sz(6));
                end
                obj.CloseStatusBar();
                
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
end

