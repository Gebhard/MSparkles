%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef TemporalSavitzkyGolayFilter < com.CalciumAnalysis.Processing.Filter.FilterStage
    %PTEMPORALMEDIANFILTER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Transient, Dependent)
        PolyOrder;
    end
    
    methods
        function obj = TemporalSavitzkyGolayFilter()
            %PTEMPORALMEDIANFILTER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.CalciumAnalysis.Processing.Filter.FilterStage('Temporal Savitzky-Golay filter', "1.0.0", "06-02-2020");
        end
    end
    
    methods
        function value = get.PolyOrder(obj)
            value = obj.StageConfig.Order;
        end
    end
    
    methods
        function data = ApplyFilter(obj, data, ~)
            %Assumption: Time is always in the last dimension
            
            sz = size(data);  
            weights = ones(obj.FilterSize,1);

            data = single(sgolayfilt(double(data), obj.PolyOrder, obj.FilterSize, weights, numel(sz)));
            data = reshape(data,size(data));
        end
    end
end



