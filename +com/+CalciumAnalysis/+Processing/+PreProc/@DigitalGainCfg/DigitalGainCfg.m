%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef DigitalGainCfg < com.common.Proc.PreProcConfig
    %DIGITALGAIN Simple offset as preprocesing stage
    
    properties
        Gain;
    end
    
    methods
        function obj = DigitalGainCfg()
            obj = obj@com.common.Proc.PreProcConfig("Digital gain (offset)");
            obj.Gain = 50;
            obj.IsSpatialFilter = false;
        end
    end
    
    methods
        function Edit(obj)
            answer = inputdlg("Digital gain (offset)", "Edit " + obj.Name, [1, 35], string(obj.Gain) );
            
            try
                if ~isempty(answer)
                    answer = str2double(answer);
                    
                    if all(~isnan(answer))
                        obj.Gain = answer;
                    else
                        errordlg("Input must be numeric");
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
            hStage = com.CalciumAnalysis.Processing.Filter.DigitalGainFilter();
        end
    end
end

