%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageCalculateF0 < com.common.Proc.PipelineStage
    %PSTAGECALCULATEF0 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=protected)
        p;
        totalSteps
    end
    
    methods
        function obj = PStageCalculateF0()
            obj = obj@com.common.Proc.PipelineStage('F0 calculation', '1.2.0', '17.06.2019');   
        end                
    end
    
    methods (Access=protected)       
        function success = OnExecuteStage(obj, dataSet) 
            %If F0 is empty (i.e. it does not exist) it is set equal to F.
            %If analyses are configured by the time of computing F0, F0 is only computet for the
            %channels required by the analyses. This way, we ensure to always have a valid F0, and
            %at the same time have no artifacts, since (F-F0)/F0 = 0 for all channels not needed by
            %any analysis.
            
            try                                  
                warn = warning ('off','all');
                
                %determine all required channels for configured analyses
                chans = com.common.Proc.PipelineStage.GetRequiredChannels(dataSet);
                if isempty(chans)
                    cMax = dataSet.MetaData.NumChannels;
                    chans = 1:cMax;
                end
                
                obj.p=1;
                obj.totalSteps = 4;
                
                S = 1;       
                if isempty(dataSet.F0)
                    dataSet.F0 = single(dataSet.F);
                end                
                
                %Only run, if channel is selected for further computations
                if ismember(obj.StageConfig.ChannelID, chans)
                    dataSet.F0(:,:,obj.StageConfig.ChannelID,:,S,:) = obj.ApplyFilter( dataSet.F(:,:,obj.StageConfig.ChannelID,:,S,:));

                    if obj.StageConfig.UseClampLBound
                        dataSet.F0( dataSet.F0 < obj.StageConfig.ClampLBound ) = obj.StageConfig.ClampLBound;
                    end

                    if obj.StageConfig.UseClampUBound
                        dataSet.F0( dataSet.F0 < obj.StageConfig.ClampUBound ) = obj.StageConfig.ClampUBound;
                    end
                end                
            catch ex
                dataSet.F0 = [];
                rethrow (ex);
            end  
                    
            success = true;
            warning (warn);
        end                
    end
    
    methods 
        function data = ApplyFilter(obj, data)            
            hCfg = obj.StageConfig;
            sz = size(data); % Get original size           
            
            if hCfg.UseF0Mask
                DynRangeImg = com.lib.ImageProc.ComputeDynRangeImage(data, [sz(1), sz(2), sz(4)],...
                    obj.StageConfig.DynRngSigma, obj.StageConfig.DynRngKrnlSize);
                th = hCfg.F0MaskThreshold;
                
                if isempty(th)
                    th = 0;
                end
                
                mask = DynRangeImg < th;
            else
                mask = zeros(1, sz(1), sz(2), sz(4));
            end

            %make row vector. invert to get the 'good' pixels
            mask = ~mask(:)';
            
            %Convert to column matrix
            data = reshape( permute(data, [6,1,2,3,4,5]) , sz(6) , sz(1) * sz(2) * sz(4) * sz(5));
            actualData = data(:,mask);  %extract only the columsn we actually need
            
            
            %Gauss filter the input stack
            if obj.StageConfig.UseGaussFilter
                fltrKrnl = GetGaussKernel(hCfg.GaussSigma, hCfg.GaussKernelHalfSize);
                fltrKrnl = fltrKrnl(:);
                actualData = imfilter(actualData, fltrKrnl, "conv", "replicate");                   
            end
            
            obj.updateProgress();
            
            %Perform cleanup and outlier removal
            if obj.StageConfig.CleanupIterations > 0
                switch hCfg.CleanupMethod
                    case com.CalciumAnalysis.Enum.F0CleanupMethod.Hampel
                        actualData = hampeln(actualData, hCfg.CleanupIterations, hCfg.CleanupSigmaFactor, 1);
                    otherwise                        
                        m = cleanup();

                        for im=1:size(actualData, 2)
                            actualData( isnan(actualData(:, im)) , im) = m(im);
                        end
                end
            end
            
            function m = cleanup()
                for i=1:hCfg.CleanupIterations 
                    m = mean(actualData, 1, 'omitnan');
                    st = std(actualData, 0, 1, 'omitnan') * hCfg.CleanupSigmaFactor;
                    actualData(actualData > (m+st) ) = nan;
                end
            end
            
            obj.updateProgress();
            
            fitGuidanceSignal(sz(6), obj.StageConfig.GuidanceOrder, obj.StageConfig.GuidanceOptimizer);   
            obj.updateProgress();
            
            fitPoly(obj.StageConfig.F0FitOrder);   
            data(:,mask) = actualData;
            
            data = reshape(permute(data, [2,3,4,5,6,1]), sz);
            
            obj.updateProgress();
            
            function fitGuidanceSignal( numSamples, numScales, optimizer)
                switch optimizer
                    case com.CalciumAnalysis.Enum.F0Optimizer.Maximum
                        startScale = 1;
                        func = @(x,y,~)max(x,y);
                        %sig = -inf(sz);
                    case com.CalciumAnalysis.Enum.F0Optimizer.Error
                        startScale = numScales;
                        func = @(x,y, siz) repmat(y, siz);
                        %sig = zeros(sz);
                    otherwise
                        startScale = 1;
                        func = @(x,y, ~)min(x,y);
                       % sig = inf(sz);
                end

                for is=startScale:numScales
                    sections = 2^(is-1);
                    secLen = numSamples / sections;

                    for s=1:sections
                        startIdx = int32(1 + (s-1)*secLen);
                        endIdx = min(int32(1 + s*secLen), numSamples);

                        %Compute mean values for current section
                        meanVal = mean(actualData(startIdx:endIdx, :), 1, 'omitnan');

                        %Apply optimizer function and update sig
                        actualData(startIdx:endIdx, :) = func(actualData(startIdx:endIdx, :), meanVal, [endIdx-startIdx + 1, 1]);
                    end
                end     
            end
            
            function fitPoly(pDeg)
                sz1 = size(actualData);
                n = sz1(1);
                o = sz1(2);
                x = (1:n)';

                mu = [mean(x); std(x)];
                x = (x - mu(1))/mu(2);

                %build vandermonde matrix
                V(:,pDeg+1) = ones(n,1,class(data));
                for j = pDeg:-1:1
                    V(:,j) = x.*V(:,j+1);
                end

                ls = zeros(pDeg+1, o);

                %tic;
                parfor ii=1:o
                    ls(:,ii) = V \ actualData(:,ii);
                end
                %toc
                
                %Direct solution of generalized least squares
                %However, inv(X) is not recommended, since mldivide uses QR decompsition
                %wichis considered more accurate, numerically stable and faster for large matrices.
%                 Vt = V';
%                 Vx = inv(Vt*V)*Vt;
% 
%                 parfor ii=1:o
%                     ls(:,ii) = Vx * actualData(:,ii);
%                 end

                %Evaluate polys
                %Use standardized y from above. we only want float

                actualData = repmat(ls(1,:), n, 1);
                for ip = 2:pDeg+1
                    actualData = x .* actualData + ls(ip,:);
                end
            end        
        end
    end
    
    methods(Access=protected)
        function updateProgress(obj)
            %Do not fail if something trivial as the status bar has a problem...
            try
                if obj.HasStatusbar
                    obj.UpdateStatus(obj.p/obj.totalSteps);
                    obj.p = obj.p+1;
                end
            catch
            end
        end          
    end        
end
