%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageDetectSignalMorphology < com.common.Proc.AnalysisStage
    %PSTAGEDETECTSTATICROIS Summary of this class goes here
    %   Detailed explanation goes here        
    
    methods
        function obj = PStageDetectSignalMorphology(analysis)
            obj = obj@com.common.Proc.AnalysisStage('Detect signal morphology', '1.1.0', '01.12.2018', analysis);   
        end                
    end
    
    methods
        function movie = ApplyTemporalSmoothing(obj, movie)
            if obj.StageConfig.TrackerTempIter > 0 && obj.StageConfig.TrackerTempSigma > 0
                
                hCfg = com.CalciumAnalysis.Processing.PreProc.TemporalGaussFilterCfg();
                hCfg.Sigma = obj.StageConfig.TrackerTempSigma;
                hCfg.KernelHalfSize = 3*hCfg.Sigma;
                tmpFltr = hCfg.CreatePipelineStage();
                
                for ii=1:obj.StageConfig.TrackerTempIter
                    movie = tmpFltr.ApplyFilter(movie);
                end
            end
        end
            
        function movie = PrepareMovie(obj, movie)
            %Clamp data below threshold, if required
            if obj.StageConfig.ClampdFF0
                movie(movie <0) = 0;
            end

            %Apply n-dimensional, spatio-temporal mean filter (separated, symmetric)
            sz = size(movie);
            for dim = 1:numel(sz)
                if sz(dim) > 1  %Only filter along a dimension if there are actually data     
                    movie = movmean(movie, obj.StageConfig.SpatialSigma, dim);                    
                end
            end
        end        
        
        function CC = ApplyFilter(obj, hDataset, movie)
            multiWaitbar('Initializing dynamic event detection...', 'Color', 'g', 'Busy');
            sz = size(movie); 
            frameOffset = numel(movie(:,:,:,:,:,1));
            
            %Init buffers
            prevL = zeros(sz(1), sz(2));    %Buffer for ROI labels of previous frame
            Iobrcbr = zeros(sz);
            maxima = zeros(sz);                  

            %Create CC-struct to hold resulting ROIs
            CC.Connectivity = 26;
            CC.ImageSize = sz;
            CC.NumObjects = 0;
            CC.PixelIdxList = {};
            
            %Prepare structuring elements
            seOpen          = strel("disk", obj.StageConfig.TrackOpenSize, 8);
            seErode         = strel("disk", obj.StageConfig.TrackErodeSize, 8);
            seDilate        = strel("disk", obj.StageConfig.TrackDilate, 8);
            seDilateMax     = strel("disk", obj.StageConfig.DilateMaxima, 8);        
            se3 = strel("square",5);
            
            %Prepare Gauss-filter to compute scale space
            gaussFilt = com.CalciumAnalysis.Processing.PreProc.SpatialGaussFilterCfg();
            gaussFilt.Sigma = obj.StageConfig.TrackerSpatialSigma;
            gaussFilt.KernelHalfSize = 3 * gaussFilt.Sigma;            
            hStage = gaussFilt.CreatePipelineStage();
            
            %Initialize multi-object Kalman tracker
            tracker = com.lib.ImageProc.KalmanTracker( @obj.DetectObjects, ...
                obj.StageConfig.TrackerPenalty, obj.StageConfig.TrackingGap);
            
            %Preprocess movie
            movie = obj.PrepareMovie(movie); 
            movie = obj.ApplyTemporalSmoothing(movie);    
            movie(isnan(movie)) = 0; %Just to be on the safe side
            
            %Compute required scale
            for i=1:obj.StageConfig.TrackerSpatialIter
                movie = hStage.ApplyFilter(movie);
            end
            
            multiWaitbar('Initializing dynamic event detection...', 'Close');
            
            multiWaitbar('Tracking...', 'Color', 'g');
            for it=1:hDataset.MetaData.SamplesT
                %Get current frame
                m = movie(:,:,:,:,:,it);                                
            
                %Minimal criterion for exclusion
                exclude = m < obj.StageConfig.MinPeakHeight;
                
                %Apply exclude mask
                m(exclude) = 0;

                %extract maxima
                [iMax, Iobrcbr(:,:,:,:,:,it)] = obj.ExtractMaxima(m, exclude, seOpen, seErode, seDilate, seDilateMax);

                %track
                tracker.TrackObjects(iMax, m, exclude, it);
                
                multiWaitbar('Tracking...', 'Value', it/hDataset.MetaData.SamplesT);
            end
            multiWaitbar('Tracking...', 'close');
            
            %Remove last centers of each track
            %Otherwise, they will probably add artificial frames to each ROI
            arrayfun(@(x) x.DeleteLastPos(obj.StageConfig.TrackingGap) ,tracker.allTracks);
            
            %Remove tracks that are too short
            tracker.DeleteShortTracks(obj.StageConfig.MinSignalDuration);
            
            %Project surviving maxima
            maxima = tracker.ProjectMaxima(maxima);          
            
            %Get the unique time indices of tracked maxima
            tIdx = tracker.GetTimeIndices();
            
            multiWaitbar('Segmenting...', 'Color', 'g');
            emptyL = zeros(sz(1), sz(2));

            iiPrev = 1;
            
            function deleteMax(y,x)
                iMax(y, x) = 0;
            end
            
            function currL = AssignTrackLabels(L)
                currL = emptyL;
                numTracks = numel(tracks);
                    
                for iTracks=1:numTracks
                    currTrack = tracks(iTracks);
                    cntr = round (currTrack.GetCenter(ii));
                    lbl = L(cntr(2), cntr(1));

                    %Prohibit sudden changes of label in center.
                    %Must be at least one frame difference
                    prevId = prevL(cntr(2), cntr(1));
                    if prevId > 0 && prevId ~= currTrack.id
                        currTrack = tracker.GetTrack(prevId);
                    end

                    if lbl > 0
                        idx = find(L==lbl);
                        currTrack.PixelIdx = cat(1, currTrack.PixelIdx, ((ii-1) * frameOffset) + idx);
                        currL(idx) = currTrack.id;
                    end
                end
            end
                    
            for ii=tIdx'              
                tracks = tracker.GetTracks(ii);
                invertRec = Iobrcbr(:,:,:,:,:,ii);
                iMax = maxima(:,:,:,:,:,ii);
                m = movie(:,:,:,:,:,ii);
                exclude = m < obj.StageConfig.Threshold;
                
                L = obj.Segment(iMax, invertRec, m, se3);
                L(exclude) = 0;
                L = AssignTrackLabels(L);
                
                %get rounded center coordinates
                cntrs = arrayfun( @(x) round(x.GetCenter(ii)), tracks, 'UniformOutput', false)' ;
                %Determine invalid maxima (sudden change of label)
                %Check if a sudden change of labels occurs for a given maximum 
                invalidC = cellfun( @(x) prevL(x(2), x(1)) > 0 && prevL(x(2), x(1)) ~= L(x(2), x(1)),cntrs);
                cntrs = cntrs(invalidC);
                
                if ~isempty(cntrs)
                    %Split tracks and append the "new" tracks to the tracker
                    arrayfun(@(x) x.Split(tracker, ii), tracks(invalidC), 'UniformOutput', false);
                    %tracker.AppendTracks(splitTracks{~isempty(splitTracks)});
                    tracks = tracker.GetTracks(ii);
                
                    cellfun( @(x) deleteMax(x(2), x(1)) , cntrs);
                    L = obj.Segment(iMax, invertRec, m, se3);
                    L(exclude) = 0;
                    L = AssignTrackLabels(L);
                end

                if (ii - iiPrev) > 1
                    prevL = emptyL;
                else
                    prevL = L;
                end
                iiPrev = ii;
                
                multiWaitbar('Segmenting...', 'Value', ii/hDataset.MetaData.SamplesT);
            end

            CC.PixelIdxList = cell(1, tracker.NumTracks);
            
            for it=1:tracker.NumTracks
                trackId = tracker.allTracks(it).id;
                CC.PixelIdxList{trackId} = tracker.allTracks(it).PixelIdx;
            end
            
            idx = cellfun(@isempty, CC.PixelIdxList);
            CC.PixelIdxList(idx) = [];
            CC.NumObjects = numel(CC.PixelIdxList);
            
            multiWaitbar('Segmenting...', 'close');
        end
        
        function [mask, stats] = DetectObjects(~, frame, img)
            mask = logical(frame);
            CC = bwconncomp( mask );
            stats = regionprops(CC, img, 'BoundingBox', 'WeightedCentroid');
            
            %Weighted centroid might be outside of tracked maximum
            %-> if this is the case, select the pixel of the respective
            %region, closest to the computed maximum
            
            for ii=1:CC.NumObjects
                idx = CC.PixelIdxList{ii};
                [Y, X] = ind2sub(size(frame), idx);
                
                dist = sqrt( sum( ( round(stats(ii).WeightedCentroid - [X,Y])) .^ 2, 2 ) );
                
                %is the center outside of the regionof connected pixels?
                if ~any(dist == 0)
                    [~, i] = min(dist);
                    stats(ii).WeightedCentroid = [X(i), Y(i)]; %choose the closest point from that region
                end
            end
        end
        
        function [iMax, Ierdc] = ExtractMaxima(obj, m, exclude, seOpen, seErode, seDilate, seDilateMax)
            Ier = imreconstruct(imerode(imopen(m,seOpen),seErode),m);      
            Ierdc = imcomplement(imreconstruct(imcomplement(imdilate(Ier,seDilate)),imcomplement(Ier)));
            %iMax = imregionalmax(Ierdc);
            iMax = imextendedmax(Ierdc, obj.StageConfig.MinPeakHeight);
            
            if obj.StageConfig.DilateMaxima > 0
                iMax = imdilate(iMax, seDilateMax );
            end
            
            iMax(exclude) = 0;
        end

         function L = Segment(obj, iMax, Iobrcbr, m, se)
            sz = size(iMax);
            if ~any(iMax(:) > 0)
                L = zeros(sz);
                return;
            end
            
            %~10x faster than graydist...
            %D = imcomplement(bwdist(iMax, "euclidean")) .* m;
            D = imcomplement(Iobrcbr + iMax); %Superimpose maxima
            bgm = watershed(D) == 0;
            
            if any(bgm > 0, "all")
                bgm = imclose(bgm, se);
                Iobrcbr(bgm) = -inf;
                Iobrcbr = imopen(Iobrcbr, se);
            end

            
            Iobrcbr = bwdist(imcomplement(imbinarize( Iobrcbr, 0 )), "euclidean") .* m;
            Iobrcbr(Iobrcbr < obj.StageConfig.Threshold | m < obj.StageConfig.Threshold) = 0;
            
            %Ensure, we only get extracted regions that contain an actual
            %maxima. We don't want split-off sub regions as disconnected
            %parts of the same signal.
            CC = bwconncomp(Iobrcbr);            
            L = labelmatrix( CC );
         end
    end
    
    methods (Access=protected)
%         function S = ClassifyPixels(obj, movie, dim)
%         %CLASSIFYPIXELS Returns a binary map of pixels. Pixels with value
%         %"1" are candidates to belong to a potential signal, Pixels with
%         %value "0" do not ne long to a signal.
%         %   To be potentially part of a signal, at least one of both values, 
%         %   temporal mean and spatial mean, mut be greater or equal to
%         %   minTh.
%         
%             % src is in range [0..1]
%             
%             try
%                 S = movie;
%                 S(S > 1) = 1;
%                 
%                 fSize = 2*obj.StageConfig.NHood + 1;
% 
%                 %1) Perform temporal filter. Computes the average
%                 %probability of a pixel within its temporal neighborhood to be part
%                 %of a signal. Assuming 1 is absolute certainity of being a
%                 %signal and 0 is absolute certanity to be not part of a
%                 %signal.
%                 S = movmean(S, fSize, dim);
% 
%                 %2) Perform spatial (per-frame) filter. Computes average probabilty
%                 % of a pixel within its neighborhood to be part of a
%                 % signal.
%                 sz = size(S);
%                 if sz(4) > 1
%                     krnl = ones([fSize,fSize,1,fSize]) / (fSize .^ 3);
%                     krnl(:,:,1,:) = 0;
%                 else 
%                     krnl = ones(fSize) / (fSize .^ 2);
%                 end
%                 
%                 fSpace = imfilter(S, krnl, "symmetric");
%                 
%                 %3) Check, if one of them is > classification TH
%                 %S = single(max(S, fSpace));
%                 S = (S + fSpace) ./ 2;
%             catch
%                 S = zeros(size(movie));
%             end
%          end
        
        function success = OnExecuteStage(obj, hDataset, ~) 
            %sz = size(hDataset.F);
            numSteps = 3 + hDataset.MetaData.SamplesT * hDataset.MetaData.SamplesZ * numel(obj.hAnalysis.Channels);            
            
            p = 0; %status tracker            
            S = 1;

            for channel=obj.hAnalysis.Channels
                hResultset = obj.hAnalysis.GetResultset(channel);
                movie = hDataset.GetNormalizedData( hDataset.F(:,:,channel,:,S,:),  hDataset.F0(:,:,channel,:,S,:), com.CalciumAnalysis.Data.NormalizationMode.dFF0);                
                hResultset.ROIs = detectMorphology(obj, hDataset, movie);
                
                updateStatus();
            end

            success = true;
            
            function updateStatus()
                p = p +1;
                obj.UpdateStatus(p/numSteps);
            end
        end
        
        function ROIs = detectMorphology(obj, hDataset, inStack)
            try 
                ROIs = obj.ApplyFilter(hDataset, inStack);

                %Remove ROIs that are shorter than n frames obj.StageConfig.MinSignalDuration
                %and that contain less than the minimum amount of pixels
                sz = size(hDataset.F);
                if obj.StageConfig.MinSignalDuration > 1
                    for k=ROIs.NumObjects:-1:1
                        %% calc Event SubMatrix
                        idx = ROIs.PixelIdxList{k}(:);
                        [~,~,~,~,~,t] = ind2sub(sz, idx);
                        [tMin, tMax] = bounds(t);

                        if tMax-tMin+1 < obj.StageConfig.MinSignalDuration
                            ROIs.PixelIdxList(k) = [];
                        else
                            numPxl = groupcounts(t);
                            if ~any(numPxl > obj.StageConfig.MinimumRoiSize)
                                ROIs.PixelIdxList(k) = [];
                            end
                        end
                    end

                    ROIs.NumObjects = numel(ROIs.PixelIdxList);
                end                    
            catch Ex
                com.common.Logging.MLogManager.Exception(Ex);
            end
        end 
        
%         function [threshold, dynRange] = GetThreshold(obj, hDataset, channelID)     
%             S = 1;
%             
%             if isempty(obj.StageConfig.ThresholdMode)
%                 obj.StageConfig.ThresholdMode = com.Enum.ThresholdTypeEnum.Guided;
%             end
%             
%             diff = hDataset.F(:,:,channelID,:,S,:) - hDataset.F0(:,:,channelID,:,S,:);
%             
%             switch (obj.StageConfig.ThresholdMode)
%                 case com.Enum.ThresholdTypeEnum.Guided
%                     [threshold, dynRange] = com.lib.ImageProc.ComputeBiModalThreshold(diff,...
%                             [hDataset.MetaData.SamplesX, hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesZ]);
%                     sensitivity = obj.StageConfig.Sensitivity;
%                     threshold = sensitivity*threshold(1) + (1-sensitivity)*threshold(2); 
%                 case com.Enum.ThresholdTypeEnum.UserDefined
%                     threshold = obj.StageConfig.UserDefThreshold;
%                     dynRange = com.lib.ImageProc.ComputeDynRangeImage(diff,...
%                             [hDataset.MetaData.SamplesX, hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesZ],...
%                             3, 7); %ToDo: Change in UI
%                 otherwise
%                     threshold = [1,1];
%                     dynRange = zeros(hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesX);
%             end
%         end
     end
end

