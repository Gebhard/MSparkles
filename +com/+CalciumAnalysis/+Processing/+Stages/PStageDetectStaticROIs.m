%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageDetectStaticROIs < com.common.Proc.AnalysisStage
    %PSTAGEDETECTSTATICROIS Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = PStageDetectStaticROIs(analysis)
            obj = obj@com.common.Proc.AnalysisStage('Detect static ROIs', '1.3.0', '07.12.2018', analysis);
        end                
    end
    
    methods (Access=protected)
        function success = OnExecuteStage(obj, dataSet, ~)
            stepsPerROIset = 4;            
            totalSteps = stepsPerROIset * numel(obj.hAnalysis.Channels);
            currentStep = 1;

            if totalSteps > 0
                for c=obj.hAnalysis.Channels
                    hResultSet = obj.hAnalysis.GetResultset(c);
                    [detectionTH, dynRange] = obj.GetThreshold(dataSet, c);                               
                    updateStatus();

                    hResultSet.ROIs = obj.ApplyFilter(dataSet, c, dynRange, detectionTH);
                    updateStatus();

                    hResultSet.DetectionTH = detectionTH;
                    updateStatus();    
                end                    
            end                      
            
            success = true;
            
            function updateStatus()
                step = currentStep;
                currentStep = currentStep +1;
                obj.UpdateStatus(step/totalSteps);
            end
        end  
        
        function [threshold, dynRange] = GetThreshold(obj, hDataset, channelID)
            S = 1;
            
            if isempty(obj.StageConfig.ThresholdMode)
                obj.StageConfig.ThresholdMode = com.Enum.ThresholdTypeEnum.Guided;
            end

            diff = hDataset.GetNormalizedData( hDataset.F(:,:,channelID,:,S,:),...
                hDataset.F0(:,:,channelID,:,S,:));                
            
            switch (obj.StageConfig.ThresholdMode)
                case com.Enum.ThresholdTypeEnum.Guided
                    [threshold, dynRange] = com.lib.ImageProc.ComputeBiModalThreshold(diff,...
                            [hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesX, hDataset.MetaData.SamplesZ],...
                            obj.StageConfig.DynRngSigma, obj.StageConfig.DynRngKrnlSize);
                    sensitivity = obj.StageConfig.Sensitivity;
                    threshold = sensitivity*threshold(1) + (1-sensitivity)*threshold(2); 
                case com.Enum.ThresholdTypeEnum.UserDefined
                    threshold = obj.StageConfig.UserDefThreshold;
                    dynRange = com.lib.ImageProc.ComputeDynRangeImage(diff,...
                            [hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesX, hDataset.MetaData.SamplesZ],...
                            obj.StageConfig.DynRngSigma, obj.StageConfig.DynRngKrnlSize);
                otherwise
                    threshold = [1,1];
                    dynRange = zeros(hDataset.MetaData.SamplesY, hDataset.MetaData.SamplesX);
            end
        end
        
        function labels = DetectRois(obj, hDataset, channelID, dynRange, detectionTH)
            %% Get regional maxima
            %maxima = imextendedmax(dynRange, detectionTH);
            maxima = imregionalmax(dynRange);
            %% Get groups of connected pixels
            cc = bwconncomp(maxima);
            
            dbg = false;
            if dbg
                fDbg = figure("Units", "pixels", "Position", [0,0,1000,1000]);
                axDbg = axes(fDbg);
                
                %Rescale and render fluorescence range map with colormap
                gamma = 1.0;
                buffer = rescale(dynRange, 'InputMin', min(dynRange, [], "all"), 'InputMax', max(dynRange, [], "all")) .^ gamma;
               
                %use pre-defined lut  
                LUTlevels = 1024;
                LUT = jet(LUTlevels);
                buffer = round(buffer * (LUTlevels-1)) + 1;
                buffer = reshape(LUT(buffer(:), :), [size(dynRange),3]);
                
                
                %Add overlay of local maximal region as white spots 
                maximaBuffer = repmat(maxima, 1, 1, 3);
                buffer( maximaBuffer > 0 ) = maximaBuffer(maximaBuffer > 0);
                
                imagesc(axDbg, buffer);
                colormap(jet);
                colorbar(axDbg);
                axDbg.CLim = [min(dynRange, [], "all"), max(dynRange, [], "all")];
                title(axDbg, "Fluorescence range with maximal regions");
            end
            
            %% Get labels to identify growing regions
            labels = single(labelmatrix(cc));
            
            %% Get pixels below detection threshold
            excludeMap = dynRange < detectionTH;
            
            %% Compute per-group mean signal over time
            %create array of of group-means
            groupMeans = zeros(hDataset.MetaData.SamplesT, cc.NumObjects);
            
            %Get Normalized/detrended signal, based on current setting, typically (F-F0)/F0
            dFF0 = hDataset.GetNormalizedData(hDataset.F(:,:,channelID,:,:,:), hDataset.F0(:,:,channelID,:,:,:));
            sz = size(dFF0);
            
            %make column matrix, dimension 6 contains time
            %Dimension 5 is currently ignored, but part of ScanImage Tiff format
            dFF0 = reshape( permute(dFF0, [6,1,2,3,4,5]) , sz(6) , sz(1) * sz(2) * sz(4) * sz(5));
            
            %Create Gauss filter and apply to time dimension
            fltrKrnl = GetGaussKernel(2, 4)';  
            
            %Smooth
            dFF0 = imfilter(dFF0, fltrKrnl, "conv", "symmetric");

            %Iterate over all groups of connected pixels
            for ig = 1:cc.NumObjects
                %Get the pixel indices and project them along time
                idx = cc.PixelIdxList{ig};
                groupMeans(:,ig) = mean( dFF0(:, idx) , 2, 'omitnan');
            end
            
            %Compute (full) correlation matrix
            corrMat = corr(groupMeans, dFF0);
            
            %% Perform iterative, breadth-first region growing
            run = true;
           
            if ndims(labels) == 3
                padSize = [1,1,1];
            else
                padSize = [1,1];
            end
            
            corrTH = obj.StageConfig.CorrelationThreshold;
            
            %Create an image, where each pixel contains its index, pad it
            %and transform to column image of 3x3(x3) n-hoods
            numPxl = prod(size(excludeMap), "all");
            idxMat = reshape(1:numPxl, size(excludeMap)); %Get image of indices
            idxMat = padarray(idxMat, padSize, 'symmetric');
            idxMat = im2colEx(idxMat, [3,3,3]);
            exmapLN = ~excludeMap(:)';
            
            while run
                %Within a 3x3x3 neighborhood of the label image
                %1) If all neighbors are 0 => Don't care
                %2) If neighbors have different labels >0 => boundary pixel
                %   between two (or more) regions
                %3) If all non-zero neighbors have the same label:
                %   - Make sure it is a boundary pixel
                %   - Compute the correlation with the respective mean signal of the center region
                %   - If the correlation coefficient is above TH, assign the group label to this pixel
                %4) Repeat until no more changes occur
               
                labels(excludeMap) = 0;
                colImg = labels(idxMat);
                colImg(:, labels ~= 0) = 0;         %Set existing labels and boundary pixels to 0
                gz = colImg > 0;                    %Get the indices of non-zero elements
                idx = find(any(gz) & exmapLN);        %Identify boundary pixels. At least one of he neighbours must be > 0
                
                um = nan(numel(idx),1);
                
                for ic=1:numel(idx)
                    kk = idx(ic);
                    u = unique( colImg(gz(:, kk), kk) );
                    if numel(u) == 1
                        um(ic) = u;
                    end
                end
                
                idxCand = ~isnan(um);
                idx = idx(idxCand);
                dummy = um(idxCand);

                corrIdx = sub2ind(size(corrMat), dummy(:), idx(:));
                R = (corrMat(corrIdx));   
                idxCorr = R > corrTH;
                
                %Create copy of current label image
                newLbl = labels;
                newLbl(idx(idxCorr)) = dummy(idxCorr);
                newLbl(idx(~idxCorr)) = -1;
                newLbl(excludeMap) = 0;
                
                run = any((labels - newLbl) ~= 0, "all");
                labels = newLbl;
            end           

            %Cleanup in last iteration
            %Set pixels with more than one label in their neighborhood to
            %be 0 (boundary)
            labels(labels == -1) = 0;            
            colImg = labels(idxMat);
            
            gz = colImg > 0;            %Get the indices of non-zero elements
            idx = find(sum(gz) > 1);	%Identify boundary pixels. At least one of he neighbours must be > 0

            for ic=1:numel(idx)
                kk = idx(ic);
                u = unique( colImg(gz(:, kk), kk) );
                if numel(u) > 1
                    labels(kk) = 0;
                end
            end
        end
    end
     
    methods
        function ROIs = ApplyFilter(obj, hDataset, channelID, dynRange, detectionTH)
            dynRangeTH = dynRange;
            dynRangeTH(dynRangeTH < detectionTH) = 0;

            %bw = imbinarize(dynRange, detectionTH);         
            bw = dynRangeTH;
            bw(bw > 0) = 1;

            switch(obj.StageConfig.SegmentationMethod)
                case 2 %Superpixels
                    regMx = imregionalmax(dynRangeTH);
                    cntr = regionprops(regMx, 'Centroid');
                    %centers = round(reshape(struct2array(cntr), numel(c(1).Centroid), numel(c)))';
                    [l,~] = superpixels(dynRangeTH, numel(cntr), 'Compactness', 1);
                    l(~bw) = 0;
                    bm = boundarymask(l);
                    l(bm) = 0;
                case 3
                    l = obj.DetectRois(hDataset, channelID, dynRange, detectionTH);

%                     movie = hDataset.GetNormalizedData( hDataset.F(:,:,channelID,:,1,:),  hDataset.F0(:,:,channelID,:,1,:), com.CalciumAnalysis.Data.NormalizationMode.dFF0);                
%                     iMax = imregionalmax(dynRange);
%                     l = bwlabel(iMax);
%                     l = com.lib.ImageProc.FindTempCorrRegions(movie, dynRange, l, detectionTH, obj.StageConfig.CorrelationThreshold);

                otherwise %watershed
                    d = -dynRange;
                    d(~bw) = Inf;
                    l = watershed(d);
                    l(~bw) = 0;
                    l(l ~= 0) = 1;
            end

            ROIs = bwconncomp(l);
            
            %Apply size constraint
            minRoiSize = obj.StageConfig.MinimumRoiSize;
            maxRoiSize = inf; %dummy for now
            
            roiSizes = cellfun(@(x) numel(x), ROIs.PixelIdxList);
            mask = ( roiSizes < minRoiSize | roiSizes > maxRoiSize);
            ROIs.PixelIdxList(mask) = [];
            ROIs.NumObjects = numel(ROIs.PixelIdxList);
        end
    end
end

