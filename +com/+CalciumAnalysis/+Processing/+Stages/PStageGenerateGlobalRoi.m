%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageGenerateGlobalRoi  < com.common.Proc.AnalysisStage
    %PSTAGEGENERATEGLOBALROI Summary of this class goes here
    %   Detailed explanation goes here
    
     methods
        function obj = PStageGenerateGlobalRoi(analysis)
            obj = obj@com.common.Proc.AnalysisStage('Generate global grid', '1.2.0', '07.12.2018', analysis); 
        end                
    end
    
    methods (Access=protected)
        
        function success = OnExecuteStage(obj, hDataset, ~)  
            sz = [hDataset.MetaData.SamplesY hDataset.MetaData.SamplesX hDataset.MetaData.SamplesZ];

            if hDataset.MetaData.SamplesZ > 1
                ROIs.Connectivity = 26;
            else
                ROIs.Connectivity = 8;
            end

            if sz(3) > 1
                ROIs.ImageSize = [sz(1) sz(2) sz(3)];             
            else
                ROIs.ImageSize = [sz(1) sz(2)];
            end

            ROIs.NumObjects = 1; 
            ROIs.PixelIdxList = { (1:(sz(1)*sz(2)*sz(3)))' };     

            arrayfun(@(x) setROIs(x), obj.hAnalysis.Channels);
            
            success = true;
            
            function setROIs(channelID)
                hResult = obj.hAnalysis.GetResultset(channelID);
                hResult.ROIs = ROIs;
            end
        end
    end
end

