%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageGenerateRoiGrid  < com.common.Proc.AnalysisStage
    %PSTAGEGENERATEROIGRID Summary of this class goes here
    %   Detailed explanation goes here
    methods
        function obj = PStageGenerateRoiGrid(hAnalysis)
            obj = obj@com.common.Proc.AnalysisStage('Generate ROI grid', '1.2.0', '17.12.2018', hAnalysis);             
        end                
    end
    
    methods (Access=protected)
        
        function success = OnExecuteStage(obj, hDataset, ~)                 
            sz = [hDataset.MetaData.SamplesY hDataset.MetaData.SamplesX hDataset.MetaData.SamplesZ];  
            
            if numel(obj.StageConfig.GridSize) < 3
                gs = [obj.StageConfig.GridSize 1];
            else
                gs = obj.StageConfig.GridSize;
            end
            
            ROIs.Connectivity = 8;
            if sz(3) > 1
                ROIs.ImageSize = [sz(1) sz(2) sz(3)];             
            else
                ROIs.ImageSize = [sz(1) sz(2)];
            end
            ROIs.NumObjects = gs(1) * gs(2) * gs(3); 
            ROIs.PixelIdxList = cell(1, gs(1)*gs(2)*gs(3));
            
            dh = sz(1) / gs(1);
            dw = sz(2) / gs(2);
            dz = sz(3) / gs(3);            
            
            try
                numIter = sz(1)*sz(3);
                for z=1:sz(3)
                    for i=1:sz(1)
                        arrayfun(@(x) getGridIndex(z, i, x), 1:sz(2)); %'UniformOutput',false
                        obj.UpdateStatus(((z-1)*sz(1) + i)/numIter);
                    end
                end                                                                
                
                for c=obj.hAnalysis.Channels
                    hResult = obj.hAnalysis.GetResultset(c);
                    hResult.ROIs = ROIs;
                end
            catch ex
                obj.hAnalysis.ResetResults();
                rethrow (ex);
            end            
            
            success = true;
            
            function [gridIdx, pxlIdx] = getGridIndex(zCoord, yCoord, xCoord)
                grdX = floor((xCoord-1)/dw)+1;
                grdY = floor((yCoord-1)/dh)+1;
                grdZ = floor((zCoord-1)/dz)+1;
                
                gridIdx = sub2ind(gs, grdY, grdX, grdZ);
                pxlIdx = sub2ind(sz, yCoord, xCoord, zCoord);
                
                ROIs.PixelIdxList{1,gridIdx} = [ROIs.PixelIdxList{1,gridIdx}; pxlIdx];                                
            end
        end
    end
end

