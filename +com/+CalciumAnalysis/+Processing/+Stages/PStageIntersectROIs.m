%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageIntersectROIs < com.common.Proc.AnalysisStage
    properties(Access = private)
        hIntersectAnalysis;
    end
    
    methods
        function obj = PStageIntersectROIs(hAnalysis, hIntersectAnalysis)
            obj = obj@com.common.Proc.AnalysisStage('Intersect ROIs', '1.2.0', '07.12.2018', hAnalysis); 
            obj.hIntersectAnalysis = hIntersectAnalysis;
        end                
    end
    
    methods (Access=protected)        
        function success = OnExecuteStage(obj, ~, ~)              
            %Iterate over all Channels in hAnalysis and intersect with
            %their respective counterpart in hIntersectAnalysis, if it
            %exists.
            
            for i=obj.hAnalysis.Channels
                cDest = obj.hAnalysis.GetResultset(i);
                cSrc = obj.hIntersectAnalysis.GetResultset(i);

                if ~isempty(cDest) && ~isempty(cSrc)
                    cDest.ROIs = IntersectRoiSets(cDest.ROIs, cSrc.ROIs, false);
                end    

                %Apply size constraint
                minRoiSize = obj.hAnalysis.AnalysisSettings.MinimumRoiSize;
                maxRoiSize = inf; %dummy for now

                roiSizes = cellfun(@(x) numel(x), cDest.ROIs.PixelIdxList);
                mask = ( roiSizes < minRoiSize | roiSizes > maxRoiSize);
                cDest.ROIs.PixelIdxList(mask) = [];
                cDest.ROIs.NumObjects = numel(cDest.ROIs.PixelIdxList);
            end  
            
            success = true;
        end
    end
end

