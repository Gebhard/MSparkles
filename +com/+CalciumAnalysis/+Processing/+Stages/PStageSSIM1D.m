%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageSSIM1D < com.common.Proc.AnalysisStage
    %PSTAGESSIM1D Summary of this class goes here
    %   Detailed explanation goes here    
    
    methods
        function obj = PStageSSIM1D(analysis, varargin)
            if (isempty(varargin))
                varargin = {'SSIM 1D', '1.2.0', '07.12.2018'};
            end            
            obj = obj@com.common.Proc.AnalysisStage(varargin{:}, analysis);            
        end               
    end
    
     methods (Access=protected)
        function OnInitializeDefaultParameterSet(~)  
        end
        
        function success = OnExecuteStage(self, InputStack, params, ~) 
            %Compute structural similarity for 1D signal against ground-truth signal    

            truth = InputStack;
            signal = requiredValues('F');
            
            numOps = 10;
            sigma = 2;    
            h = GetGaussKernel(sigma, [2,2,7]);   

            [k1, Kn, err] = decompose_kernel(h);

            %Gaussian weighted mean values as suggested in the paper

            self.UpdateStatus(1/numOps);
            %m_sig = imfilter(single(signal), h,'conv','replicate');    
            m_sig = imfilter(single(signal), k1{1},'conv','replicate');
            m_sig = imfilter(m_sig, k1{2},'conv','replicate');
            m_sig = imfilter(m_sig, k1{3},'conv','replicate');

            self.UpdateStatus(2/numOps);
            %m_tru = imfilter(single(truth), h,'conv','replicate');    
            m_tru = imfilter(single(truth), k1{1},'conv','replicate');
            m_tru = imfilter(m_tru, k1{2},'conv','replicate');
            m_tru = imfilter(m_tru, k1{3},'conv','replicate');

            self.UpdateStatus(3/numOps);
            m_sigtru = m_sig.*m_tru;
            m_sig = m_sig.^2;
            m_tru = m_tru.^2;

            %compute gauss weighted standard deviations

            self.UpdateStatus(4/numOps);
            %sigmax2 = imfilter(signal.^2, h,'conv','replicate') - m_sig; 
            sigmax2 = imfilter(signal.^2, k1{1},'conv','replicate');
            sigmax2 = imfilter(sigmax2, k1{2},'conv','replicate');
            sigmax2 = imfilter(sigmax2, k1{3},'conv','replicate') - m_sig;

            self.UpdateStatus(5/numOps);
            %sigmay2 = imfilter(truth.^2, h,'conv','replicate') - m_tru;      
            sigmay2 = imfilter(truth.^2, k1{1},'conv','replicate');
            sigmay2 = imfilter(sigmay2, k1{2},'conv','replicate');
            sigmay2 = imfilter(sigmay2, k1{3},'conv','replicate') - m_tru;

            self.UpdateStatus(6/numOps);
            %sigmaxy = imfilter(signal.*truth, h,'conv','replicate') - m_sigtru;
            sigmaxy = imfilter(signal.*truth, k1{1},'conv','replicate');
            sigmaxy = imfilter(sigmaxy, k1{2},'conv','replicate');
            sigmaxy = imfilter(sigmaxy, k1{3},'conv','replicate') - m_sigtru;

            exponents = [1,1,1];

            %%use actual dynamic range of input signal
            dynRng = max(signal(:)) - min(signal(:));        
            C = [(0.01*dynRng).^2 (0.03*dynRng).^2 ((0.03*dynRng).^2)/2];

            self.UpdateStatus(7/numOps);
            % General case: Equation 12 from [1] 
            % Luminance term
            if (exponents(1) > 0)
                num = 2*m_sigtru + C(1);
                den = m_sig + m_tru + C(1); 
                result = self.guardedDivideAndExponent(num,den,C(1),exponents(1));
            else 
                result = ones(size(A), 'like', A);
            end

            self.UpdateStatus(8/numOps);
            % Contrast term
            sigmaxsigmay = [];
            if (exponents(2) > 0)  
                sigmaxsigmay = sqrt(sigmax2.*sigmay2);
                num = 2*sigmaxsigmay + C(2);
                den = sigmax2 + sigmay2 + C(2); 
                result = result .* self.guardedDivideAndExponent(num,den,C(2),exponents(2));        
            end

            self.UpdateStatus(9/numOps);
            % Structure term
            if (exponents(3) > 0)
                num = sigmaxy + C(3);
                if isempty(sigmaxsigmay)
                    sigmaxsigmay = sqrt(sigmax2.*sigmay2);
                end
                den = sigmaxsigmay + C(3); 
                result = result .* self.guardedDivideAndExponent(num,den,C(3),exponents(3));        
            end
            
            success = true;
        end
                
        function component = guardedDivideAndExponent(self, num, den, C, exponent)

            if C > 0
                component = num./den;
            else
                component = ones(size(num),'like',num);
                isDenNonZero = (den ~= 0);
                component(isDenNonZero) = num(isDenNonZero)./den(isDenNonZero);
            end

            if (exponent ~= 1)
                component = component.^exponent;
            end
        end
     end
   
end

