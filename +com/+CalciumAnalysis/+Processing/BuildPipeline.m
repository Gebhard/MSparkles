%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function ep = BuildPipeline(hDataSetNode, finalStage, Analyses, forceRecomputeAll)   
    %% Build Pipeline 
        % hDataSetNode:     TreeNode of the dataset
        % analysisSettings: Settings object that might have different
        %                   settings than specified in the Dataset
        % finalStage:       com.Enum.PipelineStage, which allows to run the 
        %                   pipeline only partially
        % Analyses:         (Cell) Array of MAnalysis objects, specifying
        %                   which analysis operations are to be performed.
        %                   Can be a subset of the analyses defined in the
        %                   dataset.
        
    if nargin < 4
        forceRecomputeAll = false;
    end
    
    hDataSet = hDataSetNode.Dataset;
        
    reloadOrgDataset = (~isempty(hDataSet.PreProcDatasetPath) && ...
        hDataSet.PreProcDatasetPath ~= "" && ...
        ismember(com.Enum.PipelineStage.PreProcessing,finalStage)) || forceRecomputeAll;
    
    %clear PreProcDatasetPath to ensure the original dataset is reloaded to
    %re-perform pre-processing
    
    if forceRecomputeAll
        ResetAll();
    end
    
    switch min(finalStage)
        case com.Enum.PipelineStage.PreProcessing
            ResetAll();
        case com.Enum.PipelineStage.BaselineComputation
            hDataSet.ResetF0();
            hDataSet.ResetRois();
            hDataSet.ResetResults();
    end
    
    function ResetAll()
        reloadOrgDataset = true;
        hDataSet.ResetMetadata(true);
        hDataSet.ResetPreProcData(false);
        hDataSet.ResetF0();
        hDataSet.ResetRois();
        hDataSet.ResetResults();
    end
    
    ep = com.common.Proc.ExecutionPipeline(hDataSet);
          
    ep.AddPipelineStage(com.CalciumAnalysis.Processing.Stages.PStageDummy('Closing dataset (if required)'), @CloseCurrentDatasetIfRequired, {hDataSetNode});
    
    ep.AddPipelineStage(com.common.Proc.PStageOpenFile(reloadOrgDataset), @OnFileLoaded, {hDataSetNode});             
    
    %% Build pre-processing pipeline stages    
    if ( isempty(hDataSet.PreProcDatasetPath) ||  hDataSet.PreProcDatasetPath == "")    
        if ~isempty( hDataSet.PreProcConfig )
            for i=1:hDataSet.NumPreProcStages
                cfg = hDataSet.GetPreProcConfig(i);
                ep.AddPipelineStage( cfg.CreatePipelineStage()  );
            end
            
            ep.AddPipelineStage(com.CalciumAnalysis.Processing.Stages.PStageDummy('Finalizing pre-processing'), @OnPreProcessingFinished, {hDataSetNode});
        end
    else
        com.common.Logging.MLogManager.Info('Pre-processing has already been performed. Skipping...');
    end
    
    %% Add F0 computation to the queue
    if ~hDataSet.ExternalF0
        if  any(finalStage >= com.Enum.PipelineStage.BaselineComputation) && ...
            ( isempty(hDataSet.F0) && isempty(hDataSet.F0Path) ) ||...
            forceRecomputeAll || ismember(com.Enum.PipelineStage.BaselineComputation, finalStage)
                
            for hCfg = hDataSet.F0Config
                if hCfg == hDataSet.F0Config(end)
                    ep.AddPipelineStage(hCfg.CreatePipelineStage(), @OnF0Calculated, {hDataSetNode});
                else
                    ep.AddPipelineStage(hCfg.CreatePipelineStage());
                end
            end
        end
    else
        com.common.Logging.MLogManager.Info('Using external F0. Skipping...');
    end
             
    %% If we perform a static analysis, allow for ROI detection/generation
    if any(finalStage >= com.Enum.PipelineStage.RoiDetection) || forceRecomputeAll
        
        if nargin < 4 || isempty(Analyses)
            %If no subset of the defined analyses is specified, run all
            %analyses defined in the dataset.
            Analyses = hDataSet.Analyses;
        end
        
        if isa(Analyses, 'com.CalciumAnalysis.Analysis.AnalysisManager')
            numAnalyses = Analyses.Count;
        else
            numAnalyses = numel(Analyses);
            if ~iscell(Analyses)
                Analyses = {Analyses};
            end
        end
        
        %ROI detection / generation stages
        for i=1:numAnalyses
            hAnalysis = Analyses{i};                                         
            
            if forceRecomputeAll || (~hAnalysis.DoAllRoisExist() || ismember(com.Enum.PipelineStage.RoiDetection, finalStage))
                hAnalysis.ResetRois();
                hStage = hAnalysis.AnalysisSettings.CreatePipelineStage(hAnalysis);
                
                if ~isempty(hStage)
                    if ~isempty(hAnalysis.arrIntersectIDs)
                        ep.AddPipelineStage(hStage);                    
                        AddIntersectionStages(ep, hDataSetNode, hDataSet, hAnalysis);
                    else
                        ep.AddPipelineStage(hStage, @OnRoiDetectionFinished, {hAnalysis, hDataSetNode});                    
                    end                
                end              
            end
        end        
    end
     
    %% Add analysis stages
    if ( ismember(com.Enum.PipelineStage.Analysis, finalStage) || forceRecomputeAll)          
        for i=1:numAnalyses
            hAnalysis = Analyses{i};
            hAnalysis.ResetResults();                   
            ep.AddPipelineStage(com.CalciumAnalysis.Processing.Stages.PStageAnalyzeROIs(hAnalysis),  @OnAnalysisFinished, {hAnalysis, hDataSetNode});                           
        end                
    end
end

function AddIntersectionStages(ep, hDataSetNode, hDataSet, analysis)
    for i=1:numel(analysis.arrIntersectIDs)
        aIntersectID = analysis.arrIntersectIDs{i};
        aIntersect = hDataSet.Analyses.GetAnalysis(aIntersectID); 

        ep.AddPipelineStage(com.CalciumAnalysis.Processing.Stages.PStageIntersectROIs(analysis, aIntersect),...
            'CallBack', @OnRoiDetectionFinished, 'CallBackParam', {analysis, hDataSetNode});
    end
end

function CloseCurrentDatasetIfRequired(params,~)
    hDatasetNode = params{1};
     
    projMon = com.Management.MProjectManager.GetInstance();        
    if ~isempty(projMon) && (isempty(projMon.ActiveDataSetNode) || hDatasetNode ~= projMon.ActiveDataSetNode)
        hInst = MSparklesSingleton.GetInstance();
        hInst.CloseAppWindows();
    end
end

function OnFileLoaded(params, success)
    if success
        hProjectNode = params{1};          

        projMon = com.Management.MProjectManager.GetInstance();        
        if ~isempty(projMon)
            projMon.UpdateActiveDatasetNode( hProjectNode );
        end
    end
end

function OnPreProcessingFinished(params, success)
    if success
        hDataSetNode = params{1};
        hDataSet = hDataSetNode.Dataset;

        hDataSet.DeleteF0();

        if (hDataSet.SavePreProcResult())
            hDataSet.Save();
        end
    end
end

function OnF0Calculated(params, success)   
    if success
        hDataSetNode = params{1};           

        projMon = com.Management.MProjectManager.GetInstance();
        if ~isempty(projMon)
            projMon.UpdateF0(hDataSetNode);
        end
    end
end

function OnRoiDetectionFinished(params, ~)
    hAnalysis = params{1};
    hDataSetNode = params{2};
    hDataSet = hDataSetNode.Dataset;    

    hMSparkles = MSparklesSingleton.GetInstance();
    if ~isempty(hMSparkles)
        try
            %Update display
            hMSparkles.hCaMainWnd.hStackScroller.Invalidate(true);          
        catch
        end
        
        try
            %Append results to node of currently selected tree element
            hMSparkles.ProjectManager.UpdateROIsAndSave(hAnalysis);
        catch
        end
    end
    
    %ToDo: Handle 3D static ROIs...
    for c=1:hAnalysis.Channels
        hResult = hAnalysis.GetResultset(c);
        if ~isempty(hResult.ROIs)
            %Save ROI map
            fPath = hAnalysis.GetResultsFolder();                
            fName = hDataSet.DatasetName;
            pfx = hAnalysis.Name;
            channelName = hDataSet.MetaData.GetChannelName(c);
            roiMapPath = fullfile(fPath, sprintf('%s-%s-%s-ROI.png', fName, pfx, channelName));
            
            tmpPath = sprintf("%s.png", tempname);
            hAnalysis.SaveRoiImage(tmpPath, hResult);
            movefile(tmpPath, roiMapPath);
        end
    end
end

function OnAnalysisFinished(params, ~)
    warning ('off','all');
    
    hAnalysis = params{1};
    hMSparkles = MSparklesSingleton.GetInstance();
    
    if ~isempty(hMSparkles)
        try
            %Append results to node of currently selected tree element
            hMSparkles.ProjectManager.UpdateResults(hAnalysis);
            hMSparkles.ProjectManager.UpdateROIsAndSave(hAnalysis);
        catch
        end
        
        try
             if hAnalysis == hMSparkles.CurrentAnalysis
                UpdateCaWnd();
             end
        catch 
        end
    end
    
    hAnalysis.GenerateOutputFiles();
    
    warning ('on','all');
end