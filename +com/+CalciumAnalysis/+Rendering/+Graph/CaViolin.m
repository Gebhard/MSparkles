%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function CaViolin(ax, hDataSet, AreaT, MeanT, MeanMax, MeanMin)
    %AreaT = Area per timestep
    %MeanT = mean per timestep
    %PeakTime = timepoint of peak value

    %Create axes if needed
    if isempty(ax)
        f = figure;
        ax = axes('parent',f); 
%     else
%         cla(ax);
    end
    
    a = AreaT / 2;
    n = numel(a);
    
    numCol = 1024;
    j = jet (numCol); 
    
    if nargin < 5
        [minVal, maxVal] = bounds(MeanT(:));         
    else
        maxVal = MeanMax;% round ((MeanT ./ MeanMax) * numel(j(:,1)));
        minVal = MeanMin;
    end
    
    m = round( (1/(maxVal - minVal) * (MeanT - MeanMin)) * numCol);
    m(m<=0 | isnan(m)) = 1;
    M = reshape([m; m], 1, []);
    M(M>1024) = 1024;
    C = j(M, 1:3);
    
    %Ensure timing signal is row vector
    tSig = hDataSet.TimingSignal;
    if ~isrow(tSig)
        tSig = tSig';
    end
    
    %Polygon from start to end and back to maintain vertex order
    X = reshape([tSig; tSig], 1, []);
    Y = reshape([a;a], 1, []);
    
    c = repmat( [1,-1,-1,1], 1, round( n/2) );
    V = [X; Y .* c(1: 2*n)]';    
    vl = [1,2,3,4,1]; %Basic schema for vertex order of a face
    
    %Create Vertex lookup table
    L = 0:2:2*n-1;    
    L = repmat(L', 1, 5);  
    VL = repmat(vl, n, 1) + L;
    VL(end,:) = [];    
    
    colormap(ax, j);
    patch(ax, 'Faces', VL, 'Vertices', V, 'FaceColor', 'interp', 'FaceVertexCData', C, 'EdgeAlpha', 0);
end