%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegCorrPlot < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %EEGCORRPLOT Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = EegCorrPlot(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
            obj.ShowFrameIndicator = obj.Params.ShowFrameIndicator;
        end
    end
    
    methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "04.07.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Ca^{2+}-EEG correlation";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", [],   @(x) (x == 0) || (~isempty(x) && isobject(x)));
            addParameter(inParser, "FilePath", 0, @(x) ( (isstring(x) || ischar(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );            
            addParameter(inParser, "EegResultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", "", @(x) ischar(x) || isstring(x));   
            addParameter(inParser, "XLabel", "Time (sec)", @(x) ischar(x) || isstring(x)); 
            addParameter(inParser, "ShowFrameIndicator", false, @(x) islogical(x));
        end
        
        function RenderGraph(obj)
            %hGlobalSettings = MGlobalSettings.GetInstance();           
            hDataset = obj.Params.Dataset;
            hEegDataset = hDataset.EEGDataset;
            hEegResultset = obj.Params.EegResultset;
            hCaResultset = obj.Params.Resultset;
            
            hParentFig = obj.GetParentFigure();                
            hAxis = obj.GetAxis(hParentFig); 
            
            if ~isempty(hEegDataset) && ~isempty(hEegResultset) && ~isempty(hCaResultset)
                avgEegPwr = hEegResultset.AvgSigPwr(:);
                
                caSig = hCaResultset.GetEegReferenceSignal(hEegResultset.hParent.AnalysisSettings.CorrRefSignal);
                
                if ~isempty(avgEegPwr) && ~isempty(caSig)
                    
                    avgEegPwr = rescale(avgEegPwr);
                    caSig = rescale(caSig);                                            
                    
                    %2 Take synchronization offset into account 
                    settings = hDataset.Settings;
                    caTiming = hDataset.TimingSignal + settings.EegSync.EegSyncStart;
                    
                    plot(hAxis, avgEegPwr);
                    hold(hAxis, "on");
                    plot(hAxis, caTiming(1:numel(caSig)), caSig);
                    hold(hAxis, "off");
                    
                    titleStr = "Ca^{2+} time offset: " + hCaResultset.EegSignalOffset + "(sec).";
                    if ~isempty(hCaResultset.EegMaxCorrelation)
                        titleStr = titleStr + "(Max cross-corr: " + hCaResultset.EegMaxCorrelation + ")";
                    end
                    
                    title(hAxis,  titleStr);
                    legend(hAxis, "\mu EEG power (normalized)", "\mu Ca^{2+} signal (normalized)", "location", "northwest");
                end
            end            
        end
    end
end

