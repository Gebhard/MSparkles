%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef LiveEegPlot < com.common.Rendering.Graph & com.common.UI.UiControl
    %LIVEEEGPLOT Renders the selected EEG channel, synchronized with the
    %parent Ca2+ movie. THe Current time is horizontally centered and
    %indicated by a vertical red line (Frame indicator)
    
    properties
        timeOffset;     %Offset to be in the center of the displayed time span
        CurrTime;       %Current timepoint (indicated by frame indicator).
    end
    
    methods       
        function obj = LiveEegPlot(hAppWnd, hParent)
            %LIVEEEGPLOT Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.UI.UiControl(hAppWnd);
            
            if nargin == 2 && ~isempty(hParent)
                obj.hAxis = axes("Parent", hParent);

                obj.Render();
                obj.RenderFrameIndicator();
            end
        end
    end
    
    methods
        function Render(obj)
            hEegDataset = obj.hDataset.EEGDataset;  
            
            if ~isempty(hEegDataset)
            hAnalysis = hEegDataset.Analyses.CurrentAnalysis;
            
                if ~isempty(hAnalysis)
                    chan = obj.hDataset.Settings.EegSync.DisplayEegChannel;
                    
                    com.EEG.Rendering.EEGTracePlot(obj.hAxis, hEegDataset,...
                        hAnalysis, chan);                                        
                    
                    dur = obj.hDataset.Settings.EegSync.EegDisplayTimeSpan;
                    
                    obj.timeOffset = obj.hDataset.Settings.EegSync.EegSyncStart - (dur/2);
                    obj.CurrTime = obj.timeOffset;
                    tEnd = obj.timeOffset + dur;
                    
                    obj.hAxis.XLim = [obj.timeOffset, tEnd];
                end
            end
        end
    end
    
    methods(Access=protected)                
        function RenderFrameIndicator(obj)
            if mod(obj.CurrFrame, obj.hDataset.Settings.EegSync.SyncEveryNthFrame) == 0
                %Compute the current time in the EEG timeline
                time = obj.hDataset.Settings.EegSync.EegSyncStart + obj.hDataset.GetSampleTime(obj.CurrFrame);
                
                 %Get the nearest sync time that is actually present
                obj.CurrTime = obj.hDataset.EEGDataset.GetNearestSyncTime( time );                
            else
                obj.CurrTime = obj.CurrTime + (1/obj.hDataset.MetaData.VolumeRate);
            end
            
            dur = obj.hDataset.Settings.EegSync.EegDisplayTimeSpan;
            tStart = obj.CurrTime - (dur/2);
            obj.hAxis.XLim = [tStart, tStart + dur];
            obj.hFrameIndicator = com.lib.Rendering.RenderVerticalLine(obj.hAxis, obj.hFrameIndicator, obj.CurrTime);
        end
    end
end

