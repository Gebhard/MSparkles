%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDatasetOverviewGraph < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MDATASETOVERVIEWGRAPH Summary of this class goes here
    %   Detailed explanation goes here

    methods
        function obj = MDatasetOverviewGraph(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
        end
    end
    
     methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "12.03.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Dataset overview";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "FilePath", 0, @(x) ( (ischar(x) || isstring(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Analysis", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", 'Dataset overview', @(x) ischar(x) || isstring(x));  
            addParameter(inParser, "Type", 'peak', @(x) any(strcmpi(x, ["peak"; "duration"])));
        end
        
        function RenderGraph(obj)  
            try
                if ~isempty(obj.Params.Resultset.Results)
                    graphPad = com.CalciumAnalysis.Reporting.GetGroupedSignalsTable(obj.Params.Resultset.Results);
                    numTh = numel(obj.Params.Resultset.Results.Thresholds);
                          
                    labels = "TH group " + (1:numTh);
                    labels(end + 1) = "All signals";
                    
                    if ~isempty(graphPad)
                        switch( lower(obj.Params.Type) )
                            case "peak"
                                vals = graphPad(:, 2:4:4*numTh);                       
                                yAxisLabel = sprintf("Signal peak (%s)", obj.Params.Dataset.NormalizationName);
                            case "duration"
                                vals = graphPad(:, 3:4:4*numTh); 
                                yAxisLabel = sprintf("Signal duration (sec)");
                        end

                        obj.renderBoxplots(vals, labels, yAxisLabel);
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
     end
    
    methods(Access=private)                  
        function renderBoxplots(obj, vals, labels, yAxisLabel)
            hParentFig = obj.GetParentFigure();
            hAxis = obj.GetAxis(hParentFig); 
            hGlobalSettings = MGlobalSettings.GetInstance();                
            hAxis.ColorOrder = hGlobalSettings.ThresholdGroupColors;
            
            boxplot(hAxis, vals, "Colors", hGlobalSettings.ThresholdGroupColors, ...
                "Notch", "on", "Symbol", "o");

            %Set the median color
            lines = findobj(hAxis, "type", "line", "Tag", "Median");
            set(lines, "Color", hGlobalSettings.MedianColor);

            hAxis.XTickLabel = labels;
            hAxis.Box = "off";

            ylabel(hAxis, yAxisLabel);
            title(hAxis, obj.Params.Title);
         end
     end
end