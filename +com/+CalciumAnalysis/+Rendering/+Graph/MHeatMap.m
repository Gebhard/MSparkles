%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MHeatMap < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MHEATMAP Summary of this class goes here
    %   Detailed explanation goes here    
    properties(Access=protected)
        hChildPlot;
        hChildAxis;
    end
    
    methods
        function obj = MHeatMap(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
            obj.ShowFrameIndicator = obj.Params.ShowFrameIndicator;
        end
        
        function delete(obj)
            if ~isempty(obj.hChildPlot) && obj.DeleteFigurehandle
                delete( obj.hChildPlot );
            end
        end
    end
    
    methods(Access=protected)
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "Highlight", 0,   @(x) (isnumeric(x) ));
            addParameter(inParser, "FilePath", 0, @(x) ( (ischar(x) || isstring(x)) && any( exist(x, 'file') == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Analysis", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "ColorMap", com.Enum.ColorMapEnum.jet, @(x) (isenum(x)));
            addParameter(inParser, "ShowColorBar", 1, @(x) (isnumeric(x) && any(x==[0 1])));
            addParameter(inParser, "XLabel", "Time (sec)", @(x) ischar(x) || isstring(x));
            addParameter(inParser, "YLabel", "ROI #", @(x) ischar(x) || isstring(x));
            addParameter(inParser, "DataLabel", '', @(x) ischar(x) || isstring(x));
            addParameter(inParser, "Title", '', @(x) ischar(x) || isstring(x));
            addParameter(inParser, "ShowFrameIndicator", false, @(x) islogical(x));
            addOptional(inParser, "LBound", [], @(x) (isempty(x) || isnumeric(x)));
            addOptional(inParser, "UBound", [], @(x) (isempty(x) || isnumeric(x)));
            addOptional(inParser, "UseLBound", false, @(x) islogical(x));
            addOptional(inParser, "UseUBound", false, @(x) islogical(x));
            addOptional(inParser, "UsePercentileScaling", false, @(x) islogical(x));
            addOptional(inParser, "Percentiles", [0, 5], @(x) isnumeric(x) && numel(x) == 2);
            addOptional(inParser, "Rois", [], @(x) (isempty(x) || isnumeric(x)));
            addOptional(inParser, "ChildRenderFunc", [], @(x) (isempty(x) || isa(x, 'function_handle')));
            addOptional(inParser, "EegChan", 2);
            addOptional(inParser, "EegSig", com.EEG.Enum.WaveBand.none);
        end        
        
        function RenderGraph(obj)
            %Check if heatmap is to be drawn in a parent window or as
            %stand-alone figure
            obj.GetParentFigure();
            
            %ToDo, support more paper formats                        
            if isa(obj.hFigure, 'matlab.ui.Figure')            
                obj.hFigure.PaperUnits = 'centimeters';
                obj.hFigure.PaperType = 'A4';
                obj.hFigure.PaperPositionMode = 'manual';            
                obj.hFigure.PaperPosition = [2, 8.85, 17, 16];
            end
            
            try
                hasChildGraph = ~isempty(obj.Params.ChildRenderFunc);
            catch
                hasChildGraph = false;
            end
                        
            %If parent is available, otherwise create a subplot
            
            if isempty(obj.hAxis)
                if isa(obj.Params.Parent, "matlab.ui.control.UIAxes") || ...
                    isa(obj.Params.Parent, "matlab.graphics.axis.Axes")
                    obj.hAxis = obj.Params.Parent;
                    obj.hChildAxis = [];
                else
                    if hasChildGraph                    
                        obj.hAxis = subplot(2, 1, 1, 'parent', obj.Params.Parent);
                        obj.hChildAxis = subplot(2, 1, 2, 'parent', obj.Params.Parent);                      
                    else
                        obj.hAxis = subplot(1, 1, 1, 'parent', obj.Params.Parent);
                        obj.hChildAxis = [];
                    end
                end 
            end
                    
            obj.renderHeatMap(obj.hAxis);               
                        
            if obj.Params.UsePercentileScaling
                obj.Params.Title = sprintf("%s\n P%.1f = %.3f    P50 = %.3f    P%.2f = %.3f", obj.Params.Title,...
                    obj.Params.Percentiles(1), obj.Params.LBound, obj.Params.Median, ...
                    obj.Params.Percentiles(2), obj.Params.UBound);
            end
            
            if (~isempty(obj.Params.Title))
                title(obj.hAxis, obj.Params.Title);
            end            
            
            if (~isempty(obj.Params.YLabel))
                ylabel(obj.hAxis, obj.Params.YLabel);
            end                          
            
            try
                if hasChildGraph && ~isempty(obj.hChildAxis)
                    obj.hChildAxis.Units = 'centimeters';
                    obj.hAxis.Units = 'centimeters';

                    obj.hChildPlot = obj.Params.ChildRenderFunc( obj.hChildAxis, obj.Params);              

                    obj.hChildAxis.Position = [1.5, 1, 11, 2];
                    obj.hAxis.Position = [1.5, 4, 11, 6]; 

                    obj.hFigure.Units = 'normalized';
                    obj.hChildAxis.Units = 'normalized';
                    obj.hAxis.Units = 'normalized';
                else
                    if (~isempty(obj.Params.XLabel));xlabel(obj.hAxis, obj.Params.XLabel);end
                end  
            catch
            end
        end
    end
    
    methods(Access=protected)
        function value = GetVersion(~)
            value = '1.4.000';
        end
        
        function value = GetModifyDate(~)
            value = '11.03.2019';
        end
        
        function value = GetGraphName(~)
            value = 'Heatmap';
        end
        
        function tick = GetTick(~, renderTarget)
            tick = renderTarget.XTick;
        end
        
        function renderHeatMap(obj, renderTarget)  
            obj.renderStaticSignals(renderTarget);
        end
    end    

    methods (Access=protected)
        function renderStaticSignals(obj, renderTarget)
            hResultset = obj.Params.Resultset; 
            
            if ~isempty(obj.Params.Rois)
                signals = hResultset.Results.SigTAvg(obj.Params.Rois,:);
            else
                signals = hResultset.Results.SigTAvg;
            end
            
            obj.render(signals, renderTarget);
        end
        
        function render(obj, signals, renderTarget)
            if ~isempty(signals)   
                lb = [];
                ub = [];
                
                %Crop values if required  
                %either percentile scaling OR manual bounds
                if obj.Params.UsePercentileScaling
                    p = [obj.Params.Percentiles(1), 50, obj.Params.Percentiles(2)];
                    Y = prctile( signals(:), p );
                    lb = Y(1);
                    ub = Y(3);
                    obj.Params.LBound = lb;
                    obj.Params.UBound = ub;
                    obj.Params.Median = Y(2);
                else
                    if (obj.Params.UseLBound == true && ~isempty(obj.Params.LBound))
                        signals(signals < obj.Params.LBound) = obj.Params.LBound;
                        lb = obj.Params.LBound;
                    end

                    if (obj.Params.UseUBound == true && ~isempty(obj.Params.UBound))
                        signals(signals > obj.Params.UBound) = obj.Params.UBound;
                        ub = obj.Params.UBound;
                    end 
                end
                
                if isempty(lb)
                    lb = min(signals(:));
                end
                
                if isempty(ub)
                    ub = max(signals(:));
                end

                if (lb == ub)
                    ub = lb +1;
                end
                
                hDataset = obj.Params.Dataset;
                sz = size(signals);
                
                xAx = hDataset.TimingSignal;
                yAx = 1:sz(1);
                
                imagesc(renderTarget, xAx, yAx, signals);          %Render Heatmap
                renderTarget.CLim = [lb ub];                        %Scale colors
                colormap(renderTarget, char(obj.Params.ColorMap));  %Set selected colormap        

                %Draw colorbar
                if (obj.Params.ShowColorBar)
                    cb = colorbar(renderTarget);
                    cb.Label.String = obj.Params.DataLabel;
                else
                    colorbar(renderTarget, "off");
                end
            end
        end
    end
end

