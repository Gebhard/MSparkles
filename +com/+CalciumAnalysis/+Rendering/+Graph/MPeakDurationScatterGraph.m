%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MPeakDurationScatterGraph < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MPEAKDURATIONSCATTERGRAPH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=protected)
        HighlightData;
    end
    
    methods
        function obj = MPeakDurationScatterGraph(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
            obj.HighlightData = obj.Params.Highlight;
            obj.Params.Highlight = [];
        end        
    end
    
    methods
        function UpdateHighlight(obj, highlightData)
            obj.HighlightData = highlightData;
            obj.Render();
        end
    end
    
    methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "13.03.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Peak-duration scatter plot";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "FilePath", 0, @(x) ( (ischar(x) || isstring(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Analysis", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Highlight", 0, @(x) (isempty(x) || ismatrix(x)) );
            addParameter(inParser, "FaceAlpha", 0.7, @(x) (isnumeric(x) && x>=0 && x<=1) );
            addParameter(inParser, "Title", 'Peak-duration scatter plot', @(x) ischar(x) || isstring(x));
        end
        
        function RenderGraph(obj)
            try
                graphPad = com.CalciumAnalysis.Reporting.GetGroupedSignalsTable(obj.Params.Resultset.Results);                
                ScatterData = [graphPad(:, end - 2), graphPad(:, end-1)];                                                      
                obj.renderScatterPlot(ScatterData);
            catch
            end
        end        
    end
    
    methods (Access=private)        
        function renderScatterPlot(obj, scatterData)
            hParentFig = obj.GetParentFigure();                                                
            hAxis = obj.GetAxis(hParentFig); 
            cla(hAxis);

            hGlobalSettings = MGlobalSettings.GetInstance();
        
            scatter(hAxis, scatterData(:,2), scatterData(:,1), "o", "MarkerFaceColor", hGlobalSettings.ScatterColor,...
                "MarkerFaceAlpha", obj.Params.FaceAlpha, "MarkerEdgeColor", "none");

            xl = hAxis.XLim;
            yl = hAxis.YLim;
            xl(1) = min(xl(1), 0);
            yl(1) = min(yl(1), 0);
            
            hAxis.XLim = xl;
            hAxis.YLim = yl;
            
            [R, P] = corrcoef( scatterData(:,2), scatterData(:,1));            
            str = sprintf("R: %.4E    p-value: %.5E", R(1,2), P(1,2));
            dim = [ (xl(2) - xl(1)) / 15, yl(2) - (yl(2) - yl(1))/ 15];
            
            text(hAxis, dim(1), dim(2), str, "FontSize", 12, "FontWeight", "bold");
            
            hold(hAxis, "on");
            ls = lsline(hAxis);
            ls.Color = hGlobalSettings.TrendLineColor;
            ls.LineWidth = 1;
            hold(hAxis, "off");                            

            ylabel(hAxis, sprintf("Peak (%s)", obj.Params.Dataset.NormalizationName));
            xlabel(hAxis, "Duration (sec)");
                          
            title(hAxis, obj.Params.Title);
            
            if ~isempty(obj.HighlightData)
                hold(hAxis, "on");
                try
                    scatter(hAxis, obj.HighlightData(:,1), obj.HighlightData(:,2), "o",...
                        "MarkerFaceColor", [0.8235, 0.1176, 0.1961],...
                        "MarkerFaceAlpha", obj.Params.FaceAlpha, "MarkerEdgeColor", "none");
                catch
                end
                hold(hAxis, "off");
            end
            
            hAxis.Box = "off"; 
        end
    end
end

