%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalCompositionGraph < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MSIGNALCOMPOSITIONGRAPH Summary of this class goes here
    %   Detailed explanation goes here    
    
    methods
        function obj = MSignalCompositionGraph(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
        end
    end
    
    methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "11.03.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Signal composition";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "FilePath", 0, @(x) ( (isstring(x) || ischar(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", "Signal composition", @(x) ischar(x) || isstring(x));            
        end
        
        function RenderGraph(obj)
            try
                if ~isempty(obj.Params.Resultset.Results)
                    hGlobalSettings = MGlobalSettings.GetInstance();

                    sigComp = com.CalciumAnalysis.Reporting.GetSignalComposition(obj.Params.Resultset.Results);
                    numTh = numel(obj.Params.Resultset.Results.Thresholds);

                    hParentFig = obj.GetParentFigure();
                    hAxis = obj.GetAxis(hParentFig); 
                    hAxis.ColorOrder = hGlobalSettings.ThresholdGroupColors;

                    labels = cell(1, numTh);
                    for i=1:numTh
                        labels{i} = sprintf('TH %d (%.3f)', i, obj.Params.Resultset.Results.Thresholds(i));
                    end

                    ca = categorical(labels);                                

                    hold on;
                    for c=1:numTh
                        bar(ca(c), sigComp(2,c), "Parent", hAxis);
                    end
                    hold off;                

                    ylim(hAxis, [0 100]);
                    title(hAxis, obj.Params.Title);
                    ylabel(hAxis, "% of signals");
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
end