%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalDurationHeatMap < com.CalciumAnalysis.Rendering.Graph.MHeatMap
    %MSIGNALDURATIONHEATMAP Summary of this class goes here
    %   Detailed explanation goes here    
        
    
    properties(Transient, Dependent)
        InterpFactor;
    end
    
    methods
        function obj = MSignalDurationHeatMap(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MHeatMap(varargin{:});
        end
    end    
    
    methods
        function value = get.InterpFactor(~)
            value = 4;
        end
    end
    
    methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.4.000";
        end
        
        function value = GetModifyDate(~)
            value = "11.03.2019";
        end
        
        function value = GetGraphName(~)
            value = "Signal diration heatmap";
        end         
        
        function tick = GetTick(obj, renderTarget)
            tick = renderTarget.XTick / obj.InterpFactor;
        end        
    end   
    
    methods (Access=protected)       
        function renderStaticSignals(obj, renderTarget)
            hDataset = obj.Params.Dataset;
            hResultset = obj.Params.Resultset;    

            [~, sigStartEnd, peakVals] = hResultset.GetClassifiedSignals();
            
            if ~isempty(sigStartEnd) && ~isempty(peakVals)            
                endTimes = hDataset.MetaData.SamplesT;
                maxEndTime = obj.InterpFactor * ceil( max( endTimes(:) ) );
                s = size(peakVals);
                signals = zeros( s(1) , maxEndTime);

                try
                    for j=1:s(2)
                        for i=1:s(1)
                        startIdx = round(sigStartEnd(i,j,1));
                        endIdx = round(sigStartEnd(i,j,2));
                        
                            if ~isnan(startIdx) && ~isnan(endIdx)
                                startIdx = startIdx * obj.InterpFactor;
                                endIdx = min(endIdx * obj.InterpFactor, maxEndTime);

                                signals(i, startIdx:endIdx ) = peakVals(i,j);
                            end
                        end
                    end
                catch
                    fprintf('J=%d  I=%d\n', j, i);
                end         

                obj.render(signals, renderTarget);
            end
        end
        
        function render(obj, signals, renderTarget)
            hResultset = obj.Params.Resultset;
            try
                if isempty(hResultset.Results.Thresholds)
                    numTh = 1;
                else
                    numTh = numel(hResultset.Results.Thresholds);
                end
            catch
                numTh = 1;
            end
            
            hGlobalSettings = MGlobalSettings.GetInstance();
            map = [1,1,1; hGlobalSettings.ThresholdGroupColors(1:numTh, :)];            

            hDataset = obj.Params.Dataset;
            sz = size(signals);
            xAx = 0:hDataset.LastSampleTime/4:hDataset.LastSampleTime;
            yAx = 1:sz(1);
                
            imagesc(renderTarget,xAx, yAx, signals, [0,numTh]);
            colormap(renderTarget, map);            
                        
             %Draw colorbar
            if (obj.Params.ShowColorBar)
                cb = colorbar(renderTarget);
                cb.Label.String = "Threshold group"; %obj.Params.DataLabel;
                cb.Ticks = 0:numTh;        
            else
                colorbar(renderTarget, 'off');
            end
        end
    end
end

