%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSparklesGraph < com.common.Rendering.Graph
    %MSPARKLESGRAPH Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Dependent)
        Modify_date;
        Graph_version;
    end
    
    properties (SetAccess=protected)
        Params
        hFigure;        
    end
    
    properties
        DeleteFigurehandle; 
        ShowFrameIndicator = false;    
    end

    methods
        function obj = MSparklesGraph(varargin)
            obj.ParseInput(varargin{:});
            obj.Render();
        end  
        
        function delete(obj)
            if obj.DeleteFigurehandle
                obj.hFigure.delete();
            end
            
            obj.DeleteFrameIndicator();
            
            obj.Params = [];
        end
    end
    
    methods
        function set.ShowFrameIndicator(obj, value)
            obj.ShowFrameIndicator = value;
            obj.RenderFrameIndicator();
        end
    end
    
    methods
        function Render(obj)
            if ~isempty(obj.Params.Dataset) && ~isempty(obj.Params.Resultset)
                try
                    obj.RenderGraph();
                    
                    disableDefaultInteractivity(obj.hAxis);
                    try
                        axtoolbar(obj.hAxis, {'export', 'zoomin', 'zoomout'}); %2020a
                    catch
                        try
                            axtoolbar(obj.hAxis, {'zoomin', 'zoomout'}); %2019b
                        catch
                        end
                    end
                catch
                end
            end
        end
    end
    
    methods(Access=protected)
         function DeleteAxis(obj)
            if obj.DeleteFigurehandle
                DeleteAxis@com.common.Rendering.Graph(obj);
            end
         end
        
        function RenderFrameIndicator(obj)
            if obj.ShowFrameIndicator                               
                obj.hFrameIndicator = com.lib.Rendering.RenderVerticalLine(obj.hAxis, obj.hFrameIndicator, obj.CurrFrame);
            else
                obj.DeleteFrameIndicator();
            end
        end                            
        
        function hFigure = GetParentFigure(obj)
            if (obj.Params.Parent == 0)                
                if (~isempty(obj.Params.FilePath) && ischar(obj.Params.FilePath) )
                    [fPath, fName, ~] = fileparts(obj.Params.FilePath);
                    strFileName = sprintf("%s - %s", obj.GetGraphName(), fName);
                    obj.Params.Parent = figure("name", strFileName, "FileName", [fPath strFileName], "Visible", "off");
                else
                    obj.Params.Parent = figure("name", obj.GetGraphName(), "Visible", "off");
                end
                obj.DeleteFigurehandle = true;
            else
                obj.DeleteFigurehandle = false;
            end
            
            obj.hFigure = obj.Params.Parent;
            hFigure = obj.hFigure;
        end
        
        function hAxis = GetAxis(obj, hParentFig)
            if nargin == 2 && ~obj.IsAxisValid
                if isa(hParentFig, "matlab.ui.control.UIAxes") || ...
                        isa(hParentFig, "matlab.graphics.axis.Axes")                 	
                    hAxis = hParentFig; 
                else
                    hAxis = axes("Parent", hParentFig);
                end
                
                obj.hAxis = hAxis;
            else
                hAxis = GetAxis@com.common.Rendering.Graph(obj);
            end
        end
    end
    
    methods(Access=private)        
        function ParseInput(obj, varargin)
            className =  class(obj);
            
            inParser = inputParser;
            inParser.StructExpand    = true;  % if we allow parameter  structure expanding            
            inParser.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.            
            inParser.FunctionName = sprintf("%s version %s (%s)", className, obj.GetVersion(), obj.GetModifyDate());
            
            obj.SetInputParserParams(inParser);
            
             % input parsing
            parse(inParser,varargin{:});
            obj.Params = inParser.Results;
        end
    end
    
    methods(Abstract, Access=protected)
        SetInputParserParams(obj, inParser);
        RenderGraph(obj);
        value = GetVersion(obj);
        value = GetModifyDate(obj);
        value = GetGraphName(obj);
    end
end

