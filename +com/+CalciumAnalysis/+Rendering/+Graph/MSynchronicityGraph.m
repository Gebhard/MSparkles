%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSynchronicityGraph < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MSYNCHRONICITYGRAPH Summary of this class goes here
    %   Detailed explanation goes here    
    
    methods
        function obj = MSynchronicityGraph(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
            obj.ShowFrameIndicator = obj.Params.ShowFrameIndicator;
        end
    end
    
     methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "13.03.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Synchronicity graph";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", [],   @(x) (x == 0) || (~isempty(x) && isobject(x)));          
            addParameter(inParser, "FilePath", 0, @(x) ( (isstring(x) || ischar(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Analysis", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", "", @(x) ischar(x) || isstring(x));   
            addParameter(inParser, "XLabel", "Time (sec)", @(x) ischar(x) || isstring(x)); 
            addParameter(inParser, "XTick", [], @(x) isnumeric(x)); 
            addParameter(inParser, "OwnXAxes", false, @(x) islogical(x));
            addParameter(inParser, "ShowFrameIndicator", false, @(x) islogical(x));
        end
        
        function RenderGraph(obj)
            hGlobalSettings = MGlobalSettings.GetInstance();           
            hDataset = obj.Params.Dataset;
            
            hParentFig = obj.GetParentFigure();  
            
            hAxis = obj.GetAxis(hParentFig); 
            
            cla(hAxis);            
            
            if isempty(obj.Params.Resultset.SynchronicityIndex)
                obj.Params.Resultset.AnalyzeSynchronicity();
            end        
            
            plot(hAxis, hDataset.TimingSignal, obj.Params.Resultset.SynchronicityIndex,...
                "Color", hGlobalSettings.LineColor, "LineWidth", 1.5);
            
            %get frame indices
            [fS, fE, pS, tP] = arrayfun(@(x) getSyncPeakVals(x), obj.Params.Resultset.SynchronicityPeaks);

            function [fStart, fEnd, pSync, tPeak] = getSyncPeakVals(syncPeak)
                [~, fStart] = min(abs(hDataset.TimingSignal - syncPeak.PeakStartEnd(1)));
                [~, fEnd] = min(abs(hDataset.TimingSignal - syncPeak.PeakStartEnd(2)));
                pSync = syncPeak.Synchronicity;
                tPeak = syncPeak.PeakTime;
            end

            if ~isempty(fS)
                hold(hAxis, "on");

                for ii=1:numel(fS)
                    fStart = fS(ii);
                    fEnd = fE(ii);
                    xCoord = [hDataset.TimingSignal(fStart), hDataset.TimingSignal(fStart:fEnd), hDataset.TimingSignal(fEnd)];
                    yCoord = [0,obj.Params.Resultset.SynchronicityIndex(fStart:fEnd),0];
                    patch(hAxis, xCoord, yCoord, [0 0.4470 0.7410], "FaceColor", [0 0.4470 0.7410], "FaceAlpha", 0.5, "EdgeColor", "none");
                end
                
                scatter(hAxis, tP, pS,...
                        "filled", "MarkerEdgeColor", "none",...
                        "MarkerFaceColor", [0.6350 0.0780 0.1840]);

                yCoord = [hDataset.AnalysisSettings.Analysis.SyncClassificationTH,hDataset.AnalysisSettings.Analysis.SyncClassificationTH];
                xCoord = [hDataset.TimingSignal(1), hDataset.TimingSignal(end)];
                line(hAxis, xCoord, yCoord, "Color", [0.8500 0.3250 0.0980], "LineWidth", 1.5, "LineStyle", "--");

                hold(hAxis, "off");
            end

            ylabel(hAxis, "Synchronicity");
            xlabel(hAxis, obj.Params.XLabel);
            
            if ~isempty(obj.Params.Title)
                title(hAxis, obj.Params.Title)
            end
            
            hAxis.Box = "off";
            hAxis.YLim = [0, 1.05];
            hAxis.XLim = [0, hDataset.LastSampleTime];

            if ~obj.Params.OwnXAxes
                hAxis.XTick = obj.Params.XTick;            
            end
            
            hAxis.YTick = [0, 0.2, 0.4, 0.6, 0.8, 1];
        end
    end
end