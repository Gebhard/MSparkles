%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MTracePlot < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MTRACEPLOT Summary of this class goes here
    %   Detailed explanation goes here    
    
    properties
        Traces;
    end
    
    methods
        function obj = MTracePlot(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
        end               
    end
    
    methods(Access=protected) 
        function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "13.03.2019";
        end     
        
        function value = GetGraphName(~)
            value = "Trace plot";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "FilePath", 0, @(x) ( (isstring(x) || ischar(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", '', @(x) ischar(x) || isstring(x));    
            addParameter(inParser, "PlotID", '', @(x) isnumeric(x) && all(x>0));   
            addParameter(inParser, "LineWidth", 1, @(x) isnumeric(x) && x>0);  
            addParameter(inParser, "MarkerSize", 36, @(x) isnumeric(x) && x>0);
            addParameter(inParser, "ShowRiseAndDecay", true, @(x) islogical(x)); 
            addParameter(inParser, "Grid", true, @(x) islogical(x));
            addParameter(inParser, "ShowPeaks", true, @(x) islogical(x));
            addParameter(inParser, "ShowStdDev", true, @(x) islogical(x));
            addParameter(inParser, "Show3DTraces", false, @(x) islogical(x));
            addParameter(inParser, "Rotation3D", 30, @(x) x >= -90 && x <= 90);
            addParameter(inParser, "Pitch3D", 30, @(x) x >= -90 && x <= 90);
            addParameter(inParser, "UseRoiColors", true, @(x) islogical(x));
            addParameter(inParser, "RoiColormap", []);
            addParameter(inParser, "RoiColorIdx", []);
            addParameter(inParser, "HighlightSpan", []);
            addParameter(inParser, "HighlightColor", []);
            addParameter(inParser, "XLim", []);
            addParameter(inParser, "YLim", []);
        end
        
        function RenderGraph(obj)
            hGlobalSettings = MGlobalSettings.GetInstance();             
            hDataset = obj.Params.Dataset;  
            
            if ~isempty(obj.Params.YLim)
                lB = obj.Params.YLim(1);
                uB= obj.Params.YLim(2);
            else
                lB = obj.Params.Resultset.Results.SigBounds(1);
                uB = obj.Params.Resultset.Results.SigBounds(2); 
            end
            
            hParentFig = obj.GetParentFigure();
            hAxis = obj.GetAxis(hParentFig);                                    
            
            cla(hAxis);

            numPlots = numel(obj.Params.PlotID);

            if numPlots > 1 && obj.Params.UseRoiColors
                colMap = obj.Params.Resultset.hParent.AnalysisSettings.GetColormap( ...
                    max(obj.Params.Resultset.RoiCount), obj.Params.RoiColormap, obj.Params.RoiColorIdx);
            else
                colMap = [];
            end            
            
            hold(hAxis, "on");
            
            obj.Traces = [];                        
            
            for i=1:numPlots
                id = obj.Params.PlotID(i);
                if ~isempty(colMap)
                    color = colMap(id, :);
                else
                    color = hGlobalSettings.LineColor;
                end
                
                if obj.Params.Show3DTraces
                    [lB, uB, hTrace] = obj.renderTrace3D(hAxis, hDataset, id, i, hGlobalSettings, lB, uB, color);
                    obj.Traces = [obj.Traces, hTrace];                                       
                else
                    [lB, uB, hTrace] = obj.renderTrace(hAxis, hDataset, id, hGlobalSettings, lB, uB, color);
                    obj.Traces = [obj.Traces, hTrace];
                end
            end
            
            if ~isempty(obj.Params.HighlightSpan) && ~isempty(obj.Params.HighlightColor)
                if ~obj.Params.Show3DTraces
                    xCoord = [obj.Params.HighlightSpan(1), obj.Params.HighlightSpan(1), obj.Params.HighlightSpan(2), obj.Params.HighlightSpan(2)];
                    yCoord = [hAxis.YLim(1), hAxis.YLim(2), hAxis.YLim(2), hAxis.YLim(1)];
                    
                    patch(hAxis, 'XData', xCoord, 'YData', yCoord,...
                        'FaceColor', obj.Params.HighlightColor, 'FaceAlpha', 0.5, ...
                        'EdgeColor', obj.Params.HighlightColor, 'LineWidth', 0.5);
                end
            end

            hold(hAxis, "off");
            
            xlim(hAxis, [0, hDataset.LastSampleTime]);
            
            if obj.Params.Show3DTraces
                hAxis.View = [obj.Params.Rotation3D, obj.Params.Pitch3D];
                hAxis.YLim = [0, numPlots + 1];
                hAxis.ZLim = [lB, uB];
            else                
                hAxis.YLim = [lB, uB];
                hAxis.View = [0, 90];
                hAxis.ZLim = [0, 1];
            end
            
            if ~isempty(obj.Params.XLim)
                hAxis.XLim = obj.Params.XLim;
            end
                                    
            if obj.Params.Grid
                hAxis.XGrid = 'on';
                hAxis.YGrid = 'on';
                hAxis.ZGrid = 'on';
            end
        end
    end
    
    methods(Access=private)
        function [lB, uB, hTrace] = renderTrace(obj, hAxis, hDataset, plotID, hGlobalSettings, lB, uB, color)
            currSig = obj.Params.Resultset.Results.GetRoiTrace(plotID);
            currStd = obj.Params.Resultset.Results.GetRoiTraceStd(plotID);
            
            hTrace = plot(hAxis, hDataset.TimingSignal, currSig, "Color", color, "LineWidth", obj.Params.LineWidth);  
            
            if obj.Params.ShowStdDev
                polyLow = currSig-currStd;
                polyHigh = currSig+currStd;
                
                if ~isrow(polyLow)
                    polyLow = polyLow';
                end
                
                if ~isrow(polyHigh)
                    polyHigh = polyHigh';
                end
                
                fill(hAxis,...
                        [hDataset.TimingSignal fliplr(hDataset.TimingSignal)],...
                        [polyHigh fliplr(polyLow)],...
                        hGlobalSettings.LineColor,...
                        'FaceAlpha', 0.3,...
                        'linestyle','none');

                lB = min(lB, min(polyLow));
                uB = max(uB, max(polyHigh));
            end
            
            if obj.Params.ShowRiseAndDecay
                try
                    p90 = obj.Params.Resultset.Results.P90IntersectPoints(plotID, :);
                    p90Idx = round(cat(1, p90{:}));

                    pX = obj.Params.Resultset.Results.PxIntersectPoints(plotID, :);
                    pXIdx = round(cat(1, pX{:}));

                    sz = size(pXIdx);

                    for r=1:sz(1)
                        riseIdx = pXIdx(r,1) : p90Idx(r,1);
                        decayIdx = p90Idx(r,2) : pXIdx(r,2);

                        plot(hAxis, hDataset.TimingSignal(riseIdx), currSig(riseIdx), "Color", 'g',...
                            "LineWidth", obj.Params.LineWidth);

                        plot(hAxis, hDataset.TimingSignal(decayIdx), currSig(decayIdx), "Color", 'r',...
                            "LineWidth", obj.Params.LineWidth);
                    end
                catch
                end
            end
            
            if obj.Params.ShowPeaks
                if ~isempty(obj.Params.Resultset.Results.SigTAvg)               
                    sig = obj.Params.Resultset.Results;                                         
                    markerType = "o";
                    colors = hGlobalSettings.ThresholdGroupColors;                                        
                    sigCounts = zeros(numel(sig.Thresholds),1);

                    dh = obj.Params.Dataset.Settings.Analysis.DurationHeight;

                    numTh = numel(sig.Thresholds);
                    if numTh == 0 && ~isempty(sig.SigMax)
                        szTh = size(sig.SigMax);
                        numTh = szTh(2);
                    end

                    for j=1:numTh
                        if iscell(sig.SigMax)
                            peaks = sig.SigMax{plotID, j};
                        else
                            peaks = sig.SigMax(plotID, j);
                        end

                        if (~isempty(peaks)) 
                            if iscell(sig.PeakTime)
                                locs = sig.PeakTime{plotID, j};
                            else
                                locs = sig.PeakTime(plotID, j);
                            end

                            try
                                if ~isempty(sig.PxIntersectPoints) && ~isempty(sig.Prominence)
                                    intrPts = (sig.PxIntersectPoints{plotID, j}' - 1) ./ obj.Params.Dataset.MetaData.VolumeRate;
                                    proms = sig.Prominence{plotID, j};                               

                                    %Draw durations
                                    yLoc = repmat( (peaks - (proms - proms * dh))', 2, 1);
                                    line(hAxis, intrPts, yLoc, "Color", hGlobalSettings.LineColor);

                                    %Draw prominences
                                    y = [peaks, peaks-proms]';
                                    x = [locs, locs]';
                                    line(hAxis, x, y, "Color", hGlobalSettings.LineColor);
                                end
                            catch
                            end

                            scatter(hAxis, locs, peaks, markerType, "filled", "MarkerFaceColor", colors(j, :),...
                                "SizeData", obj.Params.MarkerSize);

                            sigCounts(j) = numel(peaks);
                        else
                            sigCounts(j) = 0;
                        end
                    end            
                end    
            end
        end                
        
        function [lB, uB, hTrace] = renderTrace3D(obj, hAxis, hDataset, plotID, zPos, hGlobalSettings, lB, uB, color)
            currSig = obj.Params.Resultset.Results.GetRoiTrace(plotID);
            currStd = obj.Params.Resultset.Results.GetRoiTraceStd(plotID);
            
            z = ones(1,numel(currSig)) * zPos;
            hTrace = plot3(hAxis, hDataset.TimingSignal, z, currSig, "Color", color, "LineWidth", obj.Params.LineWidth);  

            if obj.Params.ShowStdDev
                polyLow = currSig-currStd;
                polyHigh = currSig+currStd;
                
                if ~isrow(polyLow)
                    polyLow = polyLow';
                end
                
                if ~isrow(polyHigh)
                    polyHigh = polyHigh';
                end
                
                fill3(hAxis,...
                        [hDataset.TimingSignal fliplr(hDataset.TimingSignal)],...
                        [z z],...
                        [polyHigh fliplr(polyLow)],...
                        hGlobalSettings.LineColor,...
                        'FaceAlpha', 0.3,...
                        'linestyle','none');

                lB = min(lB, min(polyLow));
                uB = max(uB, max(polyHigh));
            end
            
            if obj.Params.ShowRiseAndDecay
                try
                    p90 = obj.Params.Resultset.Results.P90IntersectPoints(plotID, :);
                    p90Idx = round(cat(1, p90{:}));

                    pX = obj.Params.Resultset.Results.PxIntersectPoints(plotID, :);
                    pXIdx = round(cat(1, pX{:}));

                    sz = size(pXIdx);

                    for r=1:sz(1)
                        riseIdx = pXIdx(r,1) : p90Idx(r,1);
                        decayIdx = p90Idx(r,2) : pXIdx(r,2);

                        plot3(hAxis, hDataset.TimingSignal(riseIdx), z(riseIdx), currSig(riseIdx), "Color", 'g',...
                            "LineWidth", obj.Params.LineWidth);

                        plot3(hAxis, hDataset.TimingSignal(decayIdx), z(decayIdx), currSig(decayIdx), "Color", 'r',...
                            "LineWidth", obj.Params.LineWidth);
                    end
                catch
                end
            end
            
            if obj.Params.ShowPeaks
                if ~isempty(obj.Params.Resultset.Results.SigTAvg)               
                    sig = obj.Params.Resultset.Results;                                         
                    markerType = "o";
                    colors = hGlobalSettings.ThresholdGroupColors;                                        
                    sigCounts = zeros(numel(sig.Thresholds),1);

                    dh = obj.Params.Dataset.Settings.Analysis.DurationHeight;

                    numTh = numel(sig.Thresholds);
                    if numTh == 0 && ~isempty(sig.SigMax)
                        szTh = size(sig.SigMax);
                        numTh = szTh(2);
                    end

                    for j=1:numTh
                        if iscell(sig.SigMax)
                            peaks = sig.SigMax{plotID, j};
                        else
                            peaks = sig.SigMax(plotID, j);
                        end

                        if (~isempty(peaks)) 
                            if iscell(sig.PeakTime)
                                locs = sig.PeakTime{plotID, j};
                            else
                                locs = sig.PeakTime(plotID, j);
                            end

                            try
                                
                                if ~isempty(sig.PxIntersectPoints) && ~isempty(sig.Prominence)
                                    intrPts = (sig.PxIntersectPoints{plotID, j}' - 1) ./ obj.Params.Dataset.MetaData.VolumeRate;
                                    proms = sig.Prominence{plotID, j};                               
                                    zp = ones(size(intrPts)) .* zPos;
                                    
                                    %Draw durations
                                    yLoc = repmat( (peaks - (proms - proms * dh))', 2, 1);
                                    line(hAxis, intrPts, zp, yLoc, "Color", hGlobalSettings.LineColor);

                                    %Draw prominences
                                    y = [peaks, peaks-proms]';
                                    x = [locs, locs]';
                                    line(hAxis, x, zp, y, "Color", hGlobalSettings.LineColor);
                                end
                            catch
                            end

                            zp = ones(size(locs)) .* zPos;
                            scatter3(hAxis, locs, zp, peaks, markerType, "filled", "MarkerFaceColor", colors(j, :),...
                                "SizeData", obj.Params.MarkerSize);

                            sigCounts(j) = numel(peaks);
                        else
                            sigCounts(j) = 0;
                        end
                    end            
                end    
            end
        end
    end
end

