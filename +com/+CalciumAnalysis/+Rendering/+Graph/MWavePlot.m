classdef MWavePlot < com.CalciumAnalysis.Rendering.Graph.MSparklesGraph
    %MWAVEPLOT Summary of this class goes here
    %   Detailed explanation goes here   
    
    methods
        function obj = MWavePlot(varargin)
            obj = obj@com.CalciumAnalysis.Rendering.Graph.MSparklesGraph(varargin{:});
        end
    end
    
    methods(Access= protected)
         function value = GetVersion(~)
            value = "1.0.000";
        end
        
        function value = GetModifyDate(~)
            value = "23.09.2020";
        end     
        
        function value = GetGraphName(~)
            value = "Wave plot";
        end
        
        function SetInputParserParams(~, inParser)
            addParameter(inParser, "Parent", 0,   @(x) (x == 0) || (~isempty(x) && isobject(x)));            
            addParameter(inParser, "FilePath", 0, @(x) ( (isstring(x) || ischar(x)) && any( exist(x, "file") == [2 7])) );
            addParameter(inParser, "Dataset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Resultset", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Analysis", 0, @(x) (~isempty(x)) );
            addParameter(inParser, "Title", '', @(x) ischar(x) || isstring(x));    
            addParameter(inParser, "PlotID", '', @(x) isnumeric(x) && all(x>0));   
            %addParameter(inParser, "LineWidth", 1, @(x) isnumeric(x) && x>0);  
            %addParameter(inParser, "MarkerSize", 36, @(x) isnumeric(x) && x>0);
            %addParameter(inParser, "ShowRiseAndDecay", true, @(x) islogical(x)); 
            addParameter(inParser, "Grid", true, @(x) islogical(x));
            %addParameter(inParser, "ShowPeaks", true, @(x) islogical(x));
            %addParameter(inParser, "ShowStdDev", true, @(x) islogical(x));
            %addParameter(inParser, "Show3DTraces", false, @(x) islogical(x));
            %addParameter(inParser, "Rotation3D", 30, @(x) x >= -90 && x <= 90);
            %addParameter(inParser, "Pitch3D", 30, @(x) x >= -90 && x <= 90);
            %addParameter(inParser, "UseRoiColors", true, @(x) islogical(x));
            addParameter(inParser, "RoiColormap", "jet");
            %addParameter(inParser, "RoiColorIdx", []);
            %addParameter(inParser, "HighlightSpan", []);
            %addParameter(inParser, "HighlightColor", []);
            addParameter(inParser, "XLim", []);
            addParameter(inParser, "YLim", []);
        end
        
        function RenderGraph(obj)
            hTarget     = obj.Params.Parent;
            hAnalysis   = obj.Params.Analysis;
            hResultset  = obj.Params.Resultset;
            hDataSet    = obj.Params.Dataset;
            RoiID       = obj.Params.PlotID;
            
            settings = hAnalysis.AnalysisSettings;        
            results = hResultset.Results;

            cla(hTarget);

            if ~isempty(results)                                    
                hold(hTarget, 'on');

                startF = results.StartFrame';

                idx = ~cellfun(@isempty, startF);
                startFrame = cat(1, startF{idx});
                for i=1:numel(RoiID)
                    plotIdx = RoiID(i);
                    MorphArea = zeros(1, hDataSet.MetaData.SamplesT);
                    sf = round(startFrame(plotIdx));

                    if ~isempty(sf)
                        a = results.Areas{plotIdx};
                        fSpan = numel(a);

                        MorphArea(1, sf:(sf+fSpan-1)) = a;
                        MorphMean = results.GetRoiTrace(plotIdx);

                        switch(settings.SignalScaling)
                            case com.Enum.SignalScalingEnum.Global
                                m = hResultset.MaxArea;
                            case com.Enum.SignalScalingEnum.Individual
                                m = max(MorphArea(:));
                            case com.Enum.SignalScalingEnum.UserDefined
                                m = settings.UserDefAreaScaling;
                        end            

                        try
                            com.CalciumAnalysis.Rendering.Graph.CaViolin(hTarget, hDataSet, MorphArea, ...
                                MorphMean, max(hResultset.MaxF), max(min(hResultset.MinF), 0));
                        catch
                        end

                        if ~isempty(obj.Params.XLim)
                            hTarget.XLim = obj.Params.XLim;
                        else
                            hTarget.XLim = [0, hDataSet.LastSampleTime];
                        end
                        
                        if ~isempty(obj.Params.YLim)
                            hTarget.YLim = obj.Params.YLim;
                        else
                            hTarget.YLim = [-(m+1)/2, (m+1)/2];
                        end

                        if obj.Params.Grid
                            hTarget.XGrid = 'on';
                            hTarget.YGrid = 'on';     
                        else
                             hTarget.XGrid = 'off';
                            hTarget.YGrid = 'off';
                        end
                    end
                end

                hold(hTarget, 'off');
            end
        end
    end
end

