%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function idx = GetClickedRoiIdx(obj, clickPoint)
    idx = -1; 
    
    if ~isempty(obj.hDataset)                                       
        try
            hResultset = obj.hAnalysis.GetResultset(obj.CurrentChannel);
            
            if ~isempty(hResultset)
                groups = hResultset.ROIs;  

                if ~isempty(groups)
                    pt = floor(clickPoint);

                    pt(1:2) = floor(pt(1:2) ./ [obj.hDataset.MetaData.SampleSizeX, obj.hDataset.MetaData.SampleSizeY]);
                    
                    if obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic
                        pxlIdx = sub2ind(groups.ImageSize, pt(2), pt(1),...
                                    obj.CurrentChannel, obj.CurrentZPos, 1, ...
                                    obj.CurrentFrame);
                    else
                        pxlIdx = pt(1) * groups.ImageSize(1) + pt(2);
                    end
                    
                    idx = find(cellfun (@(x) ismember(pxlIdx, x), groups.PixelIdxList));                    
                    
                    if isempty(idx)
                        idx = -1;
                    end                    
                end
            end
        catch
            idx = -1; 
        end              
    end                            
end