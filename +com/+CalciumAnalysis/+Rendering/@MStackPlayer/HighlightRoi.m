%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function HighlightRoi(obj, roiIdx)
    try
        chan = obj.CurrentChannel(1);
        hResultset = obj.hAnalysis.GetResultset(chan);
        box = [];
        
        if ~isempty(hResultset)
            groups = hResultset.ROIs;                

            if roiIdx > 0 && ~isempty(groups)
                %Extract current ROI
                idx = groups.PixelIdxList{roiIdx}(:);

                %Convert to slices
                [x,y,~,~,~,t] = ind2sub(groups.ImageSize, idx); 

                if obj.hAnalysis.AnalysisType ~= com.Enum.AnalysisType.Dynamic  
                    tIdx = t > 0;
                else
                    tIdx = t==obj.CurrentFrame;
                end
                
                if any(tIdx>0)        
                    [xMin, xMax] = bounds(x(tIdx));
                    [yMin, yMax] = bounds(y(tIdx));

                    xMin = xMin - 0.5;
                    yMin = yMin - 0.5;
                    xMax = xMax + 0.5;
                    yMax = yMax + 0.5;

                    box.BoundingBox = [yMin, xMin, yMax-yMin, xMax-xMin];                

                    %convert from pixel coordinates to physical coordinates
                    t = [obj.hDataset.MetaData.SampleSizeX, obj.hDataset.MetaData.SampleSizeY,...
                        obj.hDataset.MetaData.SampleSizeX, obj.hDataset.MetaData.SampleSizeY];

                    box.BoundingBox = box.BoundingBox .* t;
                end
            end
        end
    catch
        box = [];
    end 
    
    obj.SelectionBox = box;       
end

