%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStackPlayer < com.common.UI.UiControl
    %MSTACKPLAYER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        WindowPosition = [0.13, 0.1, 0.775, 0.8];
    end
    
    properties(Access=protected)
        mRoiMapBuffer = [];
        mRenderBuffer = [];
        mStackSize = [0 0 0];
        
        mPlaybackTimer;
        mParent;                               
        
        hCurrRoi = [];
        ManualRoiType = com.CalciumAnalysis.Enum.ManualRoiEnum.None;
        
        hShaderFunc;
        RenderMode = '';
        RenderData = []; 
        RenderTarget = [];
        RoiColormapListener = [];
        SelectionMarker = [];
        
        mOverlayAlpha = 0.5;        % ROI / signal transparency     %Dataset.DisplaySettings    
        mShowAnnotations = true;    % Show / hide annotations
        mShowGrid;
        mLutLevels = 1024; %Dataset.DisplaySettings
        hListeners;
    end
    
    properties(SetAccess=protected)
        mAxes = [];
        LutName = 'Gray';
        SubPlots = {};
    end
    
    properties(SetObservable, AbortSet=true)
        CurrentFrame = 1; 
        CurrentChannel = 1;
        CurrentZPos = 1;        
        LastMousePos = [0, 0];                 
        IsPlaying;
        LoopPlayback = false;
    end  
    
    properties(AbortSet=true)
        hAnalysis;
        ShowTrajectories;
    end
    
    properties(SetObservable, Dependent)
        % Properties settable from outside, which ensure that a redraw call
        % is issued.
        OverlayAlpha;
        ShowROIs;
    end       
    
    properties
        SelectedRoiIdx;
        AddRoiCallback;
        CutRoiCallback;
        MergeRoiCallback;
        DisplayStackName = "F"; 
        GradientColorLUT; %Dataset.DisplaySettings
        SelectionBox = [];
        UseLUT = false; %Dataset.DisplaySettings
        ShowMerge = false;
    end
    
    properties(Access=protected, Hidden)
        Params;
    end
    
    methods
        function obj = MStackPlayer(varargin)
            %MSTACKPLAYER Construct an instance of this class
            %   Detailed explanation goes here
            obj.hListeners = {};
            
            MStackPlayer_version = '15.05.2019';
            MStackPlayer_date = '1.0.000';            
            
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding
            p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['MStackPlayer v.' MStackPlayer_version ' (' MStackPlayer_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addParameter(p,'Position', [],  @(x) (isnumeric(x) && numel(x) == 4 ));
            addParameter(p,'Name',     [],  @(x) (ischar(x) && ~isempty(x) ));           
            addParameter(p,'Parent',   [],  @(x) ~isempty(x) );
            addParameter(p,'AppFigure', [],  @(x) isempty(x) || isa(x, 'com.common.UI.AppFigure') );
            
            % input parsing
            parse(p,varargin{:});
            obj.Params = p.Results;                                      
            
            obj.hAppFigure = obj.Params.AppFigure; 
            obj.UseLUT = true;      %Dataset.DisplaySettings                              
            obj.mShowGrid = true;
            obj.CurrentFrame = 1;
            obj.mStackSize = [0 0 0]; %Dataset.DisplaySettings
            obj.mPlaybackTimer = [];
            obj.mParent = obj.Params.Parent;                        
            obj.mShowAnnotations = true; %Dataset.DisplaySettings
            
            sz  = [obj.Params.Position(3), obj.Params.Position(4)];
            
            AxisMargin = obj.GetAxisMargin();
            
            %create axes to display images
            axPos = [0 + AxisMargin(1) ,  0 + AxisMargin(2), sz(1) - (AxisMargin(1) + AxisMargin(3)),...
                sz(2) - (AxisMargin(2) + AxisMargin(4)) ];

            obj.mAxes = obj.CreateAxes(axPos);            

            obj.hListeners{1} = addlistener(obj.hDataset, 'F', 'PostSet', @obj.OnFChanged);             
            obj.hListeners{2} = addlistener(obj.mAxes, 'ButtonDownFcn', 'PreSet', @obj.OnSetBtnDwnFcn);             
            obj.hListeners{3} = addlistener(obj.hAppFigure, 'SelectedRoi', 'PostSet', @obj.OnRoiSelectionChanged);

            obj.DisplayStackName = "F";
            obj.SetColorLUT('Gray');
        end
        
        function delete(obj)
            obj.DeleteColormapListener();
            obj.StopPlaybackTimer();
            
            numListeners = numel(obj.hListeners);
            for i=1:numListeners
                try
                    delete( obj.hListeners{i} );
                catch
                end
            end
            
            try
                obj.hDataset.Annotations.DeleteShapes();
            catch
            end
            
            delete ( obj.mAxes );
        end
    end
    
    methods 
        function set.ShowTrajectories(obj, value)
            obj.ShowTrajectories = value;
            obj.RenderAndDisplay();
        end
        
        function value = get.ShowTrajectories(obj)
            if isempty(obj.ShowTrajectories)
                value = true;
            else
                value = obj.ShowTrajectories;
            end
        end
        
        function set.ShowMerge(obj, value)
            obj.ShowMerge = value;
            obj.Invalidate(true);
        end
        
        function set.CurrentFrame(obj, value)
            obj.CurrentFrame = value;            
            obj.OnUpdateCurrentFrame(value);                        
            obj.InvalidateRenderBuffer();
            
            if obj.IsDynamicSignal()
                obj.InvalidateRoiMap();
            end            
        end   
        
        function set.CurrentChannel(obj, value)
            obj.CurrentChannel = value;
            obj.hAppFigure.CurrentChannel = value;

            obj.InvalidateRenderBuffer();
            obj.InvalidateRoiMap();
        end
            
        function set.CurrentZPos(obj, value)
            obj.CurrentZPos = value;
            obj.InvalidateRenderBuffer();
            obj.InvalidateRoiMap();
        end        
        
        function set.DisplayStackName(obj, value)
            obj.DisplayStackName = string(value);
            obj.UpdateShader();                        
            obj.Invalidate(true);
        end
        
        function value = get.ShowROIs(obj)
            try
                value = obj.mRoiMapBuffer.IsVisible;
            catch
                value = true; %Fallback
            end
        end                      
        
        function value = get.OverlayAlpha(obj)
            try
                value = obj.mOverlayAlpha;
            catch
                value = 0.5; %Fallback 50% transparency
            end
        end        
        
        function set.ShowROIs(obj, value)
            obj.mRoiMapBuffer.IsVisible = value;
            obj.InvalidateRoiMap();
        end
        
        function set.OverlayAlpha(obj, value)
            obj.mOverlayAlpha = value;
            obj.InvalidateRoiMap();
        end        
        
        function set.SelectedRoiIdx(obj, value)
            obj.SelectedRoiIdx = value;
            obj.SelectedRoiChanged();                        
        end
        
        function SetManualRoiType(obj, ManualRoiType)
            obj.ManualRoiType = ManualRoiType;
            obj.CreateRoi();
        end
    end
    
    methods
        RenderAndDisplay(obj, forceNewImage);
        screenBuff = RenderFrame(obj, chanelNo, zPos, frameNo);
        screenBuff = RenderFrameTH(obj, chanelNo, zPos, frameNo);    
        HighlightRoi(obj, roiIdx);
    end
    
    methods(Access=protected)
        RenderHotCold(obj, channelNo, zPos, frameNo);
        RenderDynRange(obj, channelNo, zPos, frameNo);
        RenderDefault(obj, channelNo, zPos, frameNo);
    end
    
    methods
        function result = HitTest(obj)
            pt = obj.mAxes.CurrentPoint;
            obj.mAxes.Units = "pixels";
            pt = [pt(1), pt(3)];
            result = all(pt >= 0) && all(pt <= [obj.mAxes.XLim(2), obj.mAxes.YLim(2)]);
            obj.mAxes.Units = "normalized";
        end
        
        function OnSetBtnDwnFcn(obj, varargin)
            if (~isempty(obj.mAxes.Children))
                for i=1:numel(obj.mAxes.Children)
                    currChild = obj.mAxes.Children(i);
                    if (isa(currChild, 'matlab.graphics.primitive.Image'))                        
                        currChild.ButtonDownFcn = @obj.MouseClickedHandler;
                    end
                end                    
            end
        end
        
        function OnFChanged(obj, ~, ~)
            obj.Update();
        end
                        
        function NextFrame(obj)
            obj.GoToFrame(obj.CurrentFrame + 1);
        end
        
        function PreviousFrame(obj)
            obj.GoToFrame(obj.CurrentFrame - 1);
        end
        
        function result = GoToFrame(obj, t, c, z)
            if ~isempty(obj.hDataset)
                if (t >= 1 && t <= obj.hDataset.MetaData.SamplesT )

                    if nargin > 2
                        obj.CurrentChannel = c;
                        if nargin > 3
                            obj.CurrentZPos = z;                
                        end
                    end

                    obj.CurrentFrame = t;   

                    obj.RenderAndDisplay();                    
                    result = 1;
                else
                    result = 0;
                end   
            end
        end       
               
        function Invalidate(obj, bForceRedraw)
            obj.mRenderBuffer.IsValid = false;
            obj.mRoiMapBuffer.IsValid = false;
            
            if nargin > 1 && bForceRedraw
               obj.RenderAndDisplay(); 
            end
        end
        
        function InvalidateRenderBuffer(obj)
            if ~isempty(obj.mRenderBuffer)
                obj.mRenderBuffer.IsValid = false;
            end
        end
        
        function ClearRoiMapLUT(obj)
            if ~isempty(obj.hAnalysis)
                chan = obj.CurrentChannel(1);
                obj.hAnalysis.ResetColorLUT(chan);
            end
        end
        
        function InvalidateRoiMap(obj)
            if ~isempty(obj.mRoiMapBuffer)
                obj.mRoiMapBuffer.IsValid = false;
            end
        end
        
        function Update(obj)            
            if ~isempty(obj.hDataset)             
                obj.hAnalysis = obj.hDataset.CurrentAnalysis;
                obj.mStackSize = size(obj.hDataset.F);

                szX = obj.hDataset.MetaData.SamplesX;
                szY = obj.hDataset.MetaData.SamplesY;

                try
                    rmbVisible = obj.mRoiMapBuffer.IsVisible;
                catch
                    rmbVisible = true;
                end

                obj.mRoiMapBuffer = com.CalciumAnalysis.Rendering.PixelBuffer( [szY, szX, 3] );
                obj.mRenderBuffer = com.CalciumAnalysis.Rendering.PixelBuffer( [szY, szX, 3] );            
                obj.mRoiMapBuffer.IsVisible = rmbVisible;

                obj.mOverlayAlpha = obj.hDataset.Settings.Display.RoiOverlayTransparencyP;            
                obj.SelectionBox = [];
                obj.mAxes.XLim = [0.5 , obj.hDataset.MetaData.SamplesX + 0.5];
                obj.mAxes.YLim = [0.5 , obj.hDataset.MetaData.SamplesY + 0.5];

                obj.mAxes.XTick = obj.GetTickVector(0, obj.hDataset.MetaData.SamplesX, 10);
                obj.mAxes.YTick = obj.GetTickVector(0, obj.hDataset.MetaData.SamplesX, 10);

                tlx = obj.GetTickVector(0, obj.hDataset.MetaData.ExtentX, 10);
                tly = obj.GetTickVector(0, obj.hDataset.MetaData.ExtentY, 10);
                obj.mAxes.XTickLabel = compose('%.2f', tlx);
                obj.mAxes.YTickLabel = compose('%.2f', tly);

                obj.LastMousePos = [obj.hDataset.MetaData.SamplesX/2, obj.hDataset.MetaData.SamplesY/2];

                obj.OnUpdate();               

                obj.DeleteColormapListener(); 

                if ~isempty(obj.hAnalysis)
                    obj.RoiColormapListener = addlistener(obj.hAnalysis.AnalysisSettings, 'ColormapName', 'PostSet',@obj.handlePropEvents);
                end

                obj.CurrentFrame = 1;                        

                obj.Invalidate();
                obj.UpdateOverlay(true);

                try
                    obj.hDataset.Annotations.RenderAnnotations(obj);
                catch 
                end
            end
        end
        
        function UpdateRoiMap(obj)
            try
                if ~isempty(obj.hAnalysis) && obj.mRoiMapBuffer.IsVisible
                    chan = obj.CurrentChannel(1);
                    obj.mRoiMapBuffer.Buffer = obj.hAnalysis.RenderRoiMap(chan, obj.CurrentZPos, obj.CurrentFrame, 255);
                                        
                    obj.mRoiMapBuffer.IsValid = true;
                else
                    obj.mRoiMapBuffer.ClearToBlack();
                end  
            catch ex
                obj.mRoiMapBuffer.ClearToBlack();
            end
        end        
        
        function UpdateOverlay(obj, bRender)
            obj.InvalidateRoiMap();                      
            
            try
                if nargin < 2
                    obj.RenderAndDisplay();
                elseif nargin == 2 && bRender
                    obj.RenderAndDisplay(bRender);
                end
            catch
            end
        end                
        
        function ShowColorbar(obj, bShow)
            if (bShow)
                colorbar(obj.mAxes);
            else
                colorbar(obj.mAxes, 'off');
            end
        end
        
        function SetColorLUT(obj, lutName)
            obj.LutName = lutName;
            obj.GradientColorLUT = feval(lower(lutName), obj.mLutLevels);
            obj.InvalidateRenderBuffer();
            obj.RenderAndDisplay();
        end                             
        
        function SetRenderHint(obj, varargin)
            SetRenderHint_date='22.03.2018';
            SetRenderHint_version='1.0.000';
            
            %% parse Input
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding
            p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['SetRenderHint v.' SetRenderHint_version ' (' SetRenderHint_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addParameter(p,'Mode',          [],   @(x) (ischar(x) || isempty(x)));
            addParameter(p,'RenderData',    []);           

            % input parsing
            parse(p,varargin{:});
            par = p.Results;  
            
            obj.RenderMode = par.Mode;
            obj.RenderData = par.RenderData;            
        end
        
        function img = CaptureFrame(obj)                        
            if isa(obj.mAxes, 'matlab.graphics.axis.Axes')                
                if ~isempty(obj.SubPlots)
                    [img, ~] = getframe(obj.mParent);
                else
                    [img, ~] = getframe(obj.mAxes);
                end
            else
                img = [];
            end
        end
        
        function MouseMotionHandler(obj, ~, ~) 
            hDataSet = obj.hDataset;
            
            if ~isempty(hDataSet)
                hMetadata = hDataSet.MetaData;            
                mousePos = obj.mAxes.CurrentPoint;
                
                
                if ( mousePos(1) >= 0 && mousePos(1) <= hMetadata.ExtentX && ...
                     mousePos(3) >= 0 && mousePos(3) <= hMetadata.ExtentY )                
                    
                    %Convert to pixel coordinates
                    pos = [ (max( mousePos(1), 0 ) / hMetadata.ExtentX) * hMetadata.SamplesX,...
                            (max( mousePos(3), 0 ) / hMetadata.ExtentY) * hMetadata.SamplesY];

                    obj.LastMousePos = pos;
                    obj.hAppFigure.CurrentMousePos = obj.LastMousePos;
                end
            end
        end
        
        function MouseClickedHandler(obj, ~, eventData)  
            if ~isempty (obj.hDataset)
                try
                    obj.hDataset.Annotations.SelectAnnotation([]);
                catch
                end
                
                if ~isempty(obj.hAppFigure)  
                    selIdx = obj.GetClickedRoiIdx(eventData.IntersectionPoint);
                    
                    if selIdx == obj.SelectedRoiIdx
                        selIdx = -1;
                    end
                    
                    obj.hAppFigure.SelectedRoi = selIdx;
                end                            
            end
        end
        
        function KeyPressedHandler(obj, eventData)
            if ( ~isempty(obj.hCurrRoi) )
                
                if (isa(obj.hCurrRoi, 'imline'))
                    %We need to create a line of width 2!
                    pos = obj.hCurrRoi.getPosition();
                    v = pos(1,:) - pos(2,:);                    
                    vn = v/norm(v);
                    vno = [vn(2) -vn(1)]; %orthogonal
                    vno = vno*.75;

                    newpos = [pos(1,:) + vno; pos(1,:) - vno; pos(2,:) - vno; pos(2,:) + vno];
                    obj.hCurrRoi = impoly(obj.mAxes, newpos);                     
                end
                
                switch (eventData.Key)
                    case 't'
                        if ( ~isempty(obj.AddRoiCallback) )
                            obj.AddRoiCallback(obj.hCurrRoi);                      
                        end
                    case 'c'
                        if ( ~isempty(obj.CutRoiCallback) )
                            obj.CutRoiCallback(obj.hCurrRoi);
                        end
                    case 'm'
                        if ( ~isempty(obj.MergeRoiCallback) )
                            obj.MergeRoiCallback(obj.hCurrRoi);
                        end
                end
                obj.ClearCurrentRoi();
                obj.RenderAndDisplay();
            elseif (obj.ManualRoiType ~= com.CalciumAnalysis.Enum.ManualRoiEnum.None)
                switch (eventData.Key)
                    case 'n'
                        obj.CreateRoi();                 
                end
            end                        
        end
        
        function UpdateAnalysis(obj, hAnalysis)
            obj.hAnalysis = hAnalysis;
            obj.SelectedRoiIdx = -1;   
            %obj.UpdateOverlay();
        end
        
        function TogglePlayback(obj)
            if (isempty(obj.mPlaybackTimer) && numel(obj.mStackSize) > 2 )
                fps = 20;
                timerIntervall = round((1/fps)*1000) / 1000; %round to 1 millisecond
                
%                 obj.mPlaybackTimer = timer('Period', timerIntervall, ...
%                     'ExecutionMode', 'fixedSpacing', 'BusyMode', 'drop');
                obj.mPlaybackTimer = timer('Period', timerIntervall,...
                    'ExecutionMode', 'fixedDelay', 'BusyMode', 'drop');
                obj.mPlaybackTimer.TimerFcn = {@obj.TimerRenderFrame};
                
                if (obj.CurrentFrame == obj.mStackSize(3))
                   obj.GoToFrame(1); 
                end
                
                obj.IsPlaying = true;
                start(obj.mPlaybackTimer);               
            else
                obj.StopPlaybackTimer();
            end
        end
        
        function ShowRoiOverlay(obj, bShow)
            obj.mRoiMapBuffer.IsVisible = bShow;
            obj.RenderAndDisplay();
        end
        
        function ShowAnnotations(obj, bShow)
            obj.mShowAnnotations =  bShow;
            
            try
                if obj.mShowAnnotations
                    obj.hDataset.Annotations.ShowAll();
                else
                    obj.hDataset.Annotations.HideAll();
                end  
            catch
            end
        end    
        
        function AdjustDisplaySettings(obj)
            com.CalciumAnalysis.UI.dlgAdjustDisplay(obj, true);
        end
        
        function AttachGraph(obj, hGraph, videoSize) 
            try
                if nargin == 3 && ~isempty(videoSize)
                    videoSizeX = videoSize(1);
                    videoSizeY = videoSize(2);
                else
                    videoSizeX = 512;
                    videoSizeY = 512;
                end
                                
                SubplotWidth = 700;
                SubPlotHeight = 300;
                xPadding = 75;
                yPadding = 100;
                
                obj.SubPlots{end+1} = hGraph;                
                numChildren = numel(obj.SubPlots);
                
                %Resize Parent
                totalHeight = ((SubPlotHeight+yPadding) * numChildren) + videoSizeY + yPadding;
                obj.mParent.Position(3) = max( SubplotWidth + 2*xPadding , obj.mParent.Position(3) );
                obj.mParent.Position(4) = totalHeight;                               
                
                %Convert axes to subplot                             
                obj.mAxes = subplot(numChildren + 1, 1, 1, obj.mAxes, 'Units', 'pixels');
                obj.mAxes.Position(1) = obj.mParent.Position(3)/2 - videoSizeX/2;
                obj.mAxes.Position(2) = obj.mParent.Position(4) - (videoSizeY + yPadding/2);
                obj.mAxes.Position(3) = videoSizeX;
                obj.mAxes.Position(4) = videoSizeY;
                
                try
                    obj.RenderAndDisplay();
                catch
                end
                
                yPos = obj.mAxes.Position(2);
                
                %Overwrite axes of child plots to be a subplot and update
                for i=1:numChildren
                    yPos = yPos - (SubPlotHeight+yPadding);
                    
                    pos = [xPadding, yPos, SubplotWidth, SubPlotHeight];
                    hAxis = subplot('Position', [0,0,0,0], 'Units', 'pixels', 'Parent', obj.mParent);
                    hAxis.Position = pos;

                    obj.SubPlots{i}.hAxis = hAxis;
                    obj.SubPlots{i}.DeleteFrameIndicator();
                    obj.SubPlots{i}.Render();

                end
            catch ex
            end
        end
    end
    
    methods(Access=protected)
        function UpdateShader(obj)
            switch obj.DisplayStackName
                case "Release/Uptake"
                    obj.hShaderFunc = @obj.RenderHotCold;
                case "Dynamic Range"
                    obj.hShaderFunc = @obj.RenderDynRange;
                otherwise
                    obj.hShaderFunc = @obj.RenderDefault;
            end
        end
        
        function AdjustPosition(obj)
            if ~isempty(obj.WindowPosition)
                u = obj.mAxes.Units;
                obj.mAxes.Units = "normalized";
                obj.mAxes.Position = obj.WindowPosition;
                obj.mAxes.Units = u;
            end
        end
        
        function OnUpdateCurrentFrame(obj, frame)
            numChildren = numel(obj.SubPlots);
            
            %t = obj.hDataset.FrameTimes(frame);
            
            for i=1:numChildren
                obj.SubPlots{i}.CurrFrame = frame;                
            end
        end
        
        function margin = GetAxisMargin(~)
            margin = [0, 0, 0, 0];
        end
        
        function hAxes = CreateAxes(obj, axPos)
            hAxes = axes('Units', 'pixels', 'OuterPosition', axPos, 'Tag', obj.Params.Name, 'Parent', obj.mParent, 'Box', 'on', 'XTick', [], 'YTick', [],...
                'XTickMode', 'manual', 'YTickMode', 'manual', 'XLimMode', 'manual', 'YLimMode', 'manual') ;
            
            set( hAxes, 'Units', 'normalized');
            set( hAxes, 'ButtonDownFcn', @obj.MouseClickedHandler);
        end
        
        function OnUpdate(~)
        end
        
        function TimerRenderFrame(obj, ~, ~)
            obj.CurrentFrame = max(round(obj.mSliderT.Value), 1);            
            obj.GoToFrame( obj.CurrentFrame, obj.CurrentChannel, obj.CurrentZPos );
            drawnow;
         end
        
        function StopPlaybackTimer(obj)
            if ( ~isempty(obj.mPlaybackTimer) )
                stop(obj.mPlaybackTimer);
                delete(obj.mPlaybackTimer);

                obj.mPlaybackTimer = [];
                obj.IsPlaying = false;
            end
        end     
    end
    
    methods(Access=protected)
        function OnSelectedRoiChanged(obj)
            obj.InvalidateRoiMap();
        end
    end
    
    methods(Access = private)
        function frame = GetCurrentFrame(obj)
            frame = obj.GetFrame(obj.CurrentChannel, obj.CurrentZPos, obj.CurrentFrame);
        end 
        
        function frame = GetCurrentMorphologyFrame(obj)
            frame = obj.GetMorphologyFrame(obj.CurrentFrame);
        end
        
        function frame = GetFrame(obj, chanelNo, zPos, frameNo, stackName)
            s(1).type = '.';
            s(2).type = '()';
                        
            if nargin < 5 || stackName == ""
                stackName = obj.DisplayStackName;
            end
            
            s(1).subs = stackName;
            s(2).subs = {':' ':' ':' zPos 1 frameNo};  %We are currently ignoring multiple frames per slice         
           
           buffer = subsref(obj.hDataset, s);
           
           frame = buffer(:, :, chanelNo);
        end
        
        function frame = GetMorphologyFrame(obj, frameNo)
            s(1).type = '.';
            s(2).type = '()';
                        
            s(1).subs = 'MorphologyStack';
            s(2).subs = {':' ':' frameNo};           
           
           frame = subsref(obj.hDataset, s);
        end    
        
        function handlePropEvents(obj, src, ~)
            switch src.Name 
                case 'ColormapName'
                    obj.ClearRoiMapLUT();
                    obj.InvalidateRoiMap();
                    obj.RenderAndDisplay();
            end
        end
        
        function DeleteColormapListener(obj)
            try
               delete (obj.RoiColormapListener);
            catch
            end
            obj.RoiColormapListener = [];
        end
        
        function SelectedRoiChanged(obj)
            if ~isempty(obj.hAnalysis)                
                obj.OnSelectedRoiChanged();                                    
                obj.RenderAndDisplay();
            end
        end
        
        function ticks = GetTickVector(~, minVal, maxVal, numSteps)
            ticks = zeros(numSteps + 1, 1);
            
            for i=1:numSteps
                ticks(i+1) = i * ((maxVal-minVal)/numSteps);
            end            
        end                
        
        function result = IsDynamicSignal(obj)
            if ~isempty(obj.hDataset)               
                result = ~isempty(obj.hAnalysis) && obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic;
            else
                result = false;
            end                
        end            
        
        function SetCurrentRoi(obj, hRoi)
            if ( ~isempty(obj.hCurrRoi))
               obj.hCurrRoi.delete(); 
            end
            
            obj.hCurrRoi = hRoi;
        end
        
        function ClearCurrentRoi(obj)
           if (~isempty(obj.hCurrRoi))
              obj.hCurrRoi.delete();
              obj.hCurrRoi = [];
           end           
        end
        
        function CreateRoi(obj)
            if (~isempty(obj.hDataset) && ~isempty(obj.hDataset.F))                                         
                switch (obj.ManualRoiType)                    
                    case com.CalciumAnalysis.Enum.ManualRoiEnum.Line
                        set(obj.mAxes,'LineWidth',2)
                        obj.SetCurrentRoi( imline( obj.mAxes ) );
                    case com.CalciumAnalysis.Enum.ManualRoiEnum.Poly
                        obj.SetCurrentRoi( imfreehand( obj.mAxes ) );
                    case com.CalciumAnalysis.Enum.ManualRoiEnum.Rect
                        obj.SetCurrentRoi( imrect( obj.mAxes ) );
                    case com.CalciumAnalysis.Enum.ManualRoiEnum.Ellipse
                        obj.SetCurrentRoi( imellipse( obj.mAxes ) ); 
                    otherwise
                        obj.ClearCurrentRoi();
                end            
            else
               obj.ClearCurrentRoi(); 
            end
        end
        
        function OnRoiSelectionChanged(obj, hSrc, hEvent)
            obj.SelectedRoiIdx = hEvent.AffectedObject.(hSrc.Name);
        end                                  
        
        function RenderSelectionBox(obj)
            try
                if (~isempty(obj.SelectionBox))
                    if ~isempty(obj.SelectionMarker) && isvalid(obj.SelectionMarker)
                        obj.SelectionMarker.Visible = "on";
                        obj.SelectionMarker.Position = obj.SelectionBox.BoundingBox;
                    else                    
                        hold(obj.mAxes, "on");
                        obj.SelectionMarker = rectangle("Position", obj.SelectionBox.BoundingBox,...
                            "LineWidth", 2, "EdgeColor", [1.0000, 0.8235, 0.0471],...
                            "LineStyle", "-.", "Parent", obj.mAxes); 
                        hold(obj.mAxes, "off");
                    end
                else
                    if ~isempty(obj.SelectionMarker) && isvalid(obj.SelectionMarker)
                        obj.SelectionMarker.Visible = "off";
                    end
                end
            catch
            end
        end
    end
end

