%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function RenderAndDisplay(obj, forceNewImage)
    
    hDataset = obj.hDataset;
    
    if (~isempty(hDataset))
        
        if obj.ShowMerge
            channels = 1:hDataset.MetaData.NumChannels;
        else
            channels = obj.CurrentChannel;
        end
        
        screenBuff = obj.RenderFrame(channels, obj.CurrentZPos, obj.CurrentFrame);                                  

        if isempty(obj.RenderTarget) || ~isvalid(obj.RenderTarget) || (nargin == 2 && forceNewImage == true)
            
            try
                btnDownFcn = obj.mAxes.ButtonDownFcn;
            catch
            end   
            
            x = [0, hDataset.MetaData.ExtentX];
            y = [0, hDataset.MetaData.ExtentY];
            obj.RenderTarget = image(obj.mAxes, x, y, screenBuff);
            
            obj.AdjustPosition(); 
                        
            try                
                obj.mAxes.ButtonDownFcn = btnDownFcn;
            catch
            end
            
             obj.mAxes.XLim = x;
             obj.mAxes.YLim = y;

            if obj.mShowGrid
                ylabel(obj.mAxes, hDataset.MetaData.DomainUnit);
                obj.mAxes.XAxisLocation = 'origin';  
            else
                obj.mAxes.XGrid = 'off';
                obj.mAxes.YGrid = 'off';
                obj.mAxes.ZGrid = 'off';
            end
            
            disableDefaultInteractivity(obj.mAxes);
            try
                axtoolbar(obj.mAxes, {'export', 'zoomin', 'zoomout', 'pan', 'restoreview'}); %2020a
            catch
                axtoolbar(obj.mAxes, {'zoomin', 'zoomout', 'pan', 'restoreview'}); %2019b
            end
        else
            obj.RenderTarget.CData = screenBuff;
        end                                  
        
        dbg = false;
        if dbg
            dbgFig = figure("Units", "pixels", "Position", [0,0,800,800]); 
            dbgAx = axes(dbgFig);
            buff = obj.RenderFrame(channels, obj.CurrentZPos, 247);
            imagesc(dbgAx, buff);
            
            x = [0, hDataset.MetaData.ExtentX];
            y = [0, hDataset.MetaData.ExtentY];
            dbgAx.XLim = x;
            dbgAx.YLim = y;
            ylabel(dbgAx, hDataset.MetaData.DomainUnit);
            dbgAx.XAxisLocation = 'origin';  
        end
        
        obj.HighlightRoi(obj.SelectedRoiIdx);                   
        obj.RenderSelectionBox();
    end
end                