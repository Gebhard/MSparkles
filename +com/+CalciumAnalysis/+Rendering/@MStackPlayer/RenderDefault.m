%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function RenderDefault(obj, channelNo, zPos, frameNo)
    hDataset = obj.hDataset;

    for i=channelNo
        displayGamma = hDataset.Settings.Display.GetDisplayGamma(obj.DisplayStackName, i);
        displayBounds = hDataset.Settings.Display.GetDisplayBounds(obj.DisplayStackName, i);

        if isempty(displayBounds)
            [lb, ub] = bounds(obj.hDataset.(obj.DisplayStackName)(:,:,i,:,:,:), 'all');
            hDataset.Settings.Display.SetDisplayBounds(obj.DisplayStackName, i, [lb, ub]);
        end

        buffer = rescale(obj.GetFrame(i, zPos, frameNo), ...
            'InputMin', displayBounds(1), 'InputMax', displayBounds(2)) .^ displayGamma;

        if numel(channelNo) == 1 && obj.UseLUT && ~obj.ShowMerge
            %use pre-defined lut  
            buffer = round(buffer * (obj.mLutLevels-1)) + 1;
            buffer = reshape(obj.GradientColorLUT(buffer(:), :), obj.mRenderBuffer.BufferSize);
        else
            buffer = repmat(buffer, [1,1,3]);
            col = reshape(hDataset.Settings.Display.GetChannelColor(i), [1,1,3]);  
            buffer = bsxfun(@times,col ,buffer);
        end

        obj.mRenderBuffer.Buffer = obj.mRenderBuffer.Buffer  + buffer;
    end
end

