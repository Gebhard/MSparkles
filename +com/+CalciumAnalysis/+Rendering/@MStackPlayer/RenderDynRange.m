%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function RenderDynRange(obj, channelNo, zPos, frameNo)
    %LUT = jet(256);
    hDataset = obj.hDataset;

    mindFF0 = hDataset.normFMin(:,:, channelNo, zPos, 1, :);
    maxdFF0 = hDataset.normFMax(:,:, channelNo, zPos, 1, :);

    absMin = min(mindFF0(:));
    absMax = max(maxdFF0(:));
    
    displayGamma = hDataset.Settings.Display.GetDisplayGamma("F", channelNo);
    displayBounds = hDataset.Settings.Display.GetDisplayBounds("F", channelNo);

    if isempty(displayBounds)
        [lb, ub] = bounds(hDataset.("F")(:,:,channelNo,:,:,:), 'all');
        hDataset.Settings.Display.SetDisplayBounds("F", channelNo, [lb, ub]);
    end

    try
        frame = obj.GetFrame(channelNo, zPos, frameNo, "dFF0");
        
        if hDataset.Settings.Display.FluorescentRangeMedianSize > 0
            szFilt = repmat(hDataset.Settings.Display.FluorescentRangeMedianSize, 1, hDataset.MetaData.NumSpatialDims);
            if numel(szFilt) == 3
                frame = medfilt3(frame, szFilt);
            else 
                frame = medfilt2(frame, szFilt);
            end
        end
        
        if hDataset.Settings.Display.FluorescentRangeGaussSigma > 0
            if hDataset.MetaData.NumSpatialDims == 3
                frame = imgaussfilt3(frame, hDataset.Settings.Display.FluorescentRangeGaussSigma);
            else 
                frame = imgaussfilt(frame, hDataset.Settings.Display.FluorescentRangeGaussSigma);
            end
        end

        frame = single(frame - mindFF0);

        frame = floor(rescale( frame, 1, size(obj.GradientColorLUT,1), 'InputMin', absMin,...
            'InputMax', absMax));
        frame = reshape(obj.GradientColorLUT(frame(:), :), obj.mRenderBuffer.BufferSize);
    catch
        frame = obj.GetFrame(channelNo, zPos, frameNo, "F");
    end
    
    obj.mRenderBuffer.Buffer = frame;
   % buffer(mask) = buffer(mask) * 0.4;
    %obj.mRenderBuffer.Buffer = (frame * 0.6) + buffer;
end

