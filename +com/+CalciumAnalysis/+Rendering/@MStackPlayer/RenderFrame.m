%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function screenBuff = RenderFrame(obj, channelNo, zPos, frameNo)   
    screenBuff = [];
    try       
        if ~obj.mRenderBuffer.IsValid                    
            obj.mRenderBuffer.ClearToBlack();
            obj.hShaderFunc(channelNo, zPos, frameNo);
            obj.mRenderBuffer.IsValid = true;        
        end
    catch
    end  
    
    try
        screenBuff = obj.mRenderBuffer.Buffer;
    catch
    end
                                
    %Show ROIs?
    try
        if ~isempty(obj.mRoiMapBuffer)
            if obj.mRoiMapBuffer.IsVisible        
                %Update ROIs for dynamic signals, if needed
                if ~obj.mRoiMapBuffer.IsValid
                    obj.UpdateRoiMap();
                end            

                idx = obj.mRoiMapBuffer.Buffer > 0;
                screenBuff(idx) = screenBuff(idx) * (1-obj.mOverlayAlpha);
                screenBuff(isnan(screenBuff)) = 0;
                if ~isempty(obj.mRoiMapBuffer.Buffer)
                    screenBuff = (obj.mOverlayAlpha * obj.mRoiMapBuffer.Buffer +  screenBuff);
                end
            end
        end
    catch
    end  
    
    try
        if obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic && ...
                obj.ShowTrajectories
            screenBuff = obj.hAnalysis.RenderTrajectories(obj.hDataset,...
                obj.CurrentChannel(1), obj.CurrentFrame, screenBuff, obj.mAxes);
        end
    catch
    end
end

