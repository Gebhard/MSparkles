%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function screenBuff = RenderFrameTH(obj, channelNo, zPos, frameNo)   
    screenBuff = [];
    
    thLevels = [0.5, 1, 1.5];
        
    numTh = numel(thLevels);
    thCols = [  0,0,0;
                0, 0.4470, 0.7410;
                0.4660, 0.6740, 0.1880;
                0.6350, 0.0780, 0.1840];
    try
        if ~obj.mRenderBuffer.IsValid                    
            obj.mRenderBuffer.ClearToOne();
           % hDataset = obj.hDataset;
            
            for i=channelNo
                %displayGamma = hDataset.Settings.Display.GetDisplayGamma(i);
                %displayBounds = hDataset.Settings.Display.GetDisplayBounds(i);
                
%                 if isempty(displayBounds)
%                     [lb, ub] = bounds(obj.hDataset.F(:,:,i,:,:,:), 'all');
%                     hDataset.Settings.Display.SetDisplayBounds(i, [lb, ub]);
%                 end

                 orgBuffer = obj.GetFrame(i, zPos, frameNo);
                 contour(obj.mAxes, orgBuffer, thLevels);
%                  buffer = zeros(size(orgBuffer));
%                  for th=numTh:-1:1
%                     buffer(orgBuffer < thLevels(th)) = th;
%                  end
                
%                 orgBuffer = rescale(obj.GetFrame(i, zPos, frameNo), ...
%                     'InputMin', displayBounds(1), 'InputMax', displayBounds(2)) .^ displayGamma;
                
                %buffer = round(orgBuffer * numTh+1) + 1;
%                 obj.mRenderBuffer.Buffer = reshape(thCols(buffer(:), :), obj.mRenderBuffer.BufferSize);
                
                %obj.mRenderBuffer.Buffer = buffer;
            end

            obj.mRenderBuffer.IsValid = true;        
        end
    catch ex
    end  
    
    try
        screenBuff = obj.mRenderBuffer.Buffer;
    catch
    end
                                
    %If ROIs are active, scale ROIs to current TH levels
    %Otherwise, scale all value to TH levels
%     try
%         if ~isempty(obj.mRoiMapBuffer)
%             if obj.mRoiMapBuffer.IsVisible        
%                 %Update ROIs for dynamic signals, if needed
%                 if ~obj.mRoiMapBuffer.IsValid
%                     obj.UpdateRoiMap();
%                     obj.mRoiMapBuffer.IsValid = true;
%                 end            
% 
%                 idx = obj.mRoiMapBuffer.Buffer > 0;
%                 screenBuff(idx) = screenBuff(idx) * (1-obj.mOverlayAlpha);
%                 screenBuff(isnan(screenBuff)) = 0;
%                 if ~isempty(obj.mRoiMapBuffer.Buffer)
%                     screenBuff = (obj.mOverlayAlpha * obj.mRoiMapBuffer.Buffer +  screenBuff);
%                 end
%             end
%         end
%     catch
%     end        
end

