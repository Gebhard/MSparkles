%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function RenderHotCold(obj, channelNo, zPos, frameNo)
    LUTlevels = 512;
    LUT = fireice(LUTlevels);
    hDataset = obj.hDataset;
    
    displayGamma = hDataset.Settings.Display.GetDisplayGamma("F", channelNo);
    displayBounds = hDataset.Settings.Display.GetDisplayBounds("F", channelNo);

    if isempty(displayBounds)
        [lb, ub] = bounds(hDataset.("F")(:,:,channelNo,:,:,:), 'all');
        hDataset.Settings.Display.SetDisplayBounds("F", channelNo, [lb, ub]);
    end

    buffer = rescale(obj.GetFrame(channelNo, zPos, frameNo, "F"), ...
            'InputMin', displayBounds(1), 'InputMax', displayBounds(2)) .^ displayGamma;

    try
        frame = SmoothTempDiff( frameNo );
        frame( abs(frame) < hDataset.Settings.Display.ReleaseUptakeTH) = 0;

        if hDataset.Settings.Display.ReleaseUptakeMedFiltSize > 0
            frame = medfilt2(frame, [hDataset.Settings.Display.ReleaseUptakeMedFiltSize, hDataset.Settings.Display.ReleaseUptakeMedFiltSize]);
        end

        mask = frame ~= 0;
        maskNegVal = frame < 0;
        
        lb = hDataset.Settings.Display.ReleaseUptakeLBound;
        ub = hDataset.Settings.Display.ReleaseUptakeUBound;
        
        %frame = floor(rescale( frame, 1, LUTlevels, 'InputMin', lb, 'InputMax', ub));
        
        frame(maskNegVal) = max(frame(maskNegVal),lb); %Clamp negative values
        frame(maskNegVal) = frame(maskNegVal) ./ abs(lb); %scale to [-1,0)
        
        frame(~maskNegVal) = min(frame(~maskNegVal), ub); %Clamp positive values
        frame(~maskNegVal) = frame(~maskNegVal) ./ abs(ub); %scale to (0,1]
        
        frame = floor(rescale( frame, 1, LUTlevels, 'InputMin', -1, 'InputMax', 1));
        
        frame = reshape(LUT(frame(:), :), obj.mRenderBuffer.BufferSize);
    catch
        sz = size(buffer);
        frame = zeros(sz(1), sz(2), 3);
        mask = ones(sz);
    end

    buffer(mask) = buffer(mask) * 0.4;
    obj.mRenderBuffer.Buffer = (frame * 0.6) + buffer;
    
    function d = SmoothTempDiff(frame)
        %Compute temporal differences on a smoothed version of the dataset
        %Depending on the frame, forward, backward or central differences
        %are computed
        %differentiation step h=1
        
        if frame == 1 %forward differences
            currFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame, "dFF0"), 2 );
            nextFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame + 1, "dFF0"), 2 );
            d = single(nextFrame - currFrame);
            
        elseif frame == hDataset.MetaData.SamplesT %backward differences
            currFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame, "dFF0"), 2 );
            prevFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame - 1, "dFF0"), 2 );
            d = single(currFrame - prevFrame);
            
        else %central differences
            prevFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame - 1, "dFF0"), 2 );
            currFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame, "dFF0"), 2 );
            nextFrame = imgaussfilt( obj.GetFrame(channelNo, zPos, frame + 1, "dFF0"), 2 );
            
            %Interpolate at x+1/2h
            fwd = (nextFrame + currFrame) ./ 2;
            bck = (prevFrame + currFrame) ./ 2;
            
            d = single(fwd - bck);
        end
    end
end

