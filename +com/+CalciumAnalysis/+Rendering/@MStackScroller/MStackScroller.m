%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStackScroller < com.CalciumAnalysis.Rendering.MStackPlayer
    
    %STACKSCROLLER Summary of this class goes here
    %   Detailed explanation goes here            
        
    properties(Access=protected)
        mSliderT = [];
        mSliderC = [];
        mSliderZ = [];         
    end   
    
    properties(Constant)        
        SliderHeight = 16;
    end

    methods %Construction & destruction
        function obj = MStackScroller(varargin)                               
            obj = obj@com.CalciumAnalysis.Rendering.MStackPlayer(varargin{:});
            
            if isa(obj, 'com.CalciumAnalysis.Rendering.UIStackScroller')
                return;
            end
            
            sliderHeight = com.CalciumAnalysis.Rendering.MStackScroller.SliderHeight;
                                                  
            pos = [obj.Params.Position(1), obj.Params.Position(2)];
            sz  = [obj.Params.Position(3), obj.Params.Position(4)];
                                             
            sz(2) = sz(2) - 2*sliderHeight;
            
            %create sliders to scroll through the image stack
            %Time axis
            sliderTPos = [pos(1), pos(2), sz(1), sliderHeight];                            
            obj.mSliderT = uicontrol('Style', 'slider', 'Min', 0, 'Max', 1, 'Value', 0, 'Units', 'pixels', 'Parent', obj.mParent, 'Position', sliderTPos, 'SliderStep', [1 1]);            
            obj.hListeners{end + 1} = addlistener(obj.mSliderT, 'Value', 'PostSet', @obj.OnSliderT); 

            %Channels
            sliderCPos = [pos(1), pos(2) + sliderHeight, sz(1), sliderHeight]; 
            obj.mSliderC = uicontrol('Style', 'slider', 'Min', 0, 'Max', 1, 'Value', 0, 'Units', 'pixels', 'Parent', obj.mParent, 'Position', sliderCPos, 'SliderStep', [1 1]);            
            obj.hListeners{end + 1} = addlistener(obj.mSliderC, 'Value', 'PostSet', @obj.OnSliderC); 

            %Z-Axis
            sliderZPos = [pos(1) + sz(1) - sliderHeight, pos(2) + 2*sliderHeight, sliderHeight, sz(2)]; 
            obj.mSliderZ = uicontrol('Style', 'slider', 'Min', 0, 'Max', 1, 'Value', 0, 'Units', 'pixels', 'Parent', obj.mParent, 'Position', sliderZPos, 'SliderStep', [1 1]);            
            obj.hListeners{end + 1} = addlistener(obj.mSliderZ, 'Value', 'PostSet', @obj.OnSliderZ);                                                                                                
            
            %Set Units to normalized for dialog resizing
            set( obj.mSliderT, 'Units', 'normalized');
            set( obj.mSliderC, 'Units', 'normalized');
            set( obj.mSliderZ, 'Units', 'normalized');                                                                                                               
        end      
        
        function delete(obj)
            delete (obj.mSliderT);
            delete (obj.mSliderC);
            delete (obj.mSliderZ);
        end
    end        
    
    methods(Access=protected) 
        function OnUpdateCurrentFrame(obj, frame)
            OnUpdateCurrentFrame@com.CalciumAnalysis.Rendering.MStackPlayer(obj, frame);
            obj.hAppFigure.CurrentFrame = frame;
        end
        
        function margin = GetAxisMargin(~)
            sliderHeight = com.CalciumAnalysis.Rendering.MStackScroller.SliderHeight;
            margin = [0, 2*sliderHeight, sliderHeight, 0];
        end
        
        function OnUpdate(obj)
             if (ndims(obj.hDataset.F) >= 3)
                obj.UpdateSlider( obj.hDataset.MetaData.SamplesT, obj.hDataset.MetaData.NumChannels, obj.hDataset.MetaData.SamplesZ );
            else
                obj.UpdateSlider( 1, 1, 1 );
            end
        end        

        function UpdateSlider(obj, numT, numC, numZ, keepPositions)
            if nargin > 4 && keepPositions
                oldT = obj.mSliderT.Value;
                oldC = obj.mSliderC.Value;
                oldZ = obj.mSliderZ.Value;
            else
                oldT = 1;
                oldC = 1;
                oldZ = obj.mSliderZ.Max;
            end
            
            obj.mSliderT.Min = 0.99;
            obj.mSliderT.Max = numT;
            
            obj.mSliderC.Min = 0.99;
            obj.mSliderC.Max = numC;
                        
            obj.mSliderZ.Min = 0.99;
            obj.mSliderZ.Max = numZ;
            
            if (numT > 1)
                obj.mSliderT.SliderStep = [(1/(numT-1)), (1/(numT-1))];   
                obj.mSliderT.Enable = 'on';
            else
                obj.mSliderT.SliderStep = [0, 0];
                obj.mSliderT.Enable = 'off';
            end
            
            if (numZ > 1)
            	obj.mSliderZ.SliderStep = [(1/(numZ-1)), (1/(numZ-1))];
                obj.mSliderZ.Enable = 'on';                
            else
                obj.mSliderZ.SliderStep = [0, 0];
                obj.mSliderZ.Enable = 'off';
            end
            
            if (numC > 1)
            	obj.mSliderC.SliderStep = [(1/(numC-1)), (1/(numC-1))];
                obj.mSliderC.Enable = 'on';
            else
                obj.mSliderC.SliderStep = [0, 0];
                obj.mSliderC.Enable = 'off';
            end
            
            if nargin > 4 && keepPositions
                if oldZ > obj.mSliderZ.Max
                    oldZ = obj.mSliderZ.Max;
                end
                
                if oldC > obj.mSliderC.Max
                    oldC = obj.mSliderC.Max;
                end
                
                if oldT > obj.mSliderT.Max
                    oldT = obj.mSliderT.Max;
                end                                                
            end
            
            obj.mSliderZ.Value = oldZ;
            obj.mSliderC.Value = oldC;
            obj.mSliderT.Value = oldT;  
        end             
        
        function OnSliderT(obj, ~, ~)
            obj.CurrentFrame = max(round(obj.mSliderT.Value), 1);            
            obj.GoToFrame( obj.CurrentFrame, obj.CurrentChannel, obj.CurrentZPos );
        end   
        
        function OnSliderC(obj, ~, ~)
            obj.CurrentChannel = max(round(obj.mSliderC.Value), 1);
            obj.GoToFrame( obj.CurrentFrame, obj.CurrentChannel, obj.CurrentZPos );
        end   
        
        function OnSliderZ(obj, ~, ~)
            numZSamples = max(obj.hDataset.MetaData.SamplesZ,1);
            obj.CurrentZPos = min(max(obj.mSliderZ.Max - (round(obj.mSliderZ.Value)-1), 1), numZSamples);                     
            obj.GoToFrame( obj.CurrentFrame, obj.CurrentChannel, obj.CurrentZPos );
        end                                                  
        
        function TimerRenderFrame(obj, ~, ~)
            sliderValT = round(obj.mSliderT.Value);
            
            if sliderValT < obj.mSliderT.Max
                obj.mSliderT.Value = sliderValT +1;
            else
                if obj.LoopPlayback
                    obj.mSliderT.Value = 1;
                else
                    obj.StopPlaybackTimer();
                end
            end
            drawnow;
        end
        
        function OnSelectedRoiChanged(obj)
            OnSelectedRoiChanged@com.CalciumAnalysis.Rendering.MStackPlayer(obj);
            
            if all(obj.SelectedRoiIdx > 0) && obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic
                hResultset = obj.hAnalysis.GetResultset(obj.CurrentChannel);
                groups = hResultset.ROIs;

                idx = groups.PixelIdxList{obj.SelectedRoiIdx(1)}(:);
                [~,~,~,~,~,t] = ind2sub(groups.ImageSize, idx);            
                obj.mSliderT.Value = min(t(:));                 
            end
        end
    end
end