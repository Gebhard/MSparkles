%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef UIStackScroller < com.CalciumAnalysis.Rendering.MStackScroller
    
    properties (Access=private)
        mBtnTogglePlayback;
        mLblCurrFrame;
        mBtnSettings;
        
        playIcon;
        stopIcon;
    end
    
    methods %Construction & destruction
        function obj = UIStackScroller(varargin) 
            obj = obj@com.CalciumAnalysis.Rendering.MStackScroller(varargin{:});           
            
            icondir = iconrootdir();            
            obj.playIcon              = fullfile(icondir,'play.png');
            obj.stopIcon              = fullfile(icondir,'stop.png');
            
            hGlobalSettings = MGlobalSettings.GetInstance();
                        
            pos = [obj.Params.Position(1), obj.Params.Position(2)];
            sz  = [obj.Params.Position(3), obj.Params.Position(4)];
            
            obj.mShowGrid = false;

            sliderHeight = com.CalciumAnalysis.Rendering.MStackScroller.SliderHeight;
            pos(2)= pos(2) + (20 + 2*sliderHeight);
            sz(2) = sz(2) - (20 + 2*sliderHeight);
            sz(1) = sz(1) - sliderHeight;
            
            %Time axis
            sliderTPos = [pos(1), pos(2) - 2*sliderHeight, sz(1) + sliderHeight, sliderHeight];                            
            obj.mSliderT = uislider('Limits', [0 100], 'Value', 0,...
                'Parent', obj.mParent, 'Position', sliderTPos,...
                'MajorTicks', [], 'MajorTickLabels', {}, 'MinorTicks', [],...
                'ValueChangedFcn', @obj.OnSliderT, ...
                'ValueChangingFcn', @obj.OnDragSliderT);                                            

            %Channels
            sliderCPos = [pos(1), pos(2) - sliderHeight, sz(1) + sliderHeight, sliderHeight]; 
            obj.mSliderC = uislider('Limits', [0 100], 'Value', 0,...
                'Parent', obj.mParent, 'Position', sliderCPos,...
                'MajorTicks', [], 'MajorTickLabels', {}, 'MinorTicks', [],...
                'ValueChangedFcn', @obj.OnSliderC);    

            %Z-Axis
            sliderZPos = [pos(1) + sz(1), pos(2), sz(2), sliderHeight]; 
            obj.mSliderZ = uislider('Limits', [0 100], 'Value', 0,...
                'Parent', obj.mParent, 'Position', sliderZPos,...
                'MajorTicks', [], 'MajorTickLabels', {}, 'MinorTicks', [],...
                'ValueChangedFcn', @obj.OnSliderZ, 'Orientation', 'vertical');            
            
            obj.hListeners{end +1} = addlistener(obj.mAxes, 'ButtonDownFcn', 'PreSet', @obj.OnSetBtnDwnFcn);                         
            %addlistener(obj, 'SelectedRoiIdx_', 'PostSet', @obj.OnRoiSelectionChanged); 
            
            btnHeight = 20;
            btnOffset = 10;
            cmboWidth = 75;
            btnWidth = 50;            
            
            btnStartPos = [pos(1) sliderTPos(2) - 1.5*btnHeight cmboWidth btnHeight];
            
            %create play button            
            btnPlayPos = [btnStartPos(1), btnStartPos(2), btnWidth, btnHeight];                       
            obj.mBtnTogglePlayback = uibutton('Icon', obj.playIcon, 'Text', '', ...
                'Parent', obj.mParent, 'Position', btnPlayPos, 'ButtonPushedFcn',...
                @obj.onTogglePlayback); 

            %create frame counter
            lblPos = [btnPlayPos(1) + btnPlayPos(3) + btnOffset, btnPlayPos(2), 2*btnPlayPos(3), btnHeight];
            obj.mLblCurrFrame = uilabel('Parent', obj.mParent, 'Position', lblPos);  
                        
            btnSettings = [lblPos(1) + lblPos(3) + btnWidth, lblPos(2), btnWidth, btnHeight];
            obj.mBtnSettings = uibutton('Text', 'Adjust', 'Parent', obj.mParent, ...
                'Position', btnSettings, 'ButtonPushedFcn', @obj.OnAdjustSettings);
        end
    end
    
    methods (Access=protected) 
        function AdjustPosition(obj)
        end
        
        function margin = GetAxisMargin(~)
            sliderHeight = com.CalciumAnalysis.Rendering.MStackScroller.SliderHeight;
            margin = [0, 25 + 2*sliderHeight, sliderHeight, 0];
        end
        
        function hAxes = CreateAxes(obj, axPos)
            %create axes to display images                      
            hAxes = uiaxes('Units', 'pixels', 'OuterPosition', axPos,...
                'Tag', obj.Params.Name, 'Parent', obj.mParent, 'XTick', [], 'YTick', [],...
                'XTickMode', 'manual', 'YTickMode', 'manual', 'XLimMode', 'manual', 'YLimMode', 'manual') ;            
            
%             hAxes = axes('Units', 'pixels', 'OuterPosition', axPos, 'Tag', obj.Params.Name, 'Parent', obj.mParent, 'Box', 'on', 'XTick', [], 'YTick', [],...
%                 'XTickMode', 'manual', 'YTickMode', 'manual', 'XLimMode', 'manual', 'YLimMode', 'manual') ;
        end
        
        function OnDragSliderT(obj, hSrc, hEvent)
            %obj.CurrentFrame = hSrc.Value; %max(round(obj.mSliderT.Value), 1);            
            %obj.GoToFrame( hSrc.Value, obj.CurrentChannel, obj.CurrentZPos );
        end
        
        function OnAdjustSettings(obj, ~,~)
            com.CalciumAnalysis.UI.dlgAdjustDisplay(obj, false);
        end
        
        function onTogglePlayback(obj, ~,~)
            obj.TogglePlayback();
        end
        
        function UpdateSlider(obj, numT, numC, numZ, keepPositions)            
            numT = max(numT, 1);
            numC = max(numC, 1);
            numZ = max(numZ, 1);
            
            if nargin > 4 && keepPositions
                oldT = min(obj.mSliderT.Value, numT);
                oldC = min(obj.mSliderC.Value, numC);
                oldZ = min(obj.mSliderZ.Value, numZ);
            else
                oldT = 1;
                oldC = 1;
                oldZ = 1;
            end
            
            obj.mSliderT.Limits = [1 max(numT,2)];
            obj.mSliderC.Limits = [1 max(numC,2)];
            obj.mSliderZ.Limits = [1 max(numZ,2)];            
            
            obj.mSliderT.Enable = (numT > 1) ;
            obj.mSliderZ.Enable = (numZ > 1);                
            obj.mSliderC.Enable = (numC > 1);   
            
            obj.mSliderZ.Value = oldZ;
            obj.mSliderC.Value = oldC;
            obj.mSliderT.Value = oldT;  
        end                     
        
        function OnSliderZ(obj, ~, ~)
            slZLims = obj.mSliderZ.Limits;
            numZSamples = max(obj.hDataset.MetaData.SamplesZ,1);
            obj.CurrentZPos = min(max(slZLims(2) - (round(obj.mSliderZ.Value)-1), 1), numZSamples);           
            obj.GoToFrame(obj.CurrentFrame, obj.CurrentChannel, obj.CurrentZPos );
        end 
        
        function UpdatePlayButton(obj, icon)
            obj.mBtnTogglePlayback.Icon = icon;
        end
        
        function UpdateFrameLabel(obj, txt)
            obj.mLblCurrFrame.Text = txt;
        end  
        
        function TimerRenderFrame(obj, ~, ~)
            %TimerRenderFrame@com.CalciumAnalysis.Rendering.MStackScroller(obj);
            
            sliderValT = round(obj.mSliderT.Value);
            
            if sliderValT < obj.mSliderT.Limits(2)
                obj.mSliderT.Value = sliderValT + 1;
                obj.OnSliderT();
                drawnow(obj.mAxes);
            else
                obj.StopPlaybackTimer();
            end
         end
    end
end

