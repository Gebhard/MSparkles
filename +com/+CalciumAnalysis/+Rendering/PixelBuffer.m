%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PixelBuffer < handle
    %PIXELBUFFER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable=true)
        IsValid;
        IsVisible;
        Buffer;
        BufferSize;
    end
    
    properties(Access=private)
        mListeners = {};
    end
    
    methods %Construction & destruction
        function obj = PixelBuffer(bufferSize)
            %PIXELBUFFER Constructs a new PixelBuffer object, with the
            %   specified size
            %   bufferSize  Array, specifying the size if the buffer, e.g.
            %   [512 512] for greyscale or [512 512 3] for a color buffer
            
            obj.IsValid = true;
            obj.IsVisible = true;
            obj.BufferSize = bufferSize;
            obj.ClearToBlack();            
        end
        
        function delete(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.DeleteCallbacks();            
        end
        
        function DeleteCallbacks(obj)
            if ~isempty(obj.mListeners)
                cellfun( @(x) delete(x), obj.mListeners);
            end
        end
    end
    
    methods %Register event handlers
        function RegisterVisibilityChangedCallback(obj, hCallback )
            if isa(hCallback,'function_handle')               
                obj.mListeners = [obj.mListeners {addlistener(obj, 'IsVisible', 'PostSet', hCallback )} ];
            else
                error('Parameter supplied to "RegisterVisibilityChangedListener" in hCallback must be a function handle.')
            end
        end
        
        function RegisterValidityChangedCallback(obj, hCallback )
            if isa(hCallback,'function_handle')               
                obj.mListeners = [obj.mListeners {addlistener(obj, 'IsValid', 'PostSet', hCallback )} ];
            else
                error('Parameter supplied to "RegisterValidityChangedListener" in hCallback must be a function handle.')
            end
        end
        
        function RegisterBufferChangedCallback(obj, hCallback )
            if isa(hCallback,'function_handle')               
                obj.mListeners = [obj.mListeners {addlistener(obj, 'Buffer', 'PostSet', hCallback )} ];
            else
                error('Parameter supplied to "RegisterBufferChangedListener" in hCallback must be a function handle.')
            end
        end
    end
    
    methods
        function ClearToBlack(obj)
            obj.Buffer = zeros(obj.BufferSize);
        end
        
        function ClearToOne(obj)
            obj.Buffer = ones(obj.BufferSize);
        end
    end
end

