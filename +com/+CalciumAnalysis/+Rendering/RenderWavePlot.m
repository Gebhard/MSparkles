%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function RenderWavePlot(hTarget, hDataSet, hAnalysis, hResultset, RoiID)
    settings = hAnalysis.AnalysisSettings;        
    results = hResultset.Results;

    cla(hTarget);
    
    if ~isempty(results)                                    
        hold(hTarget, 'on');
        
        startF = results.StartFrame';
        
        idx = ~cellfun(@isempty, startF);
        startFrame = cat(1, startF{idx});
        for i=1:numel(RoiID)
            plotIdx = RoiID(i);
            MorphArea = zeros(1, hDataSet.MetaData.SamplesT);
            sf = round(startFrame(plotIdx));

            if ~isempty(sf)
                a = results.Areas{plotIdx};
                fSpan = numel(a);

                MorphArea(1, sf:(sf+fSpan-1)) = a;
                %MorphMean(1, sf:(sf+df-1)) = results.SigTAvg{plotIdx};
                MorphMean = results.GetRoiTrace(plotIdx);

                switch(settings.SignalScaling)
                    case com.Enum.SignalScalingEnum.Global
                        m = hResultset.MaxArea;
                    case com.Enum.SignalScalingEnum.Individual
                        m = max(MorphArea(:));
                    case com.Enum.SignalScalingEnum.UserDefined
                        m = settings.UserDefAreaScaling;
                end            

                try
                    com.CalciumAnalysis.Rendering.Graph.CaViolin(hTarget, hDataSet, MorphArea, ...
                        MorphMean, max(hResultset.MaxF), max(min(hResultset.MinF), 0));
                catch
                end

                hTarget.XLim = [0, hDataSet.LastSampleTime];                                
                hTarget.YLim = [-(m+1)/2, (m+1)/2];

                hTarget.XGrid = 'on';
                hTarget.YGrid = 'on';            
            end
        end
        
        hold(hTarget, 'off');
    end
end

