%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function im = renderText(img, textPositions, strings, textColor, fontSize, normFac)
    imSize = size(img);

    hf = figure('color','white','units','pixels','position',[1 1 imSize(2) imSize(1)],'visible','off'); 
    ax = axes(hf, 'units','pixels','Position',[1 1 imSize(2) imSize(1)], 'ActivePositionProperty', 'position');
    image(ax, img);
    
    for i=1:numel(textPositions(:,1))        
        text(ax, 'units','pixels','position',textPositions(i,1:2), 'HorizontalAlignment', 'center',...
            'VerticalAlignment', 'middle', 'Color',textColor ,'fontsize',fontSize,...
            'string', strings(i, :) );
    end

    tim = getframe(ax); 
    delete(hf) 
    im = imresize( single(tim.cdata) ./ normFac, [imSize(1) imSize(2)]);    
end