%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function ExportRoiTrace(hCurrDataset, hClickedNode)
%EXPORTROITRACE Summary of this function goes here
%   Detailed explanation goes here

    if ~isempty(hClickedNode)
        idx = arrayfun(@(x) ismember( x.Type ,[com.Enum.TreeNodeTypes.ROI, com.Enum.TreeNodeTypes.Channel]), hClickedNode);
        
        if ~isempty(hCurrDataset) && all(idx > 0)
            hChannelNode = hClickedNode(1).GetChannelNode();
            hAnalysisNode = hChannelNode.GetAnalysisNode();
            hDatasetNode = hAnalysisNode.GetCaDataSetNode();
            hDataset = hDatasetNode.Dataset;

            if hCurrDataset == hDataset
                hAnalysis = hDataset.Analyses.GetAnalysis(hAnalysisNode.UserData);            
                channelID = hChannelNode.UserData;
                
                if numel(hClickedNode) == 1 && hClickedNode.Type == com.Enum.TreeNodeTypes.Channel
                    roiID = [];
                elseif all(arrayfun(@(x) x.Type == com.Enum.TreeNodeTypes.ROI, hClickedNode))
                    roiID = arrayfun(@(x) x.UserData, hClickedNode(idx));
                else
                    
                    msgbox('The selected elements cannot be exported as event traces.', 'Info', 'warn');
                    return;
                end
                
                hFig = CreateTraceExportFigure();
                hAnalysis.ExportEventTraces([], channelID, roiID, 'Figure', hFig);
            else
                msgbox('The dataset must be loaded to export a ROI.', 'Info', 'warn');
            end
        end
    else
        msgbox('Please select either a single ROI or a channel for export.', 'Info', 'warn');
    end
end

