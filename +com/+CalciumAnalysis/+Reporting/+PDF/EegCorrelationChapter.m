%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegCorrelationChapter < mlreportgen.report.Chapter
    %EEGCORRELATIONCHAPTER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        hDataset;
        hAnalysis;
        hEegDataset;
        hEegResultset;
        channelId;
    end
    
    properties(Dependent)
        ChapterTitle;
        recDur;
        sampleRate;
        numChannels;
        syncChanIdx;
        timingChanIdx;
        overviewGraph;
        corrGraph;
        corrGraphSummary;
    end
    
    methods
        function obj = EegCorrelationChapter(hAnalysis, hEegResultset, channelId)
            %EEGCORRELATIONCHAPTER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@mlreportgen.report.Chapter();
            [fPath, ~, ~] = fileparts(mfilename("fullpath"));
            tmplSrc = fullfile(fPath, "staticAnalysisTmpl.pdftx");
            obj.TemplateSrc =  tmplSrc;
            obj.TemplateName = 'ChapEegCorr';
            
            obj.hDataset        = hAnalysis.hDataset;
            obj.hAnalysis       = hAnalysis;
            obj.hEegDataset     = obj.hDataset.EEGDataset;
            obj.hEegResultset   = hEegResultset;
            obj.channelId       = channelId;
        end
    end
    
    methods
        function value = get.ChapterTitle(~)
            value = "Correlated EEG analysis";
        end
        
        function value = get.recDur(obj)
            value = sprintf("%.3f", obj.hEegDataset.MetaData.Duration);
        end
        
        function value = get.sampleRate(obj)
            value = obj.hEegDataset.MetaData.SampleRate;
        end
        
        function value = get.numChannels(obj)
            value = numel(obj.hEegDataset.MetaData.Channels);
        end
        
        function value = get.syncChanIdx(obj)
            value = obj.hEegDataset.MetaData.SyncChannelIdx;
        end
        
        function value = get.timingChanIdx(obj)
            value = obj.hEegDataset.MetaData.TimingChannelIdx;
        end
        
        function fig = get.overviewGraph(obj)
             try          
                hResultset = obj.hAnalysis.GetResultset(obj.channelId);
                    
                hGlobalSettings = MGlobalSettings.GetInstance();
                if hGlobalSettings.HeatampsIncludeSyncPlot
                    hChildRenderFunc = @RenderEegTracePlot;
                else
                    hChildRenderFunc = [];
                end

                hHeatmap = com.CalciumAnalysis.Rendering.Graph.MHeatMap(...
                    "Dataset", obj.hDataset, "Analysis", obj.hAnalysis, "Resultset", hResultset, ...
                    "XLabel", "Time (sec)", "YLabel", "ROI #",...
                    "ChildRenderFunc", hChildRenderFunc,...
                    "DataLabel", obj.hDataset.NormalizationName, "Title", "Fluorescent signals aligned with EEG");

                hHeatmap.DeleteFigurehandle = false;
                refresh(hHeatmap.hFigure);
                drawnow;
                fig = mlreportgen.report.Figure(hHeatmap.hFigure);  
            catch
                fig = [];
            end
        end
        
        function value = get.corrGraph(obj)
            hResultset = obj.hAnalysis.GetResultset(obj.channelId);
            
            f = figure("Visible", "off", "Units", "normalized");
            com.CalciumAnalysis.Rendering.Graph.EegCorrPlot(...
                "Parent", f,...
                "Dataset",obj.hDataset,... 
                "Resultset", hResultset,...
                "EegResultset", obj.hEegResultset);             
            
            value = mlreportgen.report.Figure(f);
            value.Scaling = 'custom';
            value.Height = '8.5cm';
            value.Width = '17cm';
        end
        
        function value = get.corrGraphSummary(obj)
            hResultset = obj.hAnalysis.GetResultset(obj.channelId);
            
            if  hResultset.EegSignalOffset > 0
                strFiller = "trailing";
            else
                strFiller = "leading";
            end   
            
            value = sprintf("The peak of the fluorescent response is %s the peak of the EEG response by %.3f seconds.", strFiller, abs(hResultset.EegSignalOffset) );
        end
    end
end

