%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStaticAnalysisChapter < mlreportgen.report.Chapter
    %MSTATICANALYSISCHAPTER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        hDataset;
        hAnalysis
        hResultset;
        channelID;
        ChapterTitle;
        roiAnalysisParams;
        
        RoiMap;
        ActivityMap;
        signalCompositionChart;
        graphOverallPeaks;
        graphOverallDurations;
        corrGraph;
        
        thGroupName;
        thCount;
        thP25peak;
        thP50peak;
        thP75peak;
        thP25dur;
        thP50dur;
        thP75dur;      
        RoiCntRowName;
        absSignalCount;        
        RoiCount;       
        avgPeak;
        avgDuration;
        SigPerMin;
        
        signalHeatmap;
        signalDurationHeatmap;
        
        RoiTraces;
    end    
    
    properties (Access=private)
        sComp;      
        AllPeaks;
        AllDurs;
        hFigures;
    end
    
    methods
        function chapter = MStaticAnalysisChapter(hDataSet, hAnalysis, channelID)
            % Construct chapter part.
            
            chapter = chapter@mlreportgen.report.Chapter();
            [fPath, ~, ~] = fileparts(mfilename("fullpath"));
            tmplSrc = fullfile(fPath, "staticAnalysisTmpl.pdftx");
            chapter.TemplateSrc =  tmplSrc;
            
            chapter.hFigures = {};
            
            % Set Rank property for use by fill methods.
            chapter.hDataset = hDataSet;    % handle to dataset.
            chapter.hAnalysis = hAnalysis;  % handle to the analysis.
            chapter.channelID = channelID;  % Each channel gets its own chapter.                        
            chapter.hResultset = hAnalysis.GetResultset(chapter.channelID); %handle to the channels resultset
            
            % Set individual title
            chapter.ChapterTitle = sprintf("%s - channel %d",...
                hAnalysis.Name, chapter.channelID);

            graphPad = com.CalciumAnalysis.Reporting.GetGroupedSignalsTable(chapter.hResultset.Results);
            sigComp = com.CalciumAnalysis.Reporting.GetSignalComposition(chapter.hResultset.Results);

            graphPad(:, 1:4:end) = [];  %remove timing column
            graphPad(:, 3:3:end) = [];  %remove sum column
            chapter.AllPeaks = graphPad(:, 1:2:end);
            chapter.AllDurs = graphPad(:, 2:2:end);            

            if isempty(sigComp)
                chapter.sComp = [0 0 0];
            else
                chapter.sComp = sigComp;
            end
        end 
        
        function delete(obj)
            for i=1:numel(obj.hFigures)
                delete(obj.hFigures{i});
            end
        end
         
        function val = get.roiAnalysisParams(chapter)
            try
                str = chapter.hAnalysis.ToString();
                val = convertStringsToChars(str);
            catch
                val = "";
            end
        end
        
        function fig = get.RoiMap(chapter)
            try                
                if chapter.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic
                    frames = 1:chapter.hDataset.MetaData.SamplesT;
                else
                    frames = 1;
                end
                
                img = chapter.hResultset.RenderRoiMap( chapter.hAnalysis.AnalysisSettings, [], frames, 255, [], true );
                f = chapter.AddFigure(figure('Visible', 'off', 'Units', 'normalized'));
                ax = axes(f);
                image(ax, img);
                fig = mlreportgen.report.Figure(f);
                fig.Scaling = 'custom';
                fig.Height = '8.5cm';
                fig.Width = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function fig = get.ActivityMap(chapter)
            try                               
                metadata = chapter.hDataset.MetaData;
                
                [~, img] = com.lib.ImageProc.ComputeBiModalThreshold(chapter.hDataset.dFF0(:,:,chapter.channelID,:,1,:),...
                        [metadata.SamplesY, metadata.SamplesX, metadata.SamplesZ],...
                        chapter.hAnalysis.AnalysisSettings.DynRngSigma, chapter.hAnalysis.AnalysisSettings.DynRngKrnlSize);
                    
                f = chapter.AddFigure(figure('Visible', 'off', 'Units', 'normalized'));
                ax = axes(f);                                
                imagesc(ax, img);
                colormap(ax, jet);
                fig = mlreportgen.report.Figure(f);
                fig.Scaling = 'custom';
                fig.Height = '8.5cm';
                fig.Width = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function fig = get.signalCompositionChart(chapter)
            try
                hGraph = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MSignalCompositionGraph('Dataset', chapter.hDataset,...
                    'Analysis', chapter.hAnalysis, 'Resultset', chapter.hResultset));
                hGraph.DeleteFigurehandle = false;

                fig = mlreportgen.report.Figure(hGraph.hFigure);
                fig.Scaling = 'custom';
                fig.Width = '8.5cm';
                fig.Height = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function fig = get.graphOverallPeaks(chapter)
            import mlreportgen.report.*
                
            try
                hPlot = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph('Dataset',...
                    chapter.hDataset, 'Analysis', chapter.hAnalysis, 'Resultset',...
                    chapter.hResultset, 'Type', 'peak'));
                
                hPlot.DeleteFigurehandle = false;

                fig = mlreportgen.report.Figure(hPlot.hFigure);
                fig.Scaling = 'custom';
                fig.Height = '8.5cm';
                fig.Width = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function fig = get.graphOverallDurations(chapter)
            import mlreportgen.report.*
                
            try
                hPlot = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph('Dataset',...
                    chapter.hDataset, 'Analysis', chapter.hAnalysis, 'Resultset',...
                    chapter.hResultset, 'Type', 'duration'));
                
                hPlot.DeleteFigurehandle = false;

                fig = mlreportgen.report.Figure(hPlot.hFigure);
                fig.Scaling = 'custom';
                fig.Height = '8.5cm';
                fig.Width = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function fig = get.corrGraph(chapter)
            import mlreportgen.report.*
                
            try
                hPlot = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MPeakDurationScatterGraph('Dataset', chapter.hDataset,...
                    'Analysis', chapter.hAnalysis, 'Resultset', chapter.hResultset));
                
                hPlot.DeleteFigurehandle = false;

                fig = mlreportgen.report.Figure(hPlot.hFigure);
%                 fig.Scaling = 'custom';
%                 fig.Height = '8.5cm';
%                 fig.Width = '8.5cm';
            catch
                fig = [];
            end
        end
        
        function val = get.RoiCount(chapter)
            try           
                val = chapter.hResultset.RoiCount;
            catch
                val = 0;
            end
        end
        
        function val = get.absSignalCount(chapter)
            try           
                val = sprintf('%d', sum(chapter.sComp(1:2:end)));
            catch
                val = 0;
            end
        end 
        
        function val = get.avgPeak(chapter)
            try
                val =  sprintf('%.3f', mean(chapter.AllPeaks(:,end), 'omitnan'));
            catch
                val = '';
            end
        end
        
        function val = get.avgDuration(chapter)
            try
                val =  sprintf('%.3f', mean(chapter.AllDurs(:,end), 'omitnan'));
            catch
                val = '';
            end
        end
        
        function val = get.SigPerMin(chapter)
            try
                meanFreq = mean( chapter.hResultset.Results.SigMeanFreq(:,1) * 60, 'omitnan');
                val =  sprintf('%.3f', meanFreq);
            catch
                val = '';
            end
        end
        
        function val = get.thGroupName(chapter)
            try
                numTh = numel(chapter.hResultset.Results.Thresholds);
                val = cell(numTh, 1);
                
                for i=1:numTh
                    val{i} = sprintf('TH %d (%.3f)', i, chapter.hResultset.Results.Thresholds(i));
                end
            catch
                val = '';
            end
        end
        
        function val = get.thCount(chapter)
            try
                val = chapter.sComp(1,:);
            catch
                val = '';
            end
        end
        
        function val = get.thP50peak(chapter)
            try               
                val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllPeaks(:,1:end-1) , 50)', 'UniformOutput', false );
            catch
                val = '';
            end
        end
        
        function val = get.thP50dur(chapter)
            try
                val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllDurs(:,1:end-1) , 50)', 'UniformOutput', false  );
            catch
                val = '';
            end
        end                                 
                        
        function val = get.thP25peak(chapter)
            try
                val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllPeaks(:,1:end-1) , 25)', 'UniformOutput', false  );
            catch
                val = '';
            end
        end
        
        function val = get.thP75peak(chapter)
            try
            	val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllPeaks(:,1:end-1) , 75)', 'UniformOutput', false  );
            catch
                val = ''; 
            end
        end

        function val = get.thP25dur(chapter)
            try
            	val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllDurs(:,1:end-1) , 25)', 'UniformOutput', false  );
            catch
                val = ''; 
            end
        end
        
        function val = get.thP75dur(chapter)
            try
            	val = arrayfun( @(x) sprintf('%.3f', x),  prctile(chapter.AllDurs(:,1:end-1) , 75)', 'UniformOutput', false  );
            catch
                val = ''; 
            end
        end
            
        function fig = get.signalHeatmap(chapter)
            try                        
                if (~isempty(chapter.hResultset.Results)) 
                    
                    hGlobalSettings = MGlobalSettings.GetInstance();
                    if hGlobalSettings.HeatampsIncludeSyncPlot
                        hChildRenderFunc = @RenderSyncPlot;
                    else
                        hChildRenderFunc = [];
                    end
                
                    hHeatmap = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MHeatMap(...
                        "Dataset", chapter.hDataset, "Analysis", chapter.hAnalysis, "Resultset", chapter.hResultset, ...
                        "XLabel", "Time (sec)", "YLabel", "ROI #",...
                        "ChildRenderFunc", hChildRenderFunc,...
                        "DataLabel", chapter.hDataset.NormalizationName, "Title", "Heatmap"));
                    
                    hHeatmap.DeleteFigurehandle = false;
                    refresh(hHeatmap.hFigure);
                    drawnow;
                    fig = mlreportgen.report.Figure(hHeatmap.hFigure);  
                else
                    fig = [];
                end
            catch
                fig = [];
            end
        end
        
        function fig = get.signalDurationHeatmap(chapter)
            try
                if (~isempty(chapter.hResultset.Results)) 
                    hGlobalSettings = MGlobalSettings.GetInstance();
                    if hGlobalSettings.HeatampsIncludeSyncPlot
                        hChildRenderFunc = @RenderSyncPlot;
                    else
                        hChildRenderFunc = [];
                    end

                    hHeatmap = chapter.AddFigure(com.CalciumAnalysis.Rendering.Graph.MSignalDurationHeatMap(...
                        "Dataset", chapter.hDataset, "Analysis", chapter.hAnalysis, "Resultset", chapter.hResultset, ...
                        "XLabel", "Time (sec)", "YLabel", "ROI #",...
                        "ChildRenderFunc", hChildRenderFunc,...
                        "Title", "Signal duration heatmap"));      

                    hHeatmap.DeleteFigurehandle = false;
                    refresh(hHeatmap.hFigure);
                    drawnow;
                    fig = mlreportgen.report.Figure(hHeatmap.hFigure); 
                else
                    fig = [];
                end
            catch
                fig = [];
            end
        end          
    end
    
    methods(Access = private)
        function hFig = AddFigure(obj, hFig)
            obj.hFigures = cat(1, obj.hFigures, {hFig});
        end
    end
end

