%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef RoiTraceSection < mlreportgen.report.Section
    %ROITRACESECTION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        RoiTraces;
        hAnalysis;
        hResultset;
    end
    
    methods
        function obj = RoiTraceSection(hAnalysis, hResultset)
            %ROITRACESECTION Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@mlreportgen.report.Section();
            obj.hAnalysis = hAnalysis;
            obj.hResultset = hResultset;
            
            [fPath, ~, ~] = fileparts(mfilename("fullpath"));
            obj.TemplateSrc = fullfile(fPath, "staticAnalysisTmpl.pdftx");
            obj.TemplateName = 'SecRoiTraces';
        end
    end
    
    methods
        function fig = get.RoiTraces(obj)
            fig = [];
            
            try
                tracePlots = obj.hAnalysis.RenderEventTraces(obj.hResultset.ChannelID);                                    

                if ~isempty(tracePlots)
                    fig = obj.getTraces(tracePlots);
                end
            catch
                fig = [];
            end
        end
    end
    
    methods(Access=private)
        function sec = getTraces(~, tracePlots)
            try
                sec = cell(1, numel(tracePlots));                       

                for i=1:numel(tracePlots)
                    fig = mlreportgen.report.Figure(tracePlots{i});

                    fig.Scaling = 'custom';
                    fig.Height = '5cm';
                    fig.Width = '18cm';

                    sec{i} = fig;
                end
            catch
                sec = [];
            end
        end
    end
end

