%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef TitleSection < mlreportgen.report.TitlePage
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
     
    properties
        hDataset;
        hAnalysis;
    end
    
    properties (Dependent)
        DatasetName;            %Name of the dataset as string
        numSamples;             %Samples of the dataset in each dimension
        resolution;             %Physical resolution (micron)
        numChannels;            %Number of recorded channels in dataset
        frameRate;              %Frame/Volume rate of the dataset
        AnalysisType;           %Type of analysis that was performed
        ReportDate;             %When was the report generated
        msVersion;              %Version string of MSparkles
        micInfo;                %Additional info about the used microscope (if available)
        analysisParams;         %The used parameters
        Logo;
    end
    
    methods
        function obj = TitleSection(hDataset,hAnalysis)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here            
            
            obj = obj@mlreportgen.report.TitlePage;
            
            [fPath, ~, ~] = fileparts(mfilename('fullpath'));
            obj.Title = "MSparkles analysis report";
            obj.TemplateSrc = fullfile(fPath, 'staticAnalysisTmpl.pdftx');
            obj.TemplateName = 'SecTitleSheet';
            obj.hDataset = hDataset;
            obj.hAnalysis = hAnalysis;
        end
    end
    
    methods
        function value = get.DatasetName(obj)
            try
                value = obj.hDataset.DatasetName;
            catch
                value = "";
            end
        end
        
        function value = get.numSamples(obj)
            try
                hMetadata = obj.hDataset.MetaData;
                value = hMetadata.SamplesX + " x " + ...
                        hMetadata.SamplesY + " x " + ...
                        hMetadata.SamplesZ;
            catch
                value = "";
            end
        end
        
        function value = get.resolution(obj)
            try
                hMetadata = obj.hDataset.MetaData;
                value = sprintf("%.2f x %.2f x %.2f %s", ... 
                    hMetadata.ExtentX, ...
                    hMetadata.ExtentY, ...
                    hMetadata.ExtentZ, ...
                    hMetadata.DomainUnit);
            catch
                value = "";
            end
        end
        
        function value = get.numChannels(obj)
            try
                value = string( obj.hDataset.MetaData.NumChannels );
            catch
                value = "";
            end
        end
        
        function value = get.frameRate(obj)
            try
                value = sprintf("%.2f Hz", obj.hDataset.MetaData.VolumeRate);
            catch
                value = "";
            end
        end
        
        function value = get.AnalysisType(obj)
            try
                value = obj.hAnalysis.Name;
            catch
                value = '';
            end
        end
        
        function value = get.ReportDate(~)
            value = date;
        end 
        
        function value = get.msVersion(~)
            value = MSparklesSingleton.MSparklesVersion;
        end
        
        function value = get.micInfo(obj)
            try
                str = obj.hDataset.MetaData.ExtendedMetaData.ToString();           
                value = convertStringsToChars(str(:,1) + ' : ' + str(:,2))';   
            catch
                value = '';
            end
        end
        
        function value = get.analysisParams(obj)
            try
                value = obj.hAnalysis.AnalysisSettings.ToString();
            catch
                value = '';
            end
        end
        
        function value = get.Logo(~)
            logoPath = fullfile(iconrootdir, "msparkleslogo.png");
            f = figure('Visible', 'off', 'Units', 'normalized');
            image(axes(f), imread(logoPath));
            
            value = mlreportgen.report.Figure(f);
            value.Scaling = 'custom';
            value.Height = '2.0cm';
            value.Width = '2.0cm';
        end
    end
end

