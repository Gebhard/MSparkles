classdef MExcelSheet < handle    
    
    %MEXCELSHEET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
        File;
        Excel;
        ExcelWorkbook;
        workbookState;
        IsOpen;
    end
    
    methods
        function obj = MExcelSheet()
            try
                %obj.Excel = matlab.io.internal.getExcelInstance;
                obj.Excel = actxserver ('Excel.Application'); 
            catch exception %#ok<NASGU>
                warning(message('MATLAB:xlswrite:NoCOMServer'));                
                obj.Excel = [];
            end           
        end        
        
        function delete(obj)
            if ~isempty(obj.Excel)
                obj.Close();
            end
        end
    end
       
    methods
        Open(self, file);        
        Close(self);
        Write(obj, data, sheet, range);
        
        function bResult = isOpen(self)
            bResult = self.IsOpen;
        end
        
        function bResult = Exists(self)
            bResult = exist(self.File,'file');
        end                
    end
    
    methods(Access=private)
       writeExcel(self, data, sheet, range);
       writeFallback(self, data, sheet, range);
       [format,workbook,workbookState] = openExcelWorkbook(self, readOnly);
    end
    
    methods(Access=private)
        function [theMessage, TargetSheet, visibility] = activate_sheet(self,Sheet)
            % Activate specified worksheet in workbook.

            % Initialize worksheet object
            WorkSheets = self.Excel.sheets;
            theMessage = struct('message',{''},'identifier',{''});

            % Get name of specified worksheet from workbook
            try
                TargetSheet = get(WorkSheets,'item',Sheet);
            catch exception  %#ok<NASGU>
                % Worksheet does not exist. Add worksheet.
                TargetSheet = self.addsheet(WorkSheets,Sheet);
                %warning(message('MATLAB:xlswrite:AddSheet'));
                if nargout > 0
                    [theMessage.message,theMessage.identifier] = lastwarn;
                end
            end

            % We temporarily make xlSheetHidden and xlSheetVeryHidden spreadsheets
            % xlsheetVisible so we can write to them. Store the visibility for later.
            visibility = get(TargetSheet, 'Visible');

            % Activate silently fails if the sheet is hidden
            set(TargetSheet, 'Visible', 'xlSheetVisible');

            % activate worksheet
            Activate(TargetSheet);
        end
        
        function resetVisibility(~, TargetSheet, visibility)
            % Rehide the sheet after writing if it was hidden at the start
            if ismember(visibility, {'xlSheetHidden', 'xlSheetVeryHidden'})  
                set(TargetSheet, 'Visible', visibility);
            end
        end
        
        function newsheet = addsheet(~, WorkSheets,Sheet)
            % Add new worksheet, Sheet into worksheet collection, WorkSheets.

            if isnumeric(Sheet)
                % iteratively add worksheet by index until number of sheets == Sheet.
                while WorkSheets.Count < Sheet
                    % find last sheet in worksheet collection
                    lastsheet = WorkSheets.Item(WorkSheets.Count);
                    newsheet = WorkSheets.Add([],lastsheet);
                end
            else
                % add worksheet by name.
                % find last sheet in worksheet collection
                lastsheet = WorkSheets.Item(WorkSheets.Count);
                newsheet = WorkSheets.Add([],lastsheet);
            end
            % If Sheet is a string, rename new sheet to this string.
            if ischar(Sheet)
                set(newsheet,'Name',Sheet);
            end
        end               
        
        
        
        function messageStruct = exceptionHandler(nArgs, exception)
            if nArgs == 0
                throwAsCaller(exception);  	   
            else
                messageStruct.message = exception.message;       
                messageStruct.identifier = exception.identifier;
            end
        end
    end
end

