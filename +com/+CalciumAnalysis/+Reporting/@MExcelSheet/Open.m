function Open(self, file, mode)  
    try
        import matlab.io.internal.utility.*
    catch
    end

    if nargin < 3
        mode = 'w';
    else
       if ~ismember (mode, ['r', 'w'])
          error('File open mode must be "r" for read access or "w" for write acceess.'); 
       end
    end
    % handle requested Excel workbook filename.
    if ~isempty(file)
        file = convertStringsToChars(file);
        if ~ischar(file)
            error(message('MATLAB:xlswrite:InputClassFilename'));
        end
        % check for wildcards in filename
        if any(strfind(file, '*'))
            error(message('MATLAB:xlswrite:FileName'));
        end

        [Directory,file,ext]=fileparts(file);
        if isempty(ext) % add default Excel extension;
            if ispc()
                ext = '.xlsx';
            else
                ext = '.csv';
            end
        end

        file = abspath(fullfile(Directory,[file ext]));
%         [a1, a2] = fileattrib(file);
%         if a1 && ~(a2.UserWrite == 1)
%             error(message('MATLAB:xlswrite:FileReadOnly'));
%         end

        self.File = file;

    else % get workbook filename.
        error(message('MATLAB:xlswrite:EmptyFileName'));
    end

    if ispc()
        if ~self.Exists()
            % Create new workbook.  
            %This is in place because in the presence of a Google Desktop
            %Search installation, calling Add, and then SaveAs after adding data,
            %to create a new Excel file, will leave an Excel process hanging.  
            %This workaround prevents it from happening, by creating a blank file,
            %and saving it.  It can then be opened with Open.
            self.ExcelWorkbook = self.Excel.workbooks.Add;
            switch ext
                case '.xls' %xlExcel8 or xlWorkbookNormal
                   xlFormat = -4143;
                case '.xlsb' %xlExcel12
                   xlFormat = 50;
                case '.xlsx' %xlOpenXMLWorkbook
                   xlFormat = 51;
                case '.xlsm' %xlOpenXMLWorkbookMacroEnabled 
                   xlFormat = 52;
                otherwise
                   xlFormat = -4143;
            end
            self.ExcelWorkbook.SaveAs(file, xlFormat);
            self.ExcelWorkbook.Close(false);
        end

        %Open file
        readOnly = strcmpi(mode, 'r');
        [~, self.ExcelWorkbook, self.workbookState] = self.openExcelWorkbook(readOnly);    
    else
        
    end
end