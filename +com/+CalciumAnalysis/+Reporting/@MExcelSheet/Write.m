function Write(obj, data, sheet, range)
    if isempty(obj.Excel)
        obj.writeFallback(data, sheet, range);
    else
        obj.writeExcel(data, sheet, range);
    end    
end
