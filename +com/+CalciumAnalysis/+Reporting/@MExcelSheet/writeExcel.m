function writeExcel(self, data,sheet,range)
    try
        import matlab.io.internal.utility.*
    catch
    end

    Sheet1 = 1;

    if nargin < 3
        sheet = Sheet1;
        range = '';
    elseif nargin < 4
        range = '';
    end

    [sheet,range] = convertStringsToChars(sheet,range);

    try
        if self.ExcelWorkbook.ReadOnly ~= 0
            %This means the file is probably open in another process.
            error(message('MATLAB:xlswrite:LockedFile', self.File));
        end   

        if isempty(data)
            error(message('MATLAB:xlswrite:EmptyInput'));
        end
        data = convertStringsToChars(data);

        % Check for N-D array input data
        if ndims(data)>2 %#ok<ISMAT>
            error(message('MATLAB:xlswrite:InputDimension'));
        end

         % Check class of input data
        if ~(iscell(data) || isnumeric(data) || ischar(data)) && ~islogical(data)
            error(message('MATLAB:xlswrite:InputClass'));
        end

        % convert input to cell array of data.
        if iscell(data)
            A=data;
        else
            A=num2cell(data);
        end

        if nargin > 2
            % Verify class of sheet parameter.
            if ~(ischar(sheet) || (isnumeric(sheet) && sheet > 0))
                error(message('MATLAB:xlswrite:InputClassSheetArg'));
            end
            if isempty(sheet)
                sheet = Sheet1;
            end
            % parse REGION into sheet and range.
            % Parse sheet and range strings.
            if ischar(sheet) && contains(sheet,':')
                range = sheet; % only range was specified.
                sheet = Sheet1;% Use default sheet.
            elseif ~ischar(range)
                error(message('MATLAB:xlswrite:InputClassRangeArg'));
            end
        end

        try
            % select region.
            % Activate indicated worksheet.
            [theMessage, TargetSheet, visibility] = self.activate_sheet(sheet);
        catch exception
           error(message('MATLAB:xlswrite:InvalidSheetName', sheet)); 
        end

        try
            % Construct range string
            if ~contains(range,':')
                % Range was partly specified or not at all. Calculate range.
                [m,n] = size(A);
                range = calcrange(range,m,n);
            end
        catch exception
            success = false;
            theMessage = exceptionHandler(nargout, exception);
            return;
        end

        try
            % Select range in worksheet.
            Select(Range(self.Excel,sprintf('%s',range)));
        catch
            error(message('MATLAB:xlswrite:SelectDataRange'));
        end

        % Export data to selected region.
        set(self.Excel.selection,'Value',A);

        self.resetVisibility(TargetSheet, visibility);
        
        self.ExcelWorkbook.Save;
        
    catch exception
        if (~isempty(self.Excel) && exist(self.File, 'file') == 2)
            delete(self.File);
        end
        rethrow(exception);
        %success = false;
        %theMessage = exceptionHandler(nargout, exception);
    end
end

function range = calcrange(range,m,n)
    % Calculate full target range, in Excel A1 notation, to include array of size
    % m x n

    import matlab.io.spreadsheet.internal.columnLetter;
    import matlab.io.spreadsheet.internal.columnNumber;

    range = upper(range);
    cols = isletter(range);
    rows = ~cols;
    % Construct first row.
    if ~any(rows)
        firstrow = 1; % Default row.
    else
        firstrow = str2double(range(rows)); % from range input.
    end
    % Construct first column.
    if ~any(cols)
        firstcol = 'A'; % Default column.
    else
        firstcol = range(cols); % from range input.
    end
    try
        lastrow = num2str(firstrow+m-1);   % Construct last row as a string.
        firstrow = num2str(firstrow);      % Convert first row to string image.
        lastcol = columnLetter(columnNumber(firstcol)+n-1); % Construct last column.

        range = [firstcol firstrow ':' lastcol lastrow]; % Final range string.
    catch exception
        error(message('MATLAB:xlswrite:CalculateRange', range));
    end
end