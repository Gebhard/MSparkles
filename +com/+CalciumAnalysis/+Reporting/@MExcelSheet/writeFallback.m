function writeFallback(obj, data, sheet, range)
    % write data as CSV file, that is, comma delimited.
    %file = regexprep(obj.File,'(\.xls[^.]*+)$','.csv'); 
    
    try
        %dlmwrite(file,data, '-append', 'delimiter', ','); % write data.
        writematrix(data, obj.File, 'FileType','spreadsheet', 'Sheet', sheet, 'Range', range);
    catch exception
        exceptionNew = MException('MATLAB:xlswrite:dlmwrite','%s', getString(message('MATLAB:xlswrite:dlmwrite')));
        exceptionNew = exceptionNew.addCause(exception);
        throw(exceptionNew);
    end    
end