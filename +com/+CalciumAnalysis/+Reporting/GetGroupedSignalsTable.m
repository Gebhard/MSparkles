%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [grouped, hdr] = GetGroupedSignalsTable(hSignals)
%GETGROUPEDSIGNALSTABLE returns signal peaks, durations and peakframes,
%grouped by defined classes (threshold). Additionally, a collumn containing
%all peaks, durations and peakframes, respectively, is generated.
    % hSignals  object of a class derived from com.CalciumAnalysis.Analysis.MCaSignalsRoot
    % grouped   m-by-3(n+1) matrix.    Containing the peaks, durations, and
    %                                   peakFrames.
    % hdr 1-by-4(n+1) matrix, containing string with colum titles
    
    colsPerTH = 4;
    numThresh = numel(hSignals.Thresholds);
    graphPadCols = colsPerTH * (numThresh + 1);
    allTimesCol = colsPerTH * numThresh + 1;
    
    sigsTot = sum(hSignals.SignalCount);    
    
    grouped = nan( sigsTot, graphPadCols);        
    hdr = strings(1, graphPadCols);

    allFrames = [];
    allPeakVals = [];
    allDurVals = [];
    allSums = [];

    if sigsTot > 0
        for i=1:numThresh
            p = hSignals.SigMax(:, i);
            d = hSignals.Duration(:,i);    
            t = hSignals.PeakFrame(:,i);
            try
                s = hSignals.SigSum(:,i);
            catch
                s = cell(size(hSignals.SigSum,1), 1);
            end

            p = cat(1, p{:});
            d = cat(1, d{:});
            t = cat(1, t{:});
            s = cat(1, s{:});
            
            outCol = (i-1) * colsPerTH + 1;

            grouped(1:numel(t), outCol) = t;
            grouped(1:numel(p), outCol + 1) = p;
            grouped(1:numel(d), outCol + 2) = d; 
            grouped(1:numel(s), outCol + 3) = s; 

            hdr(1, outCol) = sprintf("Peak frame TH %d", i);
            hdr(1, outCol + 1) = sprintf("Peak value TH %d", i);
            hdr(1, outCol + 2) = sprintf("Duration TH %d", i);
            hdr(1, outCol + 3) = sprintf("Sum TH %d", i);

            allFrames = cat(1,allFrames,t);
            allPeakVals = cat(1,allPeakVals, p);
            allDurVals = cat(1,allDurVals, d);
            allSums = cat(1,allSums, s);
        end
    end

    hdr{1, allTimesCol } = sprintf('All frames');
    hdr{1, allTimesCol + 1 } = sprintf('All peaks');
    hdr{1, allTimesCol + 2 } = sprintf('All durations');
    hdr{1, allTimesCol + 3 } = sprintf('All sums');

    grouped(:, allTimesCol) = allFrames;
    grouped(:, allTimesCol + 1) = allPeakVals;
    grouped(:, allTimesCol + 2) = allDurVals;
    grouped(:, allTimesCol + 3) = allSums;
end

