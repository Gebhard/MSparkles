%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [sigComp, sigCompHdr] = GetSignalComposition(hSignals)
%GETSIGNALCOMPOSITION computes the number of signals per defined class
%(threshold) and the respective percentage.
    % hSignals  object of a class derived from com.CalciumAnalysis.Analysis.MCaSignalsRoot
    % sigComp   2-by-n matrix.  1st row contains signal counts per class,
    %                           2nd row contains respective percentages
    % sigCompHdr 3-by-(n+1) matrix, containing string with row/colum titles
    
    sigsTot = sum(hSignals.SignalCount);
    numThresh = numel(hSignals.Thresholds);
    
    sigComp = nan(6, numThresh);
    sigCompHdr = strings(7, numThresh +1);
    
    for i=1:numThresh
        sigCompHdr(1,1+i) = sprintf("Threshold %d (%.3f)", i, hSignals.Thresholds(i));
    end
    
    sigCompHdr(2,1) = "Total signal count";
    sigCompHdr(3,1) = "Percent";
    sigCompHdr(4,1) = "Signal peak (mean)";
    sigCompHdr(5,1) = "Signal peak (standard deviation)";
    sigCompHdr(6,1) = "Signal duration (mean)";
    sigCompHdr(7,1) = "Signal duration (standard deviation)";
    
    peakCounts      = cellfun(@numel, hSignals.PeakFrame);
    sumCounts       = sum(peakCounts, 1);
    numSignalsMax   = max(sumCounts(:));
    peakVals        = [];
    durVals         = [];
    
    %early out if no peaks are present
    if isempty(peakCounts)
        return;
    end
    
    for i=1:numThresh
        peaks = nan(numSignalsMax, 1);
        durations = nan(numSignalsMax, 1);

        p = hSignals.SigMax(:, i);
        d = hSignals.Duration(:, i);            
            
        NumRois = numel(p);
        currRow = 1;

        for r=1:NumRois
            numPeaks = numel(p{r});    

            if numPeaks > 0
                peaks(currRow:(currRow+numPeaks-1)) = p{r};
                durations(currRow:(currRow+numPeaks-1)) = d{r};
                currRow = currRow + numPeaks;
            end
        end
        
        peakVals = cat(2, peakVals, peaks);
        durVals = cat(2, durVals, durations);
    end
    
    sigComp(1,:) = sumCounts;
    sigComp(2,:) = sigComp(1,:) / sigsTot * 100;
    sigComp(3,:) = mean(peakVals,1, "omitnan");
    sigComp(4,:) = std(peakVals, 0, 1, "omitnan");
    sigComp(5,:) = mean(durVals,1, "omitnan");
    sigComp(6,:) = std(durVals, 0, 1, "omitnan");
end