%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [sig, sigRowHdr, sigCounts, sigCountHdr, sigComp, sigCompHdr, graphPad, graphPadHdr] = PrepareSignalTables( hSignals, tstamps )  
    if ~isempty(hSignals)
        numThresh = numel(hSignals.Thresholds);
        sigCounts = cellfun(@(x) numel(x), hSignals.SigMax);
        sigCountsPerClass = sum(sigCounts, 1);
        
        totalSigCount = sum(sigCounts(:));

        sigCountHdr = "Threshold " + (1:numThresh) +" (" + hSignals.Thresholds + ")";
        
        sigRowHdr = ["Signal No.", "ROI No.", "Signal class", ...
                "Peak frame", "Peak time(sec)",...
                "Peak value", "Integrated fluorescence", "Mean fluorescence",...
                "Duration", "Rise time", "Decay time"];
            
        sig = zeros(totalSigCount, numel(sigRowHdr));

        roiNums = [];
        for i=1:numThresh 
            roiNums = cat(1, roiNums, repelem(1:hSignals.NumRois, sigCounts(:,i))' );
        end
        
        %In case something went wrong and one of these is empty, we don't
        %want to crash...
        peakFrames  = cat(1, hSignals.PeakFrame{:});
        peakValues  = cat(1, hSignals.SigMax{:});
        sumFluo     = cat(1, hSignals.SigSum{:});
        meanFluo    = cat(1, hSignals.SigAvg{:});
        sigDur      = cat(1, hSignals.Duration{:});
        riseTimes   = cat(1, hSignals.RiseTime{:});
        decayTimes  = cat(1, hSignals.DecayTime{:});
        
        sig(:,1) = 1:totalSigCount; %Signal numbers
        sig(:,2) = roiNums; %Roi Numbers
        sig(:,3) = repelem( 1:numThresh, sigCountsPerClass )'; %Signal class
        if ~isempty(peakFrames)
            sig(:,4) = peakFrames; %Frame No of peak
        end
        sig(:,5) = tstamps(sig(:,4)); %Time (sec) of peak
        if ~isempty(peakValues)
            sig(:,6) = peakValues; %Peak value 
        end
        if ~isempty(sumFluo)
            sig(:,7) = sumFluo; %Summed fluorescence
        end
        if ~isempty(meanFluo)
            sig(:,8) = meanFluo; %Mean fluorescence
        end
        if ~isempty(sigDur)
            sig(:,9) =  sigDur; %Duration
        end
        if ~isempty(riseTimes)
            sig(:,10) = riseTimes; %Rise time
        end
        if ~isempty(riseTimes)
            sig(:,11) = decayTimes; %decay time    
        end

        [sigComp, sigCompHdr] = com.CalciumAnalysis.Reporting.GetSignalComposition(hSignals);
        [graphPad, graphPadHdr] = com.CalciumAnalysis.Reporting.GetGroupedSignalsTable(hSignals);
                
        if isempty(sigCounts)
           sigCounts = nan(1,1); 
        end

        if isempty(sigComp)
           sigComp = nan(1,1); 
        end

        if isempty(graphPad)
           graphPad = nan(1,1); 
        end
    else
        sig = [];
        sigRowHdr = [];
        sigCounts = [];
        sigCountHdr = [];
        sigComp = [];
        sigCompHdr = [];
        graphPad = [];
        graphPadHdr = [];
    end
end

