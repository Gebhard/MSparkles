%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaAnalysisCfg < com.common.Proc.PStageConfig
    %MCAANALYSISCFG Base class for individual analysis configurations
    
    properties(Constant)
        DEF_SIGNALSCALING = com.Enum.SignalScalingEnum.Global;
    end
    
    properties (SetObservable, AbortSet)
        ColormapName = 'MSparkles';
    end   
    
    properties
        SignalScaling com.Enum.SignalScalingEnum;
        SignalScalingUBound;
        SignalScalingLBound;
    end
    
    methods
        function obj = MCaAnalysisCfg(name)
            obj = obj@com.common.Proc.PStageConfig(name);
            
            obj.SignalScaling = obj.DEF_SIGNALSCALING;
        end
    end
    
    methods
        function colorLut = GetColormap(obj, numEntries, Colormap, ColorIdx)
            arguments
                obj (1,1) {mustBeNonempty};
                numEntries (1,1) {mustBeNonnegative, mustBeNumeric} = 0;
                Colormap = obj.ColormapName;
                ColorIdx = [];
            end
            
            if isempty(Colormap)
                Colormap = obj.ColormapName;
            end
                    
            if ~isempty(ColorIdx)
                colorLut = ones(numEntries, 3);
                numUniqueIdx = max(unique(ColorIdx(:,2)));
                dummyLut = feval( lower(Colormap), numUniqueIdx);
                colorLut(ColorIdx(:,1), :) = dummyLut(ColorIdx(:,2), :);
            else
                colorLut =  feval( lower(Colormap), numEntries);
            end
        end
                
        function value = get.SignalScaling(obj)
            if isempty(obj.SignalScaling)
                obj.SignalScaling = com.Enum.SignalScalingEnum.Global;
            end
            
            value = obj.SignalScaling;
        end    
        
        %Delete in 1.9
        function set.SignalScaling(obj, value)
            if strcmp( class(value), "SignalScalingEnum")
                value = com.Enum.SignalScalingEnum(string(value));
            end
            
            obj.SignalScaling = value;
        end
    end
end

