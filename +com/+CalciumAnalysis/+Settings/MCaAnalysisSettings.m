%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaAnalysisSettings < com.common.Cloneable
    %MCAANALYSISSETTINGS Collection of settings common to all analyses of a
    %dataset.
    %   All analyses in a given dataset are carried out using the same
    %   general rules, e.g. to exclude sub-signals, and use the same
    %   clasification thresholds.
    
    properties(Constant)
        DEF_TH = [0.5, 1.0, 1.5];
        DEF_STATIONARITYTH = 10;
        DEF_SYNCTH = 0.5;
        DEF_MINPROMINENCE = 0.25;
        DEF_ENABLESGOLAYFILT = false;
        DEF_SGOLAYORDER = 3;
        DEF_SGOLAYSIZE = 7;
        DEF_MAXPEAKWIDTH = 50;
        DEF_USEMANUALTH = true;
        DEF_EXCLUDESUBSIGNALS = false;
        DEF_DURATIONHEIGHT = 0.5;   %Default: 0.5 = FWHM
        DEF_SYNCWIDTHREF = 0.5;     %Default: 0.5 = FWHM
        DEF_SYNCEXCLUDESUBSIGNALS = false;
        DEF_SYNCMINPEAKPROM = 0;    %percent
    end  
    
    properties
        ClassificationTh;       %Array of classification thresholds
        UseManualTh;            %Boolean, If false, auto thresholding is used
        ExcludeSubSignals;      %Boolean, Allows to exclude subsignals and only extract large signals
        DurationHeight;         %Level of prominance at which the duration is to be computed
        MaxPeakWidth;           %Maximum peak width in seconds
        StationarityTH;         %If dynamic signals move further away from their origin than this threshold, they are considered non-stationary
        SyncClassificationTH;   %Threshold, abouch which a synchronous event is detected
        MinPeakProminence;      %Minimum prominence of a calcium signal to be recognized as such
        EnableSgolayFilt;       %Boolean to enable additional Savitzky-Golay filtering to the integrated ROI traces
        SGolayOrder;            %Order of Savitzky-Golay filter
        SGolaySize;             %Window size of Savitzky-Golay filter
        SyncWidthRef;           %Width reference of peak height in range [0..1] to compute duration of synchronous event
        SyncExcludeSubSignals;  %Exclude sub-sigbnals in synchronicity analysis.
        SyncMinPeakProm;        %Minimum peak prominence of sync peak
    end
    
    methods
        function obj = MCaAnalysisSettings()
            obj.ClassificationTh        = obj.DEF_TH;
            obj.UseManualTh             = obj.DEF_USEMANUALTH;
            obj.ExcludeSubSignals       = obj.DEF_EXCLUDESUBSIGNALS;
            obj.DurationHeight          = obj.DEF_DURATIONHEIGHT;   
            obj.StationarityTH          = obj.DEF_STATIONARITYTH;
            obj.SyncClassificationTH    = obj.DEF_SYNCTH;
            obj.MinPeakProminence       = obj.DEF_MINPROMINENCE;
            obj.EnableSgolayFilt        = obj.DEF_ENABLESGOLAYFILT;
            obj.SGolayOrder             = obj.DEF_SGOLAYORDER;
            obj.SGolaySize              = obj.DEF_SGOLAYSIZE;
            obj.MaxPeakWidth            = obj.DEF_MAXPEAKWIDTH;
            obj.SyncWidthRef            = obj.DEF_SYNCWIDTHREF;
            obj.SyncExcludeSubSignals   = obj.DEF_SYNCEXCLUDESUBSIGNALS;
            obj.SyncMinPeakProm         = obj.DEF_SYNCMINPEAKPROM;
        end
    end
    
    methods
        function value = get.SyncExcludeSubSignals(obj)
            if isempty(obj.SyncExcludeSubSignals)
                obj.SyncExcludeSubSignals = obj.DEF_SYNCEXCLUDESUBSIGNALS;
            end
           
            value = obj.SyncExcludeSubSignals;
        end
        
        function value = get.SyncMinPeakProm(obj)
            if isempty(obj.SyncMinPeakProm)
                obj.SyncMinPeakProm = obj.DEF_SYNCMINPEAKPROM;
            end
           
            value = obj.SyncMinPeakProm;
        end
        
        function value = get.SyncWidthRef(obj)
            if isempty(obj.SyncWidthRef)
                obj.SyncWidthRef = obj.DEF_SYNCWIDTHREF;
            end
           
            value = obj.SyncWidthRef;
        end
        
        function value = get.MaxPeakWidth(obj)
            if isempty(obj.MaxPeakWidth)
                obj.MaxPeakWidth = obj.DEF_MAXPEAKWIDTH;
            end
           
            value = obj.MaxPeakWidth;
        end
        
        function value = get.EnableSgolayFilt(obj)
            if isempty(obj.EnableSgolayFilt)
                obj.EnableSgolayFilt = obj.DEF_ENABLESGOLAYFILT;
            end
           
            value = obj.EnableSgolayFilt;
        end
        
        function value = get.SGolayOrder(obj)
            if isempty(obj.SGolayOrder)
                obj.SGolayOrder = obj.DEF_SGOLAYORDER;
            end
           
            value = obj.SGolayOrder;
        end
        
        function value = get.SGolaySize(obj)
            if isempty(obj.SGolaySize)
                obj.SGolaySize = obj.DEF_SGOLAYSIZE;
            end
           
            value = obj.SGolaySize;
        end
        
        function value = get.SyncClassificationTH(obj)
            if isempty(obj.SyncClassificationTH)
                obj.SyncClassificationTH = obj.DEF_SYNCTH;
            end
           
            value = obj.SyncClassificationTH;
        end
        
        function value = get.MinPeakProminence(obj)
            if isempty(obj.MinPeakProminence)
                obj.MinPeakProminence = obj.DEF_MINPROMINENCE;
            end
           
            value = obj.MinPeakProminence;
        end
        
        function value = get.ExcludeSubSignals(obj)
            if isempty(obj.ExcludeSubSignals)
                obj.ExcludeSubSignals = false;
            end
           
            value = obj.ExcludeSubSignals;
        end  
         
        function value = get.DurationHeight(obj)
            if isempty(obj.DurationHeight)
                obj.DurationHeight = 0.5;
            end
           
            value = obj.DurationHeight;
        end  
        
        function value = get.StationarityTH(obj)
            if isempty(obj.StationarityTH)
                obj.StationarityTH = obj.DEF_STATIONARITYTH;
            end
            
            value = obj.StationarityTH;
        end
    end
end

