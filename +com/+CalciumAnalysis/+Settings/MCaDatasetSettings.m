%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaDatasetSettings < com.common.Cloneable
    %MCADATASETSETTINGS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties       
        Analysis com.CalciumAnalysis.Settings.MCaAnalysisSettings;       
        Display com.CalciumAnalysis.Settings.MCaDisplaySettings;
        EegSync com.CalciumAnalysis.Settings.MCaEegSyncSettings;
    end
    
    methods
        function obj = MCaDatasetSettings()
            obj.Analysis    = com.CalciumAnalysis.Settings.MCaAnalysisSettings;
            obj.Display     = com.CalciumAnalysis.Settings.MCaDisplaySettings;
            obj.EegSync     = com.CalciumAnalysis.Settings.MCaEegSyncSettings;
        end
    end
    
    methods        
        function value = get.Analysis(obj)
            if (isempty(obj.Analysis))
                obj.Analysis = com.CalciumAnalysis.Settings.MCaAnalysisSettings;
            end
            
            value = obj.Analysis;
        end
        
        function value = get.Display(obj)
            if isempty(obj.Display)
                obj.Display = com.CalciumAnalysis.Settings.MCaDisplaySettings;
            end
            
            value = obj.Display;
        end
        
        function value = get.EegSync(obj)
            if isempty(obj.EegSync)
                obj.EegSync = com.CalciumAnalysis.Settings.MCaEegSyncSettings;
            end
            
            value = obj.EegSync;
        end
    end
end

