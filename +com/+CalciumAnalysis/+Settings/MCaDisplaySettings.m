%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaDisplaySettings < com.common.Cloneable
    %MCADISPLAYSETTINGS Summary of this class goes here
    %   Detailed explanation goes here
        
    properties(Constant)
        DefaultChannelColors = [    0, 240/255, 16/255;
                                    0, 38/255, 255/255;
                                    240/255, 8/255 , 0;
                                    25/2555, 128/255, 0;
                                    0, 74/255, 127/255;
                                    230/255, 32/255, 230/255;
                                    63/255, 127/255, 71/255;
                                ];
                            
        DEFAULT_RELEASEUPDATE_TH            = 0.1;
        DEFAULT_RELEASEUPTAKE_LBOUND        = -1;
        DEFAULT_RELEASEUPTAKE_UBOUND        = 1;
        DEFAULT_RELEASEUPTAKE_MEDFILTSIZE   = 3;
        DEFAULT_FLUORANGE_MEDFILTSIZE       = 3;
        DEFAULT_FLUORANGE_GAUSSFILTSIGMA    = 2;
    end
    
    properties
        ChannelColors = [];             % User-defined base colors for color mapping of individual channels
        RoiOverlayTransparency = 50;    % Transparency (alpha) value for ROI and signal dynamics overlay
        ReleaseUptakeTH;                % Threshold for visualizing release/uptake. values below this value are set to 0
        ReleaseUptakeMedFiltSize;       % Filter size of median filter for display purposes.
        ReleaseUptakeLBound;            % Lower value as minimum for visualization
        ReleaseUptakeUBound;            % Upper value as maximum for visualization
        FluorescentRangeMedianSize;     % Size of median filter for displaying fluorescent range.
        FluorescentRangeGaussSigma;     % Size of Gauss filter for displaying fluorescent range.
    end
    
    properties(Transient)%Deprecated
        ReleaseUptakeMedFilt;           % Boolean to indicate if the calculated release/uptake map should be median filterd to reduce noise
    end
    
    properties (Access=private)
        dataMap;
    end
    
    properties (Access=private, Transient, Dependent)
        DisplayBounds = {};
        DisplayGamma = {};
    end
    
    properties (Dependent, SetAccess=private)
        RoiOverlayTransparencyP;
    end
    
    methods
        function obj = MCaDisplaySettings()
            obj.ChannelColors = com.CalciumAnalysis.Settings.MCaDisplaySettings.DefaultChannelColors;
            obj.dataMap                     = containers.Map;
            obj.ReleaseUptakeTH             = obj.DEFAULT_RELEASEUPDATE_TH;
            obj.ReleaseUptakeMedFiltSize    = obj.DEFAULT_RELEASEUPTAKE_MEDFILTSIZE;
            obj.FluorescentRangeGaussSigma  = obj.DEFAULT_FLUORANGE_GAUSSFILTSIGMA;
            obj.FluorescentRangeMedianSize  = obj.DEFAULT_FLUORANGE_MEDFILTSIZE;
        end
    end
    
    methods
        function value = get.FluorescentRangeGaussSigma(obj)
            if isempty(obj.FluorescentRangeGaussSigma)
               obj.FluorescentRangeGaussSigma = obj.DEFAULT_FLUORANGE_GAUSSFILTSIGMA;
            end
            
            value = obj.FluorescentRangeGaussSigma;
        end
        
        function value = get.FluorescentRangeMedianSize(obj)
            if isempty(obj.FluorescentRangeMedianSize)
               obj.FluorescentRangeMedianSize = obj.DEFAULT_FLUORANGE_MEDFILTSIZE; 
            end
            
            value = obj.FluorescentRangeMedianSize;
        end
        
        function value = get.ReleaseUptakeMedFiltSize(obj)
            if isempty(obj.ReleaseUptakeMedFiltSize)
               obj.ReleaseUptakeMedFiltSize = obj.DEFAULT_RELEASEUPTAKE_MEDFILTSIZE; 
            end
            
            value = obj.ReleaseUptakeMedFiltSize;
        end
        
        function value = get.ReleaseUptakeTH(obj)
            if isempty(obj.ReleaseUptakeTH)
                obj.ReleaseUptakeTH = obj.DEFAULT_RELEASEUPDATE_TH;
            end
            
            value = obj.ReleaseUptakeTH;
        end
        
        function value = get.dataMap(obj)
            if isempty(obj.dataMap)
                obj.dataMap = containers.Map;
            end
            
            value = obj.dataMap;
        end
        
        function set.DisplayBounds(obj, value)
            obj.SetDisplayBounds("F", [], value);
        end
        
        function set.DisplayGamma(obj, value)
            obj.SetDisplayGamma("F", [], value);
        end
        
        function value = get.RoiOverlayTransparencyP(obj)
            value = obj.RoiOverlayTransparency / 100.0;
        end
        
        function value = get.ReleaseUptakeLBound(obj)
            if isempty(obj.ReleaseUptakeLBound)
                obj.ReleaseUptakeLBound = obj.DEFAULT_RELEASEUPTAKE_LBOUND;
            end
            
            value = obj.ReleaseUptakeLBound;
        end
        
        function value = get.ReleaseUptakeUBound(obj)
            if isempty(obj.ReleaseUptakeUBound)
                obj.ReleaseUptakeUBound = obj.DEFAULT_RELEASEUPTAKE_UBOUND;
            end
            
            value = obj.ReleaseUptakeUBound;
        end
    end
    
    methods 
        function color = GetChannelColor(obj, channel)
            try
                color = obj.ChannelColors(channel, :);
            catch
                color = obj.DefaultChannelColors( mod(channel-1, numel(obj.DefaultChannelColors)) +1, : );
            end
        end
        
        function color = GetNormalizedChannelColor(obj, channel)
            try
                color = obj.ChannelColors(channel, :);
            catch
                color = obj.DefaultChannelColors( mod(channel-1, numel(obj.DefaultChannelColors)) +1, : );
            end
            
            if any(color > 1)
               color = color ./ 255; 
            end
        end
        
        function SetChannelColor(obj, channel, col)
            obj.ChannelColors(channel, 1:3) = col;
        end
        
        function colMap = GetColormp(obj, channel, steps)
            if nargin == 2
                steps = 255;
            end
            
            try
                c = obj.ChannelColors(channel);
                
                R = linspace(0, c(1), steps);
                G = linspace(0, c(2), steps);
                B = linspace(0, c(3), steps);
                
                colMap = colormap([R(:), G(:), B(:)]);
            catch
                % channel was not a valid index. Return default
                colMap = colormap('gray');
            end
        end
        
        function SetDisplayBounds(obj, name, channel, value)
            s = obj.getSettings( name );
            
            if ~isempty( channel )
                s.DisplayBounds{channel} = value;
            else
                s.DisplayBounds = value;
            end
            
            obj.dataMap(name) = s;
        end
        
        function value = GetDisplayBounds(obj, name, channel)
            try
                s = obj.getSettings( name );
                value = s.DisplayBounds{channel};
            catch
                value = [];
            end
        end               
        
        function value = GetMaxDisplayBounds(obj, name)
            try
                s = obj.getSettings( name );
                b = zeros(numel(s.DisplayBounds) , 2);

                for i=1:numel(s.DisplayBounds)
                    b(i,:) = s.DisplayBounds{i};
                end

                value = [ min(b(:,1)), max(b(:,2)) ];
            catch
                value = [];
            end
        end
        
        function SetDisplayGamma(obj, name, channel, value)
            s = obj.getSettings( name );
            
            if ~isempty( channel )
                s.DisplayGamma{channel} = value;
            else
                s.DisplayGamma = value;
            end
            
            obj.dataMap(name) = s;
        end
        
        function value = GetDisplayGamma(obj, name, channel)
            try
                s = obj.getSettings( name );
                value = s.DisplayGamma{channel};                                
            catch
                value = 1;
            end
        end
    end
    
    methods(Access=private)
        function s = getSettings(obj, name)
            if obj.dataMap.isKey(name)
                s = obj.dataMap(name);
            else
                s.DisplayBounds = cell(1,1);
                s.DisplayGamma = {1};
            end
        end
    end
end
