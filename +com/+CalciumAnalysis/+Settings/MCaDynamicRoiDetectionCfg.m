%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaDynamicRoiDetectionCfg < com.CalciumAnalysis.Settings.MCaRoiDetectionCfg
    %MCADYNAMICROIDETECTIONCFG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        DEF_USERAREASCALING = 0;
        DEF_MINSIGNALDURATION = 2;
        DEF_SHOWTRACES = false;
        DEF_SPATIALSIGMA = 2;
        DEF_CLAMPDFF = false;
        DEF_TRACKERTEMPSIGMA = 4;
        DEF_TRACKERTEMPITER = 1;
        DEF_TRACKERSPATIALSIGMA = 4;
        DEF_TRACKERSPATIALITER = 6;
        DEF_TRACKINGGAP = 2;
        DEF_TRACKERPENALTY = 5;
        DEF_MINPEAKHEIGHT = 0.5;
        DEF_TRACKDILATESIZE = 3;
        DEF_TRACKERODESIZE = 4;
        DEF_TRACKOPENSIZE  = 3;
        DEF_DILATEMAXIMA = 4;
        DEF_THRESHOLD = 0.25;
    end
    
    properties
        UserDefAreaScaling;
        ShowTracePlots;
        MinSignalDuration;  %Signals "shorter" than this will be removed firectly after ROI detection
        SpatialSigma;       %Spatial signal for signal certanity
        
        
        ClampdFF0;          %Boolean flag indicating if values <0 should be clamped
        TrackerTempSigma;   %Sigma for temporal Gauss smoothing
        TrackerTempIter;    %Iterations for temporal smoothing (sclaes of sclae space)
        TrackerSpatialSigma;%Sigma for spatial smoothing
        TrackerSpatialIter; %Iterations for spatial smoothing (compute scales of scale space)
        Threshold;
        
        %Maxima extraction
        TrackOpenSize;      %Size of SE for opening operation to extract maxima
        TrackErodeSize;     %Size of SE for erosion after opening to reconstruct "cleaned" image
        TrackDilate;        %Size of SE for dilation to reconstruct better separated version of image
        DilateMaxima;       %Size of SE to dilate extracted maxima
        MinPeakHeight;      %Minimum peak height in (F-F0)/F0
        TrackingGap;        %A Deteckted maximum can be projected over this number of timesteps befor being removed
        TrackerPenalty;     %Penalty for non-assigned tracks
    end
    
    properties(Transient) %Deprecated
        NHood;
        MaxDistance;        %Everything above this vale is > 100%
        MorphSeSize;        %Size of Structuring element for morphological OPs
        BaselinePercentile; %Percentile along time which defines the baseline level
        MinDistance;        %Everything above this vale is < 0%
        UseSignedDistance;
        TemporalMedianSize;
        WindowHalfSize;     %Windows size for signal certanity calculation
        CleanupMorphSize;   %Size for morphological opening to remove too small maxima
    end
    
    methods
        function obj = MCaDynamicRoiDetectionCfg()
            obj = obj@com.CalciumAnalysis.Settings.MCaRoiDetectionCfg("Dynamic ROI detection");
            obj.UserDefAreaScaling  = obj.DEF_USERAREASCALING;
            obj.MinSignalDuration   = obj.DEF_MINSIGNALDURATION;
            obj.MinimumRoiSize      = obj.DEF_MINROISIZE;
            obj.ShowTracePlots      = obj.DEF_SHOWTRACES;
           
            obj.SpatialSigma        = obj.DEF_SPATIALSIGMA;
            obj.ClampdFF0           = obj.DEF_CLAMPDFF;
            obj.TrackerTempSigma    = obj.DEF_TRACKERTEMPSIGMA;
            obj.TrackerTempIter     = obj.DEF_TRACKERTEMPITER;
            obj.TrackerSpatialSigma = obj.DEF_TRACKERSPATIALSIGMA;
            obj.TrackerSpatialIter  = obj.DEF_TRACKERSPATIALITER;
            obj.TrackingGap         = obj.DEF_TRACKINGGAP;
            obj.TrackerPenalty      = obj.DEF_TRACKERPENALTY;
            obj.Threshold           = obj.DEF_THRESHOLD;

            obj.TrackOpenSize       = obj.DEF_TRACKOPENSIZE;
            obj.TrackErodeSize      = obj.DEF_TRACKERODESIZE;
            obj.TrackDilate         = obj.DEF_TRACKDILATESIZE;
            obj.MinPeakHeight       = obj.DEF_MINPEAKHEIGHT;
            obj.DilateMaxima        = obj.DEF_DILATEMAXIMA;
        end
    end
    
    methods 
        function value = get.ClampdFF0(obj)
            if isempty(obj.ClampdFF0)
                obj.ClampdFF0 = obj.DEF_CLAMPDFF;
            end
            
            value = obj.ClampdFF0;
        end
        
        function value = get.TrackerTempSigma(obj)
            if isempty(obj.TrackerTempSigma)
                obj.TrackerTempSigma = obj.DEF_TRACKERTEMPSIGMA;
            end
            
            value = obj.TrackerTempSigma;
        end
        
        function value = get.TrackerTempIter(obj)
            if isempty(obj.TrackerTempIter)
                obj.TrackerTempIter = obj.DEF_TRACKERTEMPITER;
            end
            
            value = obj.TrackerTempIter;
        end
        
        function value = get.TrackerSpatialSigma(obj)
            if isempty(obj.TrackerSpatialSigma)
                obj.TrackerSpatialSigma = obj.DEF_TRACKERSPATIALSIGMA;
            end
            
            value = obj.TrackerSpatialSigma;
        end
        
        function value = get.TrackerSpatialIter(obj)
            if isempty(obj.TrackerSpatialIter)
                obj.TrackerSpatialIter = obj.DEF_TRACKERSPATIALITER;
            end
            
            value = obj.TrackerSpatialIter;
        end
        
        function value = get.TrackingGap(obj)
            if isempty(obj.TrackingGap)
                obj.TrackingGap = obj.DEF_TRACKINGGAP;
            end
            
            value = obj.TrackingGap;
        end
        
        function value = get.TrackerPenalty(obj)
            if isempty(obj.TrackerPenalty)
                obj.TrackerPenalty = obj.DEF_TRACKERPENALTY;
            end
            
            value = obj.TrackerPenalty;
        end
        
        function value = get.DilateMaxima(obj)
            if isempty(obj.DilateMaxima)
                obj.DilateMaxima = obj.DEF_DILATEMAXIMA;
            end
            
            value = obj.DilateMaxima;
        end
        
        function value = get.TrackOpenSize(obj)
            if isempty(obj.TrackOpenSize)
                obj.TrackOpenSize = obj.DEF_TRACKOPENSIZE;
            end
            
            value = obj.TrackOpenSize;
        end
        
        function value = get.TrackErodeSize(obj)
            if isempty(obj.TrackErodeSize)
                obj.TrackErodeSize = obj.DEF_TRACKERODESIZE;
            end
            
            value = obj.TrackErodeSize;
        end
        
        function value = get.TrackDilate(obj)
            if isempty(obj.TrackDilate)
                obj.TrackDilate = obj.DEF_TRACKDILATESIZE;
            end
            
            value = obj.TrackDilate;
        end
        
        function value = get.MinPeakHeight(obj)
            if isempty(obj.MinPeakHeight)
                obj.MinPeakHeight = obj.DEF_MINPEAKHEIGHT;
            end
            
            value = obj.MinPeakHeight;
        end
        
        function value = get.Threshold(obj)
            if isempty(obj.Threshold)
                obj.Threshold = obj.DEF_THRESHOLD;
            end
            
            value = obj.Threshold;
        end
        
        function value = get.SpatialSigma(obj)
            if isempty(obj.SpatialSigma)
                obj.SpatialSigma = obj.DEF_SPATIALSIGMA;
            end
            
            value = obj.SpatialSigma;
        end   
        
        function value = get.ShowTracePlots(obj)
            if isempty(obj.ShowTracePlots)
                obj.ShowTracePlots = obj.DEF_SHOWTRACES;
            end
            
            value = obj.ShowTracePlots;
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
            hStage = com.CalciumAnalysis.Processing.Stages.PStageDetectSignalMorphology(varargin{:});
        end
    end
end

