%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaEegSyncSettings < com.common.Cloneable
    %MCAEEGSYNCSETTINGS Settings for synchronizing EEG data with calcium
    %data.
    
    properties
        EegSyncStart        = 1;        % Time in EEG signal where movie begins
        EegDisplayTimeSpan  = 10;       % Width of window for EEG in seconds
        DisplayEegChannel   = 2;        % EEG signal channel to be displayed        
        InvertSyncChannel   = true;     % Boolean flag indicating if sync channel must be inverted for signal detection
        SyncEveryNthFrame   = 1;        % Modulo divisor
        CorrelationChannel  = 2:9;    % Default: Use Average of all signal channels
        CorrelationBand     = com.EEG.Enum.WaveBand.absPower;  % Use signal power for Correlation
    end
    
    methods
        function value = get.CorrelationChannel(obj)
            if isempty(obj.CorrelationChannel)
                obj.CorrelationChannel = 2:9;
            end
            
            value = obj.CorrelationChannel;
        end
        
        function value = get.CorrelationBand(obj)
            if isempty(obj.CorrelationBand)
                obj.CorrelationBand = com.EEG.Enum.WaveBand.absPower;
            end
            
            value = obj.CorrelationBand;
        end
    end
end

