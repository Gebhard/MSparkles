%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaRoiDetectionCfg < com.CalciumAnalysis.Settings.MCaAnalysisCfg
    %MCAROIDETECTIONCFG Base class for auto matic ROI detection
    %configuration.
    
    properties(Constant)
        DEF_MINROISIZE = 50;
        DEF_THRESHOLDMODE = com.Enum.ThresholdTypeEnum.Guided;
        DEF_USERDEFTHRESHOLD = 1;
    end
    
    properties
        ThresholdMode com.Enum.ThresholdTypeEnum;
        UserDefThreshold;
        Sensitivity;
        MinimumRoiSize;
    end        
    
    methods
        function obj = MCaRoiDetectionCfg(name)
            obj = obj@com.CalciumAnalysis.Settings.MCaAnalysisCfg(name);
            obj.MinimumRoiSize = obj.DEF_MINROISIZE;
            obj.ThresholdMode = obj.DEF_THRESHOLDMODE;
            obj.UserDefThreshold = obj.DEF_USERDEFTHRESHOLD;
        end                
    end   
    
    %delete in 1.9
    methods
        function set.ThresholdMode(obj, value)
            if strcmp( class(value), "ThresholdTypeEnum")
                value = com.Enum.SignalScalingEnum(string(value));
            end
            
            obj.ThresholdMode = value;
        end
    end
end

