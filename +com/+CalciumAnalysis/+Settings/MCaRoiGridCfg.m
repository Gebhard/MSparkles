%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaRoiGridCfg < com.CalciumAnalysis.Settings.MCaAnalysisCfg
    %MCAROIGRIDCFG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
       DEFAULT_GRID_SIZE = [16 16] ;
    end
    
    properties        
        GridSize;
    end
    
    methods
        function obj = MCaRoiGridCfg() 
            obj = obj@com.CalciumAnalysis.Settings.MCaAnalysisCfg("ROI grid");
            obj.GridSize = com.CalciumAnalysis.Settings.MCaRoiGridCfg.DEFAULT_GRID_SIZE;
        end
    end
    
    methods
        function value = get.GridSize(self)
            if (isempty(self.GridSize))
               self.GridSize = com.CalciumAnalysis.Settings.MCaRoiGridCfg.DEFAULT_GRID_SIZE;
            end
            
            value = self.GridSize;
        end                
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
            hStage = com.CalciumAnalysis.Processing.Stages.PStageGenerateRoiGrid(varargin{:});
        end
    end
end

