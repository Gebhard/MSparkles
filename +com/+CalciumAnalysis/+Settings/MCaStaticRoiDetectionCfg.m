%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCaStaticRoiDetectionCfg < com.CalciumAnalysis.Settings.MCaRoiDetectionCfg
    %MCASTATICROIDETECTIONCFG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        DEF_SENSITIVITY                     = 0.5;
        DEF_USEUSERTHRESHOLD                = false;
        DEF_USERTHRESHHOLDVALUE             = 0;
        DEF_DYNRNGSIGMA                     = 3;
        DEF_DYNRNGKRNLSIZE                  = 7;
        DEF_SEGMENTATIONMETHOD              = 1; %1=Watershed, 2=Superpixels
        DEF_CORRELATIONTHRESHOLD            = 0.5;
    end
    
    properties
        bUserDefinedThreshold;      % Indicates if manually chosen threshold is to be used instead of automatically determined
        UserDefinedThresholdValue;  % Manual threshold value.
        DynRngSigma;                %Sigma parameter to smooth (dynamic) range image.
        DynRngKrnlSize;             %Kernel size for symmetric median filter if (dynamic) range image
        SegmentationMethod;
        CorrelationThreshold;
    end
    
    %Deprecated
    properties(Transient)
        EegCorrAnalysisUseSynchronicity;
    end
    
    methods
        function obj = MCaStaticRoiDetectionCfg()
            obj = obj@com.CalciumAnalysis.Settings.MCaRoiDetectionCfg("Automatic ROI detection");
            obj.Sensitivity                     = obj.DEF_SENSITIVITY;
            obj.bUserDefinedThreshold           = obj.DEF_USEUSERTHRESHOLD;
            obj.UserDefinedThresholdValue       = obj.DEF_USERTHRESHHOLDVALUE;
            obj.DynRngSigma                     = obj.DEF_DYNRNGSIGMA;
            obj.DynRngKrnlSize                  = obj.DEF_DYNRNGKRNLSIZE;
            obj.SegmentationMethod              = obj.DEF_SEGMENTATIONMETHOD;
            obj.CorrelationThreshold            = obj.DEF_CORRELATIONTHRESHOLD;
        end
    end
    
    methods
        function value = get.CorrelationThreshold(obj)
            if isempty(obj.CorrelationThreshold)
                obj.CorrelationThreshold = obj.DEF_CORRELATIONTHRESHOLD;
            end
            
            value = obj.CorrelationThreshold;
        end
        
        function value = get.SegmentationMethod(obj)
            if isempty(obj.SegmentationMethod)
                obj.SegmentationMethod = obj.DEF_SEGMENTATIONMETHOD;
            end
            
            value = obj.SegmentationMethod;
        end
        
        function value = get.DynRngSigma(obj)
            if isempty(obj.DynRngSigma)
                obj.DynRngSigma = obj.DEF_DYNRNGSIGMA;
            end
            
            value = obj.DynRngSigma;
        end
        
        function value = get.DynRngKrnlSize(obj)
            if isempty(obj.DynRngKrnlSize)
                obj.DynRngKrnlSize = obj.DEF_DYNRNGKRNLSIZE;
            end
            
            value = obj.DynRngKrnlSize;
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
            hStage = com.CalciumAnalysis.Processing.Stages.PStageDetectStaticROIs(varargin{:});
        end
    end
end

