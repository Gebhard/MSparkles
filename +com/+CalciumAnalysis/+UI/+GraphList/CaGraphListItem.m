%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef CaGraphListItem < com.common.UI.GraphListItem
    %GRAPHLISTITEM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties        
        Label;        
    end            
    
    methods
        function obj = CaGraphListItem(hGraphLst, parent, itemNo, numItems, ClickHandler)
            obj = obj@com.common.UI.GraphListItem(hGraphLst, parent, itemNo, numItems, ClickHandler);
            
            obj.Label = obj.CreateLabel(obj.itemPanel);            
            
            ctxMenu = uicontextmenu('Parent', obj.ParentFigure);
            uimenu(ctxMenu, 'Text', 'Save as image', 'MenuSelectedFcn', @obj.OnSaveAsImage);            
            obj.hAxis.UIContextMenu = ctxMenu;                
            
            obj.CurrFrame = hGraphLst.hAppFigure.CurrentFrame - 1;                   
        end
        
        function delete(obj)
            try
                delete( obj.Label );
            catch
            end
        end        
    end    
    
    methods
        function Render(obj, hDataset, hAnalysis, hResultset, plotIdx)            
            clickHandler = obj.hAxis.ButtonDownFcn;
            obj.AbsGraphIdx = plotIdx;
            obj.RenderGraph(hDataset, hAnalysis, hResultset, plotIdx);
            obj.RenderFrameIndicator(); 
            obj.hAxis.ButtonDownFcn = clickHandler;
        end
    end

    methods (Access = protected)                               
        function OnFrameChanged(obj, src, event)
            obj.CurrFrame = event.AffectedObject.(src.Name) - 1;            
        end
        
        function OnSaveAsImage(obj, ~, ~)
            try
                hDataset = obj.hGraphList.hDataset;
                
                if ~isempty(hDataset)
                    hAnalysis = hDataset.CurrentAnalysis;
                    
                    if~isempty(hAnalysis)
                        hFig = CreateTraceExportFigure;
                        hAnalysis.ExportEventTraces([], obj.hGraphList.hAppFigure.CurrentChannel, obj.AbsGraphIdx, 'Figure', hFig);
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex)
            end
        end
        
        function lbl = CreateLabel(obj, hParent)
            lbl =  uicontrol('Style', 'Text', 'Parent', hParent, 'Units', 'normalized', 'Position', [0 0 0.060 1], 'HorizontalAlignment', 'left');            
            lbl.ButtonDownFcn = @obj.OnMouseClick;
        end
    end        
end