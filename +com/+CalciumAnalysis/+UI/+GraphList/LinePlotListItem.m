%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef LinePlotListItem < com.CalciumAnalysis.UI.GraphList.CaGraphListItem     
    methods (Access = protected)
        function RenderGraph(obj, hDataset, ~, hResultset, plotIdx)
            try
                hGlobalSettings = MGlobalSettings.GetInstance();
                
                com.CalciumAnalysis.Rendering.Graph.MTracePlot('Dataset', hDataset, 'Resultset', hResultset,...
                    'PlotID', plotIdx, 'Parent', obj.hAxis, 'Grid', true, 'ShowStdDev', hGlobalSettings.RoiTraceShowStd,...
                    'ShowRiseAndDecay',hGlobalSettings.RoiTraceShowRiseDecay );

                %Fill label here
                %1) Total number of signals
                %2) Mean duration
                %3) ROI area
                peaks = hResultset.Results.SigMax(plotIdx,:);
                durs = hResultset.Results.Duration(plotIdx,:);
                area = mean(hResultset.Results.Areas{plotIdx}(:));
                areaunit = hDataset.MetaData.DomainUnit + "�";
                
                numPeaks = numel(vertcat(peaks{:}));
                meanDur = mean(vertcat(durs{:}));
                
                obj.Label.String = sprintf("ROI %d\n# Signals: %d\n� Duration: %.2f\n� Area: %.2f %s",...
                    plotIdx, numPeaks, meanDur, area, areaunit);

                %Add tooltip
                %1) Mean frequency
                %2) Per-class signal counts
                
                TH = hResultset.Results.Thresholds;
                sigCounts = "";
                for t=1:numel(TH)
                    sigCounts = sigCounts + "<b>Group " + t + " (" + TH(t) + ") : &nbsp;</b>" + numel(peaks{t}) + "<br>";
                end
                
                obj.Label.Tooltip = sprintf('<html><b>�Freq.:</b> %.3f Hz<br>Per-class signal counts:<p>%s</p></html>',...
                    hResultset.Results.SigMeanFreq(plotIdx) ,sigCounts);
            catch
            end
        end
        
        function hAxis = CreateAxes(obj)
            hAxis = axes(obj.itemPanel, 'Position', [.093 0.175 .9 .725]);
            hAxis.ButtonDownFcn = @obj.OnMouseClick;
            hAxis.XGrid = 'on';
        end
        
        function lbl = CreateLabel(obj, hParent)
            lbl =  uicontrol('Style', 'Text', 'Parent', hParent, 'Units', 'normalized', 'Position', [0 0 0.071 1], 'HorizontalAlignment', 'left');            
            lbl.ButtonDownFcn = @obj.OnMouseClick;
        end
    end
end

