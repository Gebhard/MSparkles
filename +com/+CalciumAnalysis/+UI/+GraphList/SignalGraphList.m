%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef SignalGraphList < com.common.UI.GraphList
    %SIGNALGRAPHLIST Summary of this class goes here
    %   Only numVisibleGraphs graphs exist at a time. Every graph is
    %   represented by a GraphListElement which contains a label for graph
    %   info and the actual graph.
    %   RenderGraphs re-uses these elemnts to be more efficient
            
    properties(Access = private)
        hFrameListener;
        hChannelListener;
    end                             
     
    %Construction / destruction
    methods
        function obj = SignalGraphList(hAppFig, hParent)
            obj = obj@com.common.UI.GraphList(hAppFig, hParent);              
            
            obj.hFrameListener = addlistener(hAppFig, 'CurrentFrame', 'PostSet', @obj.OnFrameChanged); 
            obj.hChannelListener = addlistener(hAppFig, 'CurrentChannel', 'PostSet', @obj.OnChannelChanged); 
        end           
        
        function delete(obj)
            delete(obj.hChannelListener);
            delete(obj.hFrameListener);
        end
    end
    
    methods(Access=protected)
        function scrollsteps = InitScrollRange(obj, ~)
            try
                scrollsteps = 1;

                if ~isempty(obj.hDataset)
                    hAnalysis = obj.hAnalysis;

                    if ~isempty(hAnalysis)
                        hResultSet = hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);
                        if ~isempty(hResultSet)
                            scrollsteps = max(hResultSet.ROIs.NumObjects - (obj.numVisibleGraphs-1), 1);
                        end
                    end
                end
            catch
                scrollsteps = 0;
            end
        end
    end
    
    %public methods
    methods                
        function RenderGraphs(obj, ~)
            if ~isempty(obj.hSlider)
                scrollOffset = uint32(obj.ScrollPos);            

                %depending on the type, render numVisibleGraphs many graphs                        
                uBound = uint32(scrollOffset + obj.NumGraphs) - 1;                
                GraphIdx = uint32(1);

                try
                    if ~isempty(obj.hDataset)
                        hAnalysis = obj.hAnalysis; 
                        if ~isempty(hAnalysis)
                            hResultset = hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);
                            
                            if ~isempty(hResultset) && ~isempty(hResultset.Results)

                                for i=scrollOffset:uBound 
                                    graph = obj.hGraphList(GraphIdx);
                                    graph.Render(obj.hDataset, hAnalysis, hResultset, i);
                                    GraphIdx = GraphIdx + 1;
                                end
                            end
                        end
                    end
                catch
                end
            end
        end                
    end
    
    %Private functions
    methods (Access = protected)   
        function OnFrameChanged(obj, hSrc, hEvent)
            frame = obj.hDataset.FrameTimes( hEvent.AffectedObject.(hSrc.Name) - 1 );
            
            for i=1:numel(obj.hGraphList)
                obj.hGraphList(i).CurrFrame = frame;
            end
        end
        
        function OnChannelChanged(obj, ~, ~)
            obj.RenderGraphs();
        end
                        
        function OnInitGraphList(obj)
            try                        
                if ~isempty(obj.hDataset)
                    hAnalysis = obj.hAnalysis;
                        
                    if ~isempty(hAnalysis)                            
                        hResultset = hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);

                        if ~isempty(hResultset)
                            numRois = hResultset.ROIs.NumObjects;
                            numGraphs = min (obj.numVisibleGraphs, numRois);

                            if obj.hAnalysis.AnalysisType ~= com.Enum.AnalysisType.Dynamic || hAnalysis.AnalysisSettings.ShowTracePlots
                               hClass = @com.CalciumAnalysis.UI.GraphList.LinePlotListItem;
                                obj.hGraphList = com.CalciumAnalysis.UI.GraphList.LinePlotListItem.empty(numGraphs, 0);
                            else
                                hClass = @com.CalciumAnalysis.UI.GraphList.WavePlotListItem;
                                obj.hGraphList = com.CalciumAnalysis.UI.GraphList.WavePlotListItem.empty(numGraphs, 0);
                            end
                            
                            if ~isempty(hClass)
                                for i=1:numGraphs
                                    obj.hGraphList(i) = hClass(obj, obj.hParent, i, numGraphs, @obj.OnItemClicked);
                                end
                            end
                        end
                    end
                end
            catch
            end
        end                                
        
        function UpdateScrollRange(obj)
            try
                if ~isempty(obj.hDataset)
                    hAnalysis = obj.hAnalysis;
                        
                    if ~isempty(hAnalysis)                            
                        hResultset = hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);
                        
                        if ~isempty(hResultset)
                            obj.hSlider.Max = max(hResultset.ROIs.NumObjects - (obj.NumGraphs-1), 1);                                                        
                            obj.ScrollPos = 1;
                            
                            if hResultset.ROIs.NumObjects > obj.numVisibleGraphs
                                obj.hSlider.Enable = 'on';
                            end
                        end
                    end
                end
            catch
                obj.hSlider.Enable = 'off';
            end
        end        
    end        
end