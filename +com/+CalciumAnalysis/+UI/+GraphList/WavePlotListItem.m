%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef WavePlotListItem < com.CalciumAnalysis.UI.GraphList.CaGraphListItem   
    methods (Access = protected)
        function RenderGraph(obj, hDataset, hAnalysis, hResultset, plotIdx)
            try
                com.CalciumAnalysis.Rendering.RenderWavePlot(obj.hAxis, hDataset, hAnalysis, hResultset, plotIdx);
            catch
            end
            
            hGlobalSettings = MGlobalSettings.GetInstance();
            
            if ~isempty(hGlobalSettings) && hGlobalSettings.WavePlotsDisplayColorBar
                if isempty(obj.hAxis.Colorbar)
                    colorbar( obj.hAxis );
                end
            else
                if ~isempty(obj.hAxis.Colorbar)
                    colorbar( obj.hAxis, 'off' );
                end
            end
            
            results = hResultset.Results;
            if ~isempty(results)
                
                st = results.StartTime(plotIdx,:);
                dt = results.Duration(plotIdx,:);
                ms = results.SigMax(plotIdx,:);
                
                st = cell2mat(st(:));
                dt = cell2mat(dt(:));
                ms = cell2mat(ms(:));
                
                obj.Label.String = sprintf('%d)\nt-Start: %.2f\nDuration: %.3f\nPeak: %.2f', plotIdx,...
                    st, dt, ms);
                
                if results.distMax(plotIdx, 1) > hDataset.Settings.Analysis.StationarityTH
                    statTxt = 'Non-stationary';
                else
                    statTxt = 'Stationary';
                end
                
                dUnit = hDataset.MetaData.DomainUnit;
                obj.Label.Tooltip = sprintf('<html><b>Average propagation speed:</b> %.3f %s<br><b>Travelled distance:</b> %.3f %s<br><b>Max area:</b> %.3f %s<br><b>Average area:</b> %.3f %s<br><b>%s</b></html>',...                    
                    results.distTotal(plotIdx, 1)./dt, dUnit+"/sec",...
                    results.distTotal(plotIdx, 1), dUnit,...
                    results.MaxArea(plotIdx), dUnit + "�",...
                    results.AvgArea(plotIdx), dUnit + "�",...
                    statTxt);
            end
        end
    end
end

