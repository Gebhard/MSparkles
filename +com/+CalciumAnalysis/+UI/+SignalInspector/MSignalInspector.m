%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalInspector < com.common.UI.UiControlEx
    %MSIGNALINSPECTOR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access = private)
        mName;
        hParent;        
        hItem;                
    end         
    
    methods
        function obj = MSignalInspector(hAppFig, varargin)            
            MSignalInspector_version = '25.10.2018';
            MSignalInspector_date = '1.0.000';
            
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding
            p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['MSignalInspector v.' MSignalInspector_version ' (' MSignalInspector_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addParameter(p,'Name',     [],  @(x) (ischar(x) && ~isempty(x) ));           
            addParameter(p,'Parent',   [],  @(x) ~isempty(x) );

            parse(p,varargin{:});
            par = p.Results; 
            
            obj.mName = par.Name;
            obj.hParent = par.Parent;
            obj.hAppFigure = hAppFig;
            
            obj.hItem = com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorLiveItem(obj, obj.hParent);                                                  
        end        
        
        function delete(obj)
            delete( obj.hItem );
        end
    end 
    
    methods
        function Invalidate(obj)
            if ~isempty(obj.hItem) && isvalid(obj.hItem)
                obj.hItem.Invalidate();
            end
        end
        
        function deleteItem(obj)
            try
                delete(obj.hItem);
            catch
            end
        end
        
        function DatasetUpdated(obj)
           if ~isempty(obj.hItem)
                obj.ShowLive();
           end
        end
        
        function ShowSignal(obj, roiID)                                                
            if all(roiID > 0)
                if obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic && ...
                    ~obj.hAnalysis.AnalysisSettings.ShowTracePlots               
                    if ~isa(obj.hItem, 'com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorWaveItem')
                        obj.deleteItem();
                        obj.hItem = com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorWaveItem(obj, obj.hParent, roiID);
                    else
                        obj.hItem.SignalID = roiID;
                    end
                else
                    if ~isa(obj.hItem, 'com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorRoiItem') || ...
                            isa(obj.hItem, 'com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorWaveItem')
                        obj.deleteItem();
                        obj.hItem = com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorRoiItem(obj, obj.hParent, roiID);
                    else
                        obj.hItem.SignalID = roiID;
                    end
                end
            else
                obj.ShowLive();
            end
        end
        
        function ShowLive(obj)
            obj.deleteItem();            
            obj.hItem = com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorLiveItem(obj, obj.hParent);
        end
    end    
end

