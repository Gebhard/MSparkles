%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalInspectorItem < com.common.Rendering.Graph
    %MSIGNALINSPECTORITEM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access = protected)
        CurrChannel;
        CurrZ;
             
        hFrameListener;
        hChannelListener;
        hZPosListener;
        
        hSignalInspector;
        hParent;
    end
    
    methods
        function obj = MSignalInspectorItem(hSigIspector, parent)
            obj.hParent = parent;                                   
            obj.hSignalInspector = hSigIspector;
            
            hAppFig = obj.hSignalInspector.hAppFigure;
            
            obj.CurrChannel = hAppFig.CurrentChannel;
            obj.CurrZ = hAppFig.CurrentZ;
            
            obj.hFrameListener = addlistener(hAppFig, 'CurrentFrame', 'PostSet', @obj.OnFrameChanged);
            obj.hChannelListener = addlistener(hAppFig, 'CurrentChannel', 'PostSet', @obj.OnChannelChanged);
            obj.hZPosListener = addlistener(hAppFig, 'CurrentZ', 'PostSet', @obj.OnZChanged);
            
            obj.CurrFrame = hAppFig.CurrentFrame - 1; %Trigger update
        end
        
        function delete(obj)
            delete(obj.hFrameListener);
            delete(obj.hChannelListener);
            delete(obj.hZPosListener);
        end
    end
    
    methods
        function DatasetUpdated(obj, hDataset)
            obj.Render();
            obj.RenderFrameIndicator();
        end 
        
        function Invalidate(obj)
            obj.DeleteAxis();            
            obj.Render();
            obj.RenderFrameIndicator();
        end
        
        function Render(obj)
            obj.OnRender();
        end
    end
    
    methods (Access = protected)
        function OnFrameChanged(obj, src, event)
            frame = event.AffectedObject.(src.Name) - 1;             
            obj.CurrFrame = obj.hSignalInspector.hDataset.FrameTimes(frame);
        end 
        
        function OnChannelChanged(obj, src, event)
            obj.CurrChannel = event.AffectedObject.(src.Name);
        end
        
        function OnZChanged(obj, src, event)
            obj.CurrZ = event.AffectedObject.(src.Name);
        end                        
    end
    
    methods (Abstract, Access = protected)
        OnRender(obj);
    end
end

