%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalInspectorLiveItem < com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorItem
    %MSIGNALINSPECTORLIVEITEM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access = private)
        hFrameIndicatorF0;
        hMousePosListener;        
        hF0Plot;
        
        hFSig;
        hF0Sig;
        hdFF0Sig;
        UpdateAxes = true; 
        
        maxHeight = 1;
        maxWidth = 1;
        margin = 0.05;
        redrawLegend = false;      
    end
    
    properties
        CurrPos;
    end
    
    methods
        function obj = MSignalInspectorLiveItem(hSigInspector, parent)
            obj = obj@com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorItem(hSigInspector, parent);
            
            hAppFig = obj.hSignalInspector.hAppFigure;
            obj.hMousePosListener = addlistener(hAppFig, "CurrentMousePos", "PostSet", @obj.OnMouseMove);
            obj.CurrPos = hAppFig.CurrentMousePos;
        end
        
        function delete(obj)
            delete(obj.hMousePosListener);
            delete(obj.hFrameIndicatorF0);
            delete(obj.hF0Plot);            
        end
    end
    
     methods
        function set.CurrPos(obj, value)
            obj.CurrPos = uint16(value);
            obj.Render();
        end                 
        
        function DatasetUpdated(obj, ~)  
            obj.UpdateAxes = true;  
            obj.Render();
        end 
        
        function Invalidate(obj)
           obj.hF0Plot = [];
            Invalidate@com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorItem(obj)            
        end
     end
    
      methods(Access = private)
        function OnMouseMove(obj, src, event)
            obj.CurrPos = event.AffectedObject.(src.Name);
        end                                               
      end
      
      methods (Access = protected)
          
        function RenderFrameIndicator(obj)
            RenderFrameIndicator@com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorItem(obj);
            obj.hFrameIndicatorF0 = com.lib.Rendering.RenderVerticalLine(obj.hF0Plot, obj.hFrameIndicatorF0, obj.CurrFrame);
        end
          
        function OnRender(obj)
            try
                hGlobalSettings = MGlobalSettings.GetInstance();
                hDataset = obj.hSignalInspector.hDataset;

                if ~isempty(hDataset) && ~isempty(hDataset.F)         
                    if ~isempty(hDataset.F0)
                        numPlots = 2;
                    else
                        numPlots = 1;
                    end

                    CaSignal = hDataset.F(obj.CurrPos(2), obj.CurrPos(1), obj.CurrChannel, obj.CurrZ, 1, :);
                    CaSignal = CaSignal(:);   

                    if isempty(obj.hAxis) || ~isvalid(obj.hAxis)                            
                        obj.hAxis = subplot("Position", ...
                            [obj.margin, 1-(1/numPlots)+obj.margin, obj.maxWidth-1.5*obj.margin, (obj.maxHeight/numPlots) - 2*obj.margin],...
                            "Parent", obj.hParent);
                        obj.hFSig = plot(obj.hAxis, hDataset.TimingSignal, CaSignal, "LineWidth", 1,...
                            "Color", hGlobalSettings.LineColor);                            
                        obj.redrawLegend = true;                            
                        obj.UpdateAxes = true;
                    else
                        obj.hFSig.YData = CaSignal;
                    end   

                    if obj.UpdateAxes
                        ylim(obj.hAxis, [hDataset.Fmin hDataset.Fmax]);
                        xlim(obj.hAxis, [0, hDataset.LastSampleTime]);
                    end

                    if numPlots > 1
                        hold(obj.hAxis, "on");

                        CaF0 = hDataset.F0(obj.CurrPos(2), obj.CurrPos(1), obj.CurrChannel, obj.CurrZ, 1, :); 

                        if isempty(obj.hF0Sig) || ~isvalid(obj.hF0Sig)
                            obj.hF0Sig = plot(obj.hAxis, hDataset.TimingSignal, CaF0(:), "LineWidth", 1 );
                            obj.redrawLegend = true;
                            obj.UpdateAxes = true;
                        else
                            obj.hF0Sig.YData = CaF0(:);
                        end

                        hold(obj.hAxis, "off");                          

                        DF = hDataset.NormalizedF(obj.CurrPos(2), obj.CurrPos(1), obj.CurrChannel, obj.CurrZ, 1, :);

                        if obj.UpdateAxes
                            p1 = prctile(DF(:), 0.1);
                            p99 = prctile(DF(:), 99);
                            dfMin = min(p1, -2);
                            dfMax = max(p99, 5);
                        end

                        DF = DF(:);

                        if isempty(obj.hF0Plot) || ~isvalid(obj.hF0Plot)                               
                           obj.hF0Plot = subplot("Position", [obj.margin, obj.margin, obj.maxWidth- 1.5*obj.margin, (obj.maxHeight/numPlots) - 2*obj.margin],...
                               "Parent", obj.hParent);
                           obj.hdFF0Sig = plot(obj.hF0Plot, hDataset.TimingSignal, DF, "LineWidth", 1,...
                               "Color", hGlobalSettings.LineColor);   
                           obj.redrawLegend = true; 
                           obj.UpdateAxes = true;
                        else
                            obj.hdFF0Sig.YData = DF;
                        end                                                       

                        if obj.UpdateAxes   
                            if isempty(dfMax)
                                dfMax = 5;
                            end

                            if isempty(dfMin)
                                dfMin = -5;
                            end

                            xlim(obj.hF0Plot, [0, hDataset.LastSampleTime]);
                            ylim(obj.hF0Plot, [dfMin dfMax]); 
                        end

                        if obj.redrawLegend == true                                
                            legend(obj.hF0Plot, hDataset.NormalizationName, "AutoUpdate", "off");
                            legend(obj.hAxis, ["F", "F_{0}"], "AutoUpdate", "off");
                        end
                    else                            
                        if obj.redrawLegend == true
                            legend(obj.hAxis, "F", "AutoUpdate", "off");
                        end
                    end 
                    obj.UpdateAxes = false;
                end
            catch
            end
        end
    end
end

