%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSignalInspectorWaveItem < com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorRoiItem
    %MSIGNALINSPECTORROIITEM Summary of this class goes here
    %   Detailed explanation goes here    
    
    methods
        function obj = MSignalInspectorWaveItem(hSigInspector, parent, roiID)
            obj = obj@com.CalciumAnalysis.UI.SignalInspector.MSignalInspectorRoiItem(hSigInspector, parent, roiID);
        end        
    end    
    
    methods(Access = protected)
        function OnRender(obj)
            try
                hDataset = obj.hSignalInspector.hDataset;
                hAnalysis = hDataset.CurrentAnalysis;
                hResultset = hAnalysis.GetResultset(obj.CurrChannel);
                
                if isempty(obj.hAxis) || ~isvalid(obj.hAxis)   
                    maxHeight = 1;
                    maxWidth = 1;
                    margin = 0.05;

                    obj.hAxis = subplot('Position', [margin, margin, maxWidth- 1.5*margin, (maxHeight) - 2*margin], 'Parent', obj.hParent);
                end 
                
                com.CalciumAnalysis.Rendering.RenderWavePlot(obj.hAxis, hDataset, hAnalysis, hResultset, obj.SignalID);                    
                
                 hGlobalSettings = MGlobalSettings.GetInstance();

                if ~isempty(hGlobalSettings) && hGlobalSettings.WavePlotsDisplayColorBar
                    colorbar( obj.hAxis );
                end
            catch
            end
        end
    end        
end

