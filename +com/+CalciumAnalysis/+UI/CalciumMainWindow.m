%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef CalciumMainWindow  < com.common.UI.ZoomableAppFigure
    %CALCIUMMAINWINDOW Summary of this class goes here
    %   Detailed explanation goes here
    
    methods(Static)
        function value = Init(hAppWindow, hDataset)
            try
                hMSparkles = MSparklesSingleton.GetInstance();

                %Delete old instance, if it exists
                if ~isempty(hMSparkles.hCaMainWnd) 
                    if hMSparkles.hCaMainWnd.hDataset ~= hDataset
                        if isvalid(hMSparkles.hCaMainWnd)
                            hMSparkles.hCaMainWnd.delete();
                        end
                        hMSparkles.hCaMainWnd = [];
                    end
                end
                
                if isempty(hMSparkles.hCaMainWnd) 
                    hMSparkles.hCaMainWnd = com.CalciumAnalysis.UI.CalciumMainWindow(hAppWindow, hDataset, 'Calcium analysis');
                else
                    hMSparkles.hCaMainWnd.UpdateUI();
                end

                value = hMSparkles.hCaMainWnd;
            catch
                value = [];
            end
        end
    end
    
    properties(SetAccess=private)
        hStackScroller;
        hGraphList;
        hDataExplorer;
    end
    
    properties(Access=private)
        hBtnPlay;
        hBtnRoiPoly;
        hBtnRoiLine;
        hBtnRoiRect;
        hBtnRoiEllipse;
        
        hPlaybackListener;
        hAnalysisListener;
        hRoiSelectionListener;
    end
    
    properties(SetObservable)
        CurrentFrame = 1;
        CurrentChannel = 1;
        CurrentZ = 1;
        CurrentMousePos = [1 1];
        SelectedRoi;
    end
    
    methods(Access=protected)
        function obj = CalciumMainWindow(hAppWindow, hDataset, appName)
            %CALCIUMMAINWINDOW Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.UI.ZoomableAppFigure(hAppWindow, hDataset, appName, "both");
            
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                obj.hAnalysisListener = addlistener(hProjMan, 'CurrentAnalysis', 'PostSet', @obj.OnAnalysisChanged);
                obj.hRoiSelectionListener = addlistener(hProjMan, 'SelectedRoi', 'PostSet', @obj.OnTreeSelectRoi);
            end                                    
        end
    end
    
    methods
        function delete(obj)
            delete( obj.hAnalysisListener );
            delete( obj.hRoiSelectionListener );
            delete( obj.hPlaybackListener );
            delete( obj.hStackScroller );
            delete( obj.hGraphList );
            delete( obj.hDataExplorer );
        end
    end
        
    methods
        function set.SelectedRoi(obj, value)
            obj.SelectedRoi = value;
            obj.RoiSelectionChanged();
        end
        
        function set.CurrentFrame(obj, value)
            obj.CurrentFrame = value;
            obj.UpdateStatusBarCurrPos();
        end
        
        function set.CurrentChannel(obj, value)
            obj.CurrentChannel = value;
            obj.UpdateStatusBarCurrPos();
        end
        
        function set.CurrentZ(obj, value)
            obj.CurrentZ = value;
            obj.UpdateStatusBarCurrPos();
        end
        
        function set.CurrentMousePos(obj, value)
            obj.CurrentMousePos = value;
            obj.UpdateStatusBarCurrPos();
        end
    end
    
    methods
        function UpdateUI(obj)
            obj.UpdateStackScroller();
            obj.UpdateGraphList();
            obj.UpdateDataExplorer();
        end
        
        function UpdateStackScroller(obj)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.Update();
            end
        end
        
        function UpdateGraphList(obj)
            if ~isempty(obj.hGraphList)
                obj.hGraphList.Update();
            end
        end
        
        function UpdateDataExplorer(obj)
            if ~isempty(obj.hDataExplorer)
                obj.hDataExplorer.Update();
            end
        end
    end
    
    methods(Access=protected)
        function OnDeleteFigure(obj, ~, ~)
            if ~isempty(obj.hDataset)
                obj.hDataset.Save();
            end
            
            OnDeleteFigure@com.common.UI.AppFigure(obj, [],[]);
        end
        
        function OnInitMenu(obj, hTabGroup)
            obj.CreateCalciumTab(hTabGroup);
            obj.CreateInsertTab(hTabGroup);
        end
        
        function CreateFigureLayout(obj, hSrc, ~)
                 
            mainFlexBox = uix.HBoxFlex( 'Parent', hSrc, 'Tag', 'RootFlexBox');

            contentPnl = uipanel( 'Parent', mainFlexBox,...
                'Tag', 'Content', 'Title', '', 'Bordertype', 'none');

            %set( mainFlexBox, 'Widths', [-1.5, -10]  );

            vContentFlexBox = uix.VBoxFlex( 'Parent', contentPnl, 'Tag', 'contentFlexBox');

            contenUtPnl = uix.Panel( 'Parent', vContentFlexBox,...
                'Tag', 'ContentU', 'Title', '', 'Bordertype', 'none');

            contenLtPnl = uix.Panel( 'Parent', vContentFlexBox,...
                'Tag', 'ContentL', 'Title', '', 'Bordertype', 'none');

            ContentUFlexBox = uix.HBoxFlex( 'Parent', contenUtPnl, 'Tag', 'ContentUFlexBox');

            uipanel( 'Parent', ContentUFlexBox,'Position', [0,0, 450,500],...
                'Tag', 'stackScroller', 'Title', '', 'CreateFcn', @obj.CreateStackScroller, ...
                'Bordertype', 'none');

            uipanel( 'Parent', ContentUFlexBox,...
                'Tag', 'signalInspector', 'Title', '', 'CreateFcn', @obj.CreateDataExplorer,...
                'Bordertype', 'none');

            uipanel( 'Parent', contenLtPnl, 'Position', [0,0,1450, 920],...
                'Units', 'pixels', 'Tag', 'signalListPanel', 'Title', '', 'CreateFcn', @obj.CreateGraphList, ...
                'Bordertype', 'none');

            set(ContentUFlexBox, 'Widths', [-1, -2]);
                        
            obj.RegisterPlaybackListener();
        end
        
        function InitFigure(obj, ~)
            obj.UpdateUI();
        end
        
        function OnMouseMove(obj, ~, ~)            
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.MouseMotionHandler();      
            end
        end
        
        function OnScrollWheel(obj, ~, hEvent)
            if ~isempty(obj.hStackScroller) && obj.hStackScroller.HitTest()
                if hEvent.VerticalScrollCount > 0
                    obj.hStackScroller.NextFrame();
                else
                    obj.hStackScroller.PreviousFrame();
                end
            elseif ~isempty(obj.hGraphList)
                oldPos = obj.hGraphList.ScrollPos;
                obj.hGraphList.ScrollPos = oldPos + (hEvent.VerticalScrollAmount * hEvent.VerticalScrollCount);
            end                                    
        end
        
        function OnKeyDown(obj, src, evnt)
            OnKeyDown@com.common.UI.ZoomableAppFigure(obj, src, evnt);
            
            if ( strcmp(evnt.Key, 'delete') )
                projMan = com.Management.MProjectManager.GetInstance();

                if projMan.SelectedRoi > -1
                    projMan.DeleteROI(projMan.CurrentAnalysis, obj.CurrentChannel, projMan.SelectedRoi);  
                    
                    if (~isempty(obj.hStackScroller))
                        obj.hStackScroller.UpdateOverlay();
                    end
                end
            else
                if (~isempty(obj.hStackScroller))
                   obj.hStackScroller.KeyPressedHandler( evnt ); 
                end
            end
        end
        
        function hDataset = GetDefaultAnalysisDataset(~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            hDataset = hProjMan.ActiveDataSetNode;
        end
        
        function AppendPipeline(obj, hDataSetNode, finalStage, analyses, forceRecomputeAll)
            arguments
                obj (1,1)
                hDataSetNode (1,1) com.Management.MSparklesTreeNode {mustBeNonempty, mustBeDatasetNode}
                finalStage (1,:) com.Enum.PipelineStage {mustBeNonempty}
                analyses (:,1) {mustBeAnalysisSet}
                forceRecomputeAll (1,1) logical = false
            end

            if ~isempty(hDataSetNode.Dataset)
                ep = com.CalciumAnalysis.Processing.BuildPipeline(hDataSetNode, finalStage, analyses, forceRecomputeAll);
                com.common.Proc.ExecutionQueue.AddExecutionPipeline(ep);
            end
        end
    end
    
    methods(Access=private)
        function CreateStackScroller(obj, hSrc, ~, ~)
        % hObject    handle to stackScroller (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    empty - handles not created until after all CreateFcns called
            oldVal = get(hSrc,'Units');
            set(hSrc, 'Units', 'pixels');

            pos = hSrc.Position;

            obj.hStackScroller = com.CalciumAnalysis.Rendering.MStackScroller( 'AppFigure', obj, ...
            'Position', [0, 0, pos(3), pos(4)], 'Name', 'StackPreview', 'Parent', hSrc);
            
            set(hSrc, 'Units', oldVal);

            obj.hStackScroller.AddRoiCallback               = @obj.OnStackScrollerAddRoi;
            obj.hStackScroller.CutRoiCallback               = @obj.OnStackScrollerCutRoi;
            obj.hStackScroller.MergeRoiCallback             = @obj.OnStackScrollerMergeRoi;    

            %obj.hStackScroller.Update();                        
        end
        
        function CreateGraphList(obj, hSrc, ~, ~)
        % hObject    handle to UIPanel (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    empty - handles not created until after all CreateFcns called
            obj.hGraphList = com.CalciumAnalysis.UI.GraphList.SignalGraphList(obj, hSrc);
            obj.hGraphList.OnItemSelected = @obj.OnGraphListItemSelected;
        end
        
        function CreateDataExplorer(obj, hSrc, ~, ~)
        % hObject    handle to signalInspector (see GCBO)
        % eventdata  reserved - to be defined in a future version of MATLAB
        % handles    empty - handles not created until after all CreateFcns called    
            obj.hDataExplorer = com.CalciumAnalysis.UI.MDataExplorer(obj, hSrc);
        end
        
        function OnGraphListItemSelected(obj, ~, graphId)
            obj.SelectedRoi = graphId;                
        end
        
        function OnStackScrollerAddRoi(obj, hRoi)
            projMan = com.Management.MProjectManager.GetInstance();

            if ~isempty(projMan)
                projMan.AddRoiToCurrentAnalysis(obj.CurrentChannel,hRoi);           

                if ~isempty(obj.hStackScroller)
                    obj.hStackScroller.InvalidateRoiMap();
                end
            end
        end
        
        function OnStackScrollerCutRoi(obj, hRoi)
            projMan = com.Management.MProjectManager.GetInstance();

            if ~isempty(projMan)
                projMan.CutRoiInCurrentAnalysis(obj.CurrentChannel, hRoi);    

                if ~isempty(obj.hStackScroller)
                    obj.hStackScroller.InvalidateRoiMap();
                end
            end
        end
        
        function OnStackScrollerMergeRoi(obj, hRoi)
            projMan = com.Management.MProjectManager.GetInstance();

            if ~isempty(projMan)
                projMan.MergeRoiInCurrentAnalysis(obj.CurrentChannel, hRoi);   

                if ~isempty(obj.hStackScroller)
                    obj.hStackScroller.InvalidateRoiMap();
                end
            end
        end
        
        function OnTreeSelectRoi(obj, hSrc, hEvent)
            %This is enough to fire the cascade
            %It will set CalciumMainWindow.SelectedRoi , which will update
            %the other UI components
            obj.SelectedRoi = hEvent.AffectedObject.(hSrc.Name);            
        end
        
        function RoiSelectionChanged(obj)            
            if ~isempty(obj.hGraphList)
                obj.hGraphList.SelectGraph(obj.SelectedRoi);
            end

            projMan = com.Management.MProjectManager.GetInstance();    
            if ~isempty(projMan)
               projMan.SelectRoiInCurrentDataset(obj.CurrentChannel, obj.SelectedRoi); 
            end

            if ~isempty(obj.hDataExplorer)
                obj.hDataExplorer.SelectRoi(obj.SelectedRoi);
            end
            
            hMSparkles = MSparklesSingleton.GetInstance();
            
            if obj.SelectedRoi > 0
                hMSparkles.AppWindow.SetStatusBarInfoText("ROI " + obj.SelectedRoi);
            else
                hMSparkles.AppWindow.ResetStatusBarInfoText();
            end
        end
        
        function CreateCalciumTab(obj, hTabGroup)
            import matlab.ui.internal.toolstrip.*    
            icondir = iconrootdir();
            
            hTabCalcium = Tab("Calcium analysis");
            hTabGroup.add(hTabCalcium);  % add to tab as the last section
            hTabCalcium.add( CreateDataMgmtButtons(true) );  % add to tab as the last section                      
            
            %% Movie
            hSecPlayback = Section("Playback");
            hTabCalcium.add(hSecPlayback);  % add to tab as the last section
            hColPlayback = hSecPlayback.addColumn();
            hColPlaybackOptions = hSecPlayback.addColumn();
            
            obj.hBtnPlay = ToggleButton("Play", fullfile(icondir, "play_large.png"));
            obj.hBtnPlay.Description = "Play / pause movie";
            obj.hBtnPlay.ValueChangedFcn = @obj.onBtnPlay;
            hColPlayback.add(obj.hBtnPlay);
            
            hChkSLoopPlayback = CheckBox("Loop", false);
            hChkSLoopPlayback.Description = "Play movie in endless loop.";
            hChkSLoopPlayback.addlistener("ValueChanged",@obj.onToggleLoopPlayback);
            hColPlaybackOptions.add(hChkSLoopPlayback);

            hSecDisplay = Section("Display");
            hTabCalcium.add(hSecDisplay);  % add to tab as the last section
            hColDisplay = hSecDisplay.addColumn();
            hColDisplay2 = hSecDisplay.addColumn();
            hColDisplay3 = hSecDisplay.addColumn();
            hColDisplay4 = hSecDisplay.addColumn();
            
            hBtnDispAdjust = Button("Adjust", fullfile(icondir, "adjust.png"));
            hBtnDispAdjust.Description = "Adjust display settings.";
            hBtnDispAdjust.ButtonPushedFcn = @obj.onBtnAdjustDisp;
            hColDisplay.add(hBtnDispAdjust);
            
            %% Display
            hLutSelection = DropDown( cellstr(MGlobalSettings.ColorLutNames' ) );
            hLutSelection.Value = "Gray";
            hLutSelection.ValueChangedFcn = @obj.OnLutChanged;
            hColDisplay2.add(hLutSelection);
            
            hDispStackSelection = DropDown( ["F"; "F0"; "deltaF"; "dFF";"dFF0"; "Ratio"; "Release/Uptake"; "Dynamic Range"] );
            hDispStackSelection.Value = "F";
            hDispStackSelection.ValueChangedFcn = @obj.onDisplayStackChanged;
            hColDisplay2.add(hDispStackSelection);

            hChkShowTrajectories = CheckBox("Show trajectories", true);
            hChkShowTrajectories.addlistener("ValueChanged",@obj.onToggleShowTrajectories);
            hChkShowTrajectories.Description = "Enable / disable trajectories of dynamic events.";
            hColDisplay2.add(hChkShowTrajectories);
            
            hChkShowRois = CheckBox("Show ROIs", true);
            hChkShowRois.addlistener("ValueChanged",@obj.onToggleShowROIs);
            hChkShowRois.Description = "Enable / disable ROI overlay.";
            hColDisplay3.add(hChkShowRois);

            hChkShowAnnotations = CheckBox("Show annotations", true);
            hChkShowAnnotations.addlistener("ValueChanged",@obj.onToggleShowAnnotations);
            hChkShowAnnotations.Description = "Enable / disable annotation overlay.";
            hColDisplay3.add(hChkShowAnnotations);
            
            hChkShowMerge = CheckBox("Show merge", false);
            hChkShowMerge.addlistener("ValueChanged",@obj.onToggleShowMerge);
            hChkShowMerge.Description = "Enable / disable merged channels overlay.";
            hColDisplay3.add(hChkShowMerge);

            obj.CreateZoomButtons( hColDisplay4 );

            %% Analysis
            hSecAnalysis = Section("Analysis");
            hTabCalcium.add(hSecAnalysis);  % add to tab as the last section
            hColAnalysis1 = hSecAnalysis.addColumn();
            hColAnalysis2 = hSecAnalysis.addColumn();
            hColAnalysis3 = hSecAnalysis.addColumn();
            hColAnalysis4 = hSecAnalysis.addColumn();
            
            hBtnAddAnalysis = DropDownButton('Add new', fullfile(icondir, 'addanalysis.png'));
            hBtnAddAnalysis.Description = 'Add new analysis to dataset.';            
            hBtnAddAnalysis.DynamicPopupFcn = @(h,e) obj.CreateAddAnalysisPopup(); % invoked when user clicks the drop-down selector widget
            hColAnalysis1.add(hBtnAddAnalysis);
            
            hBtnSetCurrAnalysis = Button('Set active', fullfile(icondir, 'currentanalysis_large.png'));
            hBtnSetCurrAnalysis.Description = 'Set the current analysis and display its data';
            hBtnSetCurrAnalysis.ButtonPushedFcn = @obj.onBtnSetCurrentAnalysis;
            hColAnalysis2.add(hBtnSetCurrAnalysis);

            hBtnPreProc = SplitButton('Pre processing', fullfile(icondir, 'filter.png'));
            hBtnPreProc.Description = 'Configure and run pre processing.';
            hBtnPreProc.ButtonPushedFcn = @obj.onBtnRunPreProc;
            hBtnPreProc.DynamicPopupFcn = @(h,e) obj.CreatePrePrcoPopup; % invoked when user cli
            hColAnalysis3.add(hBtnPreProc);

            hBtnF0 = SplitButton('F0', fullfile(icondir, 'baseline.png'));
            hBtnF0.Description = 'Run F0 computation.';
            hBtnF0.ButtonPushedFcn = @obj.onBtnRunF0;
            hBtnF0.DynamicPopupFcn = @(h,e) obj.CreateF0Popup;
            hColAnalysis3.add(hBtnF0);

            hBtnRunDetectRois = SplitButton('Detect ROIs', fullfile(icondir, 'landmark.png'));
            hBtnRunDetectRois.Description = 'Run ROI detection.';
            hBtnRunDetectRois.ButtonPushedFcn = @obj.onBtnRunDetectRois;
            hBtnRunDetectRois.DynamicPopupFcn = @(h,e) obj.CreateEditRoiPopup;
            hColAnalysis3.add(hBtnRunDetectRois);

            hBtnRunAnalysis = SplitButton('Analyze', fullfile(icondir, 'analysis.png'));
            hBtnRunAnalysis.Description = 'Run analysis.';
            hBtnRunAnalysis.ButtonPushedFcn = @obj.onBtnRunAnalysis;
            hBtnRunAnalysis.DynamicPopupFcn = @(h,e) obj.CreateEditAnalysisPopup;
            hColAnalysis4.add(hBtnRunAnalysis);

            hBtnReRunAll = SplitButton('Re-run all', fullfile(icondir, 'repeat.png'));
            hBtnReRunAll.Description = 'Re-run entire processing pipeline.';
            hBtnReRunAll.ButtonPushedFcn = @obj.onBtnRunAll;
            hBtnReRunAll.DynamicPopupFcn = @(h,e) obj.CreateReRunPopup;
            hColAnalysis4.add(hBtnReRunAll);

            %% Export
            hSecExport = Section('Export');
            hTabCalcium.add(hSecExport);  % add to tab as the last section
            hColExport1 = hSecExport.addColumn();
            hColExport2 = hSecExport.addColumn();
            hColExport3 = hSecExport.addColumn();

            hBtnExportExcel = Button('Excel', fullfile(icondir, 'exce_largel.png'));
            hBtnExportExcel.Description = 'Export results to Excel file.';
            hBtnExportExcel.ButtonPushedFcn = @obj.onBtnExportExcel;
            hColExport1.add(hBtnExportExcel);

            hBtnExportPDF = Button('PDF', fullfile(icondir, 'pdf.png'));
            hBtnExportPDF.Description = 'Export PDF report.';
            hBtnExportPDF.ButtonPushedFcn = @obj.onBtnExportPDF;
            hColExport2.add(hBtnExportPDF);
            
            hBtnExportHeatmap = Button('Heatmap', fullfile(icondir, 'heatmap.png'));
            hBtnExportHeatmap.Description = 'Export heatmap.';
            hBtnExportHeatmap.ButtonPushedFcn = @obj.onBtnExportHeatmap;
            hColExport2.add(hBtnExportHeatmap);

            hBtnExportRoiMap = Button('ROI map', fullfile(icondir, 'roi_map.png'));
            hBtnExportRoiMap.Description = 'Export map of detected ROIs.';
            hBtnExportRoiMap.ButtonPushedFcn = @obj.onBtnExportRoiMap;
            hColExport2.add(hBtnExportRoiMap);

            hBtnExportTrace = SplitButton('Trace', fullfile(icondir, 'baseline.png'));
            hBtnExportTrace.Description = 'Export ROI traces as image files.';
            hBtnExportTrace.ButtonPushedFcn = @obj.onBtnExportTrace;
            hBtnExportTrace.DynamicPopupFcn = @(h,e) obj.CreateCustomTracePopup;
            hColExport3.add(hBtnExportTrace);

            hBtnExportMovie = Button('Movie', fullfile(icondir, 'movie_export_16x16.png'));
            hBtnExportMovie.Description = 'Export movie file.';
            hBtnExportMovie.ButtonPushedFcn = @obj.onBtnExportMovie;
            hColExport3.add(hBtnExportMovie);
            
            %% Settings
            hSecSettings = Section('Settings');
            hTabCalcium.add(hSecSettings);  % add to tab as the last section
            hColSettings1 = hSecSettings.addColumn();
             
%             hBtn = SplitButton('Properties', fullfile(icondir, 'wrench.png'));
%             hBtn.Description = 'Edit properties of datasets or annotations.';
%             hBtn.DynamicPopupFcn = @(h,e) obj.CreatePropertyPopup; % invoked when user cli
%             hBtn.ButtonPushedFcn = @obj.onBtnCaDatasetProperties;
            
            hBtn = CreatePropertyButton();
            hColSettings1.add(hBtn);            
        end
        
        function onBtnCaDatasetProperties(~,~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.EditSelectedItem();                    
            end
        end
        
        function hPopup = CreatePrePrcoPopup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Open');
            hPopup.add(header);

             % list item #1
            item = ListItem('Edit pre processing', fullfile(icondir, 'edit_preproc.png'));
            item.Description = 'Edit pre processing pipeline.';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnEditPreProc;
            hPopup.add(item);
            
%             % list item #2
%             item = ListItem('Reset pre processing', fullfile(icondir, 'delete_preproc.png'));
%             item.Description = 'Reset pre-processing and subsequent operations.';
%             item.ShowDescription = true;
%             item.ItemPushedFcn = @obj.onBtnResetPreProc;
%             hPopup.add(item);     
        end
        
        function hPopup = CreateCustomTracePopup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Customize');
            hPopup.add(header);
            
            item = ListItem('Customize trace plot', fullfile(icondir, 'edit_traceplot.png'));
            item.Description = 'Create custom visualizations of one or more ROI traces.';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnCustomizeRoiTraces;
            hPopup.add(item);
        end
        
        function hPopup = CreateEditAnalysisPopup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Open');
            hPopup.add(header);

             % list item #1
            item = ListItem('Edit ROI analysis settings.', fullfile(icondir, 'edit_analysis.png'));
            item.Description = 'Edit parameters for signal analysis.';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnEditAnalysis;
            hPopup.add(item);
            
%              % list item #2
%             item = ListItem('Reset ROI analysis', fullfile(icondir, 'delete_analysis.png'));
%             item.Description = 'Reset all analysis results.';
%             item.ShowDescription = true;
%             item.ItemPushedFcn = @obj.onBtnResetAnalysis;
%             hPopup.add(item);
        end
        
        function hPopup = CreateEditRoiPopup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Open');
            hPopup.add(header);

             % list item #1
            item = ListItem('Edit ROI detection / generation', fullfile(icondir, 'edit_landmark.png'));
            item.Description = 'Edit parameters of ROI detection';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnEditRoiDetection;
            hPopup.add(item);   
            
%              % list item #2
%             item = ListItem('Reset ROIs', fullfile(icondir, 'delete_rois.png'));
%             item.Description = 'Reset ROIs and analysis results.';
%             item.ShowDescription = true;
%             item.ItemPushedFcn = @obj.onBtnResetRois;
%             hPopup.add(item);       
        end
        
        function hPopup = CreateF0Popup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Open');
            hPopup.add(header);

             % list item #1
            item = ListItem('Edit F0 computation', fullfile(icondir, 'edit_baseline.png'));
            item.Description = 'Edit parameters of F0 computation.';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnEditF0;
            hPopup.add(item); 
            
%              % list item #2
%             item = ListItem('Reset F0 computation', fullfile(icondir, 'delete_f0.png'));
%             item.Description = 'Reset F0, ROIs and results.';
%             item.ShowDescription = true;
%             item.ItemPushedFcn = @obj.onBtnResetF0;
%             hPopup.add(item); 
        end
        
        function hPopup = CreateReRunPopup(obj,~,~)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();

            hPopup = PopupList();

            % list header #1
            header = PopupListHeader('Execute');
            hPopup.add(header);

             % list item #1
            item = ListItem('Re-run ROI detection and analysis', fullfile(icondir, 'reset_roi.png'));
            item.Description = 'Delete existing ROIs and results, forcing in a clean re-start of the analysis.';
            item.ShowDescription = true;
            item.ItemPushedFcn = @obj.onBtnReRunRoiAnalysis;
            hPopup.add(item); 
        end
        
        function onBtnCustomizeRoiTraces(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hProjMan.CustomizeTracePlot();                    
            end
        end
        
        function onBtnEditPreProc(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hProjMan.EditSelectedPreProc( hProjMan.SelectedNodes );                    
            end
        end
        
        function onBtnEditRoiDetection(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.EditSelectedRoiDetection( hProjMan.SelectedNodes );
            end
        end
        
        function onBtnEditAnalysis(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                %hProjMan.EditSelectedF0();
                hProjMan.EditSelectedAnalysis( hProjMan.SelectedNodes );
            end
        end
        
        function onBtnEditF0(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.EditSelectedF0( hProjMan.SelectedNodes );                    
            end
        end
        
        function onBtnReRunRoiAnalysis(obj,~,~)
             obj.run([com.Enum.PipelineStage.RoiDetection, com.Enum.PipelineStage.Analysis], false);
        end
        
        function onBtnResetPreProc(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.ResetSelectedPreProcessing( hProjMan.SelectedNodes );                    
            end
        end
        
        function onBtnResetF0(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.ResetSelectedtF0( hProjMan.SelectedNodes );                    
            end
        end
        
        function onBtnResetRois(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.ResetSelectedRois( hProjMan.SelectedNodes );                    
            end
        end
        
        function onBtnResetAnalysis(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)                    
                hProjMan.ResetSelectedResults( hProjMan.SelectedNodes );                    
            end
        end
        
        function CreateInsertTab(obj, hTabGroup)
            import matlab.ui.internal.toolstrip.*
            icondir = iconrootdir();

            hTabInsert = Tab('Insert');
            hTabGroup.add(hTabInsert);  % add to tab as the last section

            %% ROIs
            hSecRois = Section('ROIs');
            hTabInsert.add(hSecRois);  % add to tab as the last section
            
            hColRois = hSecRois.addColumn();
            hColRois2 = hSecRois.addColumn();
            
            obj.hBtnRoiEllipse = ToggleButton('Ellipse', fullfile(icondir, 'ellipse_large.png'));
            obj.hBtnRoiEllipse.Description = 'Add elliptic ROI';
            obj.hBtnRoiEllipse.ValueChangedFcn = @obj.onBtnRoiEllipse;
            hColRois.add(obj.hBtnRoiEllipse);
            
            obj.hBtnRoiRect = ToggleButton('Rectangle', fullfile(icondir, 'rectangle.png'));
            obj.hBtnRoiRect.Description = 'Add rectangular ROI';
            obj.hBtnRoiRect.ValueChangedFcn = @obj.onBtnRoiRect;
            hColRois2.add(obj.hBtnRoiRect);
            
            obj.hBtnRoiLine = ToggleButton('Line', fullfile(icondir, 'line.png'));
            obj.hBtnRoiLine.Description = 'Add line ROI';
            obj.hBtnRoiLine.ValueChangedFcn = @obj.onBtnRoiLine;
            hColRois2.add(obj.hBtnRoiLine);
            
            obj.hBtnRoiPoly = ToggleButton('Polygon', fullfile(icondir, 'polygon.png'));
            obj.hBtnRoiPoly.Description = 'Add polygonal ROI';
            obj.hBtnRoiPoly.ValueChangedFcn = @obj.onBtnRoiPoly;
            hColRois2.add(obj.hBtnRoiPoly);

            %% Annotations
            hSecAnnotations = Section('Annotations');
            hTabInsert.add(hSecAnnotations);  % add to tab as the last section
            hColAnnotation = hSecAnnotations.addColumn();
            hColAnnotation2 = hSecAnnotations.addColumn();
            hColAnnotation3 = hSecAnnotations.addColumn();
            
            hBtnAnnoScale = Button('Scalebar', fullfile(icondir, 'scalebar_large.png'));
            hBtnAnnoScale.Description = 'Add scalebar annotation.';
            hBtnAnnoScale.ButtonPushedFcn = @obj.onBtnAnnotateScale;
            hColAnnotation.add(hBtnAnnoScale);
            
            hBtnAnnoTime = Button('Timestamp', fullfile(icondir, 'clock_large.png'));
            hBtnAnnoTime.Description = 'Add timestamp annotation.';
            hBtnAnnoTime.ButtonPushedFcn = @obj.onBtnAnnotateTime;
            hColAnnotation2.add(hBtnAnnoTime);
            
            hBtnAnnoArrow = Button('Arrow', fullfile(icondir, 'arrow2_green_16x16.png'));
            hBtnAnnoArrow.Description = 'Add arrow annotation.';
            hBtnAnnoArrow.ButtonPushedFcn = @obj.onBtnAnnotateArrow;
            hColAnnotation3.add(hBtnAnnoArrow);
            
            hBtnAnnoEllipse = Button('Ellipse', fullfile(icondir, 'ellipse_green_16x16.png'));
            hBtnAnnoEllipse.Description = 'Add ellipse annotation.';
            hBtnAnnoEllipse.ButtonPushedFcn = @obj.onBtnAnnotateEllipse;
            hColAnnotation3.add(hBtnAnnoEllipse);
            
            hBtnAnnoRect = Button('Rectangle', fullfile(icondir, 'rect_green_16x16.png'));
            hBtnAnnoRect.Description = 'Add rectangle annotation.';
            hBtnAnnoRect.ButtonPushedFcn = @obj.onBtnAnnotateRect;
            hColAnnotation3.add(hBtnAnnoRect);
        end
        
        function UpdateStatusBarCurrPos(obj)
            hMSparkles = MSparklesSingleton.GetInstance();            
            hMetaData = obj.hDataset.MetaData;            
            ptPhys = single([obj.CurrentMousePos, (obj.CurrentZ-1)]) .* [hMetaData.SampleSizeX, hMetaData.SampleSizeY, hMetaData.SampleSizeZ];
            
            msg = sprintf("Pos.: (%.2f , %.2f , %.2f) Chan.: %d Frame: %d (%.2f sec.)", ...
                ptPhys(1), ptPhys(2), ptPhys(3), ...
                obj.CurrentChannel, obj.CurrentFrame, obj.hDataset.FrameTimes(obj.CurrentFrame-1));
            
            hMSparkles.AppWindow.SetStatusBarPositionText(msg);
        end
    end
    
    methods(Access=private)
        function onBtnPlay(obj, ~, ~)
            if ~isempty(obj.hStackScroller)                    
                obj.hStackScroller.TogglePlayback();
            end
        end
        
        function onBtnAdjustDisp(obj, ~, ~)
            if ~isempty(obj.hStackScroller)                    
                obj.hStackScroller.AdjustDisplaySettings();
            end          
        end
        
        function OnLutChanged(obj, hSrc, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.SetColorLUT(hSrc.SelectedItem);
            end
        end
        
        function onToggleLoopPlayback(obj, hSender, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.LoopPlayback = hSender.Value;
            end
        end
                
        function onToggleShowTrajectories(obj, hSender, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.ShowTrajectories = hSender.Value;
            end
        end
        
        function onToggleShowROIs(obj, hSender, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.ShowRoiOverlay(hSender.Value);
            end
        end
        
        function onToggleShowAnnotations(obj, hSender, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.ShowAnnotations(hSender.Value);
            end          
        end
        
        function onToggleShowMerge(obj, hSender, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.ShowMerge = hSender.Value;
            end          
        end                
        
        function onDisplayStackChanged(obj, hSrc, ~)
            if ~isempty(obj.hStackScroller)
                obj.hStackScroller.DisplayStackName = hSrc.SelectedItem;
            end
        end
        
        function hPopup = CreateAddAnalysisPopup(obj)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();
            
            hPopup = PopupList();
 
            % list header #1
            header = PopupListHeader('Add analysis');
            hPopup.add(header);
            
             % list item #1
            item = ListItem('Auto-detect ROIs', fullfile(icondir, 'analysis_large.png'));
            item.Description = 'Create new static analysis, ROIs are automatically discovered.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddAutoRoiAnalysis;            
            hPopup.add(item);  
            
            item = ListItem('ROI Grid', fullfile(icondir, 'grid_large.png'));
            item.Description = 'Create new static analysis, ROIs are created as grid.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddRoiGridAnalysis;            
            hPopup.add(item);  
            
            item = ListItem('Global ROI', fullfile(icondir, 'global_large.png'));
            item.Description = 'Create new static analysis, using one global ROI.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddGlobalRoiAnalysis;            
            hPopup.add(item);
            
            item = ListItem('Manual ROI', fullfile(icondir, 'manual_large.png'));
            item.Description = 'Create new static analysis, using manually drawn ROIs.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddManualRoiAnalysis;            
            hPopup.add(item);
            
            item = ListItem('Dynamics analysis', fullfile(icondir, 'dynamic_large.png'));
            item.Description = 'Create new dynamics analysis, ROIs are automatically discovered.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddDynamicRoiAnalysis;            
            hPopup.add(item);
        end
        
        function AddAnalysis(~, type)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hTargetNode = hProjMan.CurrentItem;
                
                if ~isempty(hTargetNode)
                    if hTargetNode.Type ~= com.Enum.TreeNodeTypes.DataSet
                        hTargetNode = hTargetNode.GetParentDatasetNode();
                    end
                else
                    hTargetNode = hProjMan.ActiveDataSetNode;
                end
                
                if ~isempty(hTargetNode) && hTargetNode.Type == com.Enum.TreeNodeTypes.DataSet
                    hProjMan.AddAnalysis(hTargetNode, type);
                else
                    msgbox('Please select or load a propper dataset.', 'Cannot add analysis', 'warning');
                end
            end
        end
        
        function onBtnAddAutoRoiAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.Activity);
        end
        
        function onBtnAddRoiGridAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.Grid);
        end
        
        function onBtnAddGlobalRoiAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.Global);
        end
        
        function onBtnAddManualRoiAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.Manual);
        end
        
        function onBtnAddDynamicRoiAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.Dynamic);
        end
        
        function onBtnSetCurrentAnalysis(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();                  
            if ~isempty(hProjMan) 
                hProjMan.SetCurrentAnalysisNode(hProjMan.CurrentItem);
            end
        end
        
        function onBtnRunPreProc(obj, ~, ~)
            obj.run(com.Enum.PipelineStage.PreProcessing, false);
        end
        
        function onBtnRunF0(obj, ~, ~)
            obj.run(com.Enum.PipelineStage.BaselineComputation, false);
        end
        
        function onBtnRunDetectRois(obj, ~, ~)
            obj.run(com.Enum.PipelineStage.RoiDetection, false);
        end
        
        function onBtnRunAnalysis(obj, ~, ~)
            obj.run(com.Enum.PipelineStage.Analysis, false);
        end
        
        function onBtnRunAll(obj, ~, ~)
            obj.run(com.Enum.PipelineStage.Analysis, true);
        end
        
        function onBtnExportExcel(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                com.CalciumAnalysis.Reporting.Helper.ExportExcelFile(hProjMan.CurrentItem);
            end             
        end
        
        function onBtnExportPDF(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hProjMan.ExportPdfReport(hProjMan.GetCurrentTreeNode("analysis"));
            end
        end
        
        function onBtnExportHeatmap(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                com.CalciumAnalysis.Reporting.Helper.ExportHeatmap(hProjMan.GetCurrentTreeNode("dataset"))
            end
        end
        
        function onBtnExportRoiMap(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                com.CalciumAnalysis.Reporting.Helper.ExportRoiMap(hProjMan.GetCurrentTreeNode("analysis"))
            end
        end
        
        function onBtnExportTrace(~, ~, ~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                com.CalciumAnalysis.Reporting.Helper.ExportRoiTrace( ...
                    hProjMan.CurrentDataset, hProjMan.SelectedNodes);
            end    
        end
        
        function onBtnExportMovie(obj, ~, ~)
            if isempty(obj.hDataset) 
                errordlg({'Cannot export data.' 'No dataset loaded.'});
            elseif isempty(obj.hDataset.F)
                errordlg({'Cannot export data.' 'Dataset is empty.'});
            else
                com.CalciumAnalysis.UI.ExportStack(obj);
            end
        end                
        
        function onBtnRoiEllipse(obj, hButton, ~)
            obj.hBtnRoiPoly.Value = false; 
            obj.hBtnRoiLine.Value = false; 
            obj.hBtnRoiRect.Value = false;            
        
            if ~isempty(obj.hStackScroller)
                if hButton.Selected
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.Ellipse);   
                else
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.None);
                end
            end
        end
        
        function onBtnRoiRect(obj, hButton, ~)
            obj.hBtnRoiPoly.Value = false; 
            obj.hBtnRoiLine.Value = false;             
            obj.hBtnRoiEllipse.Value = false; 
        
            if ~isempty(obj.hStackScroller)
                if hButton.Selected
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.Rect);
                else
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.None);
                end
            end
        end
        
        function onBtnRoiLine(obj, hButton, ~)
            obj.hBtnRoiPoly.Value = false;            
            obj.hBtnRoiRect.Value = false; 
            obj.hBtnRoiEllipse.Value = false; 
        
            if ~isempty(obj.hStackScroller)
                if hButton.Selected
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.Line);
                else
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.None);
                end
            end
        end
        
        function onBtnRoiPoly(obj, hButton, ~)
            obj.hBtnRoiLine.Value = false; 
            obj.hBtnRoiRect.Value = false; 
            obj.hBtnRoiEllipse.Value = false; 
        
            if ~isempty(obj.hStackScroller)
                if hButton.Selected
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.Poly);
                else
                    obj.hStackScroller.SetManualRoiType(com.CalciumAnalysis.Enum.ManualRoiEnum.None);
                end
            end
        end
        
        function onBtnAnnotateScale(obj,~,~)
            obj.CreateAndAddAnnotation(com.Enum.AnnotationType.Scalebar);
        end
        
        function onBtnAnnotateTime(obj,~,~)
            obj.CreateAndAddAnnotation(com.Enum.AnnotationType.Timestamp);
        end
        
        function onBtnAnnotateArrow(obj,~,~)
            obj.CreateAndAddAnnotation(com.Enum.AnnotationType.Arrow);
        end
        
        function onBtnAnnotateEllipse(obj,~,~)
            obj.CreateAndAddAnnotation(com.Enum.AnnotationType.Ellipse);
        end
        
        function onBtnAnnotateRect(obj,~,~)
            obj.CreateAndAddAnnotation(com.Enum.AnnotationType.Rect);
        end
        
        function OnPlayStatusChanged(obj, ~, hEvent, ~)
            icondir = iconrootdir();

            if hEvent.AffectedObject.IsPlaying
                obj.hBtnPlay.Icon = fullfile(icondir, 'pause.png');
                obj.hBtnPlay.Value = true;
            else
                obj.hBtnPlay.Icon = fullfile(icondir, 'play_large.png');
                obj.hBtnPlay.Value = false;
            end
        end
        
        function RegisterPlaybackListener(obj)
            if ~isempty(obj.hStackScroller)                    
                obj.hPlaybackListener = addlistener(obj.hStackScroller, 'IsPlaying', 'PostSet', @obj.OnPlayStatusChanged); 
            end
        end
        
        function OnAnalysisChanged(obj, hSrc, hEvent)
            hAnalysis = hEvent.AffectedObject.(hSrc.Name);
            
            if ~isempty(obj.hStackScroller) && isvalid(obj.hStackScroller)
                obj.hStackScroller.UpdateAnalysis(hAnalysis);
            end
            
            if ~isempty(obj.hDataExplorer) && isvalid(obj.hDataExplorer)
                obj.hDataExplorer.Update();
            end
            
            if ~isempty(obj.hGraphList) && isvalid(obj.hGraphList)
                obj.hGraphList.Update();
            end
        end
    end
    
    methods(Access=private)
        function CreateAndAddAnnotation(obj, annotytionStyle)
        %CREATEANDADDANNOTATION Summary of this function goes here
        %   Detailed explanation goes here
            if ~isempty(obj.hDataset)
                hAnnotation = com.Annotations.MAnnotation(annotytionStyle);        
                obj.hDataset.Annotations.AddAnnotation(hAnnotation);
                obj.hDataset.Save();

                if ~isempty(obj.hStackScroller)        
                    hAnnotation.Render(obj.hStackScroller);
                end

                hProjectManager = com.Management.MProjectManager.GetInstance();
                if ~isempty(hProjectManager)            
                    hProjectManager.AddAnnotationToCurrentDataset(hAnnotation);
                end
            end
        end
    end
end

