%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDataExplorer < com.common.UI.UiControlEx
    %MDATAEXPLORER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=private)
        TabManager;
        Tabs;
        Panels;
        hChannelChangedListener;
        hFrameListener;
        hSignalInspector;
        
        hPeakOverview;
        hDurOverview;
        hSyncGraph;
        hHeatMap;
        hSDHeatMap;
        hScatter;
        hEegSyncPlot;
        hEegCorrPlot;
    end    
    
    methods
        function obj = MDataExplorer(hAppFig, parent)% Braucht handle zu dataset & StackScroller
            obj = obj@com.common.UI.UiControlEx(hAppFig);
            
            obj.TabManager = uitabgroup('Parent', parent);
                        
            obj.Tabs{1}         = uitab(obj.TabManager, 'Title', 'Signal inspector');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Signal composition');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Signal summary');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Duration summary');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Heatmap');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'SD heatmap');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Synchronicity');
            obj.Tabs{end + 1}   = uitab(obj.TabManager, 'Title', 'Scatter'); 
            
            if ~isempty(obj.hDataset.EEGDataset)
                obj.Tabs{end + 1} = uitab(obj.TabManager, 'Title', 'EEG sync'); 
                obj.Tabs{end + 1} = uitab(obj.TabManager, 'Title', 'EEG correlation'); 
            end
            
            if ~isempty(obj.hAppFigure)
                obj.hChannelChangedListener = addlistener(obj.hAppFigure, ...
                    'CurrentChannel', 'PostSet', @obj.OnChannelChanged);  
                
                obj.hFrameListener = addlistener(obj.hAppFigure,...
                    'CurrentFrame', 'PostSet', @obj.OnFrameChanged);
            end
        end
        
        function delete(obj)
            obj.DeletePanels();
            
            delete( obj.TabManager );
            delete( obj.hChannelChangedListener );
            delete( obj.hFrameListener );                        
            delete( obj.hPeakOverview );
            delete( obj.hDurOverview );
            delete( obj.hSyncGraph );
            delete( obj.hHeatMap );
            delete( obj.hSDHeatMap );
            delete( obj.hScatter );
            delete( obj.hEegSyncPlot );
            delete( obj.hEegCorrPlot );
            delete( obj.hSignalInspector );
        end
    end
    
    methods
        function Update(obj)
            try
                if ~isempty(obj.hDataset)
                    obj.RefreshPanels();

                    obj.hSignalInspector = com.CalciumAnalysis.UI.SignalInspector.MSignalInspector(obj.hAppFigure,...
                        'Parent', obj.Panels{1}, 'Name', 'SignalInspector');                            

                    if ~isempty(obj.hAnalysis)
                        hResultset = obj.hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);
                        if ~isempty(hResultset) && isa(hResultset.Results, "com.CalciumAnalysis.Analysis.MCaSignalsRoot")
                            obj.hPeakOverview = com.CalciumAnalysis.Rendering.Graph.MSignalCompositionGraph('Parent', obj.Panels{2},'Dataset',obj.hDataset,...
                                'Resultset', hResultset);
                            
                            obj.hPeakOverview = com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph('Parent', obj.Panels{3},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset, 'Type', 'peak');

                            obj.hDurOverview = com.CalciumAnalysis.Rendering.Graph.MDatasetOverviewGraph('Parent', obj.Panels{4},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset, 'Type', 'duration');

                            obj.hHeatMap = com.CalciumAnalysis.Rendering.Graph.MHeatMap('Parent', obj.Panels{5},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset, 'ShowFrameIndicator', true);

                            obj.hSDHeatMap = com.CalciumAnalysis.Rendering.Graph.MSignalDurationHeatMap('Parent', obj.Panels{6},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset, 'ShowFrameIndicator', true);

                            obj.hSyncGraph = com.CalciumAnalysis.Rendering.Graph.MSynchronicityGraph('Parent', obj.Panels{7},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset, 'OwnXAxes', true, 'ShowFrameIndicator', true);            

                            obj.hScatter = com.CalciumAnalysis.Rendering.Graph.MPeakDurationScatterGraph('Parent', obj.Panels{8},'Dataset',obj.hDataset,...
                                'Analysis', obj.hAnalysis, 'Resultset', hResultset);
                        end
                    end

                    if ~isempty(obj.hDataset.EEGDataset)
                        obj.hEegSyncPlot = com.CalciumAnalysis.Rendering.Graph.LiveEegPlot(obj.hAppFigure, obj.Panels{9});
                        
                        
                        heegResultset = obj.hDataset.EEGDataset.Analyses.CurrentAnalysis.GetFirstResultset;                        
                        
                        obj.hEegCorrPlot = com.CalciumAnalysis.Rendering.Graph.EegCorrPlot('Parent', obj.Panels{10},...
                            'Dataset',obj.hDataset, 'Resultset', hResultset, "EegResultset", heegResultset);
                    end
                end
            catch
            end
        end
        
        function SelectRoi(obj, roiID)
            if ~isempty(obj.hSignalInspector)
                obj.hSignalInspector.ShowSignal(roiID);
            end

            highlight =  [];
            
            if ~isempty(obj.hAnalysis) && all(roiID > 0  )
                hResultset = obj.hAnalysis.GetResultset(obj.hAppFigure.CurrentChannel);
                if ~isempty(hResultset) && ~isempty(obj.hScatter)
                    try                   
                        if ~isempty(hResultset.Results)
                            peaks   = cat(1, hResultset.Results.SigMax{roiID, :});
                            durs    = cat(1, hResultset.Results.Duration{roiID, :});                                    
                            highlight = [durs, peaks];
                        end                                
                    catch
                        com.common.Logging.MLogManager.Info("No signals to select");
                    end
                end
            end
            
            if ~isempty(obj.hScatter)
                obj.hScatter.UpdateHighlight( highlight );
            end
        end
    end
    
    methods(Access=private)
        
        function DeletePanels(obj)
            for i=1:numel(obj.Panels)
                delete( obj.Panels{i} );
            end
        end
        
        function RefreshPanels(obj)
            obj.DeletePanels();            
            
            numPanels = numel(obj.Tabs);
            
            for i=1:numPanels
                obj.Panels{i} = uipanel('Parent', obj.Tabs{i}, 'Units', 'normalized',...
                    'Position', [0, 0, 1, 1], 'BorderType' , 'none' );
            end                                    
        end                
        
        function OnChannelChanged(obj, ~, ~)
            obj.Update();
        end   
        
        function OnFrameChanged(obj, hSrc, hEvent)
            frame = hEvent.AffectedObject.(hSrc.Name); %-1
            
            try                 
                xPos = obj.hDataset.FrameTimes( frame );
            catch
                xPos = obj.CurrFrame;
            end
                            
            if ~isempty(obj.hHeatMap)
                obj.hHeatMap.CurrFrame = xPos;
            end
            
            if ~isempty(obj.hSDHeatMap)
                obj.hSDHeatMap.CurrFrame = xPos;
            end
            
            if ~isempty(obj.hSyncGraph)
                obj.hSyncGraph.CurrFrame = xPos;
            end
            
            if ~isempty(obj.hEegSyncPlot)
                obj.hEegSyncPlot.CurrFrame = frame;
            end
         end
        
    end
end

