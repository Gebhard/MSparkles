%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AnalysisData < com.common.Data.MSparklesObject
    %ANALYSISDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        AffectedDataset;    %String, containing the full path to a MDatasetRoot object        
        AnalysisID;         %UUID of the affected analysis
        DatasetResults;     %Copy of the com.common.Analysis.MResultRoot of the Dataset
        Channels;           %Channels to be considered for the data analysis
    end
    
    methods
        function obj = AnalysisData(hDataset, hAnalysis, channels)
            obj = obj@com.common.Data.MSparklesObject();
            obj.AffectedDataset = hDataset.ProjectFilePath;
            obj.AnalysisID = hAnalysis.ID;
            
            if nargin < 3
                obj.Channels = hAnalysis.Channels;
            else
                obj.Channels = channels;
            end
            
            obj.DatasetResults = cell(numel(obj.Channels), 2);
            
            for i=1:numel(obj.Channels)
                c = obj.Channels(i);
                hResultset = hAnalysis.GetResultset(c);
                if ~isempty(hResultset)
                    obj.DatasetResults(i,:) = {i, hResultset.Clone()};                    
                end
            end
            
        end
    end
end

