%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AnalysisRoot < com.common.Data.MSparklesObject
    %ANALYSISROOT Root object for advanced data analysis.
    %   Each Analysis consists of a list of AnalysisData objects, each
    %   containing the copy of one or more resultsets (e.g. multiple
    %   channels) of an analysis. Depending on the type of the AnalysisRoot
    %   object, the neccessary data of the resultset(s) are extracted.
    %   Finally, a graph, specialized to a particular type of analysis is
    %   generated.
    
    properties
        Data;   %Array of com.DataAnalysis.AnalysisData objects
        Type;
    end
    
    methods
        function obj = AnalysisRoot()
            obj = obj@com.common.Data.MSparklesObject();
            obj.Data = {};
        end
    end
    
    methods
        function AddAnalysisData(obj, data)
            %Data can be single AnalysisData object or a vector of multiple
            %AnalysisDataObjects
            
            %ToDo ensure data has the right orientation!
            
            obj.Data = [obj.Data, data];
        end
        
        function AddAnalysis(obj, hDataset, hAnalysis, channel)
            %This function adds individual analyses, and creates
            %AnalysisData objects for furter processing.
            
            if nargin < 3
                obj.AddAnalysisData( com.DataAnalysis.AnalysisData(hDataset, hAnalysis) );
            else
                obj.AddAnalysisData( com.DataAnalysis.AnalysisData(hDataset, hAnalysis, channel) );
            end
        end
    end
end

