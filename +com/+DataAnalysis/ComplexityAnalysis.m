%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef ComplexityAnalysis < com.DataAnalysis.AnalysisRoot
    properties
        MaxDuration;
        MaxCaDist;
        MaxAreaDist;
    end
    
    methods
        function Analyze(obj)
            obj.MaxDuration = 0;
            obj.MaxCaDist = 0;
            obj.MaxAreaDist = 0;
            
            for hData = obj.Data
                sz = size(hData.DatasetResults);
                for i=1:sz(1)
                   %channel = hData.DatasetResults(i,1);
                   hRes = hData.DatasetResults{i,2};
                   obj.MaxDuration = max(obj.MaxDuration, hRes.MaxDuration);
                   obj.MaxCaDist = max(obj.MaxCaDist, hRes.MaxCaDist);
                   obj.MaxAreaDist = max(obj.MaxAreaDist, hRes.MaxAreaDist);
                end
            end
        end

        function AnalyzeComplexity(obj, hTarget, durationColorCode, PlotComplexityDuration,... 
            xAxisDataName, yAxisDataName, plotType)
            
            cla(hTarget);
        
            if nargin < 3
                durationColorCode = false;
            end
            
            if nargin < 4
                PlotComplexityDuration = false;
            end
            
            boxdata = {};
            
            l = {};
            for hData = obj.Data
                sz = size(hData.DatasetResults);
                
                [~,fName,~] = fileparts(hData.AffectedDataset);
                l = [l, fName];
                for i=1:sz(1)
                    hRes = hData.DatasetResults{i,2};
                    
                    hMetaData = hRes.hParent.hDataset.MetaData;
                    maxDur = ceil(obj.MaxDuration * hMetaData.VolumeRate);
                    
%                     for k=1:hRes.Results.NumRois
%                         hRes.Results.ComplexityE(k) = CalcEntropy(hRes.Results.AreaMeanDists{k}(:) / obj.MaxAreaDist, maxDur);
%                         hRes.Results.ComplexityC(k) = CalcEntropy(hRes.Results.CaAcgMeanDists{k}(:) / obj.MaxCaDist, maxDur);
% 
%                         hRes.Results.Complexity(k) = hRes.Results.ComplexityE(k) + hRes.Results.ComplexityC(k);
%                     end                                        

                    
                    switch plotType
                        case "scatter"
                            
                            hTarget.XTickMode = 'auto';
                            hTarget.XTickLabelMode ='auto';
                            
                            c = jet( ceil(obj.MaxDuration*10) );
                            hTarget.Colormap = c;
                            hTarget.CLim = [0, hRes.MaxDuration];
                            if  nargin > 1 && ~isempty(hTarget)
                                hold(hTarget, 'on');
                                if durationColorCode
                                    scatter(hTarget, hRes.Results.(xAxisDataName), hRes.Results.(yAxisDataName), 38, c( round(hRes.Results.Duration * 10),: ) , 'filled',...
                                        'MarkerEdgeColor', 'none', 'MarkerFaceAlpha', 0.8);
                                else
                                    scatter(hTarget, hRes.Results.(xAxisDataName), hRes.Results.(yAxisDataName), 38, 'filled',...
                                        'MarkerEdgeColor', 'none', 'MarkerFaceAlpha', 0.8);
                                end
                                xlabel(hTarget, xAxisDataName);
                                ylabel(hTarget, yAxisDataName);
                                hold(hTarget, 'off');

                                hTarget.XLim = [min(min(hRes.Results.(xAxisDataName),0)), max(max(hRes.Results.(xAxisDataName)),2)];
                                hTarget.YLim = [min(min(hRes.Results.(yAxisDataName),0)), max(max(hRes.Results.(yAxisDataName)),2)];
                            end
                         case {"box", "bar"}
                            d = hRes.Results.(xAxisDataName);
                            boxdata = [boxdata, d(:)];
                    end                   
                end
            end
            
             if ~isempty(boxdata)
                sb = numel(boxdata);
                maxlen = max(cellfun(@(x) numel(x), boxdata));
                
                bd = nan(maxlen, sb);
                
                for j=1:sb
                    data = boxdata{j};
                    bd(1:numel(data),j) = data(:);
                end

                
                if plotType == "box"
                    boxplot(hTarget, bd);
                elseif plotType == "bar"
                    barData = mean(bd, 1, "omitnan");
                    errData = std(bd, 0, 1, "omitnan");
                    
                    bar(hTarget, barData);
                    
                    hold(hTarget, 'on');
                    er = errorbar(hTarget, barData, errData);
                    er.Color = [0 0 0];                            
                    er.LineStyle = 'none'; 
                    hold(hTarget, 'off');
                    
                    
                    hTarget.YLim = [0, max(barData + 2*errData) ];
                end
                
                hTarget.XLim = [0.5, sb+0.5];
            end
            
            if ~isempty(hTarget)
                legend(hTarget, l, 'Location', 'northoutside');
            end
        end
    end
   %Find longest duration of Ca-Dynamics (in seconds)
   %Find maximum (F-F0)/F0
   %Adapt number of samples (t) for each dataset by taking account of
   %actual frame rate
   %Re-run complexity analysis with new maximum values
   
   %Finally, make scatter plot
end

