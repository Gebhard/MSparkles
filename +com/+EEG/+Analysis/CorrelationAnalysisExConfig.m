%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef CorrelationAnalysisExConfig < com.common.Proc.PStageConfig
    %CORRELATIONANALYSISEXCONFIG Configuration parameters for advanced Calcium/EEG correlation
    %analysis
    
    properties
        CaAnalysisID;           %UUID of affected Ca2+ analysis
        AnalysisIntervalls;     %N x 2 matrix of analysis intervalls in the form start,end;
        EegSource;              %EEG source signal, com.EEG.Enum.WaveBand.*
        CaSource;               %Calcium source signal, depending on the selected analysis, com.EEG.Enum.CorrRefSig.*
        EegFilter;
        CaFilter;
    end
    
    methods
        function obj = CorrelationAnalysisExConfig()
            obj = obj@com.common.Proc.PStageConfig("Correlation analysis");
            obj.CaAnalysisID        = [];
            obj.AnalysisIntervalls  = [];
            obj.EegSource           = com.EEG.Enum.WaveBand.avgPower;              %EEG source signal, com.EEG.Enum.WaveBand.*
            obj.CaSource            = com.EEG.Enum.CorrRefSig.SyncIdx;        
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, hAnalysis)
            hStage = com.EEG.Processing.Stages.PStageAnalyzeCorrelationEx(hAnalysis);
        end
    end
end

