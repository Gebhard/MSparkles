%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegAnalysisConfig < com.common.Proc.PStageConfig
    %EEGANALYSISCONFIG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        DEF_SPIKETHRESHOLD  = 600;
        DEF_MAXSPIKEDIST    = 1.5;
        DEF_MINSPIKEDUR     = 20;
        DEF_WAVEBAND        = com.EEG.Enum.WaveBand.none;
        DEF_CORRREFSIG      = com.EEG.Enum.CorrRefSig.SyncIdx;
        DEF_MINSPIKEPROMINENCE = 25; %μV
    end
    
    properties
        SpikeThreshold;                         %Individual signal peaks (spikes) must be larger than this (micro Volt)
        MaxSpikeDistance;                       %If two spikes are (temporally) closer than this value, they are in the same spike train (Seconds)
        WaveBand com.EEG.Enum.WaveBand;         %Specified if analysis is to be carried out on a speciffic sub-band (element of enum com.EEG.Enum.WaveBand)
        MinSpikeTrainDuration;                  %Minimum duration of a series of spikes to classify as spike train. (seconds).
        CorrRefSignal com.EEG.Enum.CorrRefSig;  %Specifies the signal of the related [Ca2+] analysis to be used for correlation computation
        MinimumSpikeProminence;                 %Minimum spike prominence in μV
    end
    
    methods
        function obj = EegAnalysisConfig()
            obj = obj@com.common.Proc.PStageConfig("Spike train detection");
            obj.SpikeThreshold          = obj.DEF_SPIKETHRESHOLD;
            obj.MaxSpikeDistance        = obj.DEF_MAXSPIKEDIST;
            obj.MinSpikeTrainDuration   = obj.DEF_MINSPIKEDUR;
            obj.WaveBand                = obj.DEF_WAVEBAND;
            obj.CorrRefSignal           = obj.DEF_CORRREFSIG;
            obj.MinimumSpikeProminence  = obj.DEF_MINSPIKEPROMINENCE;
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, hAnalysis)
            hStage = com.EEG.Processing.Stages.PStageAnalyzeEEG(hAnalysis);
        end
    end
    
    methods
        function value = get.MinimumSpikeProminence(obj)
             if isempty(obj.MinimumSpikeProminence)
                obj.MinimumSpikeProminence = obj.DEF_MINSPIKEPROMINENCE;
            end
            
            value = obj.MinimumSpikeProminence;
        end
        
        function value = get.CorrRefSignal(obj)
            if isempty(obj.CorrRefSignal)
                obj.CorrRefSignal = obj.DEF_CORRREFSIG;
            end
            
            value = obj.CorrRefSignal;
        end
    end
end

