%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegAnalysisRoot < com.common.Analysis.MAnalysisRoot
    %EEGANALYSISROOT Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = EegAnalysisRoot(analysisType)
            obj = obj@com.common.Analysis.MAnalysisRoot(analysisType);
        end
    end
    
    methods(Access=protected)
        function ComputePSD(obj)
            try
                obj.hDataset.ComputePSD();
            catch 
            end
        end
        
%        function ComputeDelay(obj)
%             %Measure delays
%             if ~isempty(obj.AnalysisResults)
%                 hRes = obj.GetFirstResultset();
%                 refSig = hRes.GetBinarySpikeTrainOnsetSignal(obj.hDataset)'; % take first channel of analysis
%                 sigs = [];
%                 
%                 for hRes = obj.AnalysisResults
%                     sigs = [sigs, hRes.GetBinarySpikeTrainOnsetSignal(obj.hDataset)'];
%                 end
%                 
%                 maxlag = 10 * obj.hDataset.MetaData.SampleRate;
%                 
%                 delays = finddelay(refSig, sigs, maxlag);                
%                 delays = delays + abs(min(delays(:)));
%                 
%                 if numel(delays) > 8
%                     delays = delays(1:8);
%                 end
%                 
%                 delays = delays ./  obj.hDataset.MetaData.SampleRate;
%                 
%                 %Electrodes, counter-clockwise, 1,2,3,5,8,7,6,4
%                 xd = [1, 0.5, 0, 0, 0, 0.5, 1, 1];
%                 yd = [0, 0, 0, 0.5, 1, 1, 1, 0.5];
%                 cd = [delays(1), delays(2), delays(3), delays(5), delays(8), delays(7), delays(6), delays(4)];
%                 
%                 f = figure;
%                 ax = axes(f);
%                 patch(ax, "XData", xd, "YData", yd, "CData", cd, "FaceColor", "interp");
%                 colorbar(ax);
%             end
%         end
    end
    
    methods
        function ExportExcel(obj, outPath)
            multiWaitbar(obj.SaveWBName, 'Value', 0, 'Color', 'g');
            
            try
                if isfile( outPath )
                    delete( outPath );
                end
                
                tmpPath = sprintf("%s.xlsx", tempname);
                hXlsFile = com.CalciumAnalysis.Reporting.MExcelSheet();
                hXlsFile.Open(tmpPath);
                
                for hResultset=obj.AnalysisResults
                   hResultset.OnExportExcel(hXlsFile); 
                end 
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
            
            if ~isempty(hXlsFile) 
                hXlsFile.Close();
                try
                    movefile(tmpPath, outPath, 'f');
                catch
                    com.common.Logging.MLogManager.Warning("Could not move file " + tmpPath + ...
                        " to resukts directory (" + outPath + " ...");
                end
            end
            
            multiWaitbar(obj.SaveWBName, 'Close');
        end
        
        function ExportGraphs(obj, outPath)
            multiWaitbar('Exporting graphs', 'Color', 'g');
            numRes = numel(obj.AnalysisResults);
            try
                for i=1:numRes
                    obj.AnalysisResults(i).OnExportGraph(outPath);
                    multiWaitbar('Exporting graphs', 'Value', i/numRes);
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
            multiWaitbar('Exporting graphs', 'Close');
        end
        
        function hStage = CreateAnalysisStage(obj)
            hStage = obj.AnalysisSettings.CreatePipelineStage(obj);
        end
    end
end

