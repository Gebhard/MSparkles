%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MCorrelationAnalysisEx < com.EEG.Analysis.EegAnalysisRoot
    %MCORRELATIONANALYSISEX Extended correlation analysis between EEG anc Calcium signals.
    %   Assuming EEG and Calcium signals are in sync,it is possible to define mjultiple
    %   time intervals to compute the cross-correlation and obtian individual offsets.
    %   Thereby, this analysis allows to choose the signals on which the analysis is performed.
    %   For Calcium: INdividual ROI, Average of all ROIs, Sync
    %   For EEG: Individual Channel(or its power), Channel average (or power), Epoch power from PSD
    %   Optional smoothing is also possible.
    
    methods
        function obj = MCorrelationAnalysisEx()
            obj = obj@com.EEG.Analysis.EegAnalysisRoot(com.Enum.AnalysisType.CaEegCorrelationEx);
            obj.AnalysisSettings = obj.CreateAnalysisSettings();
        end
    end
    
    methods
        function resultset = CreateResultset(~, channel)
            resultset = com.EEG.Analysis.MCorrelationAnalysisExResult(channel);
        end
        
        function value = CreateAnalysisSettings(~)
            value = com.EEG.Analysis.CorrelationAnalysisExConfig;
        end
        
        function AnalyzeSignals(obj)
            multiWaitbar('Analyzing signal correlation...', 'Busy', 'Color', 'g');
            obj.AnalyzeCorrelation();            
            multiWaitbar('Analyzing signal correlation...', 'Close');
        end

        function Edit( obj, hDataset, isNew  )
            com.EEG.UI.dlgCorrelationEx(hDataset, hDataset.hParent, obj, isNew);
        end
    end
    
    methods (Access=private)
        function AnalyzeCorrelation(obj)
            caChannel = 1;  %For now...
            
            hEegDataset = obj.hDataset;     %Get EEG dataset
            hCaDataset = obj.hDataset.hParent;  %Get Ca2+ dataset
            
            if ~isempty(hEegDataset) && ~isempty(hCaDataset)
                hCfg = obj.AnalysisSettings;
                hCaAnalysis = hCaDataset.Analyses.GetAnalysis(hCfg.CaAnalysisID);
                
                if ~isempty(hCaAnalysis)
                    hCaResult = hCaAnalysis.GetResultset(caChannel);
                    caSig = hCaResult.GetEegReferenceSignal(hCfg.CaSource);
                    eegSig = hEegDataset.GetWaveBand(2:9, hCfg.EegSource);
                    
                    if size(eegSig,1) > 1
                        eegSig = mean(eegSig, 1, "omitnan");
                    end
                    
                    if hCfg.CaSource == com.EEG.Enum.WaveBand.avgPower
                        eegInterval = 1; 
                    else
                        eegInterval = hEegDataset.MetaData.SampleRate;
                    end
                    
                    if ~isempty(caSig) && ~isempty(eegSig)
                        
                        if ~isempty(hCfg.EegFilter)
                            hStage = hCfg.EegFilter.CreatePipelineStage();
                            eegSig = single(hStage.ApplyFilter(hEegDataset, double(eegSig)));
                        end
                        
                        if ~isempty(hCfg.CaFilter)
                            hStage = hCfg.CaFilter.CreatePipelineStage();
                            caSig = hStage.ApplyFilter([], caSig);
                        end
            
                        %Resample the eeg signal, if it has fewer samples than fluorescence.
                        %this should only be the case, if epoch average power is used.
                        if numel(eegSig) < numel(hEegDataset.TimingSignal)
                            eegSig = resample(double(eegSig), numel(hEegDataset.TimingSignal), numel(eegSig));
                        end
                        
                        hResult = obj.GetResultset(caChannel);
                        hResult.InitTable();
                        
                        for ii = 1:size(hCfg.AnalysisIntervalls, 1)
                            intervall = hCfg.AnalysisIntervalls(ii,:);
                            
                            caSignalDelay = hCaDataset.Settings.EegSync.EegSyncStart;
                            
                            [~, tIdxStartEeg] = hEegDataset.GetNearestSampleTime(intervall(1));
                            [~, tIdxEndEeg] = hEegDataset.GetNearestSampleTime(intervall(2));
                            
                            %Shift the window of the Ca2+ signal to the the correct span
                            [~, tIdxStartCa] = hCaDataset.GetNearestSampleTime(intervall(1) - caSignalDelay);
                            [~, tIdxEndCa] = hCaDataset.GetNearestSampleTime(intervall(2) - caSignalDelay);
                            
                            %Measure offset
                            [absOffset, maxCorr] = MeasureSignalOffset(...
                                double(eegSig(tIdxStartEeg:tIdxEndEeg)),...
                                double(caSig(tIdxStartCa:tIdxEndCa)),...
                                    eegInterval,...
                                    hCaDataset.MetaData.VolumeRate);

                            %subtract the computed offset from the delay of the Ca2+ signal.
                            %If absOffset is larger than caSignalDelay, deltaT will become negative,
                            %meaning the Ca2+ signal is preceedign the EEG signal
                            deltaT = caSignalDelay - absOffset;

                            hResult.AppendResult(intervall, deltaT, maxCorr);
                        end
                    end
                end
            end
        end
    end
    
end

