classdef MCorrelationAnalysisExResult < com.EEG.Analysis.MEegResultRoot
    %MCORRELATIONANALYSISEXRESULT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        tblCorrelation; % Start, End, deltaT, correlation
    end
    
    methods
        function obj = MCorrelationAnalysisExResult(channel)
            obj = obj@com.EEG.Analysis.MEegResultRoot(channel);
        end
    end
    
    methods
        function InitTable(obj)
            obj.tblCorrelation = table('Size', [0,4], ...
                'VariableTypes', ["single", "single", "single", "single"], 'VariableNames', ...
                ["Intervall start", "Intervall end", "offset", "Max. correlation"]);
        end
        
        function AppendResult(obj, intervall, deltaT, correlation)
            obj.tblCorrelation = [obj.tblCorrelation; ...
                {intervall(1), intervall(2), deltaT, correlation}];
        end
        
        function AnalyzeSignals(obj, hDataset)
        end
        
        function OnExportExcel(obj, hXlsFile)
            if ~isempty(obj.tblCorrelation)
                 workSheet = "Corr ex " + obj.ChannelID;
                 hXlsFile.Write(obj.tblCorrelation.Properties.VariableNames, workSheet, "A1");
                 hXlsFile.Write(obj.tblCorrelation.Variables, workSheet, "A2");
            end
             multiWaitbar(obj.SaveWBName, 'Value', 1);
        end
        
        function OnExportGraph(obj, outPath)
        end
    end
end

