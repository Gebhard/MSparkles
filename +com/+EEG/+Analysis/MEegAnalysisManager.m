%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MEegAnalysisManager < com.common.Analysis.AnalysisManagerRoot
    %MEEGANALYSISMANAGER Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = MEegAnalysisManager(hParent)
            %MEEGANALYSISMANAGER Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.Analysis.AnalysisManagerRoot(hParent);
        end
    end        
    
    methods
        function type = GetDefaultAnalysisType(~)
            type = com.Enum.AnalysisType.EEGSpikeTrainDetection;
        end
        
        function hAnalysis = CreateAnalysis(obj, analysisType, channels)
        %CREATEANALYSIS Creates a new analysis object of the specified type and
        %ensures it as a unique name w.r.t. all analyses already
        %managed by this instance if AnalysisManager.
            % analysisType  Member of AnalysisTypeEnum to define the type
            %               of analysis to be created.
            % channels      Channel IDs that define to which channels this
            %               analysis is applied.
            
            %Create new analysis
            switch (analysisType)
                case com.Enum.AnalysisType.EEGSpikeTrainDetection
                    hAnalysis = com.EEG.Analysis.MEegAnalysis;
                case com.Enum.AnalysisType.CaEegCorrelationEx
                    hAnalysis = com.EEG.Analysis.MCorrelationAnalysisEx;
                otherwise 
                    hAnalysis = [];
            end
            
            hAnalysis.Channels = channels;
            obj.UniqueName(hAnalysis);
        end                               
    end 
end

