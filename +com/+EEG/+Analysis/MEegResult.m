%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MEegResult < com.EEG.Analysis.MEegResultRoot
    %MEEGRESULTSET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Transient, Dependent)
        Spikes;
        SpikeCount;
        SpikeTrains;
        SpikeTrainCount;
        SpikeTrainTotalDuration;
        SpikeTrainAvgDuration;              
    end
    
    methods
        function obj = MEegResult(channelID)
            %MEEGRESULTSET Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.EEG.Analysis.MEegResultRoot(channelID);
        end
    end
    
    methods
        function value = get.Spikes(obj)
            value = obj.Results;
        end
        
        function set.Spikes(obj, value)
            obj.Results = value;
        end
        
        function value = get.SpikeTrains(obj)
            value = obj.ROIs;
        end
        
        function set.SpikeTrains(obj, value)
            obj.ROIs = value;
        end
        
        function value = get.SpikeCount(obj)
            sz = size(obj.Spikes);
            value = sz(1);
        end
                
        function value = get.SpikeTrainCount(obj)
            value = obj.RoiCount;
        end
        
        function value = get.SpikeTrainTotalDuration(obj)
            try
                value = obj.SpikeTrains.TotalSpikeTrainDuration;
            catch
                value = 0;
            end
        end
        
        function value = get.SpikeTrainAvgDuration(obj)
            try
                value = obj.SpikeTrains.AvgSpikeTrainDuration;
            catch
                value = 0;
            end
        end
    end
    
    methods(Access=protected)
        function count = CountRois(obj)
            try
                count = obj.SpikeTrains.Count;
            catch
                count = 0;
            end
        end
    end
    
    methods
        function AnalyzeSignals(obj)
        end
        
        function OnExportExcel(obj, hXlsFile)
            multiWaitbar(obj.SaveWBName, 'Value', 1/3);
            workSheet = "Channel " + obj.ChannelID;
            statsColNames = ["Avg. spike train duration", "Total time in seizure", "#Spike trains", "#Spikes"];
            statsVals = [obj.SpikeTrainAvgDuration, obj.SpikeTrainTotalDuration, obj.SpikeTrainCount, obj.SpikeCount];
            
            hXlsFile.Write(statsColNames, workSheet, "A1");
            hXlsFile.Write(statsVals, workSheet, "A2");
            
            multiWaitbar(obj.SaveWBName, 'Value', 2/3);
            trainColNames = ["No.", "t-Start (sec)", "Duration (sec)", "#Spikes", "Spiking frequency"];
            trainVals = nan(obj.SpikeTrainCount, 5);
                
            hXlsFile.Write(trainColNames, workSheet, "A4");
            
            if obj.SpikeTrainCount > 0
                trainVals(:, 1) = 1:obj.SpikeTrainCount;
                trainVals(:, 2) = obj.SpikeTrains.StartEndTimes(:,1);
                trainVals(:, 3) = obj.SpikeTrains.Durations;
                trainVals(:, 4) = obj.SpikeTrains.PeakCount;
                trainVals(:, 5) = obj.SpikeTrains.SpikingFrequency;
                
                hXlsFile.Write(trainVals, workSheet, "A5");
            end
            
            if ~isempty(obj.RelAvgPsd) && ~isempty(obj.AvgSigPwr)
                workSheet = "Channel " + obj.ChannelID + " PWR";
                psdHdr = ["Rel avg PSD (Epoch: " + obj.EpochLength + "sec)", "", "", "", "", "", "", "", "Signal power"];
                psdCols = ["delta", "theta", "alpha", "beta", "gamma", "omega", "rho", "sigma", "pwr (dB)"];

                hXlsFile.Write(psdHdr, workSheet, "A1");
                hXlsFile.Write(psdCols, workSheet, "A2");
                hXlsFile.Write([obj.RelAvgPsd', obj.AvgSigPwr'], workSheet, "A3");
            end
            
            multiWaitbar(obj.SaveWBName, 'Value', 3/3);           
        end
        
        function OnExportGraph(obj, outPath)
            hAnalysis = obj.hParent;
            hDataset = hAnalysis.hDataset;
            
            hFig = figure('Units', 'normalized', 'Position', [0,0,1,0.25], 'Visible', 'off' );
            hAx = axes(hFig);
            com.EEG.Rendering.EEGTracePlot(hAx, hDataset, hAnalysis, obj.ChannelID);
            title(hAx, "Channel " + obj.ChannelID);
            saveas(hFig, fullfile(outPath, sprintf("EEG_chan_%d", obj.ChannelID)), 'jpeg');

            hFig = figure('Units', 'normalized', 'Position', [0,0,1,0.25], 'Visible', 'off' );
            hAx = axes(hFig); 
            title(hAx, "Channel " + obj.ChannelID);
            com.EEG.Rendering.EEGProportionPlot(hAx, hDataset, hAnalysis, obj.ChannelID);
            saveas(hFig, fullfile(outPath, sprintf("EEG_PSD_chan_%d", obj.ChannelID)), 'jpeg');
        end
        
        function sig = GetBinarySpikeTrainSignal(obj)
            %GetBinarySpikeTrainSignal returns a rectangular signal, with
            %the lenth and sampling rate of the original signal. The signal
            %value is 1, only if the respective sample is inside a spike
            %train, otherwise, the signal value is 0.
            
            hDataset = obj.hParent.hDataset;
            sig = zeros(1, numel(hDataset.Data(obj.ChannelID, :)));
            times = int32(obj.SpikeTrains.StartEndTimes * hDataset.MetaData.SampleRate);
            
            sz = size(times);
            for i=1:sz(1)
                sig(times(i,1):times(i,2)) = 1;
            end
            
        end
        
        function sig = GetBinarySpikeTrainOnsetSignal(obj, hDataset)
            %GetBinarySpikeTrainSignal returns a rectangular signal, with
            %the lenth and sampling rate of the original signal. The signal
            %value is 1, only if the respective sample is inside a spike
            %train, otherwise, the signal value is 0.
            
            sig = zeros(1, numel(hDataset.Data(obj.ChannelID, :)));
            times = int32(obj.SpikeTrains.StartEndTimes * hDataset.MetaData.SampleRate);
            sig( min(times(:)) : max(times(:)) ) = 1;
        end        
    end
end

