classdef MEegResultRoot < com.common.Analysis.MResultRoot
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    %These properties are for compatibility
    properties (Dependent)
        AvgPsd;             %Absolute power per frequency band
        RelAvgPsd;          %Relative average Power spectral density per epoch
        AvgSigPwr;          %Average signal power per epoch
        EpochLength;        %Length of epoch in seconds
    end
   
    methods
        function value = get.AvgPsd(obj)
            try
                value = obj.hParent.hDataset.AvgPsd{obj.ChannelID};
            catch
                value = [];
            end
        end
        
        function value = get.RelAvgPsd(obj)
            try
                value = obj.hParent.hDataset.RelAvgPsd{obj.ChannelID};
            catch
                value = [];
            end
        end
        
        function value = get.AvgSigPwr(obj)
            try
                value = obj.hParent.hDataset.AvgSigPwr(obj.ChannelID, :);
            catch
                value = [];
            end
        end
        
        function value = get.EpochLength(obj)
            try
                value = obj.hParent.hDataset.EpochLength;
            catch
                value = 1;
            end
        end
        
        function set.AvgPsd(obj, value)
            obj.hParent.hDataset.AvgPsd(obj.ChannelID, :) = value;
        end
        
        function set.RelAvgPsd(obj, value)
           obj.hParent.hDataset.RelAvgPsd(obj.ChannelID, :) = value;
        end
        
        function set.AvgSigPwr(obj, value)
           obj.hParent.hDataset.AvgSigPwr(obj.ChannelID, :) = value;
        end
        
        function set.EpochLength(obj, value)
           obj.hParent.hDataset.EpochLength = value;
        end
    end
end

