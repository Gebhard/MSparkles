%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MEegDataset < com.common.Data.MDatasetRoot
    %MEEGDATASET Summary of this class goes here
    %   Detailed explanation goes here   
    
    properties(Transient, Dependent)
        NumChannels;
        NumSamples; 
        ChannelNames;        
        MajorSubBand;
    end  
    
    properties
        AvgPsd;             %Absolute power per frequency band
        RelAvgPsd;          %Relative average Power spectral density per epoch
        AvgSigPwr;          %Average signal power per epoch
        EpochLength = 1;    %Length of epoch in seconds
    end
    
    properties(Constant)
        WaveBands = [   0.5,4;  %delta
                        4, 8;   %theta
                        8, 12;  %alpha
                        12,30;  %beta
                        30,50;  %gamma
                        50,120; %omega
                        120,250;%sigma
                        250,600]%rho
    end
    
    methods
        function obj = MEegDataset(filePath)
            %MEEGDATASET Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.Data.MDatasetRoot(com.Enum.DatasetType.EegData, filePath);
        end
    end
    
    methods
        function value = Bandpass(obj, channelIdx, lBound, uBound)
            value = com.lib.SigProc.BandFilter(obj.Data(channelIdx, :), obj.MetaData.SampleRate,...
                [lBound, uBound], com.lib.SigProc.BandFilterType.BandPass);
        end
        
        function sig = GetWaveBand(obj, channelIdx, waveType)
            %Retrieve signal, absolute signal, signal power, mean power or variance of the entire
            %spectrum, or for a speciffic frequency band. if wavetype is a two-element row vector,
            %like [com.EEG.Enum.WaveBand.Delta, com.EEG.Enum.WaveBand.absPower], the absolute power
            %of the delta (0.5-4Hz) spectrum is returned. 
            %If waveband is a one-element enum value, either the signals of a speciffic wave band or
            %the specified property of the entire spectrum is returned.
            
            sig = obj.Data(channelIdx, :);
            
            for wt=waveType
                switch wt
                    case com.EEG.Enum.WaveBand.none
                        %sig = sig;    %return original
                    case com.EEG.Enum.WaveBand.absolute
                        sig = abs(sig);
                    case com.EEG.Enum.WaveBand.absPower
                        sig = 10*log10(abs(sig) .^ 2);
                    case com.EEG.Enum.WaveBand.avgPower
                        %sig = 10*log10( movmean(abs(sig), ceil(obj.MetaData.SampleRate/2), 2) .^ 2 );
                        if isempty(obj.AvgSigPwr)
                            obj.ComputePSD();
                        end
                        sig = obj.AvgSigPwr(channelIdx, :);
                    case com.EEG.Enum.WaveBand.variance
                        sig = movvar(sig .^ 2, obj.MetaData.SampleRate/2);
                    case com.EEG.Enum.WaveBand.meanSig 
                        try
                            if ~isempty( obj.MetaData.MeanSignalChan )
                                sig = obj.Data(obj.MetaData.MeanSignalChan, :);
                            end
                        catch
                            sig = [];
                        end
                        
                        if isempty(sig)
                            sig = mean(obj.Data(2:9, :));
                        end
                        
                    otherwise
                        bounds = com.EEG.Data.MEegDataset.WaveBands(wt, :);
                        for ch = channelIdx
                            sig(ch, :) = obj.Bandpass(ch, bounds(1), bounds(2));
                        end
                end
            end
        end
        
        function value = GetWaveBandName(~, waveType)
            switch waveType
                case com.EEG.Enum.WaveBand.none
                    value = "Original signal";
                case com.EEG.Enum.WaveBand.absolute
                    value = "Absolute signal";
                case com.EEG.Enum.WaveBand.absPower
                    value = "Absolute power";
                case com.EEG.Enum.WaveBand.avgPower
                    value = "Epoch power";
                case com.EEG.Enum.WaveBand.variance
                    value = "Variance";
                case com.EEG.Enum.WaveBand.meanSig
                    value = "Mean signal";
                otherwise
                    if isenum(waveType) && strcmpi(class(waveType), 'com.EEG.Enum.WaveBand')
                        value = "\"+string(waveType);
                    else
                        value = "";
                    end
            end
        end
        
        function ComputePSD(obj)
            if ~isempty(obj.Data)

                obj.EpochLength = 1; %seconds
                windowWidth = obj.EpochLength * obj.MetaData.SampleRate;
                numCols = ceil(obj.MetaData.NumSamples / windowWidth);
                
                requiredSamples = numCols*windowWidth;
                sampleDiff = requiredSamples - obj.MetaData.NumSamples;
                
                obj.AvgPsd      = {};
                obj.RelAvgPsd   = {};
                obj.AvgSigPwr   = [];
                
                for ii = 1:obj.NumChannels
                    sig = obj.Data(ii, :);
                    if ~isempty(sig)
                        sig = cat(2, sig, fliplr(sig((end-sampleDiff):end-1))); %Pad with mirrored samples at the end
                        sig = reshape(sig, [windowWidth, numCols]);
                        avgPsd = com.lib.SigProc.AvgPowerSpectralDensity(sig, obj.MetaData.SampleRate, com.EEG.Data.MEegDataset.WaveBands);
                        s = sum(avgPsd, 1);
                        
                        obj.AvgPsd     = cat(1, obj.AvgPsd , avgPsd);
                        obj.RelAvgPsd  = cat(1, obj.RelAvgPsd, avgPsd ./ s); %relative PSD per band
                        obj.AvgSigPwr  = cat(1, obj.AvgSigPwr, 10*log10(s));      %pwr in dB  
                    end
                end
            end
        end
        
        function value = IsReloadRequired(obj)
            value = ~obj.IsLoaded || isempty(obj.Data);
        end
        
        function success = SavePreProcResult(obj)
            try
                [fPath,fName,fExt] = fileparts(obj.OrgDatasetPath);
                preprocPath = fullfile(fPath, sprintf('PreProc-%s%s', fName, fExt));

                ans = obj.Data;
                save(preprocPath, "ans", '-mat');

                obj.PreProcDatasetPath = preprocPath;
                success = true;
            catch
                success = false;
            end
        end
        
        function ResetMetadata(obj)
            if isempty(obj.MetaData)
                obj.MetaData = com.EEG.Data.MEegMetaData();
            end
            
            obj.MetaData.InitSchweigmannMetadata(obj.Data); 
        end
        
        function [peaks, locs] = AnalyzeSyncSignal(obj, CaVolumeRate, syncSig, invert)
            minPeakHeight = 10000;
            
            if nargin < 3 || isempty(syncSig)
                syncSig = obj.Data(obj.MetaData.SyncChannelIdx, :);
            end
            
            if invert
                syncSig = -syncSig;
            end
            
            [peaks, locs] = findpeaks(syncSig,...
                obj.MetaData.SampleRate,...
                "MinPeakHeight", minPeakHeight,...
                "MinPeakDistance", 1/(2*CaVolumeRate));
            
            obj.MetaData.SyncSigPeaks = peaks;
            obj.MetaData.SyncPeakLocs = locs;
        end
        
        function value = GetNearestSyncTime(obj, t)
            [~,idx]=min(abs(obj.MetaData.SyncPeakLocs-t), [], "omitnan");
            value = obj.MetaData.SyncPeakLocs(idx);
        end
    end
    
    methods(Access=protected)
        function InitDatastore(obj, fPath)
            try
                obj.DataStore = com.common.FileIO.EEGDatastore(fPath);
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                obj.DataStore = [];
            end
        end
        
        function success = OnLoadData(obj, ~, ~, fileExt)
            success = false;
            
            if isfile(obj.DatasetPath) || isfolder(obj.DatasetPath)
                if isempty(fileExt) || ismember(fileExt, ".mat")
                    obj.InitDatastore(obj.DatasetPath);
                    obj.DataStore.readall(); 

                    if isempty(obj.MetaData)
                        obj.MetaData = obj.DataStore.Metadata;
                    end

                    if obj.Analyses.Count == 0
                        hAnalysis = obj.Analyses.CreateAnalysis(com.Enum.AnalysisType.EEGSpikeTrainDetection,...
                            obj.MetaData.NumChannels);
                        obj.Analyses.AddAnalysis( hAnalysis );
                    end
                    
                    success = true;
                end
            end  
            
            if ~success
                obj.F = [];
                obj.MetaData = [];
                com.common.Logging.MLogManager.Error(sprintf('Unsupported file format for file: "%s"', fPath));       
            end

            success = true;
        end
        
        function hMetaData = CreateMetadata(~, filePath)
            hMetaData = com.EEG.Data.MEegMetaData(filePath);
        end
        
        function hAnalysisManager = CreateAnalysisManager(obj)
            hAnalysisManager = com.EEG.Analysis.MEegAnalysisManager(obj);
        end
        
        function value = GetTimingSignal(obj)
            try
                if obj.MetaData.TimingChannelIdx > 0
                    value = obj.Data(obj.MetaData.TimingChannelIdx, :);
                else
                    value = [];
                end
            catch
                com.common.Logging.MLogManager.Warning("EEG data not loaded. Cannot access timing signal.");
                value = [];
            end
        end
    end
    
    methods
        function value = get.EpochLength(obj)
            if isempty(obj.EpochLength)
                obj.EpochLength = 1;
            end
            
            value = obj.EpochLength;
        end
        
        function value = get.NumChannels(obj)
            if isempty(obj.MetaData.Channels)
                sz = size(obj.Data);
                obj.MetaData.Channels = 1:sz(1);
            end
            
            value = numel(obj.MetaData.Channels);
        end
        
        function value = get.NumSamples(obj)
            if isempty(obj.MetaData.NumSamples)
                sz = size(obj.Data); 
                obj.MetaData.NumSamples = sz(2);
            end
                      
            value = obj.MetaData.NumSamples;
        end
        
        function value = get.ChannelNames(obj)
            try
                value = "Channel " + obj.MetaData.Channels;
            catch
                value = {};
            end
        end                
    end
end

