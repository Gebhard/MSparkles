%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MEegMetaData < com.common.Data.MMetadataRoot
    %MEEGMETADATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        TimingChannelIdx;   % Index of channel containing timing information
        SyncChannelIdx;     % Index of channel containing a synchronization signal
        SampleRate;         % Sample rate in Hz
        Duration;           % Duration in seconds
        Comment;            % Text comment
        MeasurementUnit;    % Unit of measured signal as text
        MeanSignalChan;     % Index of channel containing the mean signal of the analyzed channels
        Channels;           % Array of available channels
        NumSamples;         % Number of samples per channel (must be identical over all channels)
        
        SyncSigPeaks;       % Peak values of analyzed EEG sync channel
        SyncPeakLocs;       % Peak locations in seconds, basen on CaVolumeRate
    end
    
    properties(Transient, Dependent)
        NumChannels;        % Number of real signal channels, wihtout timing and sync
    end
    
    methods
        function obj = MEegMetaData()
            obj = obj@com.common.Data.MMetadataRoot();
        end
    end
    
    methods
        function value = get.NumChannels(obj)
            c1 = max(2, obj.Channels(1));
            c2 = max (c1, obj.Channels(end) -1);                            
            value =  c1:c2; 
        end
    end
    
    methods
        function InitSchweigmannMetadata(obj, data)
            %This function initialized meta data read form a .mat file,
            %containing EEG data, recorded by the system of Schweigmann et
            %al. All metadata can be derived from the stored EEG signals an
            %knowledge about specific channels.
            
            sz = size(data);
            obj.TimingChannelIdx    = 1;        %First channel contains timing info
            obj.SyncChannelIdx      = sz(1);    %Last channel is for sync with 2P-LSM
            obj.NumSamples          = sz(2);    %Number of samples
            obj.SampleRate          = 1 / data(obj.TimingChannelIdx, 2);    %2nd time measurement (1st is zero)
            obj.Duration            = obj.NumSamples / obj.SampleRate;      %Duration of recording in seconds
            obj.Comment             = "";
            obj.MeasurementUnit     = "{\mu}V"; %Defoult unit of measurement is mico volt
            obj.Channels            = 1:sz(1);  %Number of channels, incl. sync and timing
        end
    end
end

