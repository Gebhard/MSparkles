%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageLoPass < com.EEG.Processing.Stages.PFilterStage
    %PSTAGEGAUSS1D Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = PStageLoPass()
            obj = obj@com.EEG.Processing.Stages.PFilterStage('Low-pass filter', "1.0.0", "21.05.2019");
        end
    end
    
    methods    
        function data = ApplyFilter(obj, hDataset, data)
            data = com.lib.SigProc.BandFilter(data, hDataset.MetaData.SampleRate,...
                obj.StageConfig.Bounds, com.lib.SigProc.BandFilterType.LoPass);
        end
    end     
end

