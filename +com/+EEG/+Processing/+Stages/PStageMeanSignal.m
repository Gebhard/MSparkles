%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageMeanSignal < com.common.Proc.AnalysisStage
    %PSTAGEMEANSIGNAL Summary of this class goes here
    %   Detailed explanation goes here
    
     methods
        function obj = PStageMeanSignal()
            %PStageMeanSignal Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.Proc.AnalysisStage('Mean EEG signal', '1.0.0', '14.06.2019', []);
        end
     end
    
    methods (Access=protected)        
        function success = OnExecuteStage(obj, hDataset, ~)                                      
            if isa(hDataset, "com.EEG.Data.MEegDataset") %&& ...
                    %obj.hAnalysis.AnalysisType == com.Enum.AnalysisType.EEGSpikeTrainDetection
                chans = obj.GetRequiredChannels(hDataset);

                if isempty(hDataset.MetaData.MeanSignalChan)
                    hDataset.MetaData.MeanSignalChan = hDataset.NumChannels + 1;
                end

                %Set last channel as average signal
                hDataset.Data(hDataset.MetaData.MeanSignalChan,:) = mean(hDataset.Data( chans, : ) ); 
                success = true;
            else
                error("Dataset passed to PStageMeanSignal is not of type 'com.EEG.Data.MEegDataset'.");
            end
        end
    end
end

