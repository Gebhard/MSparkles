%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef BandFilterConfig < com.EEG.Processing.FilterConfig
    %BANDFILTERCONFIG Paremeters for Gauss filter pipeline stage
    %   Filter parameters vary, depending on the filter type
    
    properties
        Type com.lib.SigProc.BandFilterType;
        Bounds;
    end
    
    methods
        function obj = BandFilterConfig(type)
            %BANDFILTERCONFIG Construct an instance of BandFilterConfig
            %with the filter type as the config Name
            obj = obj@com.EEG.Processing.FilterConfig(string(type));
            obj.Type = type;
            obj.InitDefaultValues()
        end
    end      
    
    methods
        function InitDefaultValues(obj)
            %Creates a filter stage based on the Type-property of the
            %config object.
            if ~isempty(obj.Type)
                switch (obj.Type)
                    case com.lib.SigProc.BandFilterType.BandPass
                        obj.Bounds = [0.5, 500];
                    case com.lib.SigProc.BandFilterType.BandStop
                        obj.Bounds = [50, 75];
                    case com.lib.SigProc.BandFilterType.HiPass
                        obj.Bounds = 10;
                    case com.lib.SigProc.BandFilterType.LoPass
                        obj.Bounds = 450;
                    case com.lib.SigProc.BandFilterType.Notch
                        obj.Bounds = 50;
                end
            end
        end
        
        function Edit(obj)
            switch (obj.Type)
                    case {com.lib.SigProc.BandFilterType.BandPass, com.lib.SigProc.BandFilterType.BandStop}
                        fltStr = ["Lower frequency bound:", "Upper frequency bound:"];
                    case {  com.lib.SigProc.BandFilterType.HiPass, ...
                            com.lib.SigProc.BandFilterType.LoPass,...
                            com.lib.SigProc.BandFilterType.Notch }
                        fltStr = "Filter frequency";
            end
            
            answer = inputdlg(fltStr, "Edit " + obj.Name, [1, 35], arrayfun(@(x) string(x), obj.Bounds));
            
            try
                if ~isempty(answer)
                    answer = str2double(answer);
                    
                    if all(~isnan(answer))
                        obj.Bounds = answer;
                    else
                        errordlg("Input must be numeric");
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(obj, varargin)
            switch (obj.Type)
                case com.lib.SigProc.BandFilterType.BandPass
                    hStage = com.EEG.Processing.Stages.PStageBandpass();
                case com.lib.SigProc.BandFilterType.BandStop
                    hStage = com.EEG.Processing.Stages.PStageBandstop();
                case com.lib.SigProc.BandFilterType.HiPass
                    hStage = com.EEG.Processing.Stages.PStageHiPass();
                case com.lib.SigProc.BandFilterType.LoPass
                    hStage = com.EEG.Processing.Stages.PStageLoPass();
                case com.lib.SigProc.BandFilterType.Notch
                    hStage = com.EEG.Processing.Stages.PStageNotchFilter();
            end
        end
    end
end

