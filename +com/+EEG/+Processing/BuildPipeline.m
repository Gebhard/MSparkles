%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function ep = BuildPipeline(hDatasetNode, hAnalyses, finalStage, forceRecomputeAll)
%BUILDPIPELINE Constructs the processing pipeline for EEG analysis
%   hDataSet:           Handle to the dataset to be processed
%   finalStage:         com.Enum.PipelineStage, which allows to run the 
%                       pipeline only partially
%   hAnalysis:          (Cell) Array of MEegAnalysis objects, specifying
%                       which analysis operations are to be performed.
%                       Can be a subset of the analyses defined in the
%                       dataset.
    
    hDataset = hDatasetNode.Dataset;
    
    forceReloadDataset = (~isempty(hDataset.PreProcDatasetPath) && hDataset.PreProcDatasetPath ~= "" && ...
        finalStage == com.Enum.PipelineStage.PreProcessing) || forceRecomputeAll;
    
    if finalStage == com.Enum.PipelineStage.PreProcessing || forceRecomputeAll
        hDataset.PreProcDatasetPath = [];
        hDataset.MetaData.MeanSignalChan = [];
    end        
    
    ep = com.common.Proc.ExecutionPipeline(hDataset);    
    ep.AddPipelineStage(com.common.Proc.PStageOpenFile(forceReloadDataset), @OnFileLoaded, {hDatasetNode});
    
    if finalStage >= com.Enum.PipelineStage.PreProcessing || forceRecomputeAll
        if hDataset.NumPreProcStages > 0
            for i=1:(hDataset.NumPreProcStages)
                hPreProc = hDataset.GetPreProcConfig(i);
                ep.AddPipelineStage( hPreProc.CreatePipelineStage() );
            end
            
            ep.AddPipelineStage(com.EEG.Processing.Stages.PStageMeanSignal(), @OnPreProcFinished, {hDatasetNode} )
        end
    end
    
    if finalStage >= com.Enum.PipelineStage.Analysis || forceRecomputeAll
        
        if nargin < 2 || isempty(hAnalyses)
            hAnalyses = hDataset.Analyses;
        end
        
        if isa(hAnalyses, 'com.common.Analysis.AnalysisManagerRoot')
            numAnalyses = hAnalyses.Count;
        else
            numAnalyses = numel(hAnalyses);
            if ~iscell(hAnalyses)
                hAnalyses = {hAnalyses};
            end
        end
        
        if numAnalyses > 0
            for i=1:(numAnalyses-1)
                hAnalysis = hAnalyses{i};
                ep.AddPipelineStage( hAnalysis.CreateAnalysisStage() );
            end
             %use the last stage to execute callback to finalize analysis
            hAnalysis = hAnalyses{end};
            ep.AddPipelineStage( hAnalysis.CreateAnalysisStage(), @OnAnalysisFinished, {hDatasetNode, hAnalysis} );
        end
    end
end

function OnFileLoaded(params, success)
    if success
        hDatasetNode = params{1};          

        projMon = com.Management.MProjectManager.GetInstance();        
        if ~isempty(projMon)
            projMon.UpdateActiveDatasetNode( hDatasetNode );
        end
    end
end

function OnPreProcFinished(params, success)
    if success
        hDataSetNode = params{1};
        hDataSet = hDataSetNode.Dataset;

        if (hDataSet.SavePreProcResult())
             hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hProjMan.CurrentDataset.Save();
            end
        end
        
        hMSparkles = MSparklesSingleton.GetInstance();
        if ~isempty(hMSparkles)
            hMSparkles.hEegMainWnd.Update();
        end
    end
end

function OnAnalysisFinished(params, ~)
    warning ('off','all');
    
    try
        hDatasetNode = params{1};
        hAnalysis = params{2};
        hAnalysis.AnalyzeSignals();
                    

        hProjMan = com.Management.MProjectManager.GetInstance();
        if ~isempty(hProjMan)
            hProjMan.UpdateResults(hAnalysis);
            hProjMan.CurrentDataset.Save();

            hGlobalSettings = MGlobalSettings.GetInstance();
            if (hGlobalSettings.GenerateExcelFiles == true)
                %Save Excel file to tbhe results folder of the actual dataset,
                %not the child dataset
                fPath = hAnalysis.GetResultsFolder();    
                fName = hAnalysis.Name;
                xlsPath = fullfile(fPath, sprintf('%s.xlsx', fName));            
                
                hAnalysis.ExportExcel(xlsPath);
                hAnalysis.ExportGraphs(fPath);
            end
        end

        hMSparkles = MSparklesSingleton.GetInstance();
        if ~isempty(hMSparkles)
            hMSparkles.hEegMainWnd.Update();
        end
    catch ex
        com.common.Logging.MLogManager.Exception(ex);
    end
    
    warning ('on','all');
end

