%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function success = ComputeEegSpikeTrains(hEegDataset, hAnalysis)
%COMPUTEEEGSPIKETRAINS Summary of this function goes here
%   Detailed explanation goes here

    try
        if isa(hEegDataset, 'com.EEG.Data.MEegDataset') && ...
                hAnalysis.AnalysisType == com.Enum.AnalysisType.EEGSpikeTrainDetection            
            
            sampleRate = hEegDataset.MetaData.SampleRate;
            peakTh = hAnalysis.AnalysisSettings.SpikeThreshold;
            waveBand = hAnalysis.AnalysisSettings.WaveBand;   
            minProm = hAnalysis.AnalysisSettings.MinimumSpikeProminence;
            
            channels = hAnalysis.Channels;
            
            if ~isempty(hEegDataset.MetaData.MeanSignalChan)
                if ~ismember(hEegDataset.MetaData.MeanSignalChan, channels)
                    
                    chans = 2:9;
                    
                    channels(end+1) = hEegDataset.MetaData.MeanSignalChan;
                    if size(hEegDataset.Data, 1) < hEegDataset.MetaData.MeanSignalChan || ...
                        isempty(hEegDataset.Data(hEegDataset.MetaData.MeanSignalChan,:))
                        hEegDataset.Data(hEegDataset.MetaData.MeanSignalChan,:) = mean(hEegDataset.Data( chans, : ) ); 
                    end
                end
            end
            
            for c=channels
                hResultSet = hAnalysis.GetResultset(c);
                
                sig = hEegDataset.GetWaveBand(c, waveBand);
                [foundPeaks, peakLocations, durations] = findpeaks2(sig, sampleRate, 'MinPeakHeight', peakTh, ...
                    'MinPeakProminence',  minProm);                
                hResultSet.Spikes = [foundPeaks', peakLocations', durations'];
            
                %compute temporal derivatives of peak locations (times)
                tDiff = diff(peakLocations); 

                %Binarize, based on max spike distance
                tDiff(tDiff > hAnalysis.AnalysisSettings.MaxSpikeDistance) = 0;
                tDiff(tDiff > 0) = 1;
                minDur = hAnalysis.AnalysisSettings.MinSpikeTrainDuration;
                
                %extract consecutive strings of '1' as spike trains
                conPeaks = bwconncomp(tDiff, [0,0,0;1,1,1;0,0,0]);
                
                spikeTrains = com.EEG.Analysis.MSpikeTrains;                               
                
                %get first and last time points for each spike train
                for i=1:conPeaks.NumObjects
                    idx = conPeaks.PixelIdxList{i};
                    
                    if numel(idx) > 1
                        first = idx(1);
                    	last = idx(end);
                        
                        tStart = peakLocations(first);
                        tEnd = peakLocations(last+1);
                        duration = tEnd-tStart;
                        
                        if duration >= minDur
                            spikeTrains.StartEndTimes = [spikeTrains.StartEndTimes; tStart, tEnd];                        
                            spikeTrains.Durations = [spikeTrains.Durations; duration];
                            spikeTrains.Peaks = foundPeaks(first:last+1);
                            spikeTrains.PeakLocations = peakLocations(first:last+1);
                            spikeTrains.PeakCount = [spikeTrains.PeakCount; numel(idx) + 1];
                        end
                    end
                end
                                
                spikeTrains.Count = numel(spikeTrains.Durations);
                
                hResultSet.SpikeTrains = spikeTrains;                
            end
            success = true;
        else
            error( 'Supplied dataset must be of type "com.EEG.Data.MEegDataset" (was "%s")', class(hEegDataset) );
        end
    catch ex
        com.common.Logging.MLogManager.Exception(ex);
        success = false;
    end
end

