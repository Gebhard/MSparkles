%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef GaussFilterConfig < com.EEG.Processing.FilterConfig
    %GAUSSFILTERCONFIG Paremeters for Gauss filter pipeline stage
    
    properties(Constant)
        DEFAULT_Fs = 1200;      % 1200 Hz
        DEFAULT_Width = 0.1;    % 0.1 seconds => 120 samples @ 1200Hz
        DEFAULT_Sigma = 40;     % Optimal filter width = 3*sigma => 3*40 = 120
    end
    
    properties
        SamplingRate;
        Sigma;
        KernelHalfSize;
    end
    
    methods
        function obj = GaussFilterConfig()
            %GAUSSFILTERCONFIG Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.EEG.Processing.FilterConfig("Gauss");
        end
    end
    
    methods
        function InitDefaultValues(obj)
            %Creates a filter stage based on the Type-property of the
            %config object.
            obj.InitFilter(obj.DEFAULT_Fs, obj.DEFAULT_Width, obj.DEFAULT_Sigma)
        end
        
        function InitFilter(obj, Fs, width, sigma)
            %Creates filter parameter, based on filter width and sigma.
            %   Fs      Sample rate as integer (Samples per second)
            %   width   Width of the filter in seconds
            %   sigma   Standard deviation of the gauss filter. (Filter
            %           strength)
            %
            %   The kernel half-size is computed as ((Fs*width) - 1) / 2
            %   and then rounded to the nearest integer;                                
            
            obj.SamplingRate = Fs;
            obj.KernelHalfSize = round ( ((obj.SamplingRate * width) - 1) / 2 );                
            obj.Sigma = sigma;
        end
        
        function Edit(obj)
            answer = inputdlg(["Sampling rate:", "Filter width (sec):", "Sigma:"], "Edit " + obj.Name,...
                [1, 35], [string(obj.SamplingRate), string( (2*obj.KernelHalfSize) / obj.SamplingRate), string(obj.Sigma)]);
            try
                if ~isempty(answer)
                    answer = str2double(answer);
                    if all(~isnan(answer))
                        obj.SamplingRate = round(answer(1));
                        obj.KernelHalfSize = round( (obj.SamplingRate * answer(2) - 1) / 2);
                        obj.Sigma = answer(3);
                    else
                        errordlg("Input must be numeric");
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
           hStage = com.EEG.Processing.Stages.PStageGauss1D();
        end
    end
end

