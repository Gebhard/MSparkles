%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef SGolayFilterConfig < com.EEG.Processing.FilterConfig
    %SGOLAYFILTERCONFIG Paremeters for Savitzky-Golay pipeline stage
    %   Order must be smaller than FrameLength. 
    %   If Order = FrameLength � 1, the filter produces no smoothing.
    
    properties
        FrameLength;    % Number of samples to be used per polynomial fit. Must be an odd, positive integer.
        Order;          % Order of the polynomial. Must be a positive integer.
    end
    
    methods
        function obj = SGolayFilterConfig()
            %GAUSSFILTERCONFIG Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.EEG.Processing.FilterConfig("Savitzky-Golay");
        end
    end
    
    methods
        function InitDefaultValues(obj)
            %Default values: Polynomial of degree 7, using 25 samples
            %Assuming 1200Hz sample rate 24 samples = 0.02 seconds.
            %Since FrameLength must be odd => 25.
            obj.Order = 7;
            obj.FrameLength = 25;
        end
        
        function Edit(obj)
            answer = inputdlg(["Frame length:", "Order of polynomial:"], "Edit " + obj.Name,...
                [1, 35], [string(obj.FrameLength), string(obj.Order)]);
            try
                if ~isempty(answer)
                    answer = str2double(answer);
                    
                    if all(~isnan(answer)) 
                        if mod(answer(1), 2) ~= 0
                            obj.FrameLength = round(answer(1));
                            obj.Order = round(answer(2));
                        else
                            errordlg("Frame length must be odd.");
                        end
                    else
                        errordlg("Input must be numeric");
                    end
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, varargin)
           hStage = com.EEG.Processing.Stages.PStageSGolay();
        end
    end
end

