%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function EEGProportionPlot(hAxis, hDataset, hAnalysis, channelIdx)   
    try     
%         hAnalysis = hDataset.Analyses.GetAnalysisByType(com.Enum.AnalysisType.EEGSpikeTrainDetection);
%         if iscell(hAnalysis)
%             hAnalysis = hAnalysis{:};
%         end
        
        if ~isempty(hAnalysis)
            hResultset = hAnalysis.GetResultset(channelIdx);
            yyaxis(hAxis, "left");
            cla(hAxis);
            hAxis.YColor = [0 0 0];
            
            if ~isempty(hResultset)
                imagesc(hAxis, hResultset.RelAvgPsd); 
                epochLen = hResultset.EpochLength;
            else
                data = zeros(8, round(hDataset.MetaData.Duration) );
                imagesc(hAxis, data );
            end
            
            if isempty(epochLen)
                epochLen = 1;
            end
                        
            colorbar(hAxis);
            hAxis.Colormap = jet;
            hAxis.CLim = [0,1];
            hAxis.YTickLabel = ["\delta", "\theta", "\alpha", "\beta", "\gamma", "\omega", "\rho", "\sigma"];
            hAxis.YTick = 1:8;
            hAxis.YLabel.String = "Waveband";
            hAxis.XLabel.String = "Time (sec)";
            hAxis.XTickLabel = "" + ( hAxis.XTick * epochLen);

            com.EEG.Rendering.renderSignalPower(hAxis, hResultset.AvgSigPwr);
        end                       
    catch
    end
end

