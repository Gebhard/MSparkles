%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function EEGTracePlot(hAxis, hDataset, hAnalysis, channelIdx, overlay, invertSignal, renderOrgSig)
    w = [];
    wbName = "";
    
    if nargin < 7
        renderOrgSig = true;
    end
    
    if nargin < 6
        invertSignal = false;
    end
    
    if nargin >= 5 && ~isempty(overlay)         
        if isenum(overlay)
            if  overlay ~= com.EEG.Enum.WaveBand.none
                w = hDataset.GetWaveBand(channelIdx, overlay);
                wbName = hDataset.GetWaveBandName(overlay);
            end
        elseif isvector(overlay)
            w = overlay;
        end
    end                    
    
    try    
        hAxis.ColorOrder =  [    
            0    0.4470    0.7410
            0.8500    0.3250    0.0980
            0.9290    0.6940    0.1250
            0.4940    0.1840    0.5560
            0.4660    0.6740    0.1880
            0.3010    0.7450    0.9330
            0.6350    0.0780    0.1840];
           
        if renderOrgSig || overlay == com.EEG.Enum.WaveBand.none
            sig = hDataset.Data(channelIdx, :);
        else
            sig = [];
        end
        
        if ~isempty(sig)
            if invertSignal
                sig = -sig;
            end

            plot(hAxis, hDataset.TimingSignal, sig);            
            [orgMin, orgMax] = bounds(sig);
            yAxisLims = [orgMin, max(orgMax, 2000)];
            ylim(hAxis, yAxisLims);        
        end
        
        if ~isempty(w)
            if renderOrgSig
                col = hAxis.ColorOrder(2,:);
            else
                col = hAxis.ColorOrder(1,:);
            end
            
            %if w is shorter -> upscale
            
            if size(w,1) > 1
                com.EEG.Rendering.renderSignalPower(hAxis, w(channelIdx, :), hDataset.TimingSignal, [0.9290 0.6940 0.1250] , 0.5);
            else
                com.EEG.Rendering.renderSignalPower(hAxis, w, hDataset.TimingSignal, col , 0.5);
            end
        end

        if ~isempty(hAnalysis)
            hold(hAxis, 'on');
            hResults = hAnalysis.GetResultset(channelIdx);
            if ~isempty(hResults) && ~isempty(hResults.Spikes)
                %Plot spikes
                locs = hResults.Spikes(:,2);    
                peaks = hResults.Spikes(:,1);

                scatter(hAxis, locs, peaks, 'filled', 'MarkerFaceAlpha', 0.75, 'SizeData', 24 ); %Default = 36

                sz = size(hResults.SpikeTrains.StartEndTimes);
                y = ones(sz) * yAxisLims(2) - 10;

                %Plot spike trains
                line(hAxis, hResults.SpikeTrains.StartEndTimes',y', 'LineWidth', 5);

                if isempty(w)                    
                    if ~isempty(hResults.AvgSigPwr)
                        com.EEG.Rendering.renderSignalPower(hAxis, hResults.AvgSigPwr);
                    end
                end
            end
        end
    catch ex
    end
    
    hold(hAxis, 'off'); 
    
    if strlength(wbName) > 0
        wbName = wbName + "    ";
    end
    
    if ~isempty(sig)
        ylabel(hAxis, wbName + hDataset.MetaData.MeasurementUnit);
    else
        hAxis.YTickLabel = "";
        hAxis.YTick = 0;
    end
    
    xlabel(hAxis, 'Time (sec)');
    xlim( hAxis, [0, hDataset.LastSampleTime] );                
end

