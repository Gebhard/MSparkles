function renderSignalPower(hAxis, sigPwr, timingSig, color, lineWidth)
    arguments
        hAxis (1,1) {mustBeNonempty}
        sigPwr {mustBeNonempty, mustBeNumeric}
        timingSig = []
        color = [1 0.0780 0.1840]
        lineWidth(1,1) {mustBePositive} = 1
    end
   
    yliml = hAxis.YLim;
    ratio = yliml(1)/yliml(2);
    
    yyaxis(hAxis, "right");
    cla(hAxis);
    if ~isempty(sigPwr)
        if ~isempty(timingSig)
            if numel(sigPwr) < numel(timingSig)
                sigPwr = resample(double(sigPwr), numel(timingSig), numel(sigPwr));
            end
            plot(hAxis, timingSig, sigPwr, "color", color, "LineWidth", lineWidth);
        else
            plot(hAxis, sigPwr, "color", color, "LineWidth", lineWidth);
        end
        [y1, y2] = bounds(sigPwr, "all");
        hAxis.YLim = [min(-10, y1), max(30, y2)];
    else
        hAxis.CLim = [0,1];
        hAxis.YLim = [0,.5];
        hAxis.YTickLabelMode = "auto";
        hAxis.YDir = "normal";
    end
    
    hAxis.YColor = [0 0 0];
    ylabel(hAxis, "\mu"+"V^2 (dB)");
    
    ylimr = hAxis.YLim;
         
    if yliml(1) < 0
        if ylimr(2)*ratio<ylimr(1)
            set(hAxis,'Ylim',[ylimr(2)*ratio ylimr(2)])
        else
            set(hAxis,'Ylim',[ylimr(1), ylimr(1)/ratio])
        end
    end
    
    yyaxis(hAxis, 'left');
    hAxis.YColor = [0,0,0];
end

