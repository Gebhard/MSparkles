%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegFrequencyGraphListItem < com.common.UI.GraphListItem
    %EEGTRACEPLOT Summary of this class goes here
    %   Detailed explanation goes here
    methods
        function Render(obj, hDataSet, hAnalysis, signalID, plotIdx)
            obj.RenderGraph(hDataSet, hAnalysis, signalID,  plotIdx)
            
            disableDefaultInteractivity(obj.hAxis);
            try
                axtoolbar(obj.hAxis, {'export', 'zoomin', 'zoomout'}); %2020a
            catch
                try
                    axtoolbar(obj.hAxis, {'zoomin', 'zoomout'}); %2019b
                catch
                end
            end
        end
    end
    
    methods(Access = protected)
        function hAxis = CreateAxes(obj)
            hAxis = axes(obj.itemPanel, 'Position', [.075 0.175 .91 .725]);
            hAxis.ButtonDownFcn = @obj.OnMouseClick;
            
            hAxis.ColorOrder =  [    
                0    0.4470    0.7410
                0.8500    0.3250    0.0980
                0.9290    0.6940    0.1250
                0.4940    0.1840    0.5560
                0.4660    0.6740    0.1880
                0.3010    0.7450    0.9330
                0.6350    0.0780    0.1840];
            
            %hPlot.ButtonDownFcn = @obj.OnMouseClick;
        end
        
        function RenderGraph(obj, hDataSet, hAnalysis, channelIdx, plotIdx)
            try
%                 disableDefaultInteractivity(obj.hAxis);
%                 try
%                     axtoolbar(obj.hAxis, {'export', 'zoomin', 'zoomout'}); %2020a
%                 catch
%                     try
%                         axtoolbar(obj.hAxis, {'zoomin', 'zoomout'}); %2019b
%                     catch
%                     end
%                 end
                
                com.EEG.Rendering.EEGTracePlot(obj.hAxis, hDataSet, hAnalysis, channelIdx, com.EEG.Enum.WaveBand(plotIdx) );
            catch
            end
        end
    end
end

