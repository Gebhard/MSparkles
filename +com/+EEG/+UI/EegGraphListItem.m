%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegGraphListItem < com.common.UI.GraphListItem
    %EEGTRACEPLOT Summary of this class goes here
    %   Detailed explanation goes here
    properties(Access = protected)
        hLabel;
    end
    
    methods 
        function obj = EegGraphListItem(hGraphList, parent, itemNo, numItems, ClickHandler)
            obj = obj@com.common.UI.GraphListItem(hGraphList, parent, itemNo, numItems, ClickHandler);
            obj.hLabel =  uicontrol('Style', 'Text', 'Parent', obj.itemPanel, 'Units', 'normalized', 'Position', [0 0 0.07 1], 'HorizontalAlignment', 'left');
        end
    end
    
    methods
        function Render(obj, hDataSet, hResultset, plotIdx)
            obj.RenderGraph(hDataSet, hResultset, plotIdx);
            
            disableDefaultInteractivity(obj.hAxis);
            try
                axtoolbar(obj.hAxis, {'export', 'zoomin', 'zoomout'}); %2020a
            catch
                try
                    axtoolbar(obj.hAxis, {'zoomin', 'zoomout'}); %2019b
                catch
                end
            end
        end
    end
    
    methods(Access = protected)
        function hAxis = CreateAxes(obj)
            hAxis = axes(obj.itemPanel, 'Position', [.12 0.175 .85 .725]);
            hAxis.ColorOrder =  [    
                0    0.4470    0.7410
                0.8500    0.3250    0.0980
                0.9290    0.6940    0.1250
                0.4940    0.1840    0.5560
                0.4660    0.6740    0.1880
                0.3010    0.7450    0.9330
                0.6350    0.0780    0.1840];
            
            %hPlot.ButtonDownFcn = @obj.OnMouseClick;
        end
    
        function RenderGraph(obj, hDataSet, hAnalysis, plotIdx)  
            
            try
                if hAnalysis.ResultsetExists(plotIdx)
                    hResultset = hAnalysis.GetResultset(plotIdx);
                    if ~isempty(hResultset)                                
                        obj.hLabel.String = sprintf('Spikes: %d\nTrains: %d\nDuration: %.3f\n� Duration: %.3f', ...
                            hResultset.SpikeCount, hResultset.SpikeTrainCount,...
                            hResultset.SpikeTrainTotalDuration, hResultset.SpikeTrainAvgDuration);
                    end
                end
            catch
            end
            
            com.EEG.Rendering.EEGTracePlot(obj.hAxis, hDataSet, hAnalysis, plotIdx);
        end
        
        function RenderFrameIndicator(~)
        end
    end
end

