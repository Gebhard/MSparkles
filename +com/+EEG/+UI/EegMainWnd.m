%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegMainWnd < com.common.UI.ZoomableAppFigure
    %EEGMAINWND Summary of this class goes here
    %   Detailed explanation goes here                  
    
    properties(Access=private)
        hDatasetChangedListener
        SignalTab;
        FrequencyTab;
        ProportionTab;
        
        hPlotSignals;
        hPlotSigBands;
        hPlotSigProportions;
        hChannelList;
        
        lblFileName;
        lblFileDate;
        lblDuration;
        lblSampleRate;
        lblComments;
        
        lblTotalSpikes;
        lblTotalSpikeTrains;   
        
        hAnalysisListener;
    end
    
    methods(Static)
        function value = Init(hAppWindow, hDataset)
            try
                hMSparkles = MSparklesSingleton.GetInstance();

                %Delete old instance, it it exists
                if ~isempty(hMSparkles.hEegMainWnd)
                    if hMSparkles.hEegMainWnd.hDataset ~= hDataset
                        if isvalid(hMSparkles.hEegMainWnd)
                            hMSparkles.hEegMainWnd.delete();
                        end
                        hMSparkles.hEegMainWnd = [];
                    end                    
                end
                
                if isempty(hMSparkles.hEegMainWnd)
                    hMSparkles.hEegMainWnd = com.EEG.UI.EegMainWnd(hAppWindow, hDataset, 'EEG analysis');
                else
                    hMSparkles.hEegMainWnd.Update();
                end

                value = hMSparkles.hEegMainWnd;
            catch
                value = [];
            end
        end
    end
    
    methods(Access = protected)
        function obj = EegMainWnd(hAppWindow, hDataset, appName)
            %EEGMAINWND Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.UI.ZoomableAppFigure(hAppWindow, hDataset, appName);
            
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                obj.hAnalysisListener = addlistener(hProjMan, 'CurrentEegAnalysis', 'PostSet', @obj.OnAnalysisChanged);
            end
        end 
    end
    
    methods
        function delete(obj)
            obj.DeletePlots(); 
            
            try
                delete (obj.hAnalysisListener);
            catch
            end
            
            try
                delete(obj.hDatasetChangedListener);
            catch
            end
        end
    end    
    
    methods(Access=protected) 
        function OnAnalysisChanged(obj, hSrc, hEvent)
            UpdateEegWnd();
        end
        
        function OnScrollWheel(~, ~, ~)            
        end
        
        function OnMouseMove(~, ~, ~)
        end
         
        function OnKeyDown(obj, src, evnt)
            OnKeyDown@com.common.UI.ZoomableAppFigure(obj, src, evnt);
        end
        
        function OnDeleteFigure(obj, ~, ~)
            OnDeleteFigure@com.common.UI.AppFigure(obj, [],[]);
        end
        
        function CreateFigureLayout(obj, hSrc, ~)
            flexRoot = uix.HBoxFlex( 'Parent', hSrc, 'Tag', 'EegFlexRoot' );
            statsGrid = uix.GridFlex( 'Parent', flexRoot, 'Spacing', 5 );
            tabPanel = uitabgroup( 'Parent', flexRoot, 'Tag', 'EegTabs');

            infoPnl = uix.BoxPanel( 'Parent', statsGrid, 'Title', 'Info' );
            sigPnl  = uix.BoxPanel( 'Parent', statsGrid, 'Title', 'Channel' );
            stastPnl = uix.BoxPanel( 'Parent', statsGrid, 'Title', 'Stats' );    
            set( statsGrid, 'Widths', 200, 'Heights', [196 -1 -1] ); 

            obj.SignalTab       = uitab(tabPanel, 'Title', 'Signals');
            obj.FrequencyTab    = uitab(tabPanel, 'Title', 'Frequency bands');  
            obj.ProportionTab   = uitab(tabPanel, 'Title', 'Wave band proportions');

            infoGrid = uix.Grid('Parent', infoPnl);

            uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', 'File name:');
            uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', 'Record date:');
            uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', 'Duration:');
            uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', 'Samplerate:');
            uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', 'Comments:');    
            obj.lblFileName = uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', '', 'Tag', 'lblFileName');
            obj.lblFileDate = uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', '', 'Tag', 'lblRecDate');
            obj.lblDuration = uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', '', 'Tag', 'lblRecDuration');
            obj.lblSampleRate = uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', '', 'Tag', 'lblSamplerate');
            obj.lblComments = uicontrol('Style', 'Text', 'Parent', infoGrid, 'HorizontalAlignment', 'left', 'String', '', 'Tag', 'lblComments');    
            set( infoGrid, 'Widths', [75 -1], 'Heights', [20 20 20 20 -1] );  

            obj.hChannelList = uicontrol('Style', 'listbox', 'Parent', sigPnl, 'Tag', 'lstSignals', 'Callback', @obj.OnListItemSelected); 

            statsGrid = uix.Grid('Parent', stastPnl);
            uicontrol('Style', 'Text', 'Parent', statsGrid, 'HorizontalAlignment', 'left', 'String', '#Spikes (total):');
            uicontrol('Style', 'Text', 'Parent', statsGrid, 'HorizontalAlignment', 'left', 'String', '#Spike trains (total)');
            obj.lblTotalSpikes = uicontrol('Style', 'Text', 'Parent', statsGrid, 'HorizontalAlignment', 'left', 'String', '#Spikes (total):');
            obj.lblTotalSpikeTrains = uicontrol('Style', 'Text', 'Parent', statsGrid, 'HorizontalAlignment', 'left', 'String', '#Spike trains (total)');
                       
            set( statsGrid, 'Widths', [125 -1], 'Heights', [20 20] );

            set( flexRoot, 'Widths', [200, -1]);
        end                
        
        function OnInitMenu(obj, hTabGroup)
            obj.CreateEEgTab(hTabGroup);
        end
        
        function InitFigure(obj, ~)
            obj.OnDatasetChanged();
        end
        
        function hDataset = GetDefaultAnalysisDataset(~)
            hProjMan = com.Management.MProjectManager.GetInstance();
            hDataset = hProjMan.ActiveEegDatasetNode;
        end
        
        function AppendPipeline(obj, hDataSetNode, finalStage, analyses, forceRecomputeAll)
            arguments
                obj (1,1)
                hDataSetNode (1,1) com.Management.MSparklesTreeNode {mustBeNonempty, mustBeDatasetNode}
                finalStage (1,:) com.Enum.PipelineStage {mustBeNonempty}
                analyses (:,1) {mustBeAnalysisSet}
                forceRecomputeAll (1,1) logical = false
            end

            if ~isempty(hDataSetNode.Dataset)
                ep = com.EEG.Processing.BuildPipeline(hDataSetNode, analyses, finalStage, forceRecomputeAll);
                com.common.Proc.ExecutionQueue.AddExecutionPipeline(ep);
            end
        end
     end        
    
    methods(Access=private)
        function OnListItemSelected(obj, hSrc, ~)
           multiWaitbar('Loading...', 'Busy', 'Color', 'g');
            obj.hPlotSigBands.SignalID = hSrc.Value;
            obj.hPlotSigBands.RenderGraphs(obj.hDataset);            
           multiWaitbar('Loading...', 'close');
        end
        
        function DeletePlots(obj)
            try
                obj.hPlotSignals.delete();
            catch
            end
            
            try
                obj.hPlotSigBands.delete();
            catch
            end   
            
            try
                obj.hPlotSigProportions.delete();
            catch
            end
        end
        
        function OnDatasetChanged(obj, ~, ~)
            
            obj.DeletePlots();
            
            if isempty(obj.hDataset)
                
                obj.SignalTab.Units = 'pixels';
                pos = obj.SignalTab.Position;
                obj.SignalTab.Units = 'normalized';
                
                lblWidth = 100;
                
                obj.hPlotSignals = uicontrol('Style', 'Text', 'Parent', obj.SignalTab,...
                    'String', 'No data available', 'Position', [pos(3)/2 - lblWidth, pos(4)/2, 2*lblWidth, 32]); 
                obj.hPlotSigBands = uicontrol('Style', 'Text', 'Parent', obj.FrequencyTab,...
                    'String', 'No data available', 'Position', [pos(3)/2  - lblWidth, pos(4)/2, 2*lblWidth, 32]);                  
                obj.hPlotSigProportions = uicontrol('Style', 'Text', 'Parent', obj.ProportionTab,...
                    'String', 'No data available', 'Position', [pos(3)/2  - lblWidth, pos(4)/2, 2*lblWidth, 32]);
                
                obj.hPlotSignals.Units = 'normalized';
                obj.hPlotSigBands.Units = 'normalized';
                obj.hPlotSigProportions.Units = 'normalized';
            else
                obj.Update();
            end
        end
        
        function CreateEEgTab(obj, hTabGroup)
            import matlab.ui.internal.toolstrip.*
            icondir = iconrootdir();
            
            hTabEEG = Tab('EEG');
            hTabGroup.add(hTabEEG);  % add to tab as the last section
            hTabEEG.add( CreateDataMgmtButtons() );  % add to tab as the last section
             
            %% ROIs
            hSecAnalysis = Section('Analysis');
            hSecDisplay = Section('Display');
            hSecProperties = Section('Properties');
            
            hTabEEG.add(hSecAnalysis);
            hTabEEG.add(hSecDisplay);            
            hTabEEG.add(hSecProperties);  % add to tab as the last section
            
            hColAnalysis1 = hSecAnalysis.addColumn();
            hColAnalysis2 = hSecAnalysis.addColumn();
            hColAnalysis3 = hSecAnalysis.addColumn();
            hColDisplay1 = hSecDisplay.addColumn();
            hColEEG2 = hSecProperties.addColumn();            
            
            hBtnAddAnalysis = DropDownButton('Add new', fullfile(icondir, 'addanalysis.png'));
            hBtnAddAnalysis.Description = 'Add new analysis to dataset.';            
            hBtnAddAnalysis.DynamicPopupFcn = @(h,e) obj.CreateAddAnalysisPopup(); % invoked when user clicks the drop-down selector widget
            hColAnalysis1.add(hBtnAddAnalysis);
            
            hBtnSetCurrAnalysis = Button('Set active', fullfile(icondir, 'currentanalysis_large.png'));
            hBtnSetCurrAnalysis.Description = 'Set the current analysis and display its data';
            hBtnSetCurrAnalysis.ButtonPushedFcn = @obj.onBtnSetCurrentAnalysis;
            hColAnalysis2.add(hBtnSetCurrAnalysis);
            
            hBtnEegDetect = Button('Analyze', fullfile(icondir, 'analysis_large.png'));
            hBtnEegDetect.Description = 'Edit EEG analysis settings.';
            hBtnEegDetect.ButtonPushedFcn = @obj.onBtnAnalyzeEEG;
            hColAnalysis3.add(hBtnEegDetect);
            
            obj.CreateZoomButtons( hColDisplay1 );                        
            hColEEG2.add( CreatePropertyButton() );
        end       
        
        function onBtnSetCurrentAnalysis(~,~,~)
            hProjMan = com.Management.MProjectManager.GetInstance();                  
            if ~isempty(hProjMan) 
                hProjMan.SetCurrentAnalysisNode(hProjMan.CurrentItem);
            end
        end
        
        function hPopup = CreateAddAnalysisPopup(obj)
            import matlab.ui.internal.toolstrip.* 
            icondir = iconrootdir();
            
            hPopup = PopupList();
 
            % list header #1
            header = PopupListHeader('Add analysis');
            hPopup.add(header);
            
            % list item #1
            item = ListItem('Spike-train detector', fullfile(icondir, 'EEG_detect.png'));
            item.Description = 'Create new spike-train detector.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddSpikeTrainAnalysis;            
            hPopup.add(item);  
            
            item = ListItem('Advanced correlation analysis', fullfile(icondir, 'CaEEG_corr.png'));
            item.Description = 'Create new correlation analysis between Ca2+ and EEG signals.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAddCorrAnalysisEx;            
            hPopup.add(item);  
        end
        
        function AddAnalysis(~, type)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hTargetNode = hProjMan.CurrentItem;
                
                if ~isempty(hTargetNode)
                    if hTargetNode.Type ~= com.Enum.TreeNodeTypes.EEGDataset
                        hTargetNode = hTargetNode.GetEegDataSetNode();
                    end
                else
                    hTargetNode = hProjMan.ActiveEegDatasetNode;
                end
                
                if ~isempty(hTargetNode) && hTargetNode.Type == com.Enum.TreeNodeTypes.EEGDataset
                    hProjMan.AddAnalysis(hTargetNode, type);
                else
                    msgbox('Please select or load a propper dataset.', 'Cannot add analysis', 'warning');
                end
            end
        end
        
        function onBtnAddSpikeTrainAnalysis(obj,~,~)
            obj.AddAnalysis(com.Enum.AnalysisType.EEGSpikeTrainDetection);
        end
        
        function onBtnAddCorrAnalysisEx(obj, ~, ~)
            obj.AddAnalysis(com.Enum.AnalysisType.CaEegCorrelationEx);
        end
        
        function onBtnAnalyzeEEG(obj, ~, ~) 
            obj.run(com.Enum.PipelineStage.Analysis, false);
        end        
    end
    
    methods
        function Update(obj)
            %Update sidebar
            obj.lblFileName.String      = obj.hDataset.DatasetName;
            obj.lblFileDate.String      = obj.hDataset.FileDate;
            obj.lblDuration.String      = sprintf('%.3f sec', obj.hDataset.MetaData.Duration);
            obj.lblSampleRate.String    = sprintf('%.3f Hz', obj.hDataset.MetaData.SampleRate);
            obj.lblComments.String      = obj.hDataset.MetaData.Comment;
            obj.hChannelList.String     = obj.hDataset.ChannelNames;
            
            try
                hAnalysis = obj.hDataset.Analyses.CurrentAnalysis;
                if ~isempty(hAnalysis)
                    obj.lblTotalSpikes.String = sprintf('%d', hAnalysis.TotalSpikeCount);
                    obj.lblTotalSpikeTrains.String = sprintf('%d', hAnalysis.TotalSpikeTrainCount);
                end
            catch
            end
            
            obj.PlotSignals();
            obj.PlotFrequencyBands();
            obj.PlotBandProportionGraphs();
        end
        
        function PlotSignals(obj)
            try
                visChans = obj.hDataset.NumChannels;
                if obj.hDataset.MetaData.TimingChannelIdx > 0
                    visChans = visChans -1;
                end
                
                if obj.hDataset.MetaData.SyncChannelIdx > 0
                    visChans = visChans -1;
                end
                
                if obj.hDataset.MetaData.TimingChannelIdx == 1
                    scrollOffset = 1;
                else
                    scrollOffset = 0;
                end
                
                visChans = max(visChans, 0);
                
                obj.hPlotSignals = com.EEG.UI.EegGraphList(obj, obj.SignalTab, ...
                    'VisibleGraphs', visChans, 'ScrollOffset', scrollOffset);
                
                obj.SignalTab.UserData = obj.hPlotSignals;
                obj.hPlotSignals.RenderGraphs(obj.hDataset);
            catch
            end
        end
        
        function PlotFrequencyBands(obj)
            try
                obj.hPlotSigBands = com.EEG.UI.EegFrequencyGraphList(obj, obj.FrequencyTab, 'VisibleGraphs', 8);
                obj.FrequencyTab.UserData = obj.hPlotSigBands;
                obj.hPlotSigBands.SignalID = 2;
                obj.hPlotSigBands.RenderGraphs(obj.hDataset);
            catch
            end
        end   
        
        function PlotBandProportionGraphs(obj)
%             visChans = obj.hDataset.NumChannels;
%             if obj.hDataset.MetaData.TimingChannelIdx > 0
%                 visChans = visChans -1;
%             end
% 
%             if obj.hDataset.MetaData.SyncChannelIdx > 0
%                 visChans = visChans -1;
%             end
% 
            if obj.hDataset.MetaData.TimingChannelIdx == 1
                scrollOffset = 1;
            else
                scrollOffset = 0;
            end
% 
%             visChans = max(visChans, 0);
                
            obj.hPlotSigProportions = com.EEG.UI.EegProportionGraphList(obj, obj.ProportionTab, ...
                'VisibleGraphs', 8, 'ScrollOffset', scrollOffset);
            
            obj.ProportionTab.UserData = obj.hPlotSigProportions;
            obj.hPlotSigProportions.RenderGraphs(obj.hDataset);
        end
    end
end

