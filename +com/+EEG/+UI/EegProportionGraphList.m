%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegProportionGraphList < com.EEG.UI.EegGraphList
    %EEGPROPORTIONGRAPHLIST Summary of this class goes here
    %   Detailed explanation goes here
    
    methods(Access = protected)
        function OnInitGraphList(obj)
            obj.hGraphList = com.EEG.UI.EegProportionGraphListItem.empty(obj.numVisibleGraphs, 0);
             
            for i=1:obj.numVisibleGraphs
                obj.hGraphList(i) = com.EEG.UI.EegProportionGraphListItem(obj, obj.hParent, i, obj.numVisibleGraphs, @obj.OnItemClicked);                                    
            end     
        end
    end
     
    methods
        function RenderGraphs(obj, hDataset)
            try
                if ~isempty(obj.hSlider) && ~isempty(hDataset)                                      
                    hAnalysis = hDataset.Analyses.CurrentAnalysis;

                    for i=1:obj.numVisibleGraphs
                        graph = obj.hGraphList(i);
                        graph.Render(hDataset, hAnalysis, (obj.ScrollPos-1) + i);                                                
                    end
                end
            catch
            end
        end        
    end
end

