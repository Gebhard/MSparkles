%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef EegProportionGraphListItem < com.EEG.UI.EegGraphListItem
    %EEGTRACEPLOT Summary of this class goes here
    %   Detailed explanation goes here
    
    methods(Access = protected)
        function RenderGraph(obj, hDataSet, hAnalysis, channelIdx)
            try
                disableDefaultInteractivity(obj.hAxis);
                try
                    axtoolbar(obj.hAxis, {'export', 'zoomin', 'zoomout'}); %2020a
                catch
                    try
                        axtoolbar(obj.hAxis, {'zoomin', 'zoomout'}); %2019b
                    catch
                    end
                end
                
                obj.hLabel.String = "Channel " +channelIdx;
                com.EEG.Rendering.EEGProportionPlot(obj.hAxis, hDataSet, hAnalysis, channelIdx);
            catch
            end
        end
    end
end

