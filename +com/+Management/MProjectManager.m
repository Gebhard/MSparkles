%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MProjectManager < com.Management.MProjectTree
    %PROJECTMANAGER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=private) 
        hWindowFigure;
        SelectedRoiIdx;
    end
    
    properties(Access=private, Transient, Dependent)
        DatasetIcon;
        CurrentDatasetIcon;
        AnalysisIcon;
        CurrentAnalysisIcon;
        EegDatasetIcon;
        CurrentEegDatasetIcon;
    end
    
    properties (Constant)
       projectFileExtension = '.msd';
       supportedFileTypes = {'.tif', '.tiff'};
    end
    
    properties(SetAccess=private)
        CurrentItem;
        CurrentAnalysisNode;
        CurrentEegAnalysisNode;
    end
    
    properties(SetAccess=private, SetObservable, Dependent)
        SelectedRoi = -1;
    end
    
    properties(Dependent, SetObservable)
        CurrentAnalysis;
        CurrentEegAnalysis;
    end
    
    properties (SetObservable)
        ActiveDataSetNode; 
        ActiveEegDatasetNode;                        
    end
    
    properties(Dependent)
        CurrentDataset;
        CurrentEegDataset;        
        ActiveAnalysisNode;
    end    
    
    methods   
        function set.SelectedRoi(obj, value)
            obj.SelectedRoiIdx = value;
        end
        
        function value = get.SelectedRoi(obj)
            value = obj.SelectedRoiIdx;
        end
        
        function value = get.hWindowFigure(obj)
            try
                value = ancestor(obj.Parent, 'figure');                
            catch
                value = [];
            end
        end

        function value = get.CurrentItem(obj)
            try
                value = obj.SelectedNodes(1);
            catch
                value = [];
            end
        end       
        
        function value = get.DatasetIcon(~)
            value = fullfile(iconrootdir(), 'dataset.png');
        end
        
        function value = get.CurrentDatasetIcon(~)
            value = fullfile(iconrootdir(), 'currentdataset.png');
        end
        
        function value = get.AnalysisIcon(~)
            value = fullfile(iconrootdir(), 'analysis.png');
        end
        
        function value = get.CurrentAnalysisIcon(~)
            value = fullfile(iconrootdir(), 'currentanalysis.png');
        end   
        
        function value = get.EegDatasetIcon(~)
            value = fullfile(iconrootdir(), 'EEG.png');
        end
        
        function value = get.CurrentEegDatasetIcon(~)
            value = fullfile(iconrootdir(), 'currentEEG.png');
        end
    end    
    
    %% Construction / Destruction
    methods (Static)
        function value = GetInstance(varargin) 
            hMSparkles = MSparklesSingleton.GetInstance();
            value = hMSparkles.ProjectManager;
        end
    end
    
    methods
        function obj = MProjectManager(hParent) 
            
             obj = obj@com.Management.MProjectTree('Parent', hParent,...
                 'Units', 'normalized', 'SelectionType', 'discontiguous');                                                               

            obj.CurrentAnalysis         = [];
            obj.SelectionChangeFcn      = @(s,e)obj.OnSelectionChanged(s,e);
            obj.NodeExpandedCallback    = @(s,e)obj.OnNodeExpanded(s,e);
            obj.MouseDblClickFcn        = @(s,e)obj.OnDblClick(s,e);
        end
        
        function delete(~)
           clear ProjManInstance;
        end 
    end
    
    %% Adding stuff to the tree
    methods (Access=public)
        function node = AddDataset(obj, dataset, parentNode)
            hDataset = [];
            
            if isa(dataset, "com.common.Data.MDatasetRoot")
                hDataset = dataset;
            elseif isfile(dataset) || isfolder(dataset)
                hDataset = obj.loadDataset( dataset );
            else
                com.common.Logging.MLogManager.Error("Unsupported dataset type of class: " + class(dataset) + ...
                    "( " + string(dataset) + " )");
                node = com.Management.MSparklesTreeNode.empty;
            end
            
            if ~isempty(hDataset)
                if (~obj.DoesDatasetExistInTree(hDataset.ProjectFilePath, parentNode))
                    node = obj.AppendTree( hDataset, parentNode);
                end
            end
        end                                                                   
        
        function node = AddEEGDataset(obj, filepath, parentNode)
            %for now...
            if parentNode.Type == com.Enum.TreeNodeTypes.DataSet
                hDataset = parentNode.Dataset;
                
                if ~isempty(hDataset)
                    hDataset.EEGDataset = com.EEG.Data.MEegDataset(filepath);
                    hDataset.Save();
                    
                    %When attaching, delete old EEg node if it exists
                    node = parentNode.FindChildNodeByType(com.Enum.TreeNodeTypes.EEGDataset);                    
                    if ~isempty(node)
                        node.delete();
                    end
                    
                    node = obj.EnsureEEGNodeExists(parentNode, hDataset.EEGDataset);
                    
                    if hDataset.IsLoaded
                        hDataset.EEGDataset.LoadData();
                    end
                end
            end
        end
        
        function UpdateROIsAndSave(obj, hAnalysis, hResult)
            % UPDATEROISANDSAVE Generates roi-nodes for a speciffic analysis of a
            % dataset-node.
                % It ensures that 1) the analysis-node exists, 2)a roi
                % parent-node exists and, 3) the roi parent-node only
                % contains the ROI of the ROIs structure of the given
                % MAnalysis-object.
                %
                % hDatasetNode      TreeNode that represents the entire
                %                   Dataset.
                % hAnalysis         A MAnalysis-object
            
            %Update tree
            if nargin > 3 && ~isempty(hResult)
                obj.AddRoisToTree(hAnalysis, hResult);
            else                      
                for c=hAnalysis.Channels
                    hResult = hAnalysis.GetResultset(c);
                    obj.AddRoisToTree(hAnalysis, hResult);
                end
            end
            %Save MSparklesDataset
            hAnalysis.hDataset.Save();
        end
        
        function UpdateResults(obj, hObject)
            %UPDATERESULTS sets the resultset of the given analysis, updates 
            %the tree and saves the MSparkles dataset file.
                % hDatasetNode  Handle to the parent dataset node
                % hAnalysis     Handle to an MAnalysis object
                % results       The result set                        
            %Update tree
            if nargin == 2 && ~isempty(hObject)
                if isa(hObject, "com.common.Analysis.MResultRoot")
                    obj.EnsureResultsNodeExists(hObject);
                elseif isa(hObject, "com.common.Analysis.MAnalysisRoot")
                    for c=hObject.Channels
                      hResult = hObject.GetResultset(c); 
                      obj.EnsureResultsNodeExists(hResult);
                   end
                else
                    st = dbstack;
                    com.common.Logging.MLogManager.Error("' " + class(hObject) + "' is not supported by " + st.name+ ...
                        ". hObject must be of Type com.common.Analysis.MResultRoot, OR com.common.Analysis.MAnalysisRoot.")
                end
            end       
        end  
                       
        function SelectRoiInCurrentDataset(obj, channelID, roiID)
            rois = obj.GetActiveParentRoiNode(channelID);
                        
            if (rois ~= 0)
                hRoiNode = rois.GetRoiNode(roiID);
                if ~isempty(hRoiNode)
                    obj.SelectedRoiIdx = roiID;                
                    obj.SelectedNodes = hRoiNode; 
                else
                    obj.SelectedRoiIdx = -1;
                    obj.SelectedNodes = [];
                end
            end
        end                
        
        function LoadProjects(obj)           
            multiWaitbar( 'Loading projects...', 'Value', 0, 'Color', 'g');
            obj.Visible = 'off';
            
            projDir = obj.GetProjectDir();
            fullFileList = obj.GetProjectFiles(projDir);
            if ~isempty(fullFileList)
                fileList = erase(fullFileList, projDir);        %remove path to project dir
            else
                fileList = [];
            end
            
            numFiles = numel(fileList);
            
            datasets = cell(numFiles, 1);
            
            for ip=1:numFiles
                try
                    datasets{ip} = obj.loadDataset(fullFileList(ip));
                catch ex
                     com.common.Logging.MLogManager.Error("An error occured while loading project file " + fullFileList(ip));
                    com.common.Logging.MLogManager.Exception(ex);
                end
                multiWaitbar( 'Loading projects...', 'Value', ip/numFiles);
            end
            
            multiWaitbar( 'Loading projects...', 'Relabel', 'Building project tree...');
            
            for ii=1:numFiles
                try
                    currFile = fileList(ii);
                    path = fileparts(currFile);
                    hParentNode = obj.EnsureDirectoryNodeExists(path);
                    obj.AddDataset(datasets{ii}, hParentNode);
                catch ex
                    com.common.Logging.MLogManager.Error("An error occured while loading project file " + path);
                    com.common.Logging.MLogManager.Exception(ex);
                end
                
                 multiWaitbar( 'Building project tree...', 'Value', ii/numFiles);
            end
                                                
            obj.Visible = 'on';   
            multiWaitbar( 'Building project tree...', 'Close');
        end           
        
        function UpdateF0(obj, hNode)
            hDataSetNode = hNode.GetCaDataSetNode();
            
            if ~isempty(hDataSetNode)            
                hDataset = hDataSetNode.Dataset;                                
                if ~isempty(hDataset)   
                    hDataset.SaveF0();
                    obj.AddF0Node(hDataset);  
                end
            end
        end
        
        function CC = AddRoiToCurrentAnalysis(obj, channel, roi)
            hAnalysis = obj.CurrentAnalysis;
            
            if (isa(roi, 'imroi')) && ~isempty(hAnalysis)
                obj.EnsureChildnodesExist(obj.ActiveDataSetNode);  
                CC = hAnalysis.AddROI(channel, roi);
                
                %get ROI parent node
                hChannelNode = obj.EnsureChannelNodeExists(hAnalysis, channel);                
                hRoiParentNode = obj.EnsureRoiParentNodeExists(hChannelNode);
                
                %Append new ROI
                obj.AddRoiNode(hRoiParentNode, CC.NumObjects);
                
                %Save dataset
                obj.CurrentDataset.Save();                
            else
                CC = [];
            end
        end                
        
        function CC = MergeRoiInCurrentAnalysis(obj, channel, roi)
             if (isa(roi, 'imroi'))                                 
                hAnalysis = obj.CurrentAnalysis;                
                if ( ~isempty(hAnalysis)) 
                    hResult = hAnalysis.GetResultset(channel);
                    CC = hResult.MergeROI(roi);
                    obj.UpdateROIsAndSave(hAnalysis, hResult);             
                end
             end
        end                
        
        function CC = CutRoiInCurrentAnalysis(obj, channel, roi)
            if (isa(roi, 'imroi'))
                hAnalysis = obj.CurrentAnalysis;                
                if ( ~isempty(hAnalysis))
                    hResult = hAnalysis.GetResultset(channel);
                    CC = hResult.CutROI(roi);
                    obj.UpdateROIsAndSave(hAnalysis, hResult);                
                end
            end
        end                  
        
        function doesExist = DoesDatasetExistInTree(obj, filePath, parentNode)
            doesExist = obj.DatasetExists(filePath, parentNode);
            if doesExist
                msgbox('This file already exists at this hierarchy level.', 'Info', 'warn')
            end
        end        
        
        function AddAnnotationToCurrentDataset(obj, hAnnotation)
            if ~isempty(hAnnotation) && ~isempty(obj.ActiveDataSetNode)
                hAnnotationsRootNode = obj.EnsureAnnotationsRootNodeExists(obj.ActiveDataSetNode);
                
                if ~isempty(hAnnotationsRootNode)
                    obj.AddAnnotationNode(hAnnotationsRootNode, hAnnotation);
                end
            end
        end
        
        function AddAnalyses(obj, hDataset)
            if ~isempty(hDataset)   
                numAnalyses = hDataset.Analyses.Count;
                
                %Ensure AnalysisRootNode is Empty;
                hDatasetNode = hDataset.TreeNode;
                obj.EnsureChildnodesExist(hDatasetNode);
                hAnalysisRootNode = obj.EnsureAnalysisRootNodeExists(hDatasetNode);
                hAnalysisRootNode.RemoveAllChildren();
                
                try
                    listAnalyses = hDataset.Analyses.ValuesSorted;
                catch
                    listAnalyses = hDataset.Analyses.Values;
                end
                
                for i=1:numAnalyses
                    hAnalysis = listAnalyses{i};        
                    
                    if ~isempty(hAnalysis)
                        obj.EnsureAnalysisNodeExists(hAnalysis); %Ensure analysis node exists

                        %Iterate over resultsets                    
                        for j=1:numel(hAnalysis.AnalysisResults)
                            hResult = hAnalysis.AnalysisResults(j);

                            %Add Results-node
                            if ~isempty(hResult.Results)
                                obj.EnsureResultsNodeExists(hResult);
                            end

                            if hDataset.Type == com.Enum.DatasetType.CalciumData
                                %AddRois
                                if ~isempty(hResult.ROIs)
                                    obj.AddRoisToTree(hAnalysis, hResult);
                                end
                            end
                        end
                    end
                end                
            end
        end    
        
        function AddAnnotations(obj, hDatasetNode)
            hAnnotationsRootNode = obj.EnsureAnnotationsRootNodeExists(hDatasetNode);
            
            if ~isempty(hAnnotationsRootNode)
                hDataset = hDatasetNode.Dataset;
            
                if ~isempty(hDataset) 
                    annotations = hDataset.Annotations.Values;
                    
                    for i=1:hDataset.Annotations.Count
                        obj.AddAnnotationNode(hAnnotationsRootNode, annotations{i});
                    end
                end
            end
        end
        
        function DeleteAnalysis(obj, hDatasetNode, analysisID)
            hNode = hDatasetNode.GetAnalysisNode( analysisID );            
            obj.DeleteAnalysisNode(hDatasetNode.Dataset, hNode);             
        end
        
        function UpdateAnalysisNode(~, hAnalysis)
            if nargin > 1 && ~isempty(hAnalysis)
                hAnalysis.TreeNode.Name = hAnalysis.Name;
            end
        end
    end           
    
    methods(Access=private)
        
        function hDataset = loadDataset(obj, filePath)      
            if isfile(filePath) || isfolder(filePath)
                [fDir, ~, fExt] = fileparts(filePath);

                if ( strcmpi(fExt, com.Management.MProjectManager.projectFileExtension) )
                    lastwarn('');    % clear last warning message

                    %%it is already a project file
                    msDataSet =  load(filePath, "-mat");
                    [warnMsg, ~] = lastwarn;

                    if isa(msDataSet.project, "MSparklesDataSet")
                        hDataset = com.CalciumAnalysis.Data.MCaDataset.Convert(msDataSet.project);
                        if isempty(warnMsg)
                            warnMsg = "Converted";
                        end
                    else
                        hDataset = msDataSet.project;
                    end

                   % hDataSet.Version = 1.861;
                    upgraded = hDataset.Upgrade();
                    hDataset.ProjectFilePath = filePath;

                    % If there is a warning during loading, or, there
                    % was an upgrade, save the project again in its 
                    % new, converted state.                                                
                    if ~isempty(warnMsg) || upgraded == true
                        hDataset.Save();
                    end                        
                else                    
                    if ( any(strcmpi(fExt, com.Management.MProjectManager.supportedFileTypes)) ) || ...
                            isempty(fExt) && isfolder(filePath)
                        if ~isempty(obj.SelectedNodes)
                            hNode = obj.FindParentFolder(obj.SelectedNodes(1));
                        else
                            hNode = obj.Root;
                        end

                        hDataset = obj.CreateProjectFile(filePath, hNode); 
                    end
                end 
            end
        end
        
        function fileList = GetProjectFiles(obj, path)
            dirList = dir(path);
            numDirs = numel(dirList);
            
            fileList = [];
            
            for dd = 1:numDirs
                currDir = dirList(dd);
                
                if ~ismember(currDir.name, [".", "..", "Logs"])
                    fullPath = string(fullfile(currDir.folder, currDir.name));
                    
                    if (currDir.isdir)
                        fileList = cat(1, fileList, obj.GetProjectFiles(fullPath));
                    else
                        fileList = cat(1, fileList, fullPath);
                    end
                end
            end
        end
        
        %% Secure node access     
        function hNode = EnsureAnnotationsRootNodeExists(obj, hDatasetNode)
            % ENSUREANNOTATIONSROOTNODEEXISTS Returns the parent node of all
            % analysis nodes, belonging to the same dataset.
                % See also: GetAnalysisNode
                %
                %  hDatasetNode     TreeNode that represents the entire
                %                   Dataset.    
            
            hNode = hDatasetNode.GetAnnotationsRootNode();
            
            if isempty(hNode)
                hNode = obj.CreateChildNode(hDatasetNode, 'Annotations',...
                    com.Enum.TreeNodeTypes.AnnotationGroup, []);
            end         
        end
        
        function hNode = EnsureAnnotationNodeExists(obj, hDatasetNode, hAnnotation)
            %ENSUREANNOTATIONNODEEXISTS Returns a valid TreeNode object 
                %  GetAnalysisNode searches the children of the Analysis 
                %  root-node of hDatasetNode if an Analysis with the 
                %  given analysisID exists and returns it. If no node
                %  exists, it creates a new node and returns it.
                %  
                %  hDatasetNode     TreeNode that represents the entire
                %                   Dataset.
                %  analysisID       A GUID that uniquely identifies an
                %                   MAnalysis object of the dataset.
                %
                %  Each AnalysisNode represents one MAnalysis-object of a 
                %  MSparklesDataset 
                
            hAnnotationsRootNode = obj.EnsureAnnotationsRootNodeExists(hDatasetNode);            
            hNode = hAnnotationsRootNode.GetAnnotationNode( hAnnotation.ID );
            
            if isempty(hNode)
               obj.AddAnnotationNode(hAnnotationsRootNode, hAnnotation);
            end
        end
        
        function hNode = EnsureAnalysisRootNodeExists(obj, hDatasetNode)
            % ENSUREANALYSISROOTNODEEXISTS Returns the parent node of all
            % analysis nodes, belonging to the same dataset.
                % See also: GetAnalysisNode
                %
                %  hDatasetNode     TreeNode that represents the entire
                %                   Dataset.    
            
            hNode = hDatasetNode.GetAnalysisRootNode();
            
            if ~isempty(hNode) && hNode.Parent ~= hDatasetNode
                hNode = [];
            end
            
            if isempty(hNode)
                hNode = obj.CreateChildNode(hDatasetNode, 'Analysis',...
                    com.Enum.TreeNodeTypes.Analysis, []);
            end            
        end
        
        function hNode = EnsureAnalysisNodeExists(obj, hAnalysis)
            %ENSUREANALYSISNODEEXISTS Returns a valid TreeNode object 
                %  GetAnalysisNode searches the children of the Analysis 
                %  root-node of hDatasetNode if an Analysis with the 
                %  given analysisID exists and returns it. If no node
                %  exists, it creates a new node and returns it.
                %  
                %  hDatasetNode     TreeNode that represents the entire
                %                   Dataset.
                %  analysisID       A GUID that uniquely identifies an
                %                   MAnalysis object of the dataset.
                %
                %  Each AnalysisNode represents one MAnalysis-object of a 
                %  MSparklesDataset 
                
            hDatasetNode = hAnalysis.hDataset.TreeNode;
            hAnalysisRootNode = obj.EnsureAnalysisRootNodeExists(hDatasetNode);            
            hNode = hAnalysisRootNode.GetAnalysisNode( hAnalysis.ID );
            
            if isempty(hNode)
                hNode = obj.CreateChildNode( hAnalysisRootNode, hAnalysis.Name,...
                    com.Enum.TreeNodeTypes.AnalysisMode, hAnalysis.ID);
                hAnalysis.TreeNode = hNode;
            end
        end
        
        function hNode = EnsureRoiParentNodeExists(obj, hChannelNode, deleteIfExists)
            % ENSUREROIPARENTNODEEXISTS returns the parent node of all
            %   ROI-Nodes belonging to the same channel of an analysis.
            %   If deleteIfExists is true, then an existing ROI parent-node
            %   will be deleted and replaced by a new node.
            
            hNode = hChannelNode.GetRoiParentNode();
            
            if nargin == 3 && deleteIfExists && ~isempty(hNode)
                hNode.delete();
                hNode = [];
            end
            
            if isempty(hNode)
                hNode = obj.CreateChildNode( hChannelNode, 'ROIs',...
                   com.Enum.TreeNodeTypes.ROIGroup, []);
            end
       end
        
        function hNode = EnsureChannelNodeExists(obj, hAnalysis, channel)
            if isa(channel, 'com.common.Analysis.MResultRoot')
                channelID = channel.ChannelID;
            else
                channelID = channel;
            end
            
            hAnalysisNode = obj.EnsureAnalysisNodeExists(hAnalysis);
            
            if ~isempty(hAnalysisNode.Children)
                idx = arrayfun(@(x) (x.Value == com.Enum.TreeNodeTypes.Channel && ...
                    x.UserData == channelID), hAnalysisNode.Children);
            else
                idx = 0;
            end
            
            if any(idx)
                hNode = hAnalysisNode.Children(idx);
            else
                hDatasetNode = hAnalysisNode.GetDataSetNode();
                hDataset = hDatasetNode.Dataset;
                
                try
                    channelName = hDataset.MetaData.GetChannelName(channelID);
                catch
                    channelName = [];
                end
                
                if isempty(channelName)
                    channelName = sprintf('Channel %d', channelID);
                end
                
                hNode = obj.CreateChildNode( hAnalysisNode, channelName, ...
                    com.Enum.TreeNodeTypes.Channel, channelID);
            end
        end
        
        function hNode = EnsureResultsNodeExists(obj, hResult)
            if ~isempty (hResult)
                hAnalysis = hResult.hParent;
                
                hChannelNode = obj.EnsureChannelNodeExists(hAnalysis, hResult);
                hNode = hChannelNode.GetResultsNode();               

                if isempty( hNode )
                     hNode = obj.CreateChildNode( hChannelNode, 'Results',...
                       com.Enum.TreeNodeTypes.Results, hAnalysis.ID);
                end
            end
        end
        
        function hNode = EnsureEEGNodeExists(obj, hParentNode, hEegDataset)
            if ~isempty(hEegDataset)
                hNode = hParentNode.GetEegDataSetNode();
                
                if ~isempty(hNode)
                    hNode.delete();
                    hNode = [];
                end
                
                if isempty(hNode)
                    hNode = obj.AddEegNode(hParentNode, hEegDataset);
                end
            end
        end
        
        function hNode = EnsureDirectoryNodeExists(obj, path)
            %ENSUREDIRECTORYNODEEXISTS Always return a handle to a valid MSparklesTreeNode object.
                %path   Path of directory node, relative to root node
                %       If path is empty, the root node is returned.
                %       Path separators are treated as indicator for sub
                %       folder in tree structure.
                %
                %hNode  Handle to the respective node in the tree
                
                parts = split(path, filesep);
                parts(parts=="") = []; %remove empty strings
                numParts = numel(parts);
                hNode = obj.Root;
                
                for ii=1:numParts
                    currPath = parts(ii);
                    hTmpNode = hNode.FindDirectChildNodeByName(currPath);
                    
                    if isempty(hTmpNode)
                        hTmpNode = obj.AddDirectoryNode(currPath, hNode); 
                    end
                    
                    hNode = hTmpNode;
                end
        end
        
        function hNode = GetClickedTreeNode(~, hClickData)
            if isa(hClickData, 'matlab.ui.eventdata.ActionData')
                hNode = FindNode(hClickData.Source);
            elseif isstruct(hClickData) && ~isempty(hClickData.Nodes)
                hNode = hClickData.Nodes;
            else
                hNode = [];
            end
            
            function hNode = FindNode(srcObj)
                if isempty(srcObj)
                    hNode = [];
                elseif isa(srcObj, 'com.Management.MSparklesTreeNode')
                    hNode = srcObj;                
                else
                    try
                        if isa(srcObj.UserData, 'com.Management.MSparklesTreeNode')
                            hNode = srcObj.UserData; 
                        else
                            hNode = FindNode(srcObj.Parent);
                        end
                    catch
                        hNode = FindNode(srcObj.Parent);
                    end
                end
            end
        end
        
        function UpdateRoiNames(obj, hAnalysis, hResultset)
            hChannelNode = obj.EnsureChannelNodeExists(hAnalysis, hResultset);
            hRoiParentNode = obj.EnsureRoiParentNodeExists(hChannelNode, false);
            
            try
                if ( ~isempty(hRoiParentNode) && ~isempty(hResultset.ROIs) )

                    for i=1:hResultset.ROIs.NumObjects
                        hRoiNode = hRoiParentNode.Children(i);
                        hRoiNode.Name = sprintf('ROI %d', i);
                        hRoiNode.UserData = i;
                    end
                    
                    %Delete ROI nodes, if the ROIs where e.g. auto-deleted
                    %by the analysis
                    numNodes = numel(hRoiParentNode.Children);
                    for i=hResultset.ROIs.NumObjects+1:numNodes
                        hRoiNode = hRoiParentNode.Children(i);
                        hRoiNode.Delete();
                    end
                end  
            catch
            end
        end
        
        function AddRoisToTree(obj, hAnalysis, hResultset)
            hChannelNode = obj.EnsureChannelNodeExists(hAnalysis, hResultset);
            hRoiParentNode = obj.EnsureRoiParentNodeExists(hChannelNode, true);                

            try
                if ( ~isempty(hRoiParentNode) && ~isempty(hResultset.ROIs) )
                    for i=1:hResultset.ROIs.NumObjects
                        obj.AddRoiNode(hRoiParentNode, i);
                    end
                end  
            catch
            end
        end
        
        function hNode = AddAnnotationNode(obj, hAnnotationsRootNode, hAnnotation)
            if ~isempty(hAnnotation)
                hNode = obj.CreateChildNode( hAnnotationsRootNode, hAnnotation.Name,...
                    com.Enum.TreeNodeTypes.Annotation, hAnnotation.ID);
            end
        end
        
        function node = AddRoiNode(obj, hRoiParentNode, roiID)
            node = obj.CreateChildNode( hRoiParentNode, sprintf('ROI %d', roiID),...
                com.Enum.TreeNodeTypes.ROI, roiID);            
        end                
        
        function node = AddDataSetNode(~, hParentNode, hDataset)
            node = com.Management.MProjectTree.createTreeNode(hDataset.DatasetName, hParentNode,...
                com.Enum.TreeNodeTypes.DataSet);            
            node.Dataset = hDataset;
        end    
        
        function node = AddEegNode(~, hParentNode, hDataset)
            node = com.Management.MProjectTree.createTreeNode(hDataset.DatasetName, hParentNode,...
                com.Enum.TreeNodeTypes.EEGDataset);            
            node.Dataset = hDataset;
        end
        
        function node = CreateChildNode(~, parentNode, childNodeName, nodeType, childNodeUsrData)
            node = com.Management.MProjectTree.createTreeNode(childNodeName, parentNode, nodeType);            
            node.UserData = childNodeUsrData;
        end                                                       
        
        function node = AppendTree(obj, hDataset, hParentNode)
            %%Ensure we have a project file
            if ( isa(hDataset, 'com.common.Data.MDatasetRoot') ) 
                
                if ~isempty(hDataset.OrgDatasetPath) && hDataset.OrgDatasetPath ~= ""
                   node = obj.AddDataSetNode(hParentNode, hDataset);                   
                   obj.CreateChildNode(node, 'Loading...', com.Enum.TreeNodeTypes.Loading, []);
                   node.TooltipString = hDataset.GetToolTip();
                else
                    node = [];
                end 
            else
                node = [];
            end   
        end 
        
        function FillDatasetnode(obj, hDatasetNode)
            if ~isempty(hDatasetNode)
                           
                hDataset = hDatasetNode.Dataset;
                
                switch hDatasetNode.Value
                    case com.Enum.TreeNodeTypes.DataSet
                        if ~isempty(hDataset.F0Path)
                            obj.AddF0Node(hDataset); 
                        end
                        
                        if ~isempty(hDataset.EEGDataset)
                            hEegNode = obj.EnsureEEGNodeExists(hDatasetNode, hDataset.EEGDataset);
                            obj.FillDatasetnode(hEegNode);
                        end
                        
                        obj.AddAnnotations(hDatasetNode);
                        obj.AddAnalyses(hDataset);
                        
                    case com.Enum.TreeNodeTypes.EEGDataset
                        obj.AddAnalyses(hDataset);
                end
            end
        end
        
        function node = AddDirectoryNode(obj, dirName, parentNode)
            [~, fName, ~] = fileparts(dirName);
            node = obj.CreateChildNode(parentNode, fName, com.Enum.TreeNodeTypes.Folder, dirName);
            node.TooltipString = dirName;
        end    
        
        function AddF0Node(obj, hDataset)
            %Ensure only one F0 node exists
            hF0Node = hDataset.TreeNode.GetF0Node();
            
            if ~isempty(hF0Node)
                hF0Node.delete()
            end            
            
            obj.CreateChildNode( hDataset.TreeNode, 'F0', com.Enum.TreeNodeTypes.F0, hDataset.F0Path);                                      
        end               
        
        function DeleteRoiNode(obj, hDataset, hNode)
            if ~isempty(hDataset) %Do we have a dataset open?                
                hChannelNode = hNode(1).GetChannelNode();                
                hAnalysisNode = hChannelNode.GetAnalysisNode();

                channel = hChannelNode.UserData;
                hAnalysis = hDataset.Analyses.GetAnalysis(hAnalysisNode.UserData);
                roiIDs = arrayfun(@(x) x.UserData, hNode);
                delete(hNode);
                
                obj.DeleteROI(hAnalysis, channel, roiIDs);
                
                obj.ActiveAnalysisNode = hAnalysisNode;
            else
                warndlg('Please load a dataset before deleting ROIs.', 'Info', 'warn')
            end                  
        end
        
        function DeleteDatasetNode(obj, hDataset)
            fName = hDataset.TreeNode.Name;
            filePath = fullfile(obj.GetAbsolutePath(obj.CurrentItem), ...
                sprintf('%s%s', fName, obj.projectFileExtension));

            delete(filePath);
            hDataset.TreeNode.delete();                
        end
        
        function DeleteAnalysisRootNode(~, hDataset, hNode)
            hAnalysisRootNode = hNode.GetAnalysisRootNode();                
            hDataset.Analyses.DeleteAll();
            hAnalysisRootNode.delete();
            hDataset.Save();     
        end
        
        function DeleteAnalysisNode(obj, hDataset, hNode) 
            if hNode == obj.ActiveAnalysisNode
                obj.ActiveAnalysisNode = [];
            end

            hDataset.Analyses.DeleteAnalysis(hNode.UserData);
            hNode.delete();
            hDataset.Save();                    
        end                
        
        function DeleteAnnotation(~, hDataset, hNode)
            if hNode.Type == com.Enum.TreeNodeTypes.Annotation
                if ~isempty(hDataset)
                    %hDataset.Annotations.DeleteAnnotation will call DeleteAnnotationNode
                    hDataset.Annotations.DeleteAnnotation(hNode.UserData);
                end
            end      
        end
        
        function DeleteEEGNode(~, hDataset, hNode)
            if hNode.Type == com.Enum.TreeNodeTypes.EEGDataset
                if ~isempty(hDataset)
                    if isa(hDataset, "com.CalciumAnalysis.Data.MCaDataset")
                        hDataset.EEGDataset = [];
                    elseif isa(hDataset, "com.EEG.Data.MEegDataset")
                        hDataset.delete();
                    end
                end
                
                hNode.delete();
            end
        end
    end
    
    methods  
        function hNode = GetCurrentTreeNode(obj, fallback)
            arguments
                obj (1,1)com.Management.MProjectManager;
                fallback = [];
            end
            
            hNode = obj.CurrentItem;
            if isempty(hNode)
                if isempty(fallback)
                    fallback = "dataset";
                end

                switch(fallback)
                    case "analysis"
                        hNode = obj.ActiveAnalysisNode;
                    otherwise
                        hNode = obj.ActiveDataSetNode;
                end                    
            end
        end

        function ExportPdfReport(~, hTreeNode)
        %EXPORTPDFREPORT Summary of this function goes here
        %   Detailed explanation goes here

            if hTreeNode.Type == com.Enum.TreeNodeTypes.AnalysisMode
                hAnalysisNode = hTreeNode.GetAnalysisNode();            
                hDatasetNode = hAnalysisNode.GetCaDataSetNode();                            
                hDataset = hDatasetNode.Dataset;            

                if (~isempty(hDataset))
                     hAnalysis = hDataset.Analyses.GetAnalysis(hAnalysisNode.UserData);

                     if ~isempty(hAnalysis)
                        hAnalysis.ExportPdfReport(true);
                     end
                end
            else
                msgbox('Please select an analysis for PDF export.', 'Info', 'warn');
            end
        end

        function DeleteF0Node(~, hDataset, hNode)
            %Ensure we have a F0 node
            if hNode.Type == com.Enum.TreeNodeTypes.F0
                hF0Node = hNode;
            else
                if hNode == com.Enum.TreeNodeTypes.DataSet
                    hF0Node = hNode.GetF0Node();
                else
                    hDatasetNode = hNode.GetDataSetNode();
                    hF0Node = hDatasetNode.GetF0Node();
                end
            end
            
            if ~isempty(hF0Node)
                hDataset.DeleteF0();
                hF0Node.delete(); 
            end
            
            UpdateCaWnd("All");
        end
        
        function DeleteRoiParentNodes(obj, hDataset, hAnalysis)
            %Delete all ROI-parent nodes of hAnalysis
            if ~isempty(hDataset) && ~isempty(hDataset.TreeNode) && ~isempty(hAnalysis)
                hAnalysisRoot = hDataset.TreeNode.GetAnalysisRootNode();
                if ~isempty(hAnalysisRoot)
                    hAnalysisNode = hAnalysisRoot.GetAnalysisNode(hAnalysis.ID);
                    if ~isempty(hAnalysisNode)
                        for c=hAnalysis.Channels
                            hChannelNode = hAnalysisNode.GetChannelNode(c);
                            if ~isempty(hChannelNode)
                                hRoiParentNode = hChannelNode.GetRoiParentNode();
                                if ~isempty(hRoiParentNode)
                                    hRoiParentNode.delete();
                                end
                            end
                        end
                    end
                end
                
                if hAnalysis == obj.CurrentAnalysis
                    UpdateCaWnd("StackScroller");
                end
            end
        end
        
        function DeleteResultNodes(obj, hDataset, hAnalysis)
            %Delete all result nodes of hAnalysis
            if ~isempty(hDataset) && ~isempty(hDataset.TreeNode) && ~isempty(hAnalysis)                
                hAnalysisRoot = hDataset.TreeNode.GetAnalysisRootNode();                
                if ~isempty(hAnalysisRoot)
                    hAnalysisNode = hAnalysisRoot.GetAnalysisNode(hAnalysis.ID);
                    if ~isempty(hAnalysisNode)
                        for c=hAnalysis.Channels
                            hChannelNode = hAnalysisNode.GetChannelNode(c);
                            if ~isempty(hChannelNode)
                                hResultsNode = hChannelNode.GetResultsNode();
                                if ~isempty(hResultsNode)
                                    hResultsNode.delete();
                                end
                            end
                        end
                    end
                end
                
                if hAnalysis == obj.CurrentAnalysis
                    UpdateCaWnd(["GraphList", "DataExplorer"]);
                end
            end
         end
        
        function hDataset = DeleteAnnotationNode(obj, hAnnotation)
            hDatasetNode = obj.ActiveDataSetNode;
            
            if isempty(hDatasetNode)
                hNode = obj.SelectedNodes(1);
                if hNode.Type == com.Enum.TreeNodeTypes.Annotation
                    hDatasetNode = hNode.GetDataSetNode();
                end
            end
            
            if ~isempty(hDatasetNode)
                hAnnotationsRootNode = obj.EnsureAnnotationsRootNodeExists(hDatasetNode);            
                hNode = hAnnotationsRootNode.GetAnnotationNode( hAnnotation.ID ); 
                hDataset = hDatasetNode.Dataset;
            end
            
            if ~isempty(hNode)
                hNode.delete();
            end
        end
        
        function DeleteNode(obj, hNode)
        %DELETENODE Summary of this function goes here
        %   Detailed explanation goes here
            try
                if nargin > 1 == ~isempty(hNode)
                    hDatasetNode = hNode(1).GetDataSetNode();
                    hDataset = hDatasetNode.Dataset;

                    switch(hNode(1).Type)
                        case com.Enum.TreeNodeTypes.ROI
                            obj.DeleteRoiNode(hDataset, hNode);
                        case com.Enum.TreeNodeTypes.DataSet
                            obj.DeleteDatasetNode(hDataset);
                        case com.Enum.TreeNodeTypes.Analysis
                            obj.DeleteAnalysisRootNode(hDataset, hNode);
                        case com.Enum.TreeNodeTypes.AnalysisMode
                            obj.DeleteAnalysisNode(hDataset, hNode);
                        case com.Enum.TreeNodeTypes.F0
                            obj.DeleteF0Node(hDataset, hNode);
                        case com.Enum.TreeNodeTypes.Annotation
                            obj.DeleteAnnotation(hDataset, hNode);
                        case com.Enum.TreeNodeTypes.EEGDataset
                            obj.DeleteEEGNode(hDataset, hNode);
                    end
                end
            catch
                hNode.delete();
                com.common.Logging.MLogManager.Error(sprintf('Failed to delete object of type %s.', hNode.Type));
            end
        end
        
        function CreateNewGroup(obj, hParentNode)
            try
                answer = inputdlg('Name of new Group', 'Create project group', 1);
                if (~isempty(answer))
                                                            
                    absPath = obj.GetAbsolutePath(hParentNode);
                    newDirName = fullfile(absPath, answer{:});
                    
                    mkdir(newDirName);
                    
                    if isempty(hParentNode)
                        obj.AddDirectoryNode(newDirName, obj.Root); 
                    else
                        obj.AddDirectoryNode(newDirName, hParentNode);                    
                    end                                       
                end
            catch ex
                disp(ex.message);
                obj.CreateNewGroup(hParentNode);
            end
        end
    end
    
    methods(Access=private)
        function hRoiParentNode = GetActiveParentRoiNode(obj, channelID)
            %%Returns the parent ROI node of the active analysis mode in
            %%the active Dataset      
            
            if ~isempty(obj.CurrentAnalysis)
                obj.EnsureChildnodesExist(obj.ActiveDataSetNode); 
                hAnalysisNode = obj.ActiveAnalysisNode;
                hChannelNode = hAnalysisNode.GetChannelNode(channelID);
                if ~isempty(hChannelNode)
                    hRoiParentNode = hChannelNode.GetRoiParentNode();                
                else
                    hRoiParentNode = [];
                end
            else
                hRoiParentNode = [];
            end
        end
        
        function bResult = DatasetExists(~, filePath, hParentNode)
            [~, fName, ~] = fileparts(filePath);            
            hNode = hParentNode.GetChildDatasetByName(fName);              
            bResult = ~isempty(hNode);
        end                                             
        
        function projectDir = GetProjectDir(~)
            % Get Project directory and ensure it axists
            settings = MGlobalSettings.GetInstance();
            projectDir = settings.ProjectDir;           
                
            if (~isfolder(projectDir))
               mkdir(projectDir); 
            end                      
        end                  
        
        function path = GetAbsolutePath(obj, hObject)
           %import uiextras.jTree.*;
           %hObject can either be a TreeNode or a MSparklesProject object.
           % 1) If it is a TreeNode, we need to find its parent project-nonde
           %    and move up the three to the root node to retrieve the full path
           %    FullPath = ProjectDir/Child1/.../Childn/Project-TreeNode
           %
           % 2) If hObject is a MSparklesProject object, we first neet to find the 
           %    corresponding TreeNode, referencing it as UserData, the we proceed
           %    like in the first case.           
           
            if (isempty(hObject)) %%User clicked in empty tree space, return project dir
                path = obj.GetProjectDir();
            else
                if (isa(hObject, 'com.Management.MSparklesTreeNode'))
                    if (hObject.Value == com.Enum.TreeNodeTypes.Folder)
                        if ( ~isempty(hObject.Parent))
                            path = fullfile(obj.GetAbsolutePath(hObject.Parent), hObject.Name);
                        else
                            path = obj.GetProjectDir();
                        end
                    else
                        path = obj.GetAbsolutePath(hObject.Parent);
                    end
                end
            end

        end                                                      
        
        function hDataset = CreateProjectFile(obj, sourceFilePath, parentNode)
            hDataset = com.CalciumAnalysis.Data.MCaDataset(sourceFilePath);            
            hDataset.ProjectFilePath = obj.GetProjectFilePath(parentNode, sourceFilePath); 
            hDataset.Save();
        end                      
        
        function projFilePath = GetProjectFilePath(obj, parentNode, datasetPath)
            [~, fName, ~] = fileparts(datasetPath);
            projFilePath = fullfile(obj.GetAbsolutePath(parentNode), sprintf('%s%s', fName, obj.projectFileExtension));
        end      
        
        function OpenFileBrowser(~, path)
            %ToDo: Test Linux case
            if (ispc)
                if isfile(path)
                    cmdString = sprintf('explorer.exe /select,%s', path);            
                else
                    cmdString = sprintf('explorer.exe %s', path);            
                end
             elseif ismac
                cmdString = sprintf('open "%s"', path);
            elseif isunix
                cmdString = sprintf('nautilus "%s"', path);
            end
            
            if (cmdString ~= 0)
                system (cmdString);             
            end
        end
    end    
    
    %% Tree callbacks
    methods (Access=private)
                
        function OnSelectionChanged(obj, ~, hObject)  
            %%Is a ROI node in the tree selected?            
            hNode = obj.GetClickedTreeNode(hObject);
            
            if (~isempty(hNode))  
                result = arrayfun( @(x) x.Type == com.Enum.TreeNodeTypes.ROI, hNode);
                if any(result == 1)
                    dsNode = hNode(1).GetDataSetNode();
                    if (dsNode == obj.ActiveDataSetNode)
                        %get analysis of clicked object
                        hAnalysisNode = hNode(1).GetAnalysisNode();

                        if ~isempty(hAnalysisNode)                                
                            if ~isempty(obj.CurrentDataset)
                                if ~isempty(obj.CurrentDataset)
%                                 
                                    hAnalysis = obj.CurrentDataset.Analyses.GetAnalysis(hAnalysisNode.UserData);

                                    %Check if it's the active analysis
                                    if obj.CurrentAnalysis == hAnalysis   
                                        obj.SelectedRoi = arrayfun( @(x) x.UserData, hNode(result));                                    
                                    end
                                end
                            end
                        end 
                    end
                end
%                 switch (hNode.Type)
%                     case com.Enum.TreeNodeTypes.ROI
%                         %%Check if selected treenode is in the active analysis
%                         
%                         dsNode = hNode.GetDataSetNode();
%                         if (dsNode == obj.ActiveDataSetNode)
%                             %get analysis of clicked object
%                             hAnalysisNode = hNode.GetAnalysisNode();
%                             
%                             if ~isempty(hAnalysisNode)                                
%                                 if ~isempty(obj.CurrentDataset)
%                                 
%                                     hAnalysis = obj.CurrentDataset.Analyses.GetAnalysis(hAnalysisNode.UserData);
% 
%                                     %Check if it's the active analysis
%                                     if obj.CurrentAnalysis == hAnalysis   
%                                         obj.SelectedRoi = hNode.UserData;                                    
%                                     end
%                                 end
%                             end
%                         end                        
%                     case com.Enum.TreeNodeTypes.ROIGroup
%                         obj.SelectedRoi = -1;                        
%                 end   
            end
        end       
        
        function OnNodeExpanded(obj, ~, eventData)
            try
                obj.EnsureChildnodesExist(eventData.Nodes(1));
            catch                
            end                           
        end
        
        function OnDblClick(obj, ~, eventArgs)
            if ~isempty(eventArgs.Nodes)
                switch(eventArgs.Nodes.Type)
                    case {com.Enum.TreeNodeTypes.DataSet, com.Enum.TreeNodeTypes.EEGDataset}
                        %Load dataset
                        obj.LoadSelectedDataset();
                    case com.Enum.TreeNodeTypes.Annotation                        
                        obj.EditSelectedItem( eventArgs.Nodes );
                    case com.Enum.TreeNodeTypes.AnalysisMode
                        if obj.ActiveAnalysisNode == eventArgs.Nodes || ...
                            ~isempty(obj.CurrentEegAnalysisNode) && obj.CurrentEegAnalysisNode == eventArgs.Nodes
                            obj.EditSelectedItem( eventArgs.Nodes );
                        else
                            obj.SetCurrentAnalysisNode(eventArgs.Nodes);
                        end 
                end
                    
            end
        end
        
        function EnsureChildnodesExist(obj, node)
            persistent isExecuting;
           
            if isempty(isExecuting) || isExecuting == false
                try
                    isExecuting = true;
                    if (node.Value ==  com.Enum.TreeNodeTypes.DataSet)
                       child = node.Children(1);

                       if (child.Value ==  com.Enum.TreeNodeTypes.Loading)
                           multiWaitbar('Loading, please wait', 'Color', 'g', 'Busy');
                           obj.FillDatasetnode(node);
                           child.delete();
                           multiWaitbar('Loading, please wait', 'Close');
                       end
                    end
                catch
                    multiWaitbar('Loading, please wait', 'Close');
                end
                isExecuting = false;
                
            end
        end                                                           
        
        function FillPipelineWithChildren(obj, hChildNodes, finalStage, forceRecomputeAll)
            try                 
                if nargin < 4
                    forceRecomputeAll = false;
                end
                
                numChildren = numel(hChildNodes);
                for i=1:numChildren
                   hChild =  hChildNodes(i);
                   
                   switch (hChild.Type)
                       case com.Enum.TreeNodeTypes.DataSet
                           hDataset = hChild.Dataset;                       
                           ep = com.CalciumAnalysis.Processing.BuildPipeline(hChild, finalStage, hDataset.Analyses, forceRecomputeAll);
                           com.common.Proc.ExecutionQueue.AddExecutionPipeline(ep);
                           if ~isempty(hDataset.EEGDataset)
                               obj.FillPipelineWithChildren(hDataset.EEGDataset, finalStage, forceRecomputeAll);
                           end
                       case com.Enum.TreeNodeTypes.EEGDataset
                           hDataset = hChild.Dataset;                       
                           ep = com.EEG.Processing.BuildPipeline(hChild, hDataset.Analyses, finalStage, forceRecomputeAll);
                           com.common.Proc.ExecutionQueue.AddExecutionPipeline(ep);
                       case com.Enum.TreeNodeTypes.Folder
                           obj.FillPipelineWithChildren(hChild.Children, finalStage, forceRecomputeAll);
                   end
                end
            catch              
            end                        
        end
        
        function UpdateNodeIcon(obj, hNode, isActive)
            switch hNode.Type
                case com.Enum.TreeNodeTypes.DataSet
                    if isActive
                        setIcon(hNode, obj.CurrentDatasetIcon);
                    else
                        setIcon(hNode, obj.DatasetIcon);
                    end
                case com.Enum.TreeNodeTypes.EEGDataset
                    if isActive
                        setIcon(hNode, obj.CurrentEegDatasetIcon);
                    else
                        setIcon(hNode, obj.EegDatasetIcon);
                    end
            end
        end
        
        function datasetList = GetDatasetList(obj, hTreeNodes)
            datasetList = [];
            
            if ~isempty(hTreeNodes)
                for hNode=hTreeNodes
                    switch(hNode.Type)
                        case com.Enum.TreeNodeTypes.Folder
                            datasetList = cat(2, datasetList, obj.GetDatasetList(hNode.Children));
                        case com.Enum.TreeNodeTypes.DataSet
                            %hCurrItem = obj.GetSelectedTreeItem(hNode);
                            %hDatasetNode = hCurrItem.GetDataSetNode();

                            if ~isempty( hNode )
                                hDataset = hNode.Dataset;
                                if hDataset.Type == com.Enum.DatasetType.CalciumData
                                    datasetList = cat(2, datasetList, hDataset);
                                end
                            end
                        otherwise
                            %hDatasetNode = hNode.GetCaDataSetNode();
                            %datasetList = hDatasetNode.Dataset;
                            if isempty(datasetList)
                                datasetList = obj.CurrentDataset;
                            end
                            %com.common.Logging.MLogManager.Info("Selected item is not a dataset... skipping.");
                    end
                end
            else
                hCurrItem = obj.GetSelectedTreeItem([]);
                hDatasetNode = hCurrItem.GetDataSetNode();
                
                if ~isempty( hDatasetNode )
                    hDataset = hDatasetNode.Dataset;
                    if hDataset.Type == com.Enum.DatasetType.CalciumData
                        datasetList = hDataset;
                    end
                end
            end
        end
        
        function ShowEditDialog(obj, hTreeNodes, hEditFunc)
            datasetList = obj.GetDatasetList( hTreeNodes );
            
            if ~isempty( datasetList )
                hEditFunc(datasetList);
                UpdateCaWnd();
            end
        end
        
        function AddDatasetFromFile(obj, hParentNode, isSequence)
            arguments
                obj (1,1) com.Management.MProjectManager = com.Management.MProjectManager.GetInstance();
                hParentNode(1,1) com.Management.MSparklesTreeNode = obj.Root;
                isSequence(1,1) logical = false;
            end
            
            if ~isempty(obj)                    
                searchPath = [];
                autoImport = false;

                try
                    hGlobalSettings = MGlobalSettings.GetInstance();
                    if ~isempty(hGlobalSettings)
                        searchPath = hGlobalSettings.LastPath;
                        autoImport = hGlobalSettings.AutoImportImageJRois;
                    end
                catch
                end

                if isSequence
                    pathname = uigetdir(searchPath, 'Select source directory of image sequence');
                    filename = '';
                else
                    [filename, pathname] = uigetfile( {'*.tif;*.tiff;', 'TIFF stacks (*.tif;*.tiff;)'},...
                               'Select file(s)', searchPath, 'MultiSelect', 'on');       
                end
                
                if (ischar(pathname))
                    try
                        if ~isempty(hGlobalSettings)
                            hGlobalSettings.LastPath = pathname;
                        end
                    catch
                    end

                    if (ischar(filename))                     
                        add(fullfile(pathname, filename));
                    else        
                        numFiles = numel(filename);
                        multiWaitbar('Importing files...', 'color', 'g');
                        for i=1:numFiles
                            add(fullfile(pathname, filename{i}));
                            multiWaitbar('Importing files...', 'value', i/numFiles);
                        end
                        multiWaitbar('Importing files...', 'close');
                    end
                end                
            end 

            function add(filePath)
                if (~obj.DoesDatasetExistInTree(filePath, hParentNode))  
                    node = obj.AddDataset(filePath, hParentNode);

                    if autoImport
                        hFile = com.common.FileIO.MSparklesTiffFile(filePath);
                        hFile.ImportRois(node);
                    end
                end
            end
        end
    end
    
    methods
        function AddImageSequence(obj, hParentNode)
            obj.AddDatasetFromFile(hParentNode, true);
        end
        
        function AddFiles(obj, hParentNode)
            obj.AddDatasetFromFile(hParentNode);
        end
        
        function AddEEGFile(obj, hParentNode)
            if nargin < 2 || isempty(obj)
                obj = com.Management.MProjectManager.GetInstance();
            end

            if ~isempty(obj)                    
                searchPath = [];

                try
                    hGlobalSettings = MGlobalSettings.GetInstance();
                    if ~isempty(hGlobalSettings)
                        searchPath = hGlobalSettings.LastPath;
                    end
                catch
                end

                try
                    if hParentNode.Type ~= com.Enum.TreeNodeTypes.DataSet
                        hDatasetNode = hParentNode.GetCaDataSetNode();     
                    else
                        hDatasetNode = hParentNode;
                    end
                catch
                    hDatasetNode = [];
                end

                %ToDo: check if parent dataset is actually a calcium dataset...
                if ~isempty(hDatasetNode)
                    [filename, pathname] = uigetfile( {'*.mat;', 'matlab files (*.mat;)'},...
                                   'Select file(s)', searchPath, 'MultiSelect', 'off');
                else
                    %ToDo: Handle case of stand-alone eeg datasets
                end

                 if (ischar(pathname))            
                    if (isempty(hParentNode)) %%Fallback, just in case...
                       hParentNode = obj.Root; 
                    end

                    try
                        if ~isempty(hGlobalSettings)
                            hGlobalSettings.LastPath = pathname;
                        end
                    catch
                    end

                     obj.AddEEGDataset(fullfile(pathname, filename), hParentNode);            
                 end
            end
        end
        
        function NotifyCloseDataset(obj, hDataset)
            hCurrDataset = obj.CurrentDataset;
            
            if ~isempty(hCurrDataset) && hCurrDataset == hDataset
                if hCurrDataset.Type == com.Enum.DatasetType.CalciumData
                    obj.ActiveAnalysisNode = [];
                end
                
                obj.ActiveDataSetNode = [];
            end
            
            hCurrDataset = obj.CurrentEegDataset;
            if ~isempty(hCurrDataset) && hCurrDataset == hDataset
                obj.ActiveEegDatasetNode = [];
            end
        end
        
        function UpdateActiveDatasetNode(obj, hNode)
            if ~isempty(hNode)
                hParentDatasetNode = hNode.GetParentDatasetNode();
                hDataset = hNode.Dataset;
            else
                hParentDatasetNode = [];
                hDataset = [];
            end

            obj.ActiveDataSetNode = hParentDatasetNode;% hNode;

            if ~isempty(hDataset)
                switch(hDataset.Type)
                    case com.Enum.DatasetType.CalciumData
                        if ~isempty(hDataset.EEGDataset)
                           obj.ActiveEegDatasetNode = hNode.GetChildEegDataset();
                        else
                            obj.ActiveEegDatasetNode = [];
                        end
                    case com.Enum.DatasetType.EegData
                        obj.ActiveEegDatasetNode = obj.CurrentItem;
                end
            end
        end
        
        function LoadSelectedDataset(obj)
            obj.EnsureChildnodesExist(obj.CurrentItem);
                        
            hDataset = obj.CurrentItem.Dataset;
            
            if ~isempty(hDataset) && isa(hDataset, "com.common.Data.MDatasetRoot")
                success = hDataset.LoadData();
                
                %Always set the real parent dataset as CurrentDataset.
                %This way, we can allow to olny load the sub dataset
                if success
                    hRealDataset = obj.CurrentItem.GetParentDatasetNode();
                    obj.UpdateActiveDatasetNode(hRealDataset);
                    
                    hAnalysis = hDataset.Analyses.CurrentAnalysis;
                    
                    if ~isempty(hAnalysis)
                        hAnalysisNode = obj.CurrentItem.GetAnalysisNode(hAnalysis.ID);
                        obj.SetCurrentAnalysisNode(hAnalysisNode);
                    end
                end
            else
                msgbox("Please select a valid dataset.", "Error", "error");
            end
        end       
        
        function ProcessAllChildren(obj, hStartNode, finalStage, forceRecomputeAll)
            multiWaitbar('Gathering data', 'Busy', 'Color', 'g'); 
            
            if nargin < 4
                forceRecomputeAll = false;
            end
            
            obj.FillPipelineWithChildren(hStartNode, finalStage, forceRecomputeAll);            
            multiWaitbar('Gathering data', 'Close');
        end
        
        function AddAnalysis(obj, hClickedNode, type)
            if isempty(hClickedNode) 
                hDatasetNode = obj.ActiveDataSetNode;
            elseif hClickedNode.Type ~= com.Enum.TreeNodeTypes.DataSet && ...
                    hClickedNode.Type ~= com.Enum.TreeNodeTypes.EEGDataset

                    hDatasetNode = hClickedNode.GetParentDatasetNode();
            else
                hDatasetNode = hClickedNode;
            end
            
            CreateAndEditAnalysis(hDatasetNode.Dataset, type);
        end
        
        function ShowInBrowser(obj, hTreeNode) 
            try
                hDatasetNode = hTreeNode.GetDataSetNode();
                hDataset = hDatasetNode.Dataset;
                
                if ~isempty(hDataset)
                    obj.OpenFileBrowser(hDataset.OrgDatasetPath);
                end
            catch
            end
        end
        
        function OpenResultsFolder(obj, hTreeNode)
            try
                hDatasetNode = hTreeNode.GetDataSetNode();
                hDataset = hDatasetNode.Dataset;

                if ~isempty(hDataset)
                    if hTreeNode.Type == com.Enum.TreeNodeTypes.AnalysisMode
                        hAnalysis = hDataset.Analyses.GetAnalysis(hTreeNode.UserData);
                        fPath = hAnalysis.GetResultsFolder();
                    else
                        fPath = hDataset.ResultsFolder;
                    end            

                    obj.OpenFileBrowser(fPath);
                end
            catch
            end
        end                                             

        function hParentNode = FindParentFolder(obj, hNode)
            if ~isempty(hNode)
                hParentNode = hNode.GetContainingNode(); 
                
                if isempty(hParentNode)
                    hParentNode = obj.Root;
                end            
            else
                hParentNode = obj.Root;
            end            
        end
        
        function DeleteROI(obj, hAnalysis, channelID, roiID)
            if ~isempty(hAnalysis)
                hResultset = hAnalysis.GetResultset(channelID);
                
                if ~isempty(hResultset)
                    if obj.CurrentAnalysis == hAnalysis 
                        %Only allow to delete a ROI if it belongs to the active
                        %analysis.                        
                        hAnalysis.DeleteROI(channelID, roiID);  %Remove ROI and dependent stuff 
                        obj.UpdateRoiNames(hAnalysis, hResultset);
                        obj.SelectedRoi = -1;

                        hAnalysis.hDataset.Save();
                    else
                        warndlg('ROIs can only be deleted from the currently active analysis.', 'Info', 'warn')
                    end
                end
            end
        end                                
        
        function hItem = GetSelectedTreeItem(obj, hTreeNode)
            if nargin < 2 || isempty(hTreeNode)                
                hItem = obj.CurrentItem;
            else
                hItem = hTreeNode;
            end
            
            if isempty(hItem)
                hItem = obj.ActiveDataSetNode;
            end
        end
        
        function EditSelectedItem(obj, hTreeNode)
            if nargin < 2
                hTreeNode = [];
            end
            
            hCurrItem = obj.GetSelectedTreeItem(hTreeNode);
            hDatasetNode = hCurrItem.GetDataSetNode();
            
            switch (hCurrItem.Type)
                case com.Enum.TreeNodeTypes.Annotation
                    EditAnnotationSettings(hDatasetNode, hCurrItem);
                case com.Enum.TreeNodeTypes.AnalysisMode
                    CreateAndEditAnalysis(hDatasetNode.Dataset, hCurrItem.Dataset);
                case {com.Enum.TreeNodeTypes.DataSet, com.Enum.TreeNodeTypes.EEGDataset}
                    EditDatasetSettings(hDatasetNode);
            end            
        end
        
        function EditSelectedPreProc(obj, hTreeNode)
            if nargin < 2
                hTreeNode = [];
            end
            
            obj.ShowEditDialog(hTreeNode, @com.CalciumAnalysis.UI.dlgPreProc);
        end
        
        function CustomizeTracePlot(obj)   
            if ~isempty(obj.CurrentDataset)
                com.CalciumAnalysis.UI.dlgExportRoiTrace( obj.CurrentDataset );
            end
        end
        
        function EditSelectedF0(obj, hTreeNode)
            if nargin < 2
                hTreeNode = [];
            end
            
            obj.ShowEditDialog(hTreeNode, @com.CalciumAnalysis.UI.dlgF0estimator);           
        end
        
        function EditSelectedRoiDetection(obj, hTreeNode)
            if nargin < 2
                hTreeNode = [];
            end
            
            obj.ShowEditDialog(hTreeNode, @com.CalciumAnalysis.UI.dlgRoiConfig);          
        end
        
        function EditSelectedAnalysis(obj, hTreeNode)
            if nargin < 2
                hTreeNode = [];
            end
            
            obj.ShowEditDialog(hTreeNode, @com.CalciumAnalysis.UI.dlgAnalysisConfig);
        end
        
        function ResetSelectedPreProcessing(obj, hTreeNode)
            %ToDo
        end
        
        function ResetSelectedtF0(obj, hTreeNode)
            %ToDo
        end
        
        function ResetSelectedRois(obj, hTreeNode)
            %ToDo
        end
        
        function ResetSelectedResults(obj, hTreeNode)
            %ToDo
        end
        
        function SetCurrentAnalysisNode(obj, hTreeNode)
            if ~isempty(hTreeNode)
                obj.ActiveAnalysisNode = hTreeNode.GetAnalysisNode();                                                
            else
                obj.ActiveAnalysisNode = [];
            end
        end          
        
        function UpdateActiveAnalysisNode(obj, hAnalysis)
            if ~isempty(hAnalysis)
                hAnalysisNode = obj.ActiveDataSetNode.GetAnalysisNode(hAnalysis.ID);
                
                if ~isempty(hAnalysisNode)
                    obj.ActiveAnalysisNode = hAnalysisNode;
                end
            else
                obj.ActiveAnalysisNode = [];
            end
        end
        
        function ImportRois(obj, roiFile)
            if nargin < 2
                hGlobalSettings = MGlobalSettings.GetInstance();
                if ~isempty(hGlobalSettings)
                    searchPath = hGlobalSettings.LastPath;
                end

               [fileName, filePath] = uigetfile( ...
                        {'*.zip', 'Zipped ROI file (*.zip;)'; '*.roi', 'Binary ROI file (*.roi;)' }, ...
                        'Import ROIs', searchPath);  

                 roiFile = fullfile(filePath, fileName);
            end    

            %import
            ijRois = ReadImageJROI(roiFile);

            ImportImageJRois(obj.CurrentItem.Dataset, ijRois);
        end
    end
    
    methods
        function set.ActiveEegDatasetNode(obj, value)
             if ( ~isempty(obj.ActiveEegDatasetNode))
                try
                    if isempty(value) || (value ~= obj.ActiveEegDatasetNode)
                        obj.UpdateNodeIcon(obj.ActiveEegDatasetNode, false);                        
                        
                        if obj.ActiveEegDatasetNode.Parent.Type == com.Enum.TreeNodeTypes.DataSet
                            hCaDataset = obj.ActiveEegDatasetNode.Parent.Dataset;
                            hCaDataset.Save();
                            
                            hEegDataset = obj.ActiveEegDatasetNode.Dataset;
                            hEegDataset.Clear();
                        end
                    end   
                catch
                end
            end
            obj.ActiveEegDatasetNode = value;
            
            if ~isempty(obj.ActiveEegDatasetNode)
                obj.UpdateNodeIcon(obj.ActiveEegDatasetNode, true);            
            end
        end
        
        function set.ActiveDataSetNode(obj, value)
            if ( ~isempty(obj.ActiveDataSetNode))
                try
                    if isempty(value) || (value ~= obj.ActiveDataSetNode)
                        obj.UpdateNodeIcon(obj.ActiveDataSetNode, false);                        
                        hDataset = obj.ActiveDataSetNode.Dataset;
                        hDataset.Save();
                        hDataset.Clear();
                    end   
                catch
                end
            end
            obj.ActiveDataSetNode = value;
            
            if ~isempty(obj.ActiveDataSetNode)
                obj.UpdateNodeIcon(obj.ActiveDataSetNode, true);            
            end
        end
        
        function node = get.ActiveDataSetNode(obj)
            try
                node = obj.ActiveDataSetNode;
            catch
                node = 0;
            end
        end
        
        function set.ActiveAnalysisNode(obj, value)
            %reset current node, if it exists
            if ~isempty(value)
                hDatasetNode = value.GetDataSetNode();

                if hDatasetNode.Type == com.Enum.TreeNodeTypes.EEGDataset
                    if ~isempty(obj.CurrentEegAnalysisNode) && isvalid(obj.CurrentEegAnalysisNode)
                        obj.CurrentEegAnalysisNode.setIcon(obj.AnalysisIcon);
                    end
                    
                    if ~isempty(obj.ActiveEegDatasetNode) && obj.ActiveEegDatasetNode == hDatasetNode
                        obj.CurrentEegAnalysisNode = value;
                        obj.CurrentEegAnalysisNode.setIcon(obj.CurrentAnalysisIcon);
                        % Set active analysis bases on UUID of TreeNode
                        obj.CurrentEegAnalysis = obj.CurrentEegAnalysisNode.UserData;
                    end
                else
                    if ~isempty(obj.CurrentAnalysisNode) && isvalid(obj.CurrentAnalysisNode)
                        obj.CurrentAnalysisNode.setIcon(obj.AnalysisIcon);
                    end
                    
                    if ~isempty(obj.ActiveDataSetNode) && obj.ActiveDataSetNode == hDatasetNode
                        obj.CurrentAnalysisNode = value;
                        obj.CurrentAnalysisNode.setIcon(obj.CurrentAnalysisIcon);
                        % Set active analysis bases on UUID of TreeNode
                        obj.CurrentAnalysis = obj.CurrentAnalysisNode.UserData;
                    end
                end
            else
                obj.CurrentAnalysisNode = [];
                obj.CurrentAnalysis = [];
                obj.CurrentEegAnalysisNode = [];
                obj.CurrentEegAnalysis = [];
            end
        end    
        
        function value = get.ActiveAnalysisNode(obj)
            if isempty(obj.CurrentAnalysisNode)
                obj.UpdateActiveAnalysisNode(obj.CurrentAnalysis);
            end
            
            value = obj.CurrentAnalysisNode;
        end
        
        function hDataset = get.CurrentDataset(obj)
            try
                hDataset = obj.ActiveDataSetNode.Dataset;
            catch
                hDataset = [];
            end
        end
        
        function hDataset = get.CurrentEegDataset(obj)
            try
                hDataset = obj.ActiveEegDatasetNode.Dataset;
            catch
                hDataset = [];
            end
        end
        
        function set.CurrentAnalysis(obj, value)
            if ~isempty (obj.CurrentDataset)                 
                if isa(value, "com.CalciumAnalysis.Analysis.MCaAnalysis")                
                    obj.CurrentDataset.Analyses.SetCurrentAnalysis(value.ID);
                else 
                    %Assume it is a UUID
                    obj.CurrentDataset.Analyses.SetCurrentAnalysis(value);
                end                
            end
        end
        
        function value = get.CurrentAnalysis(obj)
            if ~isempty (obj.CurrentDataset) 
                value = obj.CurrentDataset.Analyses.CurrentAnalysis;
            else
                value = [];
            end
        end      
        
        function set.CurrentEegAnalysis(obj, value)
            if ~isempty (obj.CurrentEegDataset)                 
                if isa(value, "com.CalciumAnalysis.Analysis.MCaAnalysis")                
                    obj.CurrentEegDataset.Analyses.SetCurrentAnalysis(value.ID);
                else 
                    %Assume it is a UUID
                    obj.CurrentEegDataset.Analyses.SetCurrentAnalysis(value);
                end                
            end
        end
        
        function value = get.CurrentEegAnalysis(obj)
            if ~isempty (obj.CurrentEegDataset) 
                value = obj.CurrentEegDataset.Analyses.CurrentAnalysis;
            else
                value = [];
            end
        end      
    end
end

