%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSparklesTreeNode < uiw.widget.TreeNode
    %MSPARKLESTREENODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Transient, Dependent)
        Type com.Enum.TreeNodeTypes;
        Dataset com.common.Data.MDatasetRoot;
    end
    
    methods
        function obj = MSparklesTreeNode(varargin)
            obj = obj@uiw.widget.TreeNode(varargin{:});
        end
    end
    
    methods
        %% Get & Set function for properties
        function value = get.Type(obj)
            value = obj.Value;
        end
        
        function set.Type(obj, value)
            obj.Value = value;
            obj.UpdateIcon();
        end
        
        function value = get.Dataset(obj)
            value = obj.UserData;
        end
        
        function set.Dataset(obj, value)
            obj.UserData = value;
            obj.UserData.TreeNode = obj;
        end
    end
    
    methods
        %% Tree traversal
        function hNode = FindNodeInDataSetByType(obj, nodeType)              
            hNode = obj.FindNodeInDataSet( @(x) ~isempty(x.Type) && any (x.Type == nodeType) );
        end  
        
        function hNode = FindDirectChildNodeByType(obj, nodeType)
            hNode = obj.FindDirectChildNode( @(x) any (x.Type == nodeType) );
        end
        
        function hNode = FindDirectChildNodeByName(obj, nodeName)
            hNode = obj.FindDirectChildNode( @(x) any (x.Name == nodeName) );
        end
        
        function hNode = FindNodeInDataSet(obj, compFunc)
            if ( compFunc(obj) ) %%Supplied node is searched node
                hNode = obj; 
            elseif ( obj.Type ==  com.Enum.TreeNodeTypes.DataSet )
                %%Is supplied node the project node?
                hNode = obj.FindChildNode( compFunc );
            else
                %%Move up in the hierarchy until we find the project node
                if ~isempty(obj.Parent)
                    hNode = obj.Parent.FindNodeInDataSet( compFunc );
                else
                   hNode = []; 
                end
            end                           
        end  
        
        function hNode = FindUpstreamNodeInDataSetByType(obj, nodeType)
            if ( obj.Type == nodeType ) %%Supplied node is searched node
                hNode = obj; 
            else
                %%Move up in the hierarchy until we find the project node
                if ~isempty(obj.Parent)
                    hNode = obj.Parent.FindUpstreamNodeInDataSetByType( nodeType );
                else
                    hNode = [];
                end
            end                           
        end 
        
        function hNode = FindDirectChildNode(obj, compFunc)
            hNode = [];

            if (~isempty(obj.Children))
                for i=1:numel(obj.Children)
                    currChild = obj.Children(i);
                    if (compFunc(currChild))
                        hNode = currChild;
                        return;
                    end
                end
            end
        end
        
        function hNode = FindChildNode(obj, compFunc)
            hNode = [];

            if (~isempty(obj.Children))
                idx = arrayfun(compFunc, obj.Children);
                
                if ~any( idx == 1 )
                    for i=1:numel(obj.Children)
                        currChild = obj.Children(i);
                        if (~isempty(currChild.Children))
                            hNode = currChild.FindChildNode(compFunc);                            
                        end

                        if ~isempty (hNode)
                            return; 
                        end
                    end
                else
                    hNode = obj.Children(idx);
                end                
            end
        end
        
        function hChildNode = FindChildNodeByType(obj, nodeType)
            hChildNode = obj.FindChildNode( @(x) x.Value == nodeType );
        end
        
        function hChildNode = FindChildNodeByName(obj, nodeName, caseSensitive)
            if nargin > 2 && caseSensitive
                hChildNode = obj.FindChildNode( @(x) strcmp(nodeName,  x.Name));
            else
                hChildNode = obj.FindChildNode( @(x) strcmpi(nodeName,  x.Name));
            end
        end
        
        function hChildNode = FindChildNodeByUserData(obj, hUserData)
            hChildNode = obj.FindChildNode( @(x) x.UserData == hUserData );
        end
    end
    
    methods
        function RemoveAllChildren(obj)
            try
                obj.Children = com.Management.MSparklesTreeNode.empty;
            catch
            end
        end
        
        %%Convenience functions
        function hNode = GetDataSetNode(obj)
            % Searches the next dataset node in the hierarchy that belongs to this tree node.
            % The dataset node qualifies by having its Type to be com.Enum.TreeNodeTypes.DataSet or
            % com.Enum.TreeNodeTypes.EEGDataset.
            hNode = obj.FindNodeInDataSetByType(   [com.Enum.TreeNodeTypes.DataSet, ...
                                                    com.Enum.TreeNodeTypes.EEGDataset] );
        end
        
        function hNode = GetCaDataSetNode(obj)
            % Searches the new Calcium dataset node in the heirarchy that belongs to this tree node.
            % The dataset node qualifies by having its Type to be com.Enum.TreeNodeTypes.DataSet.           
            hNode = obj.FindNodeInDataSetByType(com.Enum.TreeNodeTypes.DataSet);
        end
        
        function hNode = GetEegDataSetNode(obj)
            % Searches the next EEG dataset node in the hierarchy that belongs to this tree node.
            % The dataset node qualifies by having its Type to be com.Enum.TreeNodeTypes.EEGDataset.          
            hNode = obj.FindNodeInDataSetByType(com.Enum.TreeNodeTypes.EEGDataset);
        end
        
        function hNode = GetParentDatasetNode(obj)
            hCandidateNode = obj.GetDataSetNode(); %Get the dataset node
            
            %check if it has a parent dataset
            if ~isempty(hCandidateNode.Parent) &&  ~isempty(hCandidateNode.Parent.Type) &&...
                    any(hCandidateNode.Parent.Type ==...
                    [com.Enum.TreeNodeTypes.DataSet; com.Enum.TreeNodeTypes.EEGDataset])
                hNode = obj.Parent;
            else
                hNode = hCandidateNode;
            end
        end
        
        function hNode = GetF0Node(obj)
            % Searches the F0 node that belongs to this tree node.
            % The F0 node qualifies by having its Type to be ProjectNodeType.F0           
            hNode = obj.FindNodeInDataSetByType(com.Enum.TreeNodeTypes.F0);
        end
        
        function hNode = GetAnalysisRootNode(obj)
            % Searches the dataset node that belongs to this tree node.
            % The dataset node qualifies by having its Type to be ProjectNodeType.DataSet  
            hNode = obj.FindDirectChildNodeByType(com.Enum.TreeNodeTypes.Analysis);
        end
        
        function hNode = GetAnnotationsRootNode(obj)
            % Searches the dataset node that belongs to this tree node.
            % The dataset node qualifies by having its Type to be ProjectNodeType.DataSet           
            hNode = obj.FindNodeInDataSetByType(com.Enum.TreeNodeTypes.AnnotationGroup);
        end
        
        function hNode = GetAnnotationNode(obj, annotationGUID)
            % Searches the analysis node which is a (grant) parent node of this
            % object. This search may only be performed 'upstrem', since
            % there could be multipe node of type
            % 'com.Enum.TreeNodeTypes.AnalysisMode'.    
            
            if nargin == 2 && ~isempty(annotationGUID) && obj.Type == com.Enum.TreeNodeTypes.AnnotationGroup
                hNode = obj.FindChildNode( @(x) x.Type == com.Enum.TreeNodeTypes.Annotation...
                    && strcmp(x.UserData, annotationGUID) );
            else
                hNode = [];
            end
        end
        
        function hNode = GetAnalysisNode(obj, analysisGUID)
            % Searches the analysis node which is a (grant) parent node of this
            % object. This search may only be performed 'upstrem', since
            % there could be multipe node of type
            % 'com.Enum.TreeNodeTypes.AnalysisMode'.    
            
            if nargin == 2 && ~isempty(analysisGUID)
                hNode = obj.FindNodeInDataSet( @(x) x.Type == com.Enum.TreeNodeTypes.AnalysisMode && strcmp(x.UserData, analysisGUID) );
            else
                hNode = obj.FindUpstreamNodeInDataSetByType(com.Enum.TreeNodeTypes.AnalysisMode);
            end
        end
        
        function hNode = GetChannelNode(obj, channelID)
            % Searches the channel node which is a (grant) parent node of this
            % object. This search may only be performed 'upstrem', since
            % there could be multipe node of type
            % 'com.Enum.TreeNodeTypes.Channel'.   
            
            if nargin == 2 && obj.Type == com.Enum.TreeNodeTypes.AnalysisMode
                hNode = obj.FindChildNode( @(x) x.Type == com.Enum.TreeNodeTypes.Channel && x.UserData == channelID );                
            else
                hNode = obj.FindUpstreamNodeInDataSetByType(com.Enum.TreeNodeTypes.Channel);
            end
        end
        
        function hNode = GetRoiParentNode(obj)
            switch (obj.Type)
                case com.Enum.TreeNodeTypes.ROI %Search upstream
                    hNode = obj.FindUpstreamNodeInDataSetByType(com.Enum.TreeNodeTypes.ROIGroup);
                case com.Enum.TreeNodeTypes.Channel % Search downstream
                    hNode = obj.FindChildNodeByType(com.Enum.TreeNodeTypes.ROIGroup);
                otherwise
                    hNode = [];
            end
        end
        
        function hNode = GetResultsNode(obj)
            if obj.Type == com.Enum.TreeNodeTypes.Channel
                hNode = obj.FindChildNodeByType(com.Enum.TreeNodeTypes.Results);
            else
                hNode = [];
            end
        end
        
        function hNode = GetRoiNode(obj, roiID)
            if obj.Type == com.Enum.TreeNodeTypes.ROIGroup
                hNode = obj.FindChildNode( @(x) x.Type == com.Enum.TreeNodeTypes.ROI && ismember(x.UserData, roiID) );
            else
                hNode = [];
            end
        end
        
        function hNode = GetChildEegDataset(obj)
            hNode = obj.FindChildNodeByType(com.Enum.TreeNodeTypes.EEGDataset);
        end
        
        function hNode = GetContainingNode(obj)
            hNode = obj.FindUpstreamNodeInDataSetByType(com.Enum.TreeNodeTypes.Folder);
        end
        
        function hNode = GetChildDatasetByName(obj, datasetName, doDeepSearch)            
            cmpFnc = @(x) x.Type == com.Enum.TreeNodeTypes.DataSet && strcmp(x.Name, datasetName);
            
            if nargin == 3 && doDeepSearch
                hNode = obj.FindChildNode( cmpFnc ); 
            else
                hNode = obj.FindDirectChildNode( cmpFnc );
            end
        end
    end    
    
    methods(Access=private)
        function UpdateIcon(obj)
            icondir = iconrootdir();
            
            switch(obj.Type)
                case com.Enum.TreeNodeTypes.None
                    fileName = '';
                case com.Enum.TreeNodeTypes.Loading
                    fileName = '';
                case com.Enum.TreeNodeTypes.Folder
                    fileName = 'folder.png';
                case com.Enum.TreeNodeTypes.DataSet
                    fileName = 'dataset.png';
                case com.Enum.TreeNodeTypes.Analysis
                    fileName = 'variation.png';
                case com.Enum.TreeNodeTypes.AnalysisMode
                    fileName = 'analysis.png';
                case com.Enum.TreeNodeTypes.ROIGroup
                    fileName = 'landmark.png';
                case com.Enum.TreeNodeTypes.ROI
                    fileName = '';
                case com.Enum.TreeNodeTypes.Results
                    fileName = 'results.png';
                case com.Enum.TreeNodeTypes.Channel
                    fileName = 'variation.png';
                case com.Enum.TreeNodeTypes.AnnotationGroup
                    fileName = 'annotation.png';
                case com.Enum.TreeNodeTypes.Annotation
                    fileName = 'annotation.png';
                case com.Enum.TreeNodeTypes.F0
                    fileName = 'baseline.png';
                case com.Enum.TreeNodeTypes.EEGDataset
                    fileName = 'EEG.png';
                otherwise
                    fileName = 'dataset.png';
            end
            
            try
                if ~isempty(fileName)
                    obj.setIcon( fullfile(icondir, fileName) );
                end
            catch
            end         
        end
    end
end

