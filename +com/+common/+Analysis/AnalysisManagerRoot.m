%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AnalysisManagerRoot < com.common.Data.MSparklesObject
    %ANALYSISMANAGERROOT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=private)
        dataMap;        
    end
    
    properties(Access=protected, Dependent)
        DefaultAnalysisType;
    end
    
    properties (Transient, SetObservable, AbortSet)
        CurrentAnalysis;
    end
    
    properties (Transient, Dependent)
        Count;
        Values;
        ValuesSorted;
    end    
    
    methods
        function obj = AnalysisManagerRoot(hParent)
            obj = obj@com.common.Data.MSparklesObject(hParent);
            obj.dataMap = containers.Map;
        end
    end
    
    methods
        function obj = Convert(obj, input)
            prop = metaclass(input);

            for k = 1:length(prop.PropertyList)
                p = prop.PropertyList(k);
                try
                    if ~strcmpi(p.Name, "ID")
                        if ~p.Constant
                            if ~p.Dependent
                                obj.(p.Name) = input.(p.Name);
                            end
                        end
                    end
                catch ex
                    
                end
            end
            
            obj.dataMap = input.dataMap;
        end
    end
    
    methods
        function CloneFrom(obj, hAnalysisMngr)
            obj.DeleteAll();
            
            hAnalyses = hAnalysisMngr.Values;
            
            for i=1:hAnalysisMngr.Count
                hAnalysis = hAnalyses{i}.Clone();
                hAnalysis.Reset();
                obj.AddAnalysis( hAnalysis );
            end
        end
        
        function result = IsNameUnique(obj, hAnalysis)
            if isa(hAnalysis, 'com.common.Analysis.MAnalysisRoot')
                analysis = obj.GetAnalysisByName(hAnalysis.Name);                
            elseif ischar(hAnalysis)
                analysis = obj.GetAnalysisByName(hAnalysis);
            else
                result = false;
                return;
            end
            
            if ischar(hAnalysis)
                result = isempty(analysis);
            else
                result = isempty(analysis) || hAnalysis == analysis;
            end
        end
        
        function UniqueName(obj, hAnalysis)
            analysisName = sprintf('%s',hAnalysis.Name);            
            cnt = 1;
            
            while ~obj.IsNameUnique(analysisName)
                analysisName = sprintf('%s-%d', hAnalysis.Name, cnt);
                cnt = cnt + 1;
            end
            
            hAnalysis.Name = analysisName;
        end
        
        function result = IsEmpty(obj)
            result = isempty(obj.Values);
        end
        
        function result = SetCurrentAnalysis(obj, analysisID)
            obj.CurrentAnalysis = obj.GetAnalysis(analysisID);
            result = ~isempty(obj.CurrentAnalysis);
        end
        
        function hAnalysis = GetAnalysis(obj,analysisID)
            try
                hAnalysis = obj.SortByName(obj.dataMap(analysisID));
            catch
                hAnalysis = {};
            end
        end                
        
        function hAnalyses = GetAnalysisByType(obj, analysisType)
            if ~isempty(analysisType)
                idx = cell2mat(cellfun( @(x) x.AnalysisType == analysisType, obj.Values, 'UniformOutput',false));
%                 idx = max(idx, [], 2);
                hAnalyses = obj.Values(idx);
            else
                hAnalyses = {};
            end
        end
        
        function hAnalysis = GetAnalysisByName(obj,analysisName)
            if ~isempty(analysisName)  && obj.Count > 0                
                idx = cell2mat(cellfun( @(x) strcmpi(x.Name, analysisName), obj.Values, 'UniformOutput',false));
%                 idx = max(idx, [], 2);
                hAnalysis = obj.Values(idx); 
                if numel(hAnalysis) == 1
                    hAnalysis = hAnalysis{:};                                    
                end
            else
                hAnalysis = {};
            end
        end
                
        function AddAnalysis(obj, hAnalysis)
            if obj.Count == 0
                obj.dataMap = containers.Map;
            end
            
            if isa(hAnalysis, 'com.common.Analysis.MAnalysisRoot')
                hAnalysis.hParent = obj;
                hAnalysis.hDataset = obj.hParent;
                
                obj.dataMap(hAnalysis.ID) = hAnalysis;
            end
        end
        
        function DeleteAll(obj)
            obj.dataMap = containers.Map;
            obj.CurrentAnalysis = [];
        end
        
        function DeleteAnalysis(obj, analysisID)
            obj.dataMap.remove(analysisID);
        end
        
        function intersectable = GetIntersectableAnalyses(obj, hAnalysis)
            sorted = obj.ValuesSorted;
            idx = cellfun( @(x) hAnalysis.IsIntersectable(x), sorted );
            intersectable = sorted(idx);
        end
        
        function UpdateParent(obj, hParent)
            %UPDATEPARENT Updates the hParent property of all child objects
            %to the supplied hParent value            
            obj.hParent = hParent;
            
            for v=obj.Values
                hAnalysis = v{:};
                hAnalysis.hParent = obj;
             	hAnalysis.hDataset = hParent;
            end
        end
        
        function ResetRois(obj)
            if ~isempty(obj.Values)
                cellfun(@(hAnalysis) hAnalysis.ResetRois(), obj.Values);
            end
        end
        
        function ResetResults(obj)
            if ~isempty(obj.Values)
                cellfun(@(hAnalysis) hAnalysis.ResetResults(), obj.Values);
            end
        end
        
        function ResetAnalyses(obj)
            if ~isempty(obj.Values)
                cellfun(@(hAnalysis) hAnalysis.Reset(), obj.Values);
            end
        end
    end
    
    methods
        function value = get.DefaultAnalysisType(obj)
            value = obj.GetDefaultAnalysisType();
        end
        
        function value = get.dataMap(obj)
            if isempty(obj.dataMap)
                obj.dataMap = containers.Map;
            end
            
            value = obj.dataMap;
        end
        
        function value = get.Values(obj)
            value = obj.dataMap.values;
        end
        
        function value = get.ValuesSorted(obj)
            value = obj.SortByName(obj.dataMap.values);
        end
        
        function set.CurrentAnalysis(obj, value)
            obj.CurrentAnalysis = value;
        end
        
        function value = get.CurrentAnalysis(obj)
            if isempty(obj.CurrentAnalysis)                
                %if no active analysis has been set explicitly, return the
                %first from the list                
                if ~isempty(obj.Values) && numel(obj.Values) > 0
                    analyses = obj.GetAnalysisByType(obj.DefaultAnalysisType);
                    
                    if ~isempty(analyses)
                        obj.CurrentAnalysis = analyses{1};
                    else
                        obj.CurrentAnalysis = obj.Values{1};
                    end                    
                else
                    obj.CurrentAnalysis = [];
                end
            end
            
            value = obj.CurrentAnalysis;
        end
        
        function value = get.Count(obj)
            try
                value = numel(obj.Values);
            catch
                value = 0;
            end
        end
    end
    
    methods
        function varargout = subsref(obj, S)
            % Allow direct indexing acces for stored analyses
            if length(S) > 1
                [varargout{1:nargout}] = builtin('subsref',obj,S);
            else
                switch (S.type)
                    case '.'
                        [varargout{1:nargout}] = builtin('subsref',obj,S);
                    otherwise
                        [varargout{1:nargout}] = builtin('subsref',obj.Values,S);
                end                
            end                       
        end
    end
    
    methods(Access = private)
        function value = SortByName(~, analyses)
            if iscell(analyses)
                names = cellfun(@(x) string(x.AnalysisName), analyses);
                [~, ind] = sort(names);
                value = analyses(ind);
            else
                value = analyses;
            end
        end
    end
    
    methods(Abstract)
        hAnalysis = CreateAnalysis(obj, analysisType, channels);
        type = GetDefaultAnalysisType(obj);
    end
end

