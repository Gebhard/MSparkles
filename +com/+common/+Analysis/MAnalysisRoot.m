%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MAnalysisRoot < com.common.Data.MSparklesObject
    %MANALYSISROOT Super class of all analyses for all types of datasets
    
    properties
        AnalysisType;
        AnalysisSettings;
        AnalysisResults = [];        
        Channels;
    end
    
    properties(Access=protected, Dependent, Hidden)
        SaveWBName;
    end
        
    properties (SetAccess=private, Transient, Dependent)
        AnalysisID;     %OBSOLETE. Delete after v 1.9
    end
    
    properties(Transient, Dependent)
        AnalysisName;     %OBSOLETE. Delete after v 1.9
    end
    
    properties (Transient)
        hDataset;
        TreeNode;
    end
    
    methods
        function obj = MAnalysisRoot(analysisType)
            obj = obj@com.common.Data.MSparklesObject();
            obj.AnalysisType = analysisType;
            obj.Name = obj.GetDefaultAnalysisName();
            obj.Channels = 1;
        end
        
        function delete(obj)
            if ~obj.BeingDeleted
                obj.BeingDeleted = true;
                
                if ~isempty(obj.hParent) && isa(obj.hParent, "com.common.Analysis.AnalysisManagerRoot")
                    obj.hParent.DeleteAnalysis(obj.ID);
                    try
                        obj.AnalysisResults.delete();
                    catch
                    end
                    
                    obj.AnalysisResults = [];
                end
            end
        end
    end
    
    methods
        function val = get.AnalysisName(obj)
            if isempty(obj.Name)
                obj.Name = obj.GetDefaultAnalysisName();
            end
            val = obj.Name;
        end
        
        function set.AnalysisName(obj, val)
            obj.Name = val;
        end
        
        function id = get.AnalysisID(obj)
            id = obj.ID;            
        end      
        
        function set.AnalysisID(obj, value)
            obj.ID = value;
        end
        
        function value = get.Channels(obj)
            if isempty(obj.Channels)
                obj.Channels = 1;
            end
            value = obj.Channels;
        end
        
        function set.Channels(obj, value)
            obj.Channels = value;
            obj.UpdateResultSet();
        end
        
        function value = get.SaveWBName(~)
            value = 'Saving Excel file...';
        end
        
        function set.AnalysisResults(obj, value)
            %For now... to maintain compatibility and reduce effoert
            obj.AnalysisResults = value;
            
            for r=obj.AnalysisResults
                r.hParent = obj;
            end
        end
        
        function value = get.AnalysisSettings(obj)
            if isempty(obj.AnalysisSettings)
                obj.AnalysisSettings = obj.CreateAnalysisSettings();
            end
            
            value = obj.AnalysisSettings;
        end
    end
    
    methods
        function AddResultset(obj, hResultset)
            % Find resultset with specified channel
            %if exists -> replace
            %otherwise add to end
            idx = find( obj.ResultsetIndex(hResultset.ChannelID) );
            if ~isempty(idx)
                obj.AnalysisResults(idx) = hResultset;
            else
                obj.AnalysisResults = [obj.AnalysisResults, hResultset];
            end     
            
            hResultset.hParent = obj;
        end
        
        function hResultset = GetResultset(obj, channelID)
            try
                ids = arrayfun(@(x) x.ChannelID == channelID, obj.AnalysisResults);
                if isempty(ids) || all(ids == 0)
                    hResultset = obj.CreateResultset(channelID);
                    obj.AddResultset(hResultset);
                else
                    hResultset = obj.AnalysisResults(ids);
                end
            catch
                hResultset = obj.CreateResultset(channelID);
                obj.AddResultset(hResultset);
            end
        end
        
        function result = ResultsetExists(obj, channelID)
            try
                result = any(obj.ResultsetIndex(channelID));
            catch
                result = false;
            end
        end
        
        function idx = ResultsetIndex(obj, channelID)
            try
                idx = arrayfun(@(x) x.ChannelID == channelID, obj.AnalysisResults);
            catch
                idx = [];
            end
        end
        
        function resultset = GetFirstResultset(obj)
            % Returns the first, available resultset, or an empty if none
            % exists
            if isempty(obj.AnalysisResults)
                resultset = [];
            else
                resultset = obj.GetResultset(obj.Channels(1));
            end
        end
        
        function fPath = GetResultsFolder(obj)
            try
                fPath = fullfile(obj.hDataset.GetResultsFolder(), obj.Name);
                
                settings = MGlobalSettings.GetInstance();
                if (settings.CreateResultsDir == true)
                    if ~isfolder(fPath)
                        mkdir(fPath)
                    end     
                end
            catch
                com.common.Logging.MLogManager.Warning("MAnalysisRoot.GetResultsFolder() returned an empty path.");
                fPath = [];
            end
        end
    end
    
    methods
        function ResetRois(obj)
            %ROI reset implies deleting all associated results.
            %Deleting the entire Results-object ensures a clean
            %starting point for the next run.
            
            %Delete ROIs from datastructure, if they are not manually drawn
            if obj.AnalysisType ~= com.Enum.AnalysisType.Manual
                obj.Reset();
            end
        end
        
        function ResetResults(obj)
            %Delete results from datastructure
            if ~isempty( obj.AnalysisResults )
                arrayfun(@(x) x.ResetResults(), obj.AnalysisResults);

                %Delete ROIs from ProjectManager
                hProjMan = com.Management.MProjectManager.GetInstance();
                if ~isempty(hProjMan)
                    hProjMan.DeleteResultNodes(obj.hDataset, obj);
                end
            end
        end
        
        function Reset(obj)
            obj.AnalysisResults = [];

            %Update ProjectManager
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan)
                hProjMan.DeleteRoiParentNodes(obj.hDataset, obj);
                hProjMan.DeleteResultNodes(obj.hDataset, obj);
            end
        end
    end
    
    methods
        function value = ToString(obj, strExclude)%Textual representation of parameters
            if nargin < 2 || isempty(strExclude)
                strExclude = ["GenerateRoiTracePlots", "ID", "hParent", "BeingDeleted", "arrIntersectIDs", ...
                    "AnalysisResults", "SaveWBName", "hDataset", "TreeNode"];
            end
            
            value = ToString@com.common.Data.MSparklesObject(obj, strExclude);
        end
    end
    
    methods(Access=protected)
        function OnFinalizeClone(obj)
            obj.Reset();
        end
    end
    
    methods (Access=private)
        function UpdateResultSet(obj)
            %1) Remove resultsets from AnalysisResults not in the list of channels
            
            try
                if ~isempty(obj.AnalysisResults)
                    toRemove = arrayfun(@(x) ~ismember(x.ChannelID, obj.Channels), obj.AnalysisResults);
                    obj.AnalysisResults(toRemove) = [];
                end
            catch
                obj.AnalysisResults = [];
            end
                
            %2) Add resultsets from the list of channels that are not in AnalysisResults
            for channel=obj.Channels
                if ~any(arrayfun(@(x) x.ChannelID == channel, obj.AnalysisResults))
                    %ChannelID has not been found in existing resultset.
                    %Append new MAnalysisResult-object
                    obj.AnalysisResults = [obj.AnalysisResults obj.CreateResultset(channel)];
                end
            end
        end
        
        function val = GetDefaultAnalysisName(obj)
            switch obj.AnalysisType
                case com.Enum.AnalysisType.Dynamic
                    val = "Signal dynamics";
                case com.Enum.AnalysisType.Activity
                    val = "Activity dependent analysis";
                case com.Enum.AnalysisType.Grid
                    val = "ROI grid analysis";
                case com.Enum.AnalysisType.Global
                    val = "Global ROI analysis";
                case com.Enum.AnalysisType.Manual
                    val = "Manual ROI analysis";
                case com.Enum.AnalysisType.EEGSpikeTrainDetection
                    val = "Spike train detector";
                case com.Enum.AnalysisType.CaEegCorrelationEx
                    val = "Advanced correlation analysis";
                otherwise %None, Static,
                    val = "Analysis";
            end
        end
    end
    
    methods(Abstract)
        resultset = CreateResultset(obj, channelID);
        value = CreateAnalysisSettings(obj);
        AnalyzeSignals(obj);
        ExportExcel(obj, outPath);
        ExportGraphs(obj, outPath);
        Edit( obj, hDataset, isNew  );
    end
end

