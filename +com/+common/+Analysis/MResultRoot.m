%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MResultRoot < com.common.Data.MSparklesObject
    %MRESULTROOT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ROIs = [];      % CC-struct, as returned by bwconncomp
        ChannelID = 1;  % Scalar. Channel to which these results apply
        Results = [];   % Handle to signals class to hold the actual results.
    end
    
    properties(Access=protected, Dependent, Hidden)
        SaveWBName;
    end
    
    properties(Access=private)
        currExportStep;
        numExportSteps;
    end
    
    properties(Access=protected)
        numSpecialExcelSheets;
    end
    
    properties(Dependent)
        RoiCount;
    end
    
    methods
        function obj = MResultRoot(channelID)
            obj.ChannelID = channelID;
        end
    end
    
    methods
        function set.Results(obj, value)
            obj.Results = value;
            
            try
                obj.Results.hParent = obj;
            catch
            end
            
            obj.OnSetResults();
        end
        
        function set.ROIs(obj, value)
            obj.ROIs = value;
            obj.OnSetROIs();
        end  
        
        function count = get.RoiCount(obj)
            count = obj.CountRois();
        end
        
        function value = get.SaveWBName(~)
            value = 'Saving Excel file...';
        end
    end
    
    methods
        function ExportExcel(obj, outPath)
            try
                obj.InitExportWaitbar();
                
                %Delete if this file already exists to prevent possible
                %remains of older version if table size has changed
                %(smaller than before)
                if isfile( outPath )
                    delete( outPath );
                end
                
                ext = 'xlsx';
                tmpPath = sprintf("%s.%s", tempname, ext);
                                
                hXlsFile = com.CalciumAnalysis.Reporting.MExcelSheet();
                hXlsFile.Open(tmpPath);            
                obj.OnExportExcel(hXlsFile);
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
            
            if ~isempty(hXlsFile) 
                hXlsFile.Close();
                if isfile(tmpPath)
                    movefile(tmpPath, outPath, 'f');
                end
            end
            
            obj.CloseExportWaitbar();            
        end
        
        function ResetRois(obj)
            com.common.Logging.MLogManager.Info("Resetting ROIs for analysis " + obj.hParent.Name + ...
                "(Channel " + obj.ChannelID + ").");
            obj.ROIs = [];
        end
        
        function ResetResults(obj)
            com.common.Logging.MLogManager.Info("Resetting results for analysis " + obj.hParent.Name + ...
                "(Channel " + obj.ChannelID + ").");
            if ~isempty(obj.Results)
                obj.Results = feval(class(obj.Results)+".empty");
            end
        end
        
        function Reset(obj)
            obj.ResetRois();
            obj.ResetResults();            
        end
    end
    
    methods(Access=protected)
        function OnSetResults(~)
        end
        
        function OnSetROIs(obj)
        end
        
        function count = CountRois(obj)
            try
                count = obj.ROIs.NumObjects;
            catch
                count = 0;
            end
        end
        
        function InitExportWaitbar(obj)
            try
                multiWaitbar(obj.SaveWBName, 'Value', 0, 'Color', 'g');
                obj.currExportStep = 0;
                obj.numExportSteps = numel(obj.Results.Thresholds) + 8 + obj.numSpecialExcelSheets;
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                if ~isempty(obj.Results)
                    com.common.Logging.MLogManager.Critical(obj.Results.ToString());
                else
                    com.common.Logging.MLogManager.Critical("Results are empty!");
                end
            end
        end
        
        function UpdateExportWaitbar(obj)
            obj.currExportStep = obj.currExportStep+1;
            multiWaitbar(obj.SaveWBName, 'Value',obj.currExportStep ./ obj.numExportSteps, 'Color', 'g');
        end
        
        function CloseExportWaitbar(obj)
            multiWaitbar(obj.SaveWBName, 'Close');
        end
    end
    
    methods(Abstract)
        AnalyzeSignals(obj, hDataset);
        OnExportExcel(obj, hXlsFile);
    end
end

