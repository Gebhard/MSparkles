%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDatasetRoot < com.common.Data.MSparklesObject
    
    properties(SetAccess = protected)
        Type com.Enum.DatasetType;
        MetaData com.common.Data.MMetadataRoot;          
        FileDate;
        Analyses;
    end
    
    properties(Transient, Dependent)
        DatasetName;
        DatasetPath;        % Path to dataset. This will return either PreProcDatasetPath, or, if this is empty, RawDatasetPath;        
        TimingSignal;       % Get per-sample timing information
        LastSampleTime;
        NumPreProcStages;
    end
    
    properties(SetAccess=protected, SetObservable, Transient)
        IsLoaded;
    end
    
    properties(SetObservable, Transient, Dependent)
        Data;
    end
    
    properties(SetObservable, Transient)
        %Intended to replace the Data propery. 
        %Derived objects instantiate datastores derived from com.common.FileIO.MSparklesDatastore
        DataStore;  
    end

    properties(Transient)
        ProjectFilePath;        %Path where Project file is stored
        TreeNode;               %Handle to the parent treenode in the ProjectManager
    end
    
    properties
        ResultsFolder;          % Path to results folder to store output files
        OrgDatasetPath;         % Path to original dataset
        PreProcDatasetPath;     % Path to dataset after pre-processing
        PreProcConfig;   
    end
    
    methods
        function obj = MDatasetRoot(datasetType, filePath)
            %MDATASETROOT Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.Data.MSparklesObject();
            
            obj.Type = datasetType;            
            obj.OrgDatasetPath = filePath;
            if isfile(filePath) || isfolder(filePath)
                FileInfo = dir(filePath);
                obj.FileDate = FileInfo.date;
            else
                com.common.Logging.MLogManager.Error("File path not valid: '" + filePath + "'");
            end
            obj.PreProcDatasetPath = "";
            obj.PreProcConfig = {};
            
            obj.Analyses = obj.CreateAnalysisManager();
        end
    end
    
    methods(Abstract)
        value = IsReloadRequired(obj);
        success = SavePreProcResult(obj);
        ResetMetadata(obj); %Resets the current data and reloads the metadata from the original dataset file
    end
    
    methods(Access=protected, Abstract)
        success = OnLoadData(obj, filePath, fileName, fileExt);
        hAnalysisManager = CreateAnalysisManager(obj);
        InitDatastore(obj, fPath);
    end        
    
    methods
%         function set.MetaData(obj, value)
%             obj.MetaData = value;
%         end
        
        function value = get.Data(obj)
            value = obj.DataStore.Data;
        end
        
        function set.Data(obj, value)
            obj.DataStore.Data = value;
        end

        function value = get.Analyses(obj)
            if isempty(obj.Analyses)
                obj.Analyses = obj.CreateAnalysisManager();
            end
            
            value = obj.Analyses;
        end
        
        function value = get.DatasetName(obj)
            if ~isempty(obj.OrgDatasetPath) && obj.OrgDatasetPath ~= ""
                [~, value, ~] = fileparts(obj.OrgDatasetPath);
            else
                value = "";
            end
        end
        
        function value = get.DatasetPath(obj)
            value = obj.GetActualDatasetPath();
        end
        
        function value = get.ResultsFolder(obj)
            if isempty(obj.ResultsFolder)
                settings = MGlobalSettings.GetInstance();
                try
                    [fPath, fName,~] = fileparts(obj.OrgDatasetPath);

                    if (settings.CreateResultsDir == true)
                        obj.ResultsFolder = fullfile(fPath, "Results " + fName);

                        if ~isfolder(obj.ResultsFolder)
                            mkdir(obj.ResultsFolder);
                        end
                    else
                        obj.ResultsFolder = fPath;
                    end
                catch
                    obj.ResultsFolder = [];
                end
            end
            
            value = obj.ResultsFolder;
        end
        
        function value = get.TimingSignal(obj)
            value = obj.GetTimingSignal();
        end
        
        function value = get.LastSampleTime(obj)
            ts = obj.TimingSignal;
            if ~isempty(ts)
                value = ts(end);
            else
                value = 0;
            end
        end
        
        function value = get.NumPreProcStages(obj)
            value = numel(obj.PreProcConfig);
        end
        
        function set.Analyses(obj, value)
            obj.Analyses = value;
            obj.Analyses.UpdateParent(obj);
        end
    end
    
    methods
        function strToolTip = GetToolTip(obj)
            strToolTip = "<html><p><b>"+obj.OrgDatasetPath+"</b></p>"+obj.onGetToolTip()+"</html>";
        end
        
        function strToolTipEx = onGetToolTip(~)
            strToolTipEx = "";
        end
        
        function CloneAnalysesFrom(obj, hAnalysisMngr)
            if isa(hAnalysisMngr, "com.CalciumAnalysis.Analysis.AnalysisManager")
                obj.Analyses.CloneFrom( hAnalysisMngr );
            end
        end
        
        function cfg = ClonePreProcCfg(obj)
            numCfg = numel(obj.PreProcConfig);
            cfg = cell(numCfg, 1);
            
            for i=1:numCfg
                cfg{i} = obj.PreProcConfig{i}.Clone();
            end
        end
        
        function Save(project)
            if ~isempty(project.ProjectFilePath)
                save(project.ProjectFilePath, 'project', '-v7.3');
            end
        end
        
        function success = LoadData(obj)
            multiWaitbar('Loading Dataset', 'Busy', 'Color', 'g');
            com.common.Logging.MLogManager.Info("Loading dataset '" + obj.DatasetPath + "' ...");
            try
                if isfile( obj.DatasetPath ) || isfolder( obj.DatasetPath )
                    [fPath, fName, fExt] = fileparts(obj.DatasetPath);
                    success = obj.OnLoadData(fPath, fName, fExt); 
                    com.common.Logging.MLogManager.Info("Loading dataset ... Done!");
                else
                    com.common.Logging.MLogManager.Error("Failed to load dataset '" + obj.DatasetPath + "' ...");
                    success = false; 
                end
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                obj.MetaData = com.common.Data.MMetadataRoot.empty;
                obj.Data = [];
                success = false;
            end            
            
            obj.IsLoaded = success;
            multiWaitbar('Loading Dataset', 'Close');
        end
        
        function Clear(obj)
            delete( obj.DataStore );
            obj.DataStore = [];
            obj.IsLoaded = false;                        
        end
        
        function fPath = GetResultsFolder(obj)
            fPath = obj.ResultsFolder;                                    
    
            settings = MGlobalSettings.GetInstance();
            if (settings.CreateResultsDir == true)
                if ~isfolder(fPath)
                    mkdir(fPath)
                end     
            end
        end                
        
        function value = GetSampleTime(obj, sampleIdx)
            try
                value = obj.TimingSignal(sampleIdx);
            catch
                value = -1;
            end
        end
        
        function [value, idx] = GetNearestSampleTime(obj, sampleTime)
            %Return the nearest sample time and index, based on an (interpolated) time point.
            [~,idx] = min(round(abs(obj.TimingSignal-sampleTime)));
            value=obj.TimingSignal(idx);
        end
    end
    
    %Resetting of data to initial state
    methods        
        function ResetPreProcData(obj, bLoadOrgData)
            %Resets the pre-proc path and reloads the original data as well
            %as the original dataset.
            if nargin < 2
                hMSparkles = MSparklesSingleton.GetInstance();
                isCurrent = ~isempty(hMSparkles) && hMSparkles.CurrentDataset == obj;                                
                
                bLoadOrgData = ~isempty(obj.PreProcDatasetPath) && ...
                                obj.PreProcDatasetPath ~= "" && ...
                                isCurrent;
            end
            
            com.common.Logging.MLogManager.Info("Resetting pre-processed dataset path for dataset " + obj.DatasetName + ".");
            obj.PreProcDatasetPath = [];
            
            com.common.Logging.MLogManager.Info("Restoring original metadata for dataset " + obj.DatasetName + ".");
            obj.ResetMetadata();
            
            obj.Save();
            
            if bLoadOrgData
                obj.LoadData();
            end
        end
        
        function ResetRois(obj)
            %Deletes all ROIs of each analysis. This function is a wrapper
            %for com.common.Analysis.AnalysisManager.ResetRois()
            
            %Update Project manager
            %Update GraphList
            obj.Analyses.ResetRois();
            obj.Save();
        end
        
        function ResetResults(obj)
            %Deletes all results of each analysis. This function is a wrapper
            %for com.common.Analysis.AnalysisManager.ResetResults
            
            %Update Project manager
            %Update GraphList
            obj.Analyses.ResetResults();
            obj.Save();
        end
    end
    
    %Pre-processing stuff
    methods
        function AddPreProcConfig(obj, hPreProcConfig)
            if isempty(obj.PreProcConfig)
                obj.PreProcConfig = {hPreProcConfig};
            else
                obj.PreProcConfig{end+1} = hPreProcConfig;
            end
        end
        
        function value = GetPreProcConfig(obj, idx)
             try
                value = obj.PreProcConfig{idx};
             catch
                value = [];
            end
        end
        
        function RemovePreProcConfig(obj, idx)
            try
                obj.PreProcConfig(idx) = [];
            catch
            end
        end
        
        function ClearPreProcConfig(obj)
            obj.PreProcConfig = com.common.Proc.PreProcConfig.empty;
        end
        
        function value = GetPreProcList(obj)
            if ~isempty(obj.PreProcConfig)
                value = cellfun(@(x) x.Name, obj.PreProcConfig); 
            else
               value = []; 
            end
        end
        
        function value = GetPreProConfigString(obj)
            if ~isempty(obj.PreProcConfig)
                value = cellfun(@(x) x.ToString(), obj.PreProcConfig, 'UniformOutput', false);                 
            else
               value = ""; 
            end
        end
    end
    
    methods(Access=protected)
        function value = GetActualDatasetPath(obj)
            if isempty(obj.PreProcDatasetPath) || obj.PreProcDatasetPath == ""
                value = obj.OrgDatasetPath;
            else
                value = obj.PreProcDatasetPath;
            end
        end
        
        function value = GetTimingSignal(~)
            value = [];
        end
    end
end

