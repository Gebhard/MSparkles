%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MImageDataset < com.common.Data.MDatasetRoot
    %MIMAGEDATASET Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Transient, Dependent, SetObservable)    
        F;                  % Shortcut for Ca-Data. (Moved here for compatibility reasons)        
    end 
    
    properties(Transient, SetAccess=protected)         
        Fmin;
        Fmax;   
    end
    
    methods
        function obj = MImageDataset(datasetType, filePath)
            %MIMAGEDATASET Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.Data.MDatasetRoot(datasetType, filePath);
        end       
    end
    
    methods
        function strToolTipEx = onGetToolTip(obj)
            try
                strToolTipEx = "<p><ul style='list-style-type:none'>" + ...
                    "<li>Bit-depth: "+ obj.MetaData.BitDepth +"</li>" + ...
                    "<li>Frame rate: "+ obj.MetaData.VolumeRate +"</li>" + ...
                    "<li>Duration: "+ obj.GetSampleTime(obj.MetaData.SamplesT) +" sec.</li>" + ...
                    "</ul></p>";
            catch
                strToolTipEx = "";
            end
        end
        
        function Clear(obj)
            Clear@com.common.Data.MDatasetRoot(obj);
            obj.Fmin    = 0;
            obj.Fmax	= 0;
        end
        
        function Crop(obj, bBox)
            % Crop the stack, F, in X and Y direction and update metadata
            % bBox      Bounding rectabgle with Xmin, Ymin, Xmax, Ymax
            obj.MetaData.SamplesX = bBox(3) - bBox(1) + 1;
            obj.MetaData.SamplesY = bBox(4) - bBox(2) + 1;
            obj.F = obj.F(bBox(2):bBox(4), bBox(1):bBox(3), : , : , : , :);  
        end
        
        function UpdateBounds(obj)
            if (~isempty(obj.DataStore.Data))
                [obj.Fmin, obj.Fmax] = bounds(obj.DataStore.Data, "all");                
            else
               obj.Fmin = 0;
               obj.Fmax = 0;
            end
        end
        
        function ResetMetadata(obj, force)
            arguments
                obj(1,1) {mustBeNonempty};
                force (1,1) logical = false;
            end
            
            reset = false;
            
            if isempty(obj.DataStore)
                obj.InitDatastore(obj.DatasetPath);
            end
            
            if ~isempty(obj.DataStore)
                %if isempty(obj.DataStore.Metadata)|| force
                    if ~isempty(obj.OrgDatasetPath) && isfile(obj.OrgDatasetPath) ||...
                            isfolder(obj.OrgDatasetPath) 
                        obj.MetaData = obj.DataStore.readmetadata(obj.OrgDatasetPath);
                        reset = true;
                    else
                        com.common.Logging.MLogManager.Error("Cannot read metadata from path: '" + ...
                            obj.OrgDatasetPath + "'");
                    end
                %else
                    %obj.MetaData = obj.DataStore.Metadata;
                    %reset = true;
                %end
            end
            
            if reset
                obj.ResetResults();
                obj.ResetRois();
                obj.ResetF0();
                obj.PreProcDatasetPath = "";
                obj.bRequiresReload = true;
            end
        end
        
        function ReadExtendedMetadata(obj, force)
            arguments
                obj(1,1) {mustBeNonempty};
                force (1,1) logical = false;
            end
            
            if  isempty(obj.DataStore.Metadata) ||...
                    isempty(obj.DataStore.Metadata.ExtendedMetaData) ||...
                    force
                if ~isempty(obj.OrgDatasetPath) && isfile(obj.OrgDatasetPath)
                    obj.MetaData.ExtendedMetaData = obj.DataStore.readmetadataex(obj.OrgDatasetPath);
                else
                    com.common.Logging.MLogManager.Error("Cannot read extended metadata from path: '" +...
                        obj.OrgDatasetPath + "'");
                end
            else
                obj.MetaData.ExtendedMetaData = obj.DataStore.Metadata.ExtendedMetaData;
            end
        end
    end
    
    methods
        function value = get.F(obj)
            if isempty(obj.DataStore)
                value = [];
            else
                value = obj.DataStore.Data;
            end
        end
        
        function set.F(obj, value)
            obj.DataStore.Data = value;
            obj.UpdateBounds();
        end
    end
    
    methods(Access=protected)
        function InitDatastore(obj, fPath)
            try
                obj.DataStore = com.common.FileIO.TiffDataStore(fPath);
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                obj.DataStore = [];
            end
        end
        
        function success = OnLoadData(obj, filePath, fileName, fileExt)
            fPath = sprintf('%s%s', fullfile(filePath, fileName), fileExt);
            
            success = false;
            
            if isfile(fPath) || isfolder(fPath)
                if isempty(fileExt) || ismember(fileExt, [".tif", ".tiff"])
                    obj.InitDatastore(fPath);
                    obj.DataStore.readall(); 

                    if isempty(obj.MetaData)
                        obj.MetaData = obj.DataStore.Metadata;
                    end

                    obj.DataStore.Data = reshape(obj.DataStore.Data, obj.MetaData.NumSamples);
                    obj.UpdateBounds();
                    success = true;
                end
            end  
            
            if ~success
                obj.F = [];
                obj.MetaData = [];
                com.common.Logging.MLogManager.Error(sprintf('Unsupported file format for file: "%s"', fPath));       
            end
        end                
    end
end

