%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSparklesObject < com.common.Cloneable
    %MSPARKLESOBJECT base object for MSparkles classes
    %   Provides a unique identifyer and a transient handle to it's parent
    %   object. Parent handles must be set at runtime (when creating or
    %   loading an object). This prevents problems with saving cyclic
    %   dependencies.
    
    properties(SetAccess = {?com.common.Cloneable})
        ID;   %UUID to identify this object
    end
    
    properties
        Name = "";
    end
    
    properties(Transient)
        hParent;    %Handle to parent object. Has to be set at runtime. DO NOT SAVE to avoid cyclic dependencies
    end
    
    properties(SetAccess=protected, SetObservable)
        BeingDeleted;
    end
    
    methods
        function obj = MSparklesObject(hParent)
           obj.ID = com.lib.GetUUID();
           
           if nargin == 1
            obj.hParent = hParent;
           end
        end
    end
    
    methods
        function value = get.Name(obj)
            if isempty(obj.Name)
                value = "";
            else
                value = obj.Name;
            end
        end
    end
    
    methods        
        function value = ToString(obj, strExclude)%Textual representation of parameters
            prop = metaclass(obj);

            if nargin < 2 || isempty(strExclude)
                strExclude = ["ID", "hParent", "BeingDeleted"];
            end
            
            value = strings(1, 1);
            
            for iprop = 1:length(prop.PropertyList)
                p = prop.PropertyList(iprop);

                if strcmp(p.GetAccess, "public") && ...
                        ~p.Constant && ~p.Dependent && ~p.Hidden
                    %Leave out internal management stuff
                    if ~any(strcmpi(p.Name, strExclude))
                        if ~strcmpi(p.Name, "name")
                             try
                                 s = obj.(p.Name).ToString();
                                 value = cat (1, value, "    " + p.Name + ": " + string(s));
                             catch
                                try
                                    value = cat(1, value, "	   " + p.Name + ": " + string(obj.(p.Name)));
                                catch
                                    value = cat(1, value, "    " + p.Name + ": ");
                                end
                             end    
                        else
                            if ~isempty(obj.(p.Name))
                                value(1) = p.Name + ": " + string(obj.(p.Name));
                            else
                                value(1) = p.Name + ": ";
                            end
                        end
                    end
                end
            end

            %remove empty lines
            value(value == "") = [];
        end
    end
end

