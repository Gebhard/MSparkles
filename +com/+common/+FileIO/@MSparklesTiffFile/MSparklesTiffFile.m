%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Sealed) MSparklesTiffFile < com.common.FileIO.MSparklesImageFile
    %MSPARKLESTIFFFILE Class to open TIFF filed from ScanImage and ImageJ.
    %   This class first tries to open a TIFF file using the ScanImage TIFF
    %   reader. If this fails, the tiff file will be opend using libTIFF.
    %   In the latter case, a Format, compatible to ImageJ's tiff files is
    %   expected.
    %   This class also provides basic functionality to write tiff files.
    
    properties
        hFile;  %Handle to the open TIFF file. Either a ScanImage tiff file handle, or a file handle from libTIFF.
        ROIs;   %In case of an ImageJ file with embedded ROIs, they can be accessed via this property.
    end
    
    methods(Static)
        %Static methods to load file data
        function [imageData, metaData] = LoadFile(filePath)
            hTiff = com.common.FileIO.MSparklesTiffFile(filePath);
            hTiff.Load();
            imageData = hTiff.FileData;            
            metaData = hTiff.MetaData;
            hTiff.Close();
        end
        
        function metaData = LoadMetadata(filePath)
            hTiff = com.common.FileIO.MSparklesTiffFile(filePath);
            metaData = hTiff.ReadMetadata();
            hTiff.Close();
        end
        
        function metaData = LoadExtendedMetadata(filePath)
            hTiff = com.common.FileIO.MSparklesTiffFile(filePath);
            hTiff.Open();
            
            if hTiff.IsOpen()  
                try
                    [header, ~] = ScanImageTiffReader.getHeaderData(hTiff.hFile); 
                    metaData = com.CalciumAnalysis.Data.M2PSiMetaData(header);
                    
                    if ~isempty(fieldnames(header))
                        verInfo = ScanImageTiffReader.getSITiffVersionInfo(header);
                        metaData.ScanImageVersion = verInfo.SI_MAJOR+"."+verInfo.SI_MINOR;
                    end                                        
                catch                    
                end
            end
            hTiff.Close();
        end
    end
    
    methods
        function obj = MSparklesTiffFile(filePath)
            obj = obj@com.common.FileIO.MSparklesImageFile(filePath);
        end                
    end
    
    methods
        function r = get.ROIs(obj)
            %Try reading inofficial file tag 50839 from ImageJ and import
            %ROI coordinates.
            try
                metaData = imfinfo(obj.FilePath);                                                  
                
                for i=1:numel(metaData)    
                    for m=1:numel(metaData(i).UnknownTags)
                        if (metaData(i).UnknownTags(m).ID == 50839)
                          r = metaData(i).UnknownTags(m).Value;
                          return
                        end
                    end
                end
            catch
            end
            r = [];
        end
    end
    
    methods
        function success = Open(obj)
            %Open closes the file handle if this is already open and resets
            %all file pointers. In case of already existing metadata, it is
            %already known wheather this is an ScanImage or ImageJ (legacy)
            %tiff file and file contennts are read accordingly. 
            %Otherwise, Open() first tries to read ScanImageMetaData to see
            %if the file is a ScanImage file. If this fails, it is opend
            %using libTIFF.
            if obj.IsOpen()
                obj.Close();
            end
            
            %Ensure we have valid metadata
            if isempty(obj.MetaData)
                isLegacy = false;
            else
                isLegacy = obj.MetaData.IsLegacyTiff;
            end

            try
                if isLegacy
                    obj.hFile = Tiff(obj.FilePath, 'r');
                else
                    obj.hFile = ScanImageTiffReader.ScanImageTiffReader(obj.CFilePath);
                    
                    %Fallback if something went wrong
                    %-> SIReader fails to read metadata from ordinary tiff
                    if isempty(obj.hFile.metadata)
                        warning("off","imageio:tiffmexutils:libtiffWarning");
                        obj.hFile = Tiff(obj.FilePath, 'r');
                        warning("on","imageio:tiffmexutils:libtiffWarning");
                    end
                end
            catch
                try
                    obj.hFile = Tiff(obj.FilePath, 'r');
                catch ex
                    com.common.Logging.MLogManager.Error("Opening file '" + obj.FilePath + "' failed.");
                    com.common.Logging.MLogManager.Exception(ex);
                    obj.hFile = [];
                end
            end
                                        
            success = obj.IsOpen();
        end
        
        function result = IsOpen(obj)
            result = ~isempty(obj.hFile);
        end
        
        function Close(obj)
            if obj.IsOpen()
                obj.hFile.close();
            end            
            
            obj.hFile = [];
        end
        
        function data = ReadData(obj)
            if ~obj.IsOpen()
                obj.Open();
            end
            
            if obj.IsOpen()
                
                if obj.MetaData.IsLegacyTiff                                                           
                    data = obj.ReadLegacy();
                else                    
                    data = single(obj.hFile.data());

                    N = ndims(data);
                    d = [2,1]; %Transpose X and Y

                    %Keep the other dimensions as they are
                    for n=3:N
                        d(n) = n;
                    end

                    data = single(permute(data , d)); 
                end
            else
                data = [];
            end
        end
        
        function metaData = ReadMetadata(obj)
            if ~obj.IsOpen()
                obj.Open();
            end                        
                        
            metaData = com.CalciumAnalysis.Data.MSparklesMetadata(obj);    
            obj.MetaData = metaData;
        end
        
        function ImportRois(obj, hTreeNode)
            if ~isempty(obj.ROIs)
                tmpName = [tempname,'.tmp'];

                fileID = fopen(tmpName,'w+');
                fwrite(fileID, obj.ROIs, 'uint8', 'ieee-be');    
                fclose(fileID);

                t = fileread(tmpName);

                delimiter = 'Iout';
                tok = strsplit(t, delimiter);

                numTok = numel(tok) - 2 ;

                if numTok > 1
                    fNames = cell(numTok, 1);

                    for i=1:numTok
                        tmpName = [tempname,'.roi'];
                        fNames{i} = tmpName;

                        fileID = fopen(tmpName,'w');
                        fwrite(fileID, ['Iout'  tok{i+1}], 'uint8', 'ieee-be'); 
                        fclose(fileID);
                    end

                    zipfilename = [tempname,'.zip'];
                    zip(zipfilename,fNames);
                    
                    ijRois = ReadImageJROI(zipfilename);
                    ImportImageJRois(hTreeNode, ijRois);
                    
                    delete(tmpName); 
                    delete(zipfilename);

                    for fname=fNames
                        delete (fname{:});
                    end
                end              
            end
        end
    end  
    
    methods
        success = SaveRGBTiff(obj, filePath);
        success = SaveBigTiff(obj, filePath);
    end
    
    methods (Access=protected)
        function [BitDepth, sampleFormat, SampleSize] = getSampleFormat(obj)
            %Based on the data type of FileData, this function returns the
            %bit-depth, sample format and sample size for writing tiff
            %files.
            switch( class(obj.FileData) )
                case {'int8'}
                    BitDepth = 8; 
                    sampleFormat = 2;
                case {'uint8'}
                    BitDepth = 8; 
                    sampleFormat = 1;
                case {'int16'}
                    BitDepth = 16;
                    sampleFormat = 2;
                case {'uint16'}
                    BitDepth = 16;
                    sampleFormat = 1;
                case {'int32'}
                    BitDepth = 32;
                    sampleFormat = 2;
                case {'uint32'}
                    BitDepth = 1;
                    sampleFormat = 32;
                case {'single', 'double'}   %TIFF can't handle double...
                    BitDepth = 32;
                    sampleFormat = 3;
                otherwise
                    BitDepth = 8;
                    sampleFormat = 1;
            end
            
            SampleSize = size(obj.FileData);                                    
            
            if numel(SampleSize) < 3
                SampleSize(3) = 1;
            end            
        end
        
        function tagstruct = prepareTagStruct(obj)
            %Prepare the tag struct for writing with mandatory parameters.
            
            [BitDepth, sampleFormat, sampleSize] = obj.getSampleFormat();
            
            tagstruct.ImageLength = sampleSize(1);
            tagstruct.ImageWidth = sampleSize(2);
            tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
            tagstruct.BitsPerSample = BitDepth;
            tagstruct.SampleFormat = sampleFormat;
            tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
            tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
            tagstruct.SamplesPerPixel = sampleSize(3);
            tagstruct.Software = 'MSparkles';
            tagstruct.XResolution = obj.MetaData.ExtentX;
            tagstruct.YResolution = obj.MetaData.ExtentY;
            
            if isempty(tagstruct.XResolution)
                tagstruct.XResolution = 1;
            end
            
            if isempty(tagstruct.YResolution)
                tagstruct.YResolution = 1;
            end
        end
        
        function data = ReadLegacy(obj)  
            %Read non-ScanImage tiff file, by iterating over all tiff
            %directories.
            if ~isa(obj.hFile, "Tiff")
                obj.Open();
            end
            
            mImage = obj.MetaData.SamplesX;
            nImage = obj.MetaData.SamplesY;            
            NumberImages = obj.MetaData.TotalFrameCount;
            data=zeros(nImage,mImage,NumberImages, 'single');
                        
            warning("off","imageio:tiffmexutils:libtiffWarning");
            
            %Ensure we are at the first directory and read it
            obj.hFile.setDirectory(1);
            data(:,:,1) = single(obj.hFile.read());
            
            for i=2:NumberImages
               obj.hFile.nextDirectory();
               data(:,:,i) = single(obj.hFile.read());
            end
            
            warning("on","imageio:tiffmexutils:libtiffWarning");
        end
    end          
end

