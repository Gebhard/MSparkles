%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function success = SaveBigTiff(obj)
    try
        sz = size(obj.FileData);  
        incompleteMetadata = false;
        
        %obj.MetaData.SamplesT = sz(end);
        tagstruct = obj.prepareTagStruct();
        tagstruct.SamplesPerPixel = 1;
        framesWritten = 1; 
        totalFrameCount = obj.MetaData.TotalFrameCount;   
        
        if isempty(obj.MetaData.SamplesT) || obj.MetaData.SamplesT == 0
            obj.MetaData.SamplesT = sz(end);
            incompleteMetadata = true;
        end
        
        if isempty(obj.MetaData.SamplesS) || obj.MetaData.SamplesS == 0
            obj.MetaData.SamplesS = 1;
            incompleteMetadata = true;
        end
        
        if isempty(obj.MetaData.SamplesZ) || obj.MetaData.SamplesZ == 0
            obj.MetaData.SamplesZ = 1;
            incompleteMetadata = true;
        end
        
        if isempty(obj.MetaData.NumChannels) || obj.MetaData.NumChannels == 0
            obj.MetaData.NumChannels = 1;
            incompleteMetadata = true;
        end
        
        if incompleteMetadata
            com.common.Logging.MLogManager.Warning("Incomplete metadata. Setting default values.")
        end
        
        %open file for writing
        TifLink = Tiff(obj.FilePath, 'w8'); %w8 == BigTIFF, w == TIFF(classic)

        multiWaitbar('Saving Image', 'Value', 0, 'Color', 'g');
                                                   
        for t=1:obj.MetaData.SamplesT
            for s=1:obj.MetaData.SamplesS
                for z=1:obj.MetaData.SamplesZ
                    for c=1:obj.MetaData.NumChannels
                        %Update image description
                        try
                            tagstruct.ImageDescription = obj.MetaData.orgFrameDescs{framesWritten};                            
                        catch
                        end
                        
                        TifLink.setTag(tagstruct);
                        TifLink.write(obj.FileData(:,:,c,z,s,t) );
                        TifLink.writeDirectory();
                        multiWaitbar('Saving Image', 'Value', framesWritten/totalFrameCount);
                        framesWritten = framesWritten + 1;
                    end
                end
            end
        end
        success = true;
    catch ex
        com.common.Logging.MLogManager.Exception(ex);
        success = false;
    end
    
    TifLink.close();
    multiWaitbar('Saving Image', 'Close');
end

