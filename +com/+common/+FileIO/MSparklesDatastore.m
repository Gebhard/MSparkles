%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MSparklesDatastore < matlab.io.Datastore
    %MSPARKLESDATASTORE Base class for datastores in MSparkles.
    %This class provides a Data property that holds the data read by the
    %datastore.
    %The readall function stores the read data in this object's Data
    %property.
    
    properties(Transient)
        Data;
        Metadata com.common.Data.MMetadataRoot;
    end
    properties(Access = protected, Transient)
        CurrentFileIndex double;
    end
    
    properties (Access = protected)
        FileSet matlab.io.datastore.DsFileSet
    end
    
    properties(Dependent)
        AlternateFileSystemRoots
        FileCount;
    end
    
    methods
        function obj = MSparklesDatastore(fileSet, altRoots)
            obj.FileSet = fileSet;
            obj.AlternateFileSystemRoots = altRoots;
            obj.CurrentFileIndex = 1;
            obj.reset();
        end
    end
    
    methods
        % Getter for AlternateFileSystemRoots property
        function altRoots = get.AlternateFileSystemRoots(obj)
            altRoots = obj.FileSet.AlternateFileSystemRoots;
        end

        % Setter for AlternateFileSystemRoots property
        function set.AlternateFileSystemRoots(obj, altRoots)
            try
              % The DsFileSet object manages the AlternateFileSystemRoots
              % for your datastore
              obj.FileSet.AlternateFileSystemRoots = altRoots;

              % Reset the datastore
              obj.reset();  
            catch ME
              throw(ME);
            end
        end
        
        function value = get.FileCount(obj)
            value = obj.FileSet.NumFiles;
        end
    end
    
    methods
        function IncrementFileIndex(obj)
            obj.CurrentFileIndex = obj.CurrentFileIndex + 1;
        end
        
        function data = readall(obj)
            %READALL   Attempt to read all data from the datastore.
            %   Returns all the data in the datastore and resets it.
            %   This is the default implementation for the readall method,
            %   subclasses can implement an efficient version of this method
            %   by preallocating the data variable. Subclasses should also
            %   consider implementing a more efficient version of this
            %   method for improved tall array construction performance.
            %   In the provided default implementation, a copy of the 
            %   original datastore is first reset. While hasdata is true,
            %   it calls read on the copied datastore in a loop.
            %   All the data returned from the individual reads should be
            %   vertically concatenatable, and the datatype of the output
            %   should be the same as that of the read method.
            %
            %   See also matlab.io.Datastore, read, hasdata, reset, preview,
            %   progress.
            if obj.FileCount > 1
                multiWaitbar('Reading file(s)', 'color', 'g');
            else
                multiWaitbar('Reading file(s)', 'color', 'g', 'busy');
            end   
            
            obj.reset();
            [data, obj.Metadata] = obj.read();  
            
            while obj.hasdata()
                data = [data; obj.read()]; %#ok<AGROW>
                multiWaitbar('Reading file(s)', 'value', obj.progress);
            end
            
            obj.Data = data;
            
            multiWaitbar('Reading file(s)', 'close');
        end
        
        function tf = hasdata(obj)
            % Return true if more data is available.
            tf = hasfile(obj.FileSet);
        end
        
        function reset(obj)
            % Reset to the start of the data.
            obj.FileSet.reset();
            obj.CurrentFileIndex = 1;
        end
        
        function frac = progress(obj)
            % Determine percentage of data read from datastore
            if obj.hasdata() 
               frac = (obj.CurrentFileIndex-1) / obj.FileSet.NumFiles; 
            else 
              frac = 1;  
            end 
        end
    end
    
    methods(Abstract)
        metadata = readmetadata(obj, fPath);
        metadata = readmetadataex(obj, fPath);       
    end
end

