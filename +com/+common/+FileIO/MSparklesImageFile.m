%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) MSparklesImageFile < handle    
    %MSPARKLESIMAGEFILE Is the base class for image input files.
    %   It Provides read access to file data and meta data. 
    %   Classes derived from MSparklesImageFile must implement the
    %   following public interface:
    %   IsOpen()
    %   Open()
    %   Close()
    %   ReadData()
    %   ReadMetadata()
    %
    %   For details, see below.
    
    properties
        FilePath (1,1) string;  %String, containing the fully qualified path to the file.
        MetaData (1,:) com.common.Data.MMetadataRoot;   %Handle to an object derived from MMetadataRoot to handle image metadata
        FileData;   %Actual file (imgae) contents, typically an N-D matrix.
    end
    
    properties(Dependent)
        CFilePath;  %For limited backwards compatibility, get the file path as character array.
    end
    
    methods
        function obj = MSparklesImageFile(filePath)
            arguments
                filePath (1,1) string {isfile}  %Ensure we have a valid path to a file upon construction
            end

            obj.FilePath = filePath;
            obj.MetaData = com.common.Data.MMetadataRoot.empty;
            obj.FileData = [];
        end   
        
        function delete(obj)
            obj.Close();
        end
    end
    
    methods
        function value = get.CFilePath(obj)
            value = char(obj.FilePath);
        end
    end
    
    methods(Sealed)
        function success = Load(obj)
            %Loading of meta data and image data. Return true on success,
            %otherwise false.
            try
                if ~obj.IsOpen()
                    obj.Open();
                end
                
                if obj.IsOpen()
                    obj.MetaData = obj.ReadMetadata();
                    obj.FileData = obj.ReadData();
                    success = true;
                else
                   success = false; 
                end
            catch
                success = false;
            end
        end        
    end
    
    methods(Abstract)
        %Abstract methods to be implemented by derived classes. These
        %functions depend on specifics of a given file format.
        success = Open(obj);            %Opens the file in FilePath
        result = IsOpen(obj);           %Returns true, if the file in FilePath is open and a valid file handle exists, otherwise false.
        Close(obj);                     %Closes the file and associated file handle.
        data = ReadData(obj);           %Reads the file's image data.
        metaData = ReadMetadata(obj);   %Reads the file#s meta data.
    end  
end