%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef TiffDataStore < com.common.FileIO.MSparklesDatastore           
    % Property to support saving, loading, and processing of
    % datastore on different file system machines or clusters. 
    % In addition, define the methods get.AlternateFileSystemRoots()
    % and set.AlternateFileSystemRoots() in the methods section.    
    
    methods
        function obj = TiffDataStore(location, altRoots)
            arguments
                location(1,:) {mustBeNonempty};
                altRoots = string.empty;
            end
            
            obj = obj@com.common.FileIO.MSparklesDatastore(...
                com.common.FileIO.TiffDataStore.CreateFileSet(location),...
                altRoots);
        end
    end        
    
    methods
        function [data,info] = read(obj)
            % Read data and information about the extracted data.
            if ~obj.hasdata()
                error(  sprintf("No more data to read.\nUse the reset "+...
                        "method to reset the datastore to the start of "+...
                        "the data. \nBefore calling the read method, "+...
                        "check if data is available to read "+...
                        "by using the hasdata method."));
            end

            try
                fileInfoTbl = obj.FileSet.nextfile();
                hTiff = com.common.FileIO.MSparklesTiffFile(fileInfoTbl.FileName); %%Todo: read tiff file

                %When reading a single tiff, this will also read the metadata.
                %When reading a directory with a series of tiff files, the
                %metadata are typically identical and we can save some time if
                %we only read them once.

                if obj.CurrentFileIndex == 1
                    hTiff.ReadMetadata();
                    info = hTiff.MetaData.Clone();
                    if info.SamplesT <= 1
                        info.SamplesT = obj.FileCount;
                    end
                else
                    info = com.common.Data.MMetadataRoot.empty;
                end

                data = single(hTiff.ReadData());

                % Update CurrentFileIndex for tracking progress
                obj.CurrentFileIndex = obj.CurrentFileIndex + 1 ;
            catch ex
                com.common.Logging.MLogManager.Critical("Failed to read file '" + ...
                    fileInfoTbl.FileName + "'");
                com.common.Logging.MLogManager.Exception(ex);
                throw(ex);  
            end
        end

        function tiffStack = readall(obj)
            %READALL   Attempt to read all data from the datastore.
            %   Returns all the data in the datastore and resets it.
            %   This is the default implementation for the readall method,
            %   subclasses can implement an efficient version of this method
            %   by preallocating the data variable. Subclasses should also
            %   consider implementing a more efficient version of this
            %   method for improved tall array construction performance.
            %   In the provided default implementation, a copy of the 
            %   original datastore is first reset. While hasdata is true,
            %   it calls read on the copied datastore in a loop.
            %   All the data returned from the individual reads should be
            %   vertically concatenatable, and the datatype of the output
            %   should be the same as that of the read method.
            %
            %   See also matlab.io.Datastore, read, hasdata, reset, preview,
            %   progress.
            if obj.FileCount > 1
                multiWaitbar('Reading file(s)', 'color', 'g');
            else
                multiWaitbar('Reading file(s)', 'color', 'g', 'busy');
            end   
            
            obj.reset();
            [obj.Data, obj.Metadata] = obj.read();  
            
%             sz = size(data);
%             tiffStack = zeros([sz, obj.FileCount], "single");
%             offset = prod(sz);
%             tiffStack( 1:offset) = single(data);
            
            while obj.hasdata()
%                 idxStart = (obj.CurrentFileIndex-1)*offset+1;
%                 idxEnd = idxStart + offset - 1;
                obj.Data = cat(3, obj.Data, obj.read());
%                 data = obj.read();
%                 tiffStack( idxStart:idxEnd ) = single(data);
                multiWaitbar('Reading file(s)', 'value', obj.progress);
            end
            
            %obj.Data = tiffStack;
            tiffStack = obj.Data;
            
            multiWaitbar('Reading file(s)', 'close');
        end
        
        function metadata = readmetadata(obj, fPath)
            if nargin < 2 || isempty(fPath)
                copyFS = obj.FileSet.copy();
                fileInfoTbl = copyFS.nextfile();
                fPath = fileInfoTbl.FileName;
            end
            
            %if fPath is a folder, read metadata from the first file
            isFolder = isfolder(fPath);
            if isFolder
                fs = com.common.FileIO.TiffDataStore.CreateFileSet(fPath);
                fileInfoTbl = fs.nextfile();
                fPath = fileInfoTbl.FileName;
            end
            
            metadata = com.common.FileIO.MSparklesTiffFile.LoadMetadata(fPath);
            
            if isFolder
                metadata.SamplesT = obj.FileCount;
            end
            
            obj.Metadata = metadata;
        end
        
        function metadataex = readmetadataex(obj, fPath)
            if nargin < 2 || isempty(fPath)
                copyFS = obj.FileSet.copy();
                fileInfoTbl = copyFS.nextfile();
                fPath = fileInfoTbl.FileName;
            end
            
            %if fPath is a folder, read metadata from the first file
            if isfolder(fPath)
                fs = com.common.FileIO.TiffDataStore.CreateFileSet(fPath);
                fileInfoTbl = fs.nextfile();
                fPath = fileInfoTbl.FileName;
            end

            metadataex = com.common.FileIO.MSparklesTiffFile.LoadExtendedMetadata(fPath);
            obj.Metadata.ExtendedMetaData = metadataex;
        end
    end
    
    methods(Static, Access = protected)
        function fs = CreateFileSet(location)
            if isfile(location)
                fs = matlab.io.datastore.DsFileSet(location, "FileExtensions", [".tif", ".tiff"]);
            elseif isfolder(location)
                fs = matlab.io.datastore.DsFileSet(location);
            else
                fs = [];
            end
        end
    end
end

