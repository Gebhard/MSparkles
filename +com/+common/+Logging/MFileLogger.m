%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MFileLogger < com.common.Logging.MLogWriter
    
    properties(Access=private)
       hFile;
       FilePath;       
    end
    
    methods
        function obj = MFileLogger(filePath)
            try
                obj.FilePath = filePath;
                obj.hFile = fopen(obj.FilePath, 'a');
            catch
               obj.hFile = []; 
               warning('Could not open logfile "%s" for writing', obj.FilePath);
            end
        end
        
        function delete(obj)
            try
                fclose(obj.hFile);
            catch                
            end
        end
    end
    
    methods
        function AppendLogMessage(obj, logMessage)
            try
                fprintf(obj.hFile, '%s\n', logMessage.ToString());
            catch
                warning('Failed to append log %s', obj.FilePath);
            end
        end
    end
end

