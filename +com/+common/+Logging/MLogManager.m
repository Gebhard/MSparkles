%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MLogManager < handle
    
    properties(Access=private)
       LogWriterList = {};   %List of available log writer objects 
    end
        
    methods
        function obj = MLogManager(logFilePath)
            try
                if isempty(logFilePath)
                    settings = MGlobalSettings.GetInstance();
                    logFilePath = fullfile(settings.GetLogDir(), sprintf('%s.log',datestr(now, 'yyyy-mm-dd')));
                end            
            
                obj.LogWriterList = cell(1,2);
                obj.LogWriterList(1) = {com.common.Logging.MFileLogger(logFilePath)};
                obj.LogWriterList(2) = {com.common.Logging.MConsoleLogger};   
                obj.LogWriterList(3) = {com.common.Logging.MStatusbarLogger};   
            catch
                obj.LogWriterList = {};
            end
        end
        
        function delete(obj)
            try
                cellfun(@(lw) lw.delete(), obj.LogWriterList);
            catch
            end
        end
    end
    
    methods(Static)
        function Info(msg)
            log = com.common.Logging.MLogManager('');
            log.LogInfo(msg);
        end
        
        function Warning(msg)
            log = com.common.Logging.MLogManager('');
            log.LogWarning(msg);
        end
        
        function Error(msg)
            log = com.common.Logging.MLogManager('');
            log.LogError(msg);
        end
        
        function Critical(msg)
            log = com.common.Logging.MLogManager('');
            log.LogCritical(msg);
        end
        
        function Exception(ex)
            log = com.common.Logging.MLogManager('');
            log.LogException(ex);
        end
        
        function PipelineStart(hDataset)
            log = com.common.Logging.MLogManager('');
            log.LogPipelineStart(hDataset);
        end
        
        function PipelineEnd( hDataset)
            log = com.common.Logging.MLogManager('');
            log.LogPipelineEnd(hDataset);
        end
        
        function StageBegin(hStage, hDataset)
            log = com.common.Logging.MLogManager('');
            log.LogStageBegin(hStage, hDataset);
        end
        
        function StageEnd(stageName, dataSetName)
            log = com.common.Logging.MLogManager('');
            log.LogStageEnd(stageName, dataSetName);
        end
        
        function StageError(stageName, dataSetName, ex)
            log = com.common.Logging.MLogManager('');
            log.LogStageError(stageName, dataSetName, ex);
        end
    end
    
    methods
        function Log(obj, logMessage)
            try
                cellfun(@(lw) lw.AppendLogMessage(logMessage), obj.LogWriterList);
            catch
            end
        end

        function LogInfo(obj, strMessage)
            obj.Log(com.common.Logging.MLogMessage(strMessage, com.Enum.MLogLevelEnum.Info));
        end
        
        function LogWarning(obj, strMessage)
            obj.Log(com.common.Logging.MLogMessage(strMessage, com.Enum.MLogLevelEnum.Warning));
        end
        
        function LogError(obj, strMessage)
            obj.Log(com.common.Logging.MLogMessage(strMessage, com.Enum.MLogLevelEnum.Error));
        end
        
        function LogCritical(obj, strMessage)
            obj.Log(com.common.Logging.MLogMessage(strMessage, com.Enum.MLogLevelEnum.Critical));
        end
        
        function LogException(obj, ex)
            obj.LogCritical(getReport(ex, 'extended'));
        end

        %special processing pipeline logging
        
        function LogPipelineStart(obj, hDataset)
            strMessage = sprintf('Starting processing pipeline with dataset "%s"', hDataset.DatasetName);
            obj.LogInfo(strMessage);
        end        
        
        function LogPipelineEnd(obj, hDataset)
            strMessage = sprintf('Finished processing pipeline with dataset "%s"', hDataset.DatasetName);
            obj.LogInfo(strMessage);
        end
        
        function LogStageBegin(obj, hStage, hDataset)
            strMessage = sprintf("Beginning to process pipeline stage '%s' with dataset '%s'", hStage.Name, hDataset.DatasetName);
            obj.LogInfo(strMessage);
            
            if ~isempty(hStage.StageConfig)
                try
                    strMessageParam = sprintf("Used parameters: %s",hStage.StageConfig.ToString());
                    obj.LogInfo(strtrim(strMessageParam));
                catch
                end                    
            end        
        end
        
        function LogStageEnd(obj, stageName, dataSetName)
            strMessage = sprintf('Processing pipeline stage "%s" with dataset "%s" ended normally',stageName, dataSetName);
            obj.LogInfo(strMessage);
        end
        
        function LogStageError(obj, stageName, dataSetName, ex)
            strMessage = sprintf('Pipeline stage "%s" with dataset "%s" terminated with exception.\n%s',...
                stageName, dataSetName, getReport(ex, 'extended'));
            obj.LogCritical(strMessage);
        end

        function AddLogWriter(obj, logWriter)
            obj.LogWriterList = [obj.LogWriterList logWriter];
        end
    end
end

