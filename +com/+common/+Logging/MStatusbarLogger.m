%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStatusbarLogger < com.common.Logging.MLogWriter
  methods
        function AppendLogMessage(~, logMessage)
            switch logMessage.LogLevel
                case com.Enum.MLogLevelEnum.Info 
                    textCol = [0, 0, 0];
                    bkgndCol = [240/255, 240/255, 240/255];
                case com.Enum.MLogLevelEnum.Warning
                    textCol = [255/255, 106/255, 0];
                    bkgndCol = [240/255, 240/255, 240/255];
                case com.Enum.MLogLevelEnum.Error
                    textCol = [255/255, 0, 0];
                    bkgndCol = [240/255, 240/255, 240/255];
                case com.Enum.MLogLevelEnum.Critical
                    textCol = [255/255, 255/255, 255/255];
                    bkgndCol = [127/255, 0/255, 0/255];
            end
            hMSparkles = MSparklesSingleton.GetInstance();
            hMSparkles.AppWindow.SetStatusBarMessageText(logMessage.ToString(), ...
                'TextColor', textCol, 'BackgroundColor', bkgndCol);
        end
    end
end

