%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef ExecutionPipeline < com.common.Proc.ProcBase
    %EXECUTIONPIPELINE An ExecutionPipeline represents the processing
    %pipeline for a single dataset, i.e. it contains the necessary steps
    %for pre-processing, processing and analysis. It is intended, that
    %individual queues can be configured for each dataset. E.g. if drift
    %correction or deconvolotion are not needed, they can be omitted.
    %Each ExecutionPipeline thereby consists of one or more PipelineStage
    %objects, where each PipelineStage represents one speciffic algorithm
    %with its corresponding parameter set.
    %
    %Pipeline stages on the other hand are organized in one, global
    %ExecutionQueue. 
    
    properties (Access=private)
        PipelineStages = [];
        hDataset;
    end
    
    properties (Dependent)
        DatasetName;
    end
    
    properties (Transient, Dependent)
       numStages; 
    end
    
    methods
        function value = get.DatasetName(obj)
            value = obj.hDataset.DatasetName;
        end
    end
    
    methods
        function value = get.numStages(obj)
            value = numel(obj.PipelineStages);
        end
    end
    
    methods
        function obj = ExecutionPipeline(dataSet)
            obj.hDataset = dataSet;
            obj.Name = sprintf('Processing Dataset %s', obj.hDataset.DatasetName);
        end
        
        function AddPipelineStage(obj, pipelineStage, hCallBack, callbackParams)
            arguments
                obj (1,1) com.common.Proc.ExecutionPipeline {mustBeNonempty} 
                pipelineStage (1,1) {mustBeNonempty}
                hCallBack = [];
                callbackParams = {};
            end
            
            if (isa(pipelineStage, 'com.common.Proc.PipelineStage'))
                pipelineStage.FcnOnExecutionFinished = hCallBack;
                pipelineStage.ParamsOnExecutionFinished = callbackParams;
                obj.PipelineStages = cat(2, obj.PipelineStages, {pipelineStage});
            else
                error("Cannot add object of type '" + class(pipelineStage) + "'. Pipeline stages must be derived from 'com.common.Proc.PipelineStage'.");
            end
        end
        
        function ExecutePipeline(obj)
            try
                obj.CreateStatusBar();
                com.common.Logging.MLogManager.PipelineStart(obj.hDataset);
                
                for i=1:obj.numStages
                    pStage = obj.PipelineStages{i};                 
                    pStage.ExecuteStage(obj.hDataset);  
                    obj.UpdateStatus(i/obj.numStages);                              
                end   
                
                com.common.Logging.MLogManager.PipelineEnd(obj.hDataset);
            catch ex
                try
                    com.common.Logging.MLogManagerStageError(pStage.Name, obj.hDataset.DatasetName, ex);
                catch
                end
                com.common.Logging.MLogManager.Critical(sprintf("Processing of dataset '%s' aborted",obj.hDataset.DatasetName ));
            end
            
            obj.CloseStatusBar();
        end               
                
        function ClearPipeline(obj)
            obj.PipelineStages = [];
        end                
    end
end

