%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef ExecutionQueue < com.common.Proc.ProcBase
    %EXECUTIONQUEUE ExecutionQueue processes all pending
    %ExecutionPipeline-objects, where each ExecutionPipeline represents a
    %DataSet with a set of algorithms defining the way the DataSet is
    %processed.
    %   Detailed explanation goes here
    
    properties(Hidden)
        mProcessingJob = [];
    end
    
    properties
        LogMan com.common.Logging.MLogManager;
        OnExecutionFinished; 
    end
    
    methods (Static)
        function obj = GetInstance()
           persistent instance;
           if (isempty(instance))
              instance = com.common.Proc.ExecutionQueue(); 
           end
           obj = instance;
        end        
        
        function RunPipeline(pipeline)
            com.common.Proc.ExecutionQueue.AddExecutionPipeline(pipeline);
            com.common.Proc.ExecutionQueue.Run();
        end
        
        function AddExecutionPipeline(pipeline)
            theQueue = com.common.Proc.ExecutionQueue.GetInstance();
            com.common.Logging.MLogManager.Info("Adding dataset to processing queue: " + pipeline.DatasetName);
            theQueue.AddToQueue(pipeline);
        end
        
        function SetExecutionFinishedHandler(hExFinished)
            theQueue = com.common.Proc.ExecutionQueue.GetInstance();
            theQueue.OnExecutionFinished = hExFinished;
        end
        
        function Reset()
            theQueue = com.common.Proc.ExecutionQueue.GetInstance();
            theQueue.ResetJob();                        
        end
        
        function Run()
            try                
                theQueue =  com.common.Proc.ExecutionQueue.GetInstance();                
                theQueue.LogMan = com.common.Logging.MLogManager('');
                
                theQueue.RunSequential();
            catch ex
                disp(ex.message);
            end  
            
            theQueue.ResetJob();
            
            try
                if ~isempty(theQueue.LogMan)
                    theQueue.LogMan.delete();
                    theQueue.LogMan = [];                
                end
            catch ex
                disp(ex.message);
            end
        end       
    end
    
    methods (Access=private)
        function obj = ExecutionQueue()
            obj.OnExecutionFinished = [];
            obj.Name = 'Total progress';
        end                
        
        function RunSequential(obj)
            %gcp();

            try
                ExecQueue(obj);
                
                if (~isempty(obj.OnExecutionFinished))
                    obj.OnExecutionFinished();
                end
            catch ex
                obj.LogMan.LogException(ex);
            end
        end
        
        function EndOfSequentialRun(obj)
            if (~isempty(obj.OnExecutionFinished))
                obj.OnExecutionFinished(results);
            end
            
            obj.LogEndOfExecution();
        end
        
        function ResetJob(obj)
            if (~isempty(obj.mProcessingJob))
                
                for i=1:numel(obj.mProcessingJob)
                    delete(obj.mProcessingJob{i})
                end
                    
               obj.mProcessingJob = [];
            end
        end
        
        function AddToQueue(obj, pipeline)
            obj.mProcessingJob = cat(2, obj.mProcessingJob, {pipeline});
        end
    end
    
    methods
        function CreateProgressBar(~, name, varargin)
            multiWaitbar(name, 'Value', 0, 'Color', 'g', varargin{:});
        end
        
        function CloseProgressBar(~, name)
            multiWaitbar(name, 'Close');
        end
        
        function UpdateProgressBar(~, name, value)
            multiWaitbar(name, 'Value', value);
        end
        
        function UpdateProgressBarColor(~, name, color)
            multiWaitbar(name, 'Color', color);
        end
    end 
    
    methods(Access=protected)
        function LogBeginOfExecution(obj)
            obj.LogMan.LogInfo(sprintf("Beginning processing with %d dataset(s) in processing queue", numel(obj.mProcessingJob)));
            hMSparkles = MSparklesSingleton.GetInstance();
            hMSparkles.AppWindow.SetStatusBarInfoText("Processing");
        end
        
        function LogEndOfExecution(obj)
            obj.LogMan.LogInfo(sprintf("Finished processing with %d dataset(s) in processing queue", numel(obj.mProcessingJob)));  
            hMSparkles = MSparklesSingleton.GetInstance();
            hMSparkles.AppWindow.ResetStatusBarInfoText();
        end
        
        function LogExecutionError(obj, err)
            if isa(err, 'MException')
                obj.LogMan.LogCritical(sprintf("Error while processing. Processing aborted!"));
                obj.LogMan.LogException(err);                        
            elseif ischar(err) || isstring(err)
                obj.LogMan.LogError(err);
            end  
        end       
    end
end

function ExecQueue(hQueue)
    persistent isRunning;
    
    try             
        if (isempty(isRunning) || isRunning == false ) 
            isRunning = true;
            hQueue.LogBeginOfExecution();
                        
            progBarName = hQueue.Name;
            
            if (numel(hQueue.mProcessingJob) > 1)
                hQueue.CreateProgressBar(progBarName, 'CancelFcn', @AbortQueue);
            end 
            
            userAbort = false;
            for i=1:numel(hQueue.mProcessingJob)
                if userAbort
                   break;
                else
                    hQueue.mProcessingJob{i}.ExecutePipeline();
                    if (numel(hQueue.mProcessingJob) > 1)
                        hQueue.UpdateProgressBar(progBarName, i/numel(hQueue.mProcessingJob));
                    end
                end
            end

            if (numel(hQueue.mProcessingJob) > 1) 
                hQueue.CloseProgressBar(progBarName); 
            end
            
            hQueue.EndOfSequentialRun();
            isRunning = false;                        
        else
            hQueue.LogExecutionError("Cannot start processing queue, because queue is locked. ##Restarting MSparkles might help.##");
        end
    catch ex   
        hQueue.LogExecutionError(ex);
        isRunning = false;
    end
    
    function AbortQueue(~,~)
        newProgBarName = sprintf('%s (aborting...)', hQueue.Name);
        multiWaitbar(progBarName, 'Relabel', newProgBarName);
        progBarName = newProgBarName;
        
        hQueue.UpdateProgressBarColor(progBarName, 'r');         
        userAbort = true;
        com.common.Logging.MLogManager.Warning("Processing aborted by user.");
    end
end

