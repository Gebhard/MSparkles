%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef LinearUnmixCfg < com.common.Proc.PreProcConfig
    properties
        SpectraNames;
        Spectra; % 700xN matrix, where column n contains the full spectrum of the fluorophore in channel n
        SpectraBounds;% Mx2 Lower and upper bounds as imposed by bandpass filter
    end   
    
    properties(Transient, Dependent)
        Count;
    end
    
    methods
        function obj = LinearUnmixCfg(spectraNames, spectra, spectraBounds)
            obj = obj@com.common.Proc.PreProcConfig("Linear unmixing");
            obj.IsSpatialFilter = true;
            
            if nargin > 0
               obj.SpectraNames = spectraNames;
            else
                obj.SpectraNames = [];
            end
            
            if nargin > 1
               obj.Spectra = spectra;
            else
                obj.Spectra = [];
            end
            
            if nargin > 2
                obj.SpectraBounds = spectraBounds;
            else
                obj.SpectraBounds = [];
            end
        end
    end
    
    methods
        function value = get.Count(obj)
            value = numel(obj.SpectraNames);
        end
    end
    
    methods
        function Add(obj, specName, spec, bounds)
            %specName           Name of the spectrum to add as char array or string
            %spec (optional)    Definition of the spectrum from 300..999 nm
            %                   as 700x1 vector
            %bounds (optional)  Lower and upper wavelength bounds of the
            %                   respective channel
            %
            %Remarks:           
            %If no explicit spectrum is specified, only
            %the name of a spectrum, this function will use the default
            %spectrum with the specified name, as defined in
            %com.lib.EmissionSpectra. If no spectrum with the given name
            %exists, this function will thro an error.
            %
            %If no bounds are specified, this function will use the peak
            %value of the specified spectrum +/- 10 nm as upper and lower
            %bounds, respectively.
            %
            %A new spectrum will only be added it it is not already in the
            %list, i.e. an entry with the same name.
            
            if isempty(obj.SpectraNames) || isempty(find(obj.SpectraNames == specName, 1))
                es = com.lib.EmissionSpectra;
                
                if nargin == 2
                    spec = es.Spectra.(specName);
                end

                if nargin < 3
                    [~, idx] = max(spec);
                    lambda = es.Wavelength(idx);
                    bounds = [lambda-10, lambda+10];
                end

                obj.SpectraNames = [obj.SpectraNames, string(specName)];
                obj.Spectra = [obj.Spectra, spec];
                obj.SpectraBounds = [obj.SpectraBounds; bounds];  
            end
        end
        
        function Remove(obj, specName)
            idx = find(obj.SpectraNames == specName);
            if ~isempty(idx)
                obj.SpectraNames(idx) = [];
                obj.SpectraBounds(idx,:) = [];
                obj.Spectra(:,idx) = [];
            end
        end
        
        function spec = GetSpectrum(obj, specName)
            if nargin < 2 || isempty(specName)                
                spec = [];
                return;
            end
            
            idx = find(obj.SpectraNames == specName);
            if ~isempty(idx)
                spec = obj.Spectra(:, idx);
            else
                spec = [];
            end            
        end
        
        function specBounds = GetBounds(obj, specName)
            if nargin < 2 || isempty(specName)                
                specBounds = [];
                return;
            end
            
            idx = find(obj.SpectraNames == specName);
            if ~isempty(idx)
                specBounds = obj.SpectraBounds(idx,:);
            else
                specBounds = [];
            end            
        end
        
        function SetBounds(obj, specName, specBounds)
            if nargin < 3 || isempty(specName) || isempty(specBounds)             
                return;
            end
            
            idx = find(obj.SpectraNames == specName);
            if ~isempty(idx)
                obj.SpectraBounds(idx,:) = specBounds;
            end
        end
        
        function ratios = GetUnmixSpectraRatios(obj)
            ratios = zeros(obj.Count);
            
            %Normalize
            refSpecN = obj.Spectra ./ max(obj.Spectra);         % Normlaize reference spectra
            refSpecN(isnan(refSpecN)) = 0;
            
            szR = size( refSpecN );
            
            es = com.lib.EmissionSpectra;
            
            for i=1:obj.Count
                spec = zeros(szR);
                
                boundsL = obj.SpectraBounds(i,:);
                idxBounds = find( max(es.Wavelength == boundsL, [], 2));
                
                spec(idxBounds(1):idxBounds(2), : ) = refSpecN(idxBounds(1):idxBounds(2), : );                 
                spec = spec ./ sum(spec, 'all');
                
                ratios(i,:) = sum(spec);            
            end
        end
        
        function Edit(obj)
            com.common.UI.dlgLinearUnmix(obj);
        end
    end
    
    methods(Access=protected)
        function hStage = OnCreatePipelineStage(~, ~)
            hStage = com.common.Proc.PStageLinearUnmix();
        end
    end
end

