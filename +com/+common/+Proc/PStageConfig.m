%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef(Abstract) PStageConfig < com.common.Data.MSparklesObject
    %PSTAGECONFIG Basic, named configuration for pipeline stages.
    %   By calling CreatePipelineStage, derived classes are able to create
    %   and return a specialized object, derived from
    %   com.common.Proc.PipelineStage. The base implementation of
    %   CreatePipelineStage ensures, the calling PStageConfig object is
    %   attached to the generated PipelineStage as the stage's
    %   StageConfig-object.    
    
    methods
        function obj = PStageConfig(name)
            %PSTAGECONFIG Construct an instance of this class
            obj.Name = name;
        end
        
        function hStage = CreatePipelineStage(obj,varargin)
            %CREATEPIPELINESTAGE calls the abstract methods
            %OnCreatePipelineStage, which has to be implemented by derived
            %classes. Then, it attaches itself as the stage's StageConfig
            %object.
            try
                hStage = obj.OnCreatePipelineStage(varargin{:});
                hStage.StageConfig = obj;
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
                hStage = [];
            end
        end
        
%         function strrep = ToString(obj)
%             props = properties(obj);
%             numProps = numel(props);
%             strrep = strings( numProps, 1 );
%             
%             for i=1:numProps
%                 strrep(i) = props{i} + ": " + string(obj.(props{i}));
%             end
%             
%             strrep = flipud(strrep);
%         end
    end
    
    methods(Abstract, Access=protected)
        hStage = OnCreatePipelineStage(obj, varargin);
    end
end