%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef PStageLinearUnmix < com.common.Proc.PipelineStage
    %PSTAGELINEARUNMIX Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = PStageLinearUnmix()
            obj = obj@com.common.Proc.PipelineStage('Linear unmixing', '1.0.0', '06.11.2019');   
        end                
    end
    
    methods (Access=protected)        
        function  success = OnExecuteStage(obj, hDataset)
            %Todo: determine type of image, and call ApplyFilter per
            %frame
            for i=1:hDataset.MetaData.SamplesT
                 hDataset.F(:,:,:,:,:,i) = obj.ApplyFilter(hDataset.F(:,:,:,:,:,i));
            end

            hDataset.UpdateBounds();
            success = true;
        end
    end
    
    methods
        function result = ApplyFilter(obj, data)
            %Must work for 2D, 3D
            %Data is expected to be a single image
            %If image stack is to be processed, call this function per
            %frame, or call ExecuteStage
            
            if size(data,3) > 1
                nF = permute(data, [3,1,2,4,5,6]);
                sz = size(nF);

                while numel(sz) < 6
                    sz(end+1) = 1;
                end

                nF = reshape(nF, sz(1), sz(2)*sz(3)*sz(4)*sz(5)*sz(6));

                tF = sum(nF, 1); % get per-pixel total fluorescence
                nF = nF ./ max(nF,[], 1, 'omitnan'); %Normalize measured values
                nF(isnan(nF)) = 0;            

                ratios = obj.StageConfig.GetUnmixSpectraRatios();

                unmixImg = zeros(size(ratios,1), size(nF, 2));
                parfor ii=1:size(nF,2)
                    unmixImg(:,ii) = ratios \ nF(:,ii);
                end

                unmixImg(unmixImg <0) = 0;

                %Unfiltered image
                result = unmixImg .* tF;
                result = reshape(result, sz(1), sz(2), sz(3), sz(4), sz(5), sz(6));
                result = permute(result,  [2,3,1,4,5,6]);
            else
                result = data;
            end
        end
    end
end

