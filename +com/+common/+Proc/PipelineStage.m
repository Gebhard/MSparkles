%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef (Abstract) PipelineStage  < com.common.Proc.ProcBase
    %PIPELINESTAGE A PipelineStage represents an speciffic algorithm with
    %corresponding parameters
    %   For each algorithm, i.e. processing step, derive a class from
    %   PipelineStage. All Steps (stages) to process one DataSet are
    %   organized in one ExecutionPipeline.    
    properties
        StageConfig;
        FcnOnExecutionFinished;     %Handle to a callback function that is run, if 'ExecuteStage' finished successfully
        ParamsOnExecutionFinished;  %Cell-array of parameters to be passed to FcnOnExecutionFinished
    end
    
    properties (Access=private)
        PipelineStage_Version;
        PipelineStage_Date;                 
    end
    
    properties (GetAccess=public, SetAccess=private)
        StageID;
    end        
    
    methods
        function obj = PipelineStage(stageName, stageVersion, stageDate)            
            obj.StageID = dicomuid;            
            obj.Name = stageName;
            obj.PipelineStage_Version = stageVersion;
            obj.PipelineStage_Date = stageDate;   
        end               
        
        function ExecuteStage(obj, dataSet)           
            try     
                obj.CreateStatusBar();
                com.common.Logging.MLogManager.StageBegin(obj, dataSet);
                success = obj.OnExecuteStage(dataSet);
                
                %execute callback, when stage is finished
                try
                    if ~isempty(obj.FcnOnExecutionFinished) && isa(obj.FcnOnExecutionFinished,'function_handle')
                        obj.FcnOnExecutionFinished(obj.ParamsOnExecutionFinished, success);
                    end
                catch ex
                    com.common.Logging.MLogManager.Error("Finalizing pipeline stage failed.");
                    rethrow (ex);
                end
                
                com.common.Logging.MLogManager.StageEnd(obj.Name, dataSet.DatasetName);
                obj.CloseStatusBar();
            catch ex
                obj.CloseStatusBar();
                rethrow (ex);
            end
        end                      
    end
    
    methods(Static)
        function chans = GetRequiredChannels(hDataset)
            chans = [];
                
            for a=1:hDataset.Analyses.Count
                try
                    hAnalysis = hDataset.Analyses.Values{a};
                    chans = cat(1, chans, hAnalysis.Channels(:));                    
                catch
                end
            end

            chans = unique(chans);
        end
    end
    
    %Virtual function to be overloaded by derived classes
    methods (Abstract, Access=protected)
        success = OnExecuteStage(obj, dataSet, params);
    end
end

