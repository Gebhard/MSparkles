%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef (Abstract) Graph < handle
    %GRAPH Base class for Graphs, providing a frame indicator
    
    properties(Access=private, Hidden)
        ax;
    end
    
    properties
        hFrameIndicator;
        CurrFrame = 0;
    end
    
    properties(Dependent)
        hAxis;
        IsAxisValid;
    end
    
    methods
        function hAxis = get.hAxis(obj)
            hAxis = obj.GetAxis();
        end  
        
        function set.hAxis(obj, value)
            obj.ax = value;
        end
        
        function value = get.IsAxisValid(obj)
            value = ~isempty(obj.ax) && isvalid(obj.ax);
        end
        
        function set.CurrFrame(obj, value)
           obj.CurrFrame = value;
           obj.RenderFrameIndicator();
        end
    end
    
    methods
        function delete(obj)
            obj.DeleteAxis();           
            obj.DeleteFrameIndicator();
        end
    end
    
    methods
        function DeleteFrameIndicator(obj)
            if ~isempty( obj.hFrameIndicator )
                delete( obj.hFrameIndicator )
            end
            
            obj.hFrameIndicator = [];
        end
        
        function Save(obj, filePath, format)
            
        end
    end
    
    methods(Abstract)
        Render(obj);
    end
    
    methods(Access = protected)
        function DeleteAxis(obj)
            try
                delete( obj.ax );
            catch
            end
            obj.ax = [];
        end
        
        function RenderFrameIndicator(obj)
            obj.hFrameIndicator = com.lib.Rendering.RenderVerticalLine(obj.hAxis, obj.hFrameIndicator, obj.CurrFrame);
        end                   
        
        function hAxis = GetAxis(obj)
            hAxis = obj.ax;
        end
    end
end

