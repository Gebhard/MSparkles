%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AppWindow < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess=private)
        %Common
        hToolGroup;
        hTabGroup;        
        
        %Property listeners
        hDatasetListener;                   
    end
    
    properties(Access=private)
        topLevelWnd;    % Handle to top-level window of App
        statusBar;      % Handle to status-bar of top-level window
        lblInfo;        % Statusbar label for short info
        lblCurrPos;     % Statusbar label for current mouse pos, when over stack preview 
        lblMsg;         % Statusbar label for messages, e.g. last log message
        progBar;        % Statusbar progressbar for showing top-level progress of current operation
    end
    
    methods
        function obj = AppWindow()
            import matlab.ui.internal.toolstrip.*  % for convenience below
            
            hMSparkles = MSparklesSingleton;
            assignin('base', 'hMSparkles', hMSparkles);          
            hMSparkles.AppWindow = obj;
            pause(0.5);
            
            % Create a new non-visible empty App (Tool Group)
            %obj.hToolGroup = matlab.ui.internal.desktop.ToolGroup('MSparkles');                      
            obj.hToolGroup = com.common.UI.ToolGroupEx('MSparkles', 'MSparkles');
            obj.hToolGroup.setClosingApprovalNeeded(true);

            obj.hToolGroup.setIcon( javax.swing.ImageIcon(fullfile(iconrootdir,"msparkleslogo16x16.png" )) );
            obj.hToolGroup.setContextualHelpCallback(@(es, ed) web('help/index.html'));
            
            obj.hToolGroup.OnClose = @obj.OnCloseApp;
            
            % Display the ToolGroup window
            obj.hToolGroup.open()

            % Create a new tab group            
            obj.hTabGroup = TabGroup();
            
            %Create tab groups    
            obj.CreateStartTab();            

            %Remove the View tab (for now)
            obj.hToolGroup.hideViewTab;
            obj.hToolGroup.addTabGroup(obj.hTabGroup);            
            
            obj.InitDataBrowser();
            obj.InitStatusBar();
            
            %Maximize window
            obj.topLevelWnd.setExtendedState(javax.swing.JFrame.MAXIMIZED_BOTH);
            
            hMSparkles.ProjectManager.LoadProjects();
        end
        
        function delete(obj)
            try
                delete (obj.hDatasetListener);
            catch
            end                                  
            
            try
                delete (obj.hTabGroup);
            catch
            end
            
            try
                delete (obj.hToolGroup);
            catch
            end
            
            MSparklesSingleton.DeleteInstance();        
            multiWaitbar('CloseAll');
        end
    end    
    
    methods
        function AddClientTabGroup(obj, hFigure, hTabGroup)
            obj.hToolGroup.addClientTabGroup(hFigure, hTabGroup);
            obj.topLevelWnd.setStatusBar(obj.statusBar);
            obj.topLevelWnd.setStatusBarVisible(1);
        end    
        
        function RemoveClientTabGroup(obj, hFigure)
            obj.hToolGroup.removeClientTabGroup(hFigure);
        end
        
        function SetStatusBarInfoText(obj, msg, varargin)
            try
                st = dbstack;
                namestr = st.name;
                params = obj.parseStatusBarTextProperties(namestr, varargin{:});

                obj.lblInfo.setText(msg);                                    
                obj.lblInfo.setForeground(params.TextColor);
                obj.lblInfo.setBackground(params.BackgroundColor);
            catch
            end
        end
        
        function ResetStatusBarInfoText(obj)
            obj.SetStatusBarInfoText("Idle");
        end
        
        function SetStatusBarPositionText(obj, msg, varargin)
            try
                st = dbstack;
                namestr = st.name;
                params = obj.parseStatusBarTextProperties(namestr, varargin{:});

                obj.lblCurrPos.setText(msg);                                    
                obj.lblCurrPos.setForeground(params.TextColor);
                obj.lblCurrPos.setBackground(params.BackgroundColor);
            catch
            end
        end
        
        function SetStatusBarMessageText(obj, msg, varargin)
            try
                st = dbstack;
                namestr = st.name;
                params = obj.parseStatusBarTextProperties(namestr, varargin{:});

                obj.lblMsg.setText(msg);
                obj.lblMsg.setForeground(params.TextColor);
                obj.lblMsg.setBackground(params.BackgroundColor);
            catch
            end
        end
        
        function SetStatusBarProgress(obj, value)
            try
                if value < 1
                   value = value * 100; 
                end
                
                obj.progBar.setValue(value)
            catch
            end
        end
        
        function InitStatusbarProgress(obj, lBound, uBound)
            try
                obj.progBar.setMinimum(lBound);
                obj.progBar.setMaximum(uBound);
            catch
            end
        end
        
        function SetStatusBarProgressbarIndeterminate(obj, value)
            try
                obj.progBar.setIndeterminate(value);
            catch
            end
        end
        
        function ResetStatusBarProgressBar(obj)
            try
                obj.SetStatusBarProgressbarIndeterminate(false);
                set(obj.progBar, 'Minimum',0, 'Maximum', 100, 'Value', 0);                 
            catch
            end
        end
    end
    
    %% UI construction
    methods(Access=private)
        function params = parseStatusBarTextProperties(~, callerName, varargin)
            API_version = '1.0.0';
            Modify_date = '30.12.2019';                        

            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding            
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = [callerName  ' v. ' API_version ' (' Modify_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addOptional(p,'TextColor', java.awt.Color.black, @(x) (isnumeric(x) && numel(x) == 3) );
            addOptional(p,'BackgroundColor', [240/255,240/255,240/255], @(x) (isnumeric(x) && numel(x) == 3));
            parse(p,varargin{:});
            params = p.Results;
            
            if ~isa(params.TextColor, 'java.awt.Color')
                params.TextColor = java.awt.Color(params.TextColor(1), params.TextColor(2), params.TextColor(3));
            end
            
            if ~isa(params.BackgroundColor, 'java.awt.Color')
                params.BackgroundColor = java.awt.Color(params.BackgroundColor(1), params.BackgroundColor(2), params.BackgroundColor(3));
            end
        end
        
        function OnCloseApp(~)
            MSparklesSingleton.DeleteInstance();
        end        
                
        function InitDataBrowser(obj)
            hMSparkles = MSparklesSingleton.GetInstance();            
            pojMan = com.Management.MProjectManager([]);  
            hMSparkles.ProjectManager = pojMan;
            
            obj.hDatasetListener = addlistener(pojMan, 'ActiveDataSetNode', 'PostSet', @obj.OnDatasetChanged);                         
                                                        
            jTree = pojMan.GetJavaObj();
            jTree.setPreferredSize(java.awt.Dimension(100, 850));
            
            jPanel = javaObjectEDT('com.mathworks.mwswing.MJPanel');
            jPanel.setLayout(java.awt.BorderLayout());
            jPanel.add(jTree, java.awt.BorderLayout.CENTER);
            jPanel.setPreferredSize(java.awt.Dimension(100, 850));            
            jPanel.add(jTree);

            obj.hToolGroup.setDataBrowser(jPanel);

            jParent = jPanel.getParent();
            obj.topLevelWnd = jParent.getTopLevelAncestor();
        end       
        
        function InitStatusBar(obj)
            try
                if ~isempty(obj.statusBar)
                    delete ( obj.statusBar );
                end
            catch
            end
            
            %statBar = topLevel.getStatusBar(); %Get existing status bar
            obj.statusBar = com.mathworks.mwswing.MJStatusBar; %Create new, custom status bar
            
            obj.lblInfo = javax.swing.JLabel;
            obj.lblInfo.setPreferredSize(java.awt.Dimension(100,16));
            obj.lblInfo.setOpaque(1);
            
            obj.lblCurrPos = javax.swing.JLabel;
            obj.lblCurrPos.setPreferredSize(java.awt.Dimension(350,16));
            obj.lblCurrPos.setOpaque(1);
            
            obj.lblMsg =  javax.swing.JLabel;
            obj.lblMsg.setOpaque(1);
            
            obj.progBar = javax.swing.JProgressBar;
            obj.progBar.setPreferredSize(java.awt.Dimension(100,16));
            %obj.progBar.setStringPainted(1);
            obj.ResetStatusBarProgressBar();

            separator1 =  javax.swing.JSeparator(javax.swing.SwingConstants.VERTICAL);
            separator1.setPreferredSize(java.awt.Dimension(3,16));
            separator2 =  javax.swing.JSeparator(javax.swing.SwingConstants.VERTICAL);
            separator2.setPreferredSize(java.awt.Dimension(3,16));
            separator3 =  javax.swing.JSeparator(javax.swing.SwingConstants.VERTICAL);
            separator3.setPreferredSize(java.awt.Dimension(3,16));
            
            obj.statusBar.add(obj.lblInfo, java.awt.BorderLayout.WEST);
            obj.statusBar.add(separator1, java.awt.BorderLayout.WEST);
            obj.statusBar.add(obj.lblCurrPos, java.awt.BorderLayout.WEST);
            obj.statusBar.add(separator2, java.awt.BorderLayout.WEST);
            obj.statusBar.add(obj.lblMsg, java.awt.BorderLayout.WEST);
            obj.statusBar.add(separator3, java.awt.BorderLayout.EAST); 
            obj.statusBar.add(obj.progBar, java.awt.BorderLayout.EAST);            
            
            obj.topLevelWnd.setStatusBar(obj.statusBar);
            obj.topLevelWnd.setStatusBarVisible(1);
            
            obj.ResetStatusBarInfoText();
        end
        
        function hPopup = CreateHelpPopup(obj)
            import matlab.ui.internal.toolstrip.* 
            
            hPopup = PopupList();
            
             % list item #1
            item = ListItem('About');
            item.Description = 'Show program information.';
            item.ShowDescription = true;
            item.ItemPushedFcn =@obj.onBtnAbout;            
            hPopup.add(item);  
        end
        
        function CreateStartTab(obj)
            import matlab.ui.internal.toolstrip.*    
            icondir = iconrootdir();

            hTabStart = Tab('Start');
            obj.hTabGroup.add(hTabStart);  % add to tab as the last section
            hTabStart.add( CreateDataMgmtButtons() );  % add to tab as the last section                        
            
            %% Settings
            hSecSettings = Section('Settings');
            hTabStart.add(hSecSettings);  % add to tab as the last section
            hColSettings = hSecSettings.addColumn();  
            hColSettings2 = hSecSettings.addColumn();
            
            hBtnGlobalSettings = Button('Global settings', fullfile(icondir, 'gear_large.png'));
            hBtnGlobalSettings.Description = 'Edit globa MSparkles settings.';
            hBtnGlobalSettings.ButtonPushedFcn = @obj.onBtnGlobalSettings;
            hColSettings.add(hBtnGlobalSettings);
            
            hColSettings2.add( CreatePropertyButton() );
            
            hSecSettings = Section('Help');
            hTabStart.add(hSecSettings);  % add to tab as the last section
            hColSettings = hSecSettings.addColumn();
             
            hBtnHelp = SplitButton('Help', fullfile(icondir, 'help.png'));
            hBtnHelp.Description = 'Show help.';
            hBtnHelp.ButtonPushedFcn = @obj.onBtnHelp;
            hBtnHelp.DynamicPopupFcn = @(h,e) obj.CreateHelpPopup(); % invoked when user clicks the drop-down selector widget
            hColSettings.add(hBtnHelp);                                    
        end                                                     
    end
    
    %% UI callback handlers
    methods(Access=private)          
        function OnDatasetChanged(obj, ~, hEvent)
            hProjMan = hEvent.AffectedObject;
            hDatasetNode = hProjMan.ActiveDataSetNode;
            
            %Ensure we have a dataset(node)
            if ~isempty(hDatasetNode)                
                multiWaitbar('Loading UI...', 'Busy', 'Color', 'g');
                try
                    obj.UpdateView(hDatasetNode.Dataset);
                catch ex
                    com.common.Logging.MLogManager.Exception(ex);
                end
                multiWaitbar('Loading UI...', 'Close');
            end            
        end   
        
        function UpdateView(obj, hDataset)
            if ~isempty(hDataset)
                switch (hDataset.Type)
                    case com.Enum.DatasetType.CalciumData
                        if hDataset.IsLoaded
                            com.CalciumAnalysis.UI.CalciumMainWindow.Init(obj, hDataset);
                        end
                        obj.UpdateView(hDataset.EEGDataset);
                    case com.Enum.DatasetType.EegData
                        com.EEG.UI.EegMainWnd.Init(obj, hDataset);
                    case com.Enum.DatasetType.CellcountData
                end
            end
            
            %obj.topLevelWnd.setStatusBar(obj.statusBar);
            %obj.topLevelWnd.setStatusBarVisible(1);
            obj.InitStatusBar();
        end                                                                                                                                                                                                                   
        
        function onBtnGlobalSettings(~, ~, ~)
            com.common.UI.GlobalSettings();

            %ToDo: Change to update current figure(s)
            UpdateCaWnd();
        end                                
        
        function onBtnAbout(~,~,~)
            web('help/sites/about.html');
        end
        
        function onBtnHelp(~,~,~)
            web('help/index.html');
        end
        
    end
end