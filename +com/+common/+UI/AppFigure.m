%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef AppFigure < handle
    %APPFIGURE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Access=private)
        hAppWindow;
    end
    
    properties (SetAccess=private)
        hFigure;
        hTabGroup;
        CurrentKey;
    end
        
    properties(Transient)                
        hDataset;
    end
    
    methods(Abstract, Static)
        Init(hAppWindow, hDataset);
    end
    
    methods(Access=protected)
        function obj = AppFigure(hAppWnd, hDataset, appName)
            %APPFIGURE Construct an instance of this class
            %   Detailed explanation goes here                        
            
            obj.hAppWindow      = hAppWnd;
            obj.hDataset        = hDataset;
            obj.hFigure         = obj.CreateFigure(appName);  
            obj.hFigure.Name    = obj.hDataset.DatasetName;
            
            obj.InitMenu();
            obj.InitFigure(obj.hDataset);
        end
    end
    
    methods
        function delete(obj)
            try                
                obj.hAppWindow.RemoveClientTabGroup(obj.hFigure);
                delete( obj.hTabGroup );
            catch 
            end
            
            try
                %Project manager might be already deleted, if the entire
                %app is being closed
                hProjMan = com.Management.MProjectManager.GetInstance();

                if ~isempty(hProjMan)
                    hProjMan.NotifyCloseDataset(obj.hDataset);
                end
            catch
            end
            
            try
                obj.hDataset.Clear();
            catch
            end
            
           
            
            obj.hAppWindow = [];
            delete( obj.hFigure );
        end
    end   
    
    methods(Access=protected)
        function hFigure = CreateFigure(obj, appName)
            hFigure = figure("Name", appName, ...
                "ToolBar", "none", "MenuBar", "none", ...
                "CreateFcn", @obj.CreateFigureLayout, ...
                "DeleteFcn", @obj.OnDeleteFigure, ...
                "WindowButtonMotionFcn", @obj.OnMouseMove, ...
                "KeyPressFcn", @obj.OnKeyDown, ...
                "KeyReleaseFcn", @obj.OnKeyUp,...
                "WindowScrollWheelFcn", @obj.OnScrollWheel,...
                "Visible", false); 
        end
        
        function OnDeleteFigure(obj, ~, ~)
            obj.delete();
        end
    end        
    
    methods(Access=protected)
        function OnKeyDown(obj, hSrc, hEvent)
            hInst = MSparklesSingleton.GetInstance();
            hInst.CurrentKey = hEvent.Key;
            obj.CurrentKey = hEvent.Key;
        end
        
        function OnKeyUp(obj, hSrc, hEvent)
            hInst = MSparklesSingleton.GetInstance();
            hInst.CurrentKey = [];
            obj.CurrentKey = [];
        end
    end
    
    methods(Access=protected, Abstract)
        OnInitMenu(obj, hTabGroup);        
        CreateFigureLayout(obj, hSrc, hEvent);
        InitFigure(obj, hDataset);
        OnMouseMove(obj, hSrc, hEvent);
        OnScrollWheel(obj, hSrc, hEvent);
        hDataset = GetDefaultAnalysisDataset(obj);
        AppendPipeline(obj, hDataSetNode, finalStage, analyses, forceRecomputeAll);
    end
    
    methods(Access=private)
        function InitMenu(obj)
            obj.hTabGroup = matlab.ui.internal.toolstrip.TabGroup();
            obj.OnInitMenu(obj.hTabGroup);
            obj.hAppWindow.AddClientTabGroup(obj.hFigure, obj.hTabGroup);
            obj.hFigure.Visible = 'on';
        end
    end
    
    methods(Access=protected)
        function run(obj, stage, forceRecomputeAll)
            hProjMan = com.Management.MProjectManager.GetInstance();
            if ~isempty(hProjMan) 
                hSelItem = hProjMan.SelectedNodes;
                numSelectedItems = numel(hSelItem);
                hProcessNode = [];
                
                %We have nothing selected in the tree, try with the current
                %dataset
                if isempty(hSelItem)
                    hProcessNode = obj.GetDefaultAnalysisDataset();
                end
                
                for i=1:numSelectedItems
                    hItem = hSelItem(i);
                    switch (hItem.Type)
                        case com.Enum.TreeNodeTypes.Folder
                            if numSelectedItems == 1
                                answer = questdlg('Process current analysis or selected folder?',...
                                'Analysis', 'Current dataset', 'Selected folder', 'Selected folder');
                            else
                                hProcessNode = cat(1, hProcessNode, hItem);
                            end
                        case com.Enum.TreeNodeTypes.AnalysisMode
                            if  any ( stage == [com.Enum.PipelineStage.RoiDetection; com.Enum.PipelineStage.Analysis])
                                
                                if (~isempty(hProjMan.ActiveAnalysisNode) && hItem ~= hProjMan.ActiveAnalysisNode)...
                                        && numSelectedItems == 1
                                    answer = questdlg('Process current analysis or selected analysis?',...
                                        'Analysis', 'Current analysis', 'Selected analysis', 'Selected analysis'); 
                                else
                                    hProcessNode = cat(1, hProcessNode, hItem);
                                end
                            else
                                if numSelectedItems == 1
                                    hProcessNode = hProjMan.ActiveDataSetNode;
                                end
                            end
                        case {com.Enum.TreeNodeTypes.DataSet, com.Enum.TreeNodeTypes.EEGDataset}
                            if (isempty(hProjMan.ActiveDataSetNode) || hItem ~= hProjMan.ActiveDataSetNode) && numSelectedItems == 1
                                answer = questdlg('Process current analysis or selected dataset?',...
                                    'Analysis', 'Current analysis', 'Selected dataset', 'Selected dataset');                                                                
                            else
                                hProcessNode = cat(1, hProcessNode, hItem);
                            end
                        otherwise
                            if numSelectedItems == 1
                                hProcessNode = obj.GetDefaultAnalysisDataset();
                            end
                    end
                end
                
                try
                    if isempty(hProcessNode)  
                        %Cancel operation
                        if isempty(answer)
                            return;
                        end
                                
                        switch(answer)
                            case 'Current analysis'
                                hProcessNode = hProjMan.ActiveAnalysisNode;
                            otherwise
                                hProcessNode = hSelItem;
                        end
                    end
                catch
                end
                                                
                if isempty(hProcessNode)
                    errordlg('Please load or select a dataset to analyze', 'Cannot analyze empty dataset');
                else
                    
                    ns = numel(stage);
                    msg = strings(ns, 1);
                    for s=1:ns
                        switch stage(s)
                            case com.Enum.PipelineStage.PreProcessing
                                msg(s) = "All data of the selected / current dataset";
                            case com.Enum.PipelineStage.BaselineComputation
                                msg(s) = "F0, ROIs and analysis results of selected / current dataset";
                            case com.Enum.PipelineStage.RoiDetection
                                msg(s) = "ROIs and analysis results of the selected / current dataset";
                            otherwise %case com.Enum.PipelineStage.Analysis
                                if forceRecomputeAll
                                    msg(s) = "All data of the selected / current dataset";
                                else
                                    msg(s) = "All analysis results of the selected / current dataset";
                                end
                        end
                    end
                    
                    if ShowResetWarning(msg)                        
                        com.common.Proc.ExecutionQueue.Reset();
                        
                        numNodes = numel(hProcessNode);
                        for i=1:numNodes
                            hNode = hProcessNode(i);
                            switch (hNode.Type)
                                case com.Enum.TreeNodeTypes.AnalysisMode
                                    hDsNode = hNode.GetDataSetNode();
                                    hAnalysis = hDsNode.Dataset.Analyses.GetAnalysis(hNode.UserData);
                                    obj.AppendPipeline(hDsNode, stage, hAnalysis, forceRecomputeAll);            
                                case {com.Enum.TreeNodeTypes.DataSet, com.Enum.TreeNodeTypes.EEGDataset}
                                    obj.AppendPipeline(hNode, stage, [], forceRecomputeAll);   
                                case com.Enum.TreeNodeTypes.Folder
                                    hProjMan.ProcessAllChildren(hNode, stage, forceRecomputeAll);
                            end
                        end
                        
                        com.common.Proc.ExecutionQueue.Run();
                    end
                end
            end
        end
    end
end

