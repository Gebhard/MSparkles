%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef GraphList < com.common.UI.UiControlEx
    %GRAPHLIST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable, AbortSet=true)
        numVisibleGraphs; 
        SelectedGraphID;
    end
    
    properties
        OnItemSelected;
        ScrollOffset;
    end
    
    properties(Access = protected)
        hGraphList;
        hSlider;
        hRenderFunc;
        hParent;
    end
    
    properties(Dependent)
        ScrollPos;        
        NumGraphs;
        SelectedGraphIdx;
        ParentFigure;        
    end
    
    properties(Access=private)
        hScrollListener;
    end
    
    methods
        function obj = GraphList(hAppFig, hParent, varargin)
            %GRAPHLIST Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.UI.UiControlEx(hAppFig);
            
            GraphList_date = '02.05.2019';
            GraphList_version = '1.0.000';            
            
            p = inputParser;
            p.StructExpand    = true;  % if we allow parameter  structure expanding
            p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
            p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
            p.FunctionName    = ['GraphList v.' GraphList_version ' (' GraphList_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

            addParameter(p,'VisibleGraphs', 5,  @(x) (isnumeric(x) && x > 0 ));    
            addParameter(p,'ScrollOffset', 0,  @(x) (isnumeric(x) && x > 0 ));    
            
            % input parsing
            parse(p,varargin{:});
            par = p.Results;                                      
            
            obj.hParent = hParent;
            obj.numVisibleGraphs = par.VisibleGraphs;
            obj.ScrollOffset = par.ScrollOffset;
            
            scrollsteps = obj.InitScrollRange(par);

            obj.hSlider = uicontrol('Style', 'Slider', 'Parent', hParent, ...
                'Units', 'normalized', 'Position', [0.99 0 0.01 1], 'Value', 1 + obj.ScrollOffset,...
                'Min', 1, 'Max', max(1.1, scrollsteps));    
            
            if scrollsteps == 0
                obj.hSlider.Enable = 'off';
            end            
            
            obj.hScrollListener = addlistener(obj.hSlider, 'Value', 'PostSet', @obj.slider_callback);                         
        end  
        
        function delete(obj)
            obj.ClearGraphList();
            obj.hGraphList = [];            
            
            try
                delete( obj.hSlider );
            catch
            end
            
            try
                delete( obj.hScrollListener );
            catch
            end
        end
    end
    
     %Getters / Setters for propeties
    methods
        function value = get.ParentFigure(obj)
            try
                value = ancestor(obj.hParent, 'Figure');
            catch
                value = [];
            end
        end
        
        function value = get.SelectedGraphIdx(obj)
            value = double(obj.SelectedGraphID) - obj.ScrollPos + 1;
        end
        
        function value = get.NumGraphs(obj)
            if ~isempty(obj.hGraphList)
                value = numel(obj.hGraphList);
            else
                value = 0;
            end
        end
        
        function set.numVisibleGraphs(obj, value)
            if isnumeric(value)
                value = max(value, 0); % ensure its not negative
                obj.numVisibleGraphs = value;
                obj.Update();
            end
        end       
        
        function value = get.ScrollPos(obj)
            try
                pos = round(obj.hSlider.Value);
                value = obj.hSlider.Max - pos + 1; 
            catch
                value = 1;
            end
        end
        
        function set.ScrollPos(obj, value)
            pos = min(max(round(value), obj.hSlider.Min), obj.hSlider.Max);
            obj.hSlider.Value = obj.hSlider.Max - (pos - 1);             
        end
    end
    
    methods
        function ZoomX(obj, xl)
            arrayfun( @(x) x.ZoomX(xl), obj.hGraphList);  
        end
        
        function ZoomY(obj, yl)
            arrayfun( @(x) x.ZoomY(yl), obj.hGraphList);
        end
        
        function Zoom(obj, xl, yl)
             obj.ZoomX(xl);
             obj.ZoomY(yl);
        end
        
        function SelectGraph(obj, graphID)  
            
            obj.SelectedGraphID = graphID;
            
            if (any(graphID > 0))
                %Ensure the last selected element is visible in the
                %graphlist
                lastSel = graphID(end);
                if (lastSel < obj.ScrollPos ||  lastSel > (obj.ScrollPos + obj.NumGraphs -1)) &&...
                        obj.ScrollPos ~= lastSel
                    obj.ScrollPos = lastSel;
                end                                
            end
            
            obj.SelectGraphIdx(obj.SelectedGraphIdx);                     
        end
        
        function Update(obj)
            obj.InitGraphList();
            obj.UpdateScrollRange();
            obj.RenderGraphs(obj.hDataset);
        end
    end
    
    methods(Access=protected)                
        function slider_callback(obj,~,~) 
            try
                obj.RenderGraphs(obj.hDataset);
                obj.SelectGraphIdx(obj.SelectedGraphIdx);
            catch
                obj.InitGraphList();
                obj.UpdateScrollRange();
                obj.SelectGraphIdx(obj.SelectedGraphIdx);
                obj.RenderGraphs(obj.hDataset);
            end                                    
        end
    end
    
    methods(Access=protected)
        function OnItemClicked(obj, hItem)
            
            hInst = MSparklesSingleton.GetInstance();
            if ~isempty(hInst.CurrentKey) && hInst.CurrentKey == "control"
                idx = obj.SelectedGraphID == hItem.AbsGraphIdx;
                clickedGraphID = obj.SelectedGraphID;
                if any(idx > 0)
                    clickedGraphID(idx) = [];
                else
                    clickedGraphID = cat(2, clickedGraphID, hItem.AbsGraphIdx);
                end
                
            else
                clickedGraphID = hItem.AbsGraphIdx;
            end
            
            obj.SelectGraph(clickedGraphID);
            
            if ~isempty(obj.OnItemSelected)
                obj.OnItemSelected(obj, obj.SelectedGraphID);
            end
        end
        
        function SelectGraphIdx(obj, graphIdx)
            arrayfun(@(x) x.Select(ismember(x.itemNo,graphIdx)), obj.hGraphList);    
        end                                
        
        function ClearGraphList(obj)
            if ~isempty(obj.hGraphList)                
                for i=1:numel(obj.hGraphList)
                    if ~isempty(obj.hGraphList(i))
                        try
                            delete( obj.hGraphList(i) );
                        catch
                        end
                    end
                end
                
                obj.hGraphList = [];
            end
        end
        
        function InitGraphList(obj)
            try
                obj.ClearGraphList();
                obj.OnInitGraphList();
            catch ex
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
    end
    
    methods(Access = protected, Abstract)
        OnInitGraphList(obj);
        UpdateScrollRange(obj);
        scrollsteps = InitScrollRange(obj, params);
    end
    
    methods(Abstract)
        RenderGraphs(obj);
    end
end

