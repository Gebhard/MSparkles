%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef GraphListItem < com.common.Rendering.Graph
    %GRAPHLISTITEM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        itemNo;
        numItems;
        Selected logical;
        AbsGraphIdx;
    end
    
    properties(Access = protected)
        hParent;
        hGraphList;
        itemPanel; 
        ClickHandler;     
    end
    
    properties(Dependent, Access=protected)
        ParentFigure;
    end
   
    methods
        function obj = GraphListItem(hGraphLst, parent, itemNo, numItems, ClickHandler)
            obj.hGraphList = hGraphLst;
            obj.itemNo = itemNo;
            obj.numItems = numItems;
            obj.ClickHandler = ClickHandler;
            obj.hParent = parent;
            
            itemHeight = 1 / obj.numItems;                
            obj.itemPanel = uipanel('Parent', parent, 'Units', 'normalized', 'Position', [0 (1-(obj.itemNo*itemHeight)) 0.99 itemHeight]);                
            obj.itemPanel.ButtonDownFcn = @obj.OnMouseClick;                                                           
        end  
        
        function delete(obj)
            delete(obj.itemPanel);
            obj.itemPanel = [];
            obj.ClickHandler = [];
        end
     end
    
    methods
        function value = get.ParentFigure(obj)
            try
                value = ancestor(obj.hParent, 'Figure');
            catch
                value = [];
            end
        end                
        
        function set.Selected(obj, value)
            obj.Selected = value;   
            obj.OnSelectionStatusChanged();            
        end                   
    end
     
    methods
        function ZoomX(obj, xl)
            try
                obj.hAxis.XLim = xl;   
            catch
            end
        end
        
        function ZoomY(obj, yl)
            try
                obj.hAxis.YLim = yl;   
            catch
            end 
        end
        
        function Select(obj, isSelected)
            obj.Selected = isSelected;
        end
    end
    
    methods(Access=protected)
        function hAxis = CreateAxes(obj)
            hAxis = axes(obj.itemPanel, 'Position', [.075 0.175 .91 .725]);
            hAxis.ButtonDownFcn = @obj.OnMouseClick;
        end
        
        function hAxis = GetAxis(obj)
            if ~obj.IsAxisValid  
                obj.hAxis = obj.CreateAxes();                
            end
            hAxis = GetAxis@com.common.Rendering.Graph(obj);
        end
        
        function OnMouseClick(hObject, ~, ~)
            try
                if ~isempty(hObject.ClickHandler)
                    hObject.ClickHandler(hObject);
                end
            catch
            end
        end
    end
    
    methods(Access=private)
        function OnSelectionStatusChanged(obj)      
            obj.UpdateBgColor();
        end
        
        function UpdateBgColor(obj)
            if isvalid(obj.hAxis)
                if obj.Selected == true
                    obj.hAxis.Color = [201/255 232/255 255/255];
                else
                    obj.hAxis.Color = [1 1 1];
                end
            end
        end        
    end
    
    methods(Abstract)
        Render(obj, hDataSet, hResultset, plotIdx);
    end
    
    methods(Access = protected, Abstract)
        RenderGraph(obj, hDataSet, hResultset, plotIdx); 
    end
end

