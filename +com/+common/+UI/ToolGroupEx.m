%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef ToolGroupEx < matlab.ui.internal.desktop.ToolGroup
    
    properties
        OnClose;
    end
    
    properties (Access=private)
        CAListener;
        GAListener;
    end
    
    methods
        function obj = ToolGroupEx(apptitle, appname)
            obj = obj@matlab.ui.internal.desktop.ToolGroup(apptitle, appname); 
            
            obj.CAListener = addlistener(obj, 'ClientAction', @(hSrc,hData) onClientAction(obj,hData));
            obj.GAListener = addlistener(obj, 'GroupAction', @(hSrc,hData) onGroupAction(obj,hData));
        end
        
        function delete(obj)
            obj.OnClose = [];
            obj.CAListener.delete();
            obj.GAListener.delete();
        end                
    end
    
    methods(Access=private)
        function onClientAction(obj, hData)
            switch (hData.EventData.EventType)
                case 'CLOSING'
                    if strcmpi(hData.EventData.ClientTitle, 'Data Browser')
                        if ~isempty(obj.OnClose)
                            obj.OnClose();
                        end
                    end
            end
        end
        
        function onGroupAction(obj, hData)
            switch (hData.EventData.EventType)
                case 'CLOSING'
                    
                    if obj.isClosingApprovalNeeded
                        answer = questdlg("Do you really want to exit MSparkles?", "Confirm exit",...
                            "Yes", "No", "No");
                        
                        if answer == "Yes"
                            obj.approveClose;
                        else
                            obj.vetoClose;
                        end
                    end
                    
            end
        end
    end
end

