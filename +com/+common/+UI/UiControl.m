%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef UiControl < handle
    %UICONTROL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess=protected)
        hAppFigure;         %Handle to parent app figure
    end
    
    properties(Dependent)
        hDataset;
    end
    
    methods
        function obj = UiControl(hAppFig)
            %UICONTROL Construct an instance of this class
            %   Detailed explanation goes here
            
            if nargin == 1
                obj.hAppFigure = hAppFig;
            end
        end
        
        function delete(obj)
            obj.hAppFigure = [];
        end
    end
    
    methods
        function value = get.hDataset(obj)
            if ~isempty(obj.hAppFigure)
                value = obj.hAppFigure.hDataset;
            else
               value = []; 
            end
        end
    end            
end

