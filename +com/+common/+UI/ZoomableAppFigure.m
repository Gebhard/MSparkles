%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef ZoomableAppFigure < com.common.UI.AppFigure
    %ZOOMABLEAPPFIGURE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=protected)
        hBtnZoomIn;
        hBtnZoomOut;
        hBtnPan;
        
        hZoom;
        hPan;
    end
    
    methods
        function obj = ZoomableAppFigure(hAppWindow, hDataset, appName, zoomDir)
            %ZOOMABLEAPPFIGURE Construct an instance of this class
            %   Detailed explanation goes here
            obj = obj@com.common.UI.AppFigure(hAppWindow, hDataset, appName);
            
            if nargin < 4
                zoomDir = "horizontal";
            end
            
            obj.hZoom = zoom(obj.hFigure);            
            obj.hZoom.Motion = zoomDir;
            obj.hZoom.Enable = "off";
            obj.hZoom.ActionPostCallback = @obj.OnZoom;
            
            obj.hPan = pan(obj.hFigure);
            obj.hPan.Motion = zoomDir;
            obj.hPan.Enable = "off";
            obj.hPan.ActionPostCallback = @obj.OnPan;
        end
        
        function delete(obj)
            delete( obj.hZoom );
            delete( obj.hPan );
        end
    end
    
     methods(Access=protected)
        function OnZoom(~, ~, hEvent)
            try
                hGraphList = hEvent.Axes.Parent.Parent.UserData;
                hGraphList.ZoomX( hEvent.Axes.XLim );
            catch
            end
        end
        
        function OnPan(~,~,hEvent)
            try
                hGraphList = hEvent.Axes.Parent.Parent.UserData;
                hGraphList.ZoomX( hEvent.Axes.XLim );
            catch
            end
        end
        
        function CreateZoomButtons(obj, hColumn)
            import matlab.ui.internal.toolstrip.*
            icondir = iconrootdir();
            
            obj.hBtnZoomIn = ToggleButton('Zoom in', fullfile(icondir, 'zoomIn.png'));           
            obj.hBtnZoomIn.ValueChangedFcn = @obj.onBtnZoomIn;
            hColumn.add(obj.hBtnZoomIn);
            
            obj.hBtnZoomOut = ToggleButton('Zoom out', fullfile(icondir, 'zoomOut.png'));            
            obj.hBtnZoomOut.ValueChangedFcn = @obj.onBtnZoomOut;
            hColumn.add(obj.hBtnZoomOut);
            
            obj.hBtnPan = ToggleButton('Pan', fullfile(icondir, 'hand.png'));            
            obj.hBtnPan.ValueChangedFcn = @obj.onBtnPan;
            hColumn.add(obj.hBtnPan);            
        end
        
        function onBtnZoomIn(obj, hSrc, ~)
            if hSrc.Selected
                obj.hPan.Enable = 'off';
                obj.hZoom.Enable = 'on';
                obj.hZoom.Direction = 'in';
                
                obj.hBtnZoomOut.Value = false;
                obj.hBtnPan.Value = false;                                
            else
                obj.hZoom.Enable = 'off';
            end
        end
        
        function onBtnZoomOut(obj, hSrc, ~)
            if hSrc.Selected
                obj.hPan.Enable = 'off';
                obj.hZoom.Enable = 'on';
                obj.hZoom.Direction = 'out';
                
                obj.hBtnZoomIn.Value = false;
                obj.hBtnPan.Value = false;                                
            else
                obj.hZoom.Enable = 'off';
            end
        end
        
        function onBtnPan(obj, hSrc, ~)
            if hSrc.Selected
                obj.hPan.Enable = 'on';
                obj.hZoom.Enable = 'off';
                
                obj.hBtnZoomIn.Value = false;
                obj.hBtnZoomOut.Value = false;                                
            else
                obj.hPan.Enable = 'off';
            end
        end     
     end
end

