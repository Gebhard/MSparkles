%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef Cloneable < handle
    %CLONEABLE Base class for objects allowing to make a deep copy of
    %themselves.
    %   The Clone method of a Cloneable creates a new instance of the 
    %   object to be cloned and then tries to recursively call the Clone
    %   mehtod on its properties. If the to-be-cloned property is primitive
    %   or not derived from Cloneable, its value is simply assigned to the
    %   property with the same name in new new object.
    
    methods 
        function cpy = Clone(obj)
            cpy = eval( class(obj) );
            
            prop = metaclass(obj);
            
            for iprop = 1:length(prop.PropertyList)
                p = prop.PropertyList(iprop);
                if ~strcmpi(p.Name , "ID")
                    if ~p.Dependent
                        try
                            if ~strcmpi(p.Name , "hParent") && isa(obj.(p.Name), com.common.Cloneable)
                                cpy.(p.Name) = obj.(p.Name).Clone();
                            else
                                cpy.(p.Name) = obj.(p.Name);
                            end
                        catch
                            try %Try to copy private properties, if clonable is granted access
                                cpy.(p.Name) = obj.(p.Name);
                            catch
                            end
                        end
                    end
                end
            end
            
            %OnFinalizeClone is called on target of clone operation 
            cpy.OnFinalizeClone();
        end   
        
        function CopyFrom(obj, src)
            %Copy only properties that exist in the target (calling) object
            prop = metaclass(obj);
            
            for iprop = 1:length(prop.PropertyList)
                p = prop.PropertyList(iprop);
                if ~p.Dependent
                    try
                        obj.(p.Name) = src.(p.Name).Clone();
                    catch
                        try %Try to copy private properties, if clonable is granted access
                            obj.(p.Name) = src.(p.Name);
                        catch
                        end
                    end
                end
            end
            
            %OnFinalizeClone is called on target of clone operation 
            obj.OnFinalizeClone();
        end
    end
    
    methods(Access=protected)
        function OnFinalizeClone(obj)
            %Empty on default;
        end
    end
end

