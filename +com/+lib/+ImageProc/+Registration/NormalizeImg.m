%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function normImg = NormalizeImg(img)
    normImg = img;
    % Get linear indices to finite valued data
    finiteIdx = isfinite(normImg(:));

    % Replace NaN values with 0
    normImg(isnan(normImg)) = 0;

    % Replace Inf values with 1
    normImg(normImg==Inf) = 1;

    % Replace -Inf values with 0
    normImg(normImg==-Inf) = 0;

    % Normalize input data to range in [0,1].
    FIXEDmin = min(normImg(:));
    FIXEDmax = max(normImg(:));
    if isequal(FIXEDmax,FIXEDmin)
        normImg = 0*normImg;
    else
        normImg(finiteIdx) = (normImg(finiteIdx) - FIXEDmin) ./ (FIXEDmax - FIXEDmin);
    end
end

