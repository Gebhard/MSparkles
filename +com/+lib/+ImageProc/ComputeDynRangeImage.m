%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function dynRangeImg = ComputeDynRangeImage(inputStack, sampleSize, sigma, krnlSize)
%COMPUTEDYNRANGEIMAGE Computes the dynamic range of inputStack.
%The dynamic range is computed along the temporal axes inputStack, which is
%contained in the 6th dimension.
%input
%inputStack     2D+t OR 3D+t, single channel dataset as 6D matrix with extent 
%               [sampleSize(1), sampleSize(2), sampleSize(3), 1, 1, :]. 
%sampleSize     3-element vector containing the spatial extent of the dataset.
%sigma          OPTIONAL Sigma parameter for gaussian smoothing if empty no
%               gaussian smoothing will be performed.
%
%dynRangeImg    The 2D or 3D dynamic range image

    [dMin, dMax] = bounds(inputStack, 6);         
    dynRangeImg = single(reshape(dMax - dMin, sampleSize));      
    
    if nargin <4 || isempty(krnlSize)
        krnlSize = 0;
    end
    
    %Prepare structuring elements, and Gauss kernel.
    %Finally, perform median filter on dynamic range map.
    if ndims(dynRangeImg) == 2
        seOpen = strel("disk", 2);  
        seErode = strel("square", 3);
        gf = @imgaussfilt;
        mf = @medfilt2;
        KrnlSize = [krnlSize, krnlSize];
    else
        seOpen = strel("sphere", 2);  
        seErode = strel("cube", 3);
        gf = @imgaussfilt3;
        mf = @medfilt3;  
        KrnlSize = [krnlSize, krnlSize, krnlSize];
    end
    
    %Create gauss kernel, if neccessary, and de-compose it.
    if nargin >= 3 && ~isempty(sigma) && sigma > 0
        % Guass-Smooth range map 
        dynRangeImg = gf(dynRangeImg, sigma, 'Padding', 'symmetric');
    end
    
    if nargin >=4 && ~isempty(krnlSize) && krnlSize > 0
        dynRangeImg = mf(dynRangeImg, KrnlSize, "symmetric");
    end
    
    dynRangeE = imerode(dynRangeImg, seErode);
    dynRangeE = imopen(dynRangeE, seOpen);
    dynRangeImg = imreconstruct(dynRangeE, dynRangeImg);
end

