%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function D = FindRidges(img)
    [H, dx, dy] = com.lib.ImageProc.imhessian(img);
    
    sz = size(img);
    D = zeros(sz);
    %Dt = zeros(sz);
    
    %neccessary:
    %1st derivative must be 0 everywhere
    [cy, cx] = find(ismembertol(dx,0, 0.01) & ismembertol(dy,0, 0.01));     %Candidate pixel coordinates
    
    numCandidates = numel(cx);
    
    for ii = 1:numCandidates
        i = cx(ii);
        j = cy(ii);
        
        h = reshape(H(i,j,:), [2,2]);   %Hessian at position i,j
        g = [dx(i,j) ; dy(i,j)];        %gradient
        c = [-dy(i,j); dx(i,j)];        %co-gradient

        %Eigenvalues
        hg = h*g;
        hc = h*c;

        %Dt(i,j) = det( [g,  hg]);

        [~,Ig] = max(abs(g));
        [~,Ic] = max(abs(c));

        L1 = hg(Ig) ./ g(Ig);
        L2 = hc(Ic) ./ c(Ic);

        if L1 >= L2
            D(i,j) = L2; 
            
%             if L2 > 0
%                D(i,j) = 1; 
%             elseif L2 < 0
%                 D(i,j) = -1; 
%             end                
        end
    end
end

