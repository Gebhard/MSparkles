function labels = FindTempCorrRegions(dFF0, dynRange, labels, rangeTH, corrTH)
    arguments
        dFF0
        dynRange
        labels
        rangeTH (1,1) {mustBeInRange} = 0.5;
        corrTH (1,1) {mustBeInRange} = 0.5;
    end
    
    % Get pixels below detection threshold
    excludeMap = dynRange == 0 | dynRange < rangeTH;

    % Get regional maxima    
    %dynRange(excludeMap) = 0;
    %maxima = imregionalmax(dynRange);

    % Get groups of connected pixels
%     orgIdx = bwconncomp(labels);
    
%     orgLblIds = zeros(orgIdx.NumObjects, 1);
%     for ii = 1:orgIdx.NumObjects
%         orgLblIds(ii) =  labels(orgIdx.PixelIdxList{ii});
%     end
    
    se = strel("octagon", 3);
    labels = imdilate(labels, se);
    ccMax = bwconncomp(labels);
    
    %centroids = regionprops(ccMax, 'Centroid');
    %coords = struct2array(centroids);
    
    % Get labels to identify growing regions
    labels = single(labelmatrix(ccMax));

    szDFF0 = size(dFF0);
    %create array of of group-means
    groupMeans = zeros(szDFF0(6), ccMax.NumObjects);

    %Get (F-F0/F0) for current channel
    %dFF0 = hDataset.dFF0(:,:,channelID,:,:,:);

    %Reshape, s.t. time is in first dimension
    dFF0 = reshape(dFF0, [szDFF0(1) * szDFF0(2) * szDFF0(4), szDFF0(6)])';

    %Iterate over all groups of connected pixels
    for ig = 1:ccMax.NumObjects
        idx = ccMax.PixelIdxList{ig};                
        groupMeans(:,ig) = mean(dFF0(:, idx), 2, 'omitnan'); %2nd dimension containsspatial information per timestep
    end

    %Get indices of valid pixels
    validIdx =  ~excludeMap ;

    %Compute all correlations of groupMeans and valid pixel traces
    corrMtx = zeros(ccMax.NumObjects, size(dFF0, 2));
    corrMtx(:,validIdx) = corr(groupMeans, dFF0(:,validIdx));

    groupCorr = corr(groupMeans, groupMeans);
    groupCorr = groupCorr - diag(diag(groupCorr)); % Remove auto correlations

    %apply correlation TH
    corrMtx(corrMtx < corrTH) = 0;     

    %% Perform iterative, breadth-first region growing
    run = true;

    if ndims(labels) == 3
        padSize = [1,1,1];
    else
        padSize = [1,1];
    end
    
    %Create an image, where each pixel contains its index, pad it
    %and transform to column image of 3x3(x3) n-hoods
    numPxl = prod(size(excludeMap), "all");
    idxMat = reshape(1:numPxl, size(excludeMap)); %Get image of indices
    idxMat = padarray(idxMat, padSize, 'symmetric');
    idxMat = im2colEx(idxMat, [3,3,3]);                                              

%     persistent f;
%     persistent f2;  
    
%     if isempty(f) || ~isvalid(f)
%         f = figure;
%         hAx = axes(f);
%         hImg = imagesc(hAx, labels);
%     else
%         hAx = f.Children(1);
%         hImg = hAx.Children(1);
%     end
    
%      if isempty(f2) || ~isvalid(f2)
%         f2 = figure;
%         hAx2 = axes(f2);
%         hImg2 = imagesc(hAx2, labels);
%     else
%         hAx2 = f2.Children(1);
%         hImg2 = hAx2.Children(1);
%     end
    
    

    while run
        %Within a 3x3x3 neighborhood of the label image
        %1) If all neighbors are 0 => Don't care
        %2) If neighbors have different labels >0 => boundary pixel
        %   between two (or more) regions
        %3) If all non-zero neighbors have the same label:
        %   - Make sure it is a boundary pixel
        %   - Compute the correlation with the respective mean signal of the center region
        %   - If the correlation coefficient is above TH, assign the group label to this pixel
        %4) Repeat until no more changes occur

        %labels(excludeMap) = 0;
        newLbl = labels;
        colImg = labels(idxMat);
        colImg(:, labels ~= 0) = 0;         %Set existing labels and boundary pixels to 0
        idx = find(any(colImg > 0, 1));     %Identify boundary pixels. At least one of he neighbours must be > 0

        cm = corrMtx(:,idx);    %Get affectec columns of correlation matrix
        B = any(cm > 0, 1);     %Get columan siwith non-zeor correlation
        idx = idx(B);           %Reduce to relevant indices   
        
        ci = colImg(:,idx);     %Get affected columns of col-image
        cm = corrMtx(:,idx);    %Get affected columns of correlation matrix
         
        numCols = numel(idx);
        for ic = 1:numCols
            c = ci(:,ic);
            c=c(c>0);
            [~, li] = max(cm(c,ic));
            newLbl(idx(ic)) = c(li);
        end

        newLbl(excludeMap) = 0;
        run = any((labels - newLbl) ~= 0, "all");
        labels = newLbl;
%                 
%         hImg.CData = labels;
%         pause(0.0001);
    end    
    
    

%     dImg = dynRange .* validIdx;
%     dImg = watershed(-dImg);
%     dImg(excludeMap) = 0;
%     hImg2.CData = dImg;
    
    


%     hImg.CData = labels;
%     pause(0.00001);
    
%     return;
    
    %Cleanup in last iteration
    %Set pixels with more than one label in their neighborhood to
    %be 0 (boundary)
%     labels(labels == -1) = 0;   
%     idx = find(labels > 0);    
%     colImg = labels(idxMat);
%     
%     %get only indices of boundary pixels of neighboring regions
%     nums = splitapply(@(x)(numel(unique(x(x>0)))), colImg(:,idx), 1:numel(idx));
%     idx = idx(nums > 1);        
%         
%     ur = unique(colImg(:,idx)', "rows");
%     uc = cell(size(ur,1),1);
%     for iu=1:size(ur,1)
%         u = ur(iu, :);
%         uc{iu} = unique(u(u>0));
%     end
%     
%     maxWidth = max(cellfun(@numel, uc));
%     arr = zeros(numel(uc), maxWidth);
%     for iu=1:numel(uc)
%         arr(iu, 1:numel(uc{iu})) = uc{iu};
%     end
%     
%     ur = unique(arr, "rows");
%     uItems = unique(arr(arr>0));
%     eqClasses = cell(numel(uItems), 1);
%    
%     try
%         for i=1:size(ur,1)
%             r = ur(i,:);
%             r= r(r>0);
%             id = find(max(ismember(ur, r), [], 2));
% 
%             idUr = ur(id,:);
%             idUr = unique(idUr(:));
% 
%             for d=idUr'
%                 uIdx = find(uItems == d);
%                 %cat(2,eqClasses{uIdx}, ur(d,:))
%                 if ~isempty(uIdx)
%                     u = ur(id,:);
%                     eqClasses{uIdx} = cat(2,eqClasses{uIdx}, u(:)');
%                 end
%             end
%             %eqClasses{id} = cat(2,eqClasses{id}, ur(id,:));
%         end
%     catch ex
%         
%     end
%     
%     maxWidth = max(cellfun(@numel, eqClasses));
%     arr = zeros(numel(eqClasses), maxWidth);
%     for iu=1:numel(eqClasses)
%         u = unique(eqClasses{iu});
%         u= u(:);
%         arr(iu, 1:numel(u)) = u;
%     end
%     
%     arr = unique(arr, "rows"); % arr now contains ROI indices which are neighboring per row
%     
%     for ia=1:size(arr,1)
%         a = arr(ia,:);
%         a = a(a > 0);
%         
%         crr = groupCorr(a,a);
%         crr(crr<mergeTH) = 0;
%         
%         if any(crr >0)
%             mc = max(crr);
%             [~, i] = max(mc);
%             b = a(a~=a(i));
%             
%             im = ismember(labels, b);
%             labels(im) = a(i);
%         end
%     end
%     [~, ~, kk] = unique(ur, 'rows', 'stable'); %'
%     result = accumarray(kk, (1:numel(kk)).', [], @(x) {sort(x).'});
% 
%     ur = unique(colImg(:,idx)', "rows");
   % idx = find(any(colImg(5,:) > 0 & colImg > 0));              %Identify boundary pixels. At least one of he neighbours must be > 0
    
%     numCols = numel(idx);
%     for ic=1:numCols
%         c = colImg(:,idx(ic));                  
%         ul = unique(c(c>0));
%         if numel(ul) > 1
%             for i=1:numel(ul)
%                 crr = groupCorr(c(5), ul);
% 
%                 [pCorr, lblIdx] = max(crr);
%                 if pCorr > mergeTH
%                     labels(labels == c(5)) = ul(lblIdx);
%                     colImg(colImg == c(5)) = ul(lblIdx);
% %                                 hImg.CData = labels;
% %                                 pause(0.0001);
%                 else
%                     labels(idx(ic)) = 0;
%                 end
%             end
%         end
%     end
    
    %assign original labels
%     for ii=1:numel(orgLblIds)
%         labels(labels==ii) = orgLblIds(ii);
%     end

%     hImg.CData = labels;
    pause(0.0001);
end

