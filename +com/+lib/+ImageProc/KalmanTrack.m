classdef KalmanTrack < handle
    %KALMANTRACK Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id;
        bbox;
        kalmanFilter;
        age;
        totalVisibleCount;
        consecutiveInvisibleCount;
        centroids;
        timeIdx;
        PixelIdx;
        PredictedCentroid;
        IsPredicted;
    end
    
    properties(Dependent)
        CurrCenter;
        LastKnownCenter;
    end
    
    methods
        function obj = KalmanTrack(id, centroid, bbox, timeIdx, hasKalmanFilter)
            if nargin < 5
                hasKalmanFilter = true;
            end
            
            obj.id = id;
            obj.bbox = bbox;
            
            if  hasKalmanFilter
                obj.kalmanFilter = configureKalmanFilter('ConstantVelocity', centroid, [25, 10], [15, 5], 10);
            end
            
            obj.age = 1;
            obj.totalVisibleCount = 1;
            obj.consecutiveInvisibleCount = 0;
            obj.centroids = centroid;
            obj.timeIdx = timeIdx;
            obj.PixelIdx = [];
            obj.IsPredicted = false;
        end
    end
    
    methods
        function value = get.CurrCenter(obj)
            if ~isempty(obj.centroids)
                value = obj.centroids(end,:);
            else
                value = [];
            end
        end
        
        function value = get.LastKnownCenter(obj)
            if ~isempty(obj.centroids)
                if numel(obj.centroids) > 1
                    value = obj.centroids(end-1,:);
                else
                    value = obj.centroids(end,:);
                end
            else
                value = [];
            end
        end
    end
    
    methods
        function Update(obj, timeIdx, centroid, isPredicted)
            obj.age           = obj.age + 1;
            obj.timeIdx       = cat(1, obj.timeIdx, timeIdx);
            obj.centroids     = cat(1, obj.centroids, centroid);
            obj.IsPredicted   = cat(1, obj.IsPredicted, isPredicted);
            
            if ~isPredicted
                obj.totalVisibleCount = obj.totalVisibleCount + 1;
                obj.consecutiveInvisibleCount = 0;
            else
                obj.consecutiveInvisibleCount = obj.consecutiveInvisibleCount + 1;
            end
        end
        
        function cntr = GetCenter(obj, tIdx)
            %Get the center occuring at a speciffic time point
            cntr = obj.centroids(obj.timeIdx == tIdx, :);
        end
        
        function DeleteLastPos(obj, n)
            %Removed the last n positions, iff they are predicted. n should be the maximum number
            %of approximated positions, before a track is considered to be
            %lost.
            if obj.age > n
                obj.age = obj.age - n;
                obj.centroids   = obj.centroids(1:end-n,:);
                obj.timeIdx     = obj.timeIdx(1:end-n);
                obj.IsPredicted = obj.IsPredicted(1:end-n);
            else
                obj.age         = 0;
                obj.centroids   = [];
                obj.timeIdx     = [];
                obj.IsPredicted = [];
            end
        end
        
        function Predict(obj)
            % Predict the current location of the track.
            centroid = predict(obj.kalmanFilter);
            obj.PredictedCentroid = centroid;
            
            % Shift the bounding box so that its center is at
            % the predicted location.
            centroid = centroid - obj.bbox(3:4) / 2;
            obj.bbox = [centroid, obj.bbox(3:4)];    
        end
        
        function Correct(obj, pos)
            correct(obj.kalmanFilter, pos);
        end
        
        function cost = Distance(obj, centroids)
            cost = distance(obj.kalmanFilter, centroids);
        end
        
        function Split(obj, hTracker, frameNo)
            
            splitIdx = find(obj.timeIdx == frameNo);
            
            if ~isempty(splitIdx)
                obj.centroids(splitIdx,:) = [];
                obj.timeIdx(splitIdx,:) = [];
                
                if splitIdx > 1 && splitIdx < size(obj.centroids, 1)
                    remainder = com.lib.ImageProc.KalmanTrack(hTracker.NextValidTrackID,...
                        obj.centroids(splitIdx:end,:), [], obj.timeIdx(splitIdx:end,:), false);
                    remainder.age = size(remainder.centroids, 1);
                    
                    hTracker.AppendTracks(remainder)
                end
                
                obj.centroids(splitIdx:end, :) = [];
                obj.timeIdx(splitIdx:end, :) = [];
                obj.age = size(obj.centroids, 1);
            end
        end
    end
end

