classdef KalmanTracker < handle
    %KALMANTRACKER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        allTracks;
        tracks;
    end
    
    properties (Dependent)
        NumTracks;
        NextValidTrackID;
    end
    
    properties(Access=private)      
        centroids;
        bboxes;
        mask;
        assignments;
        unassignedTracks;
        unassignedDetections;
        nextId;
        detectorFun;
        CostOfNonAssignment;
        InvisibleForTooLong;
        
        debugOutput;
        maskFigure;
        maskAxis;
        maskImg;
        frameFigure;
        frameAxis;
        frameImg;
    end
    
    methods
        function obj = KalmanTracker(detectorFun, costOfNonAssignment, invisibleForTooLong, debugOutput)
            arguments
                detectorFun (1,1) {mustBeNonempty};
                costOfNonAssignment (1,1) {mustBePositive, mustBeInteger} = 5;
                invisibleForTooLong (1,1) {mustBePositive, mustBeInteger} = 2;
                debugOutput (1,1) logical = false;
            end
            
            obj.CostOfNonAssignment = costOfNonAssignment;
            obj.InvisibleForTooLong = invisibleForTooLong;
            
            obj.detectorFun = detectorFun;
            obj.debugOutput = debugOutput;
            obj.nextId = 1;
            obj.initTracks();
            
            if obj.debugOutput
                obj.maskFigure = figure;
                obj.maskAxis = axes(obj.maskFigure);

                obj.frameFigure = figure;
                obj.frameAxis = axes(obj.frameFigure);
            end
        end
        
        function delete(obj)
            delete (obj.maskFigure);
            delete (obj.frameFigure);
        end
    end
    
    methods
        function value = get.NumTracks(obj)
            value = numel(obj.allTracks);
        end
        
        function value = get.NextValidTrackID(obj)
            value = obj.nextId;
            obj.nextId = obj.nextId + 1;
        end
    end
    
    methods
        function TrackObjects(obj, frame, img, exclude, timeIdx)
            obj.detectObjects(frame, img);
            obj.predictNewLocationsOfTracks();
            obj.detectionToTrackAssignment();
            obj.updateAssignedTracks(timeIdx);
            obj.updateUnassignedTracks(exclude, timeIdx);
            obj.deleteLostTracks();
            obj.createNewTracks(timeIdx);

            if obj.debugOutput
                obj.displayTrackingResults(frame);
            end
        end 
        
        function DeleteShortTracks(obj, minLen)
            idx = arrayfun(@(x) x.age >= minLen, obj.allTracks);
            obj.allTracks = obj.allTracks(idx);
            numTracks = numel(obj.allTracks);            
            
            for i = 1:numTracks
                obj.allTracks(i).id = i;
            end
        end
        
        function iMax = ProjectMaxima(obj, iMax)
            %Project all maxima into the 6D-Matrix iMax
            %Each maxima is assigned the ID of its corresponding track
            
            sz = size(iMax);
            c = round(cat(1,obj.allTracks(:).centroids));
            cntrs = ones(size(c,1), 6);
            cntrs(:,1) = c(:,2);
            cntrs(:,2) = c(:,1);
            cntrs(:,6) = cat(1,obj.allTracks(:).timeIdx);
            
            idx = sub2ind(sz, cntrs(:,1), cntrs(:,2), cntrs(:,3), cntrs(:,4), cntrs(:,5), cntrs(:,6));
            trackIDs = repelem( cat(1,obj.allTracks(:).id), cat(1,obj.allTracks(:).age) );
            iMax(idx) = trackIDs;
            
%             se = strel("disk", 2, 8);
%             for ii=1:sz(6)
%                 iMax(:,:,:,:,:,ii) = imdilate(iMax(:,:,:,:,:,ii), se);
%             end
        end
        
        function tIdx = GetTimeIndices(obj)
            %Get unique time indices in ascending order
            tIdx = unique(cat(1,obj.allTracks(:).timeIdx));
        end
        
        function tracks = GetTracks(obj, tIdx)
            %Get all tracks that have are valit at time tIdx
            
            idx = arrayfun(@(x) ismember(tIdx, x.timeIdx), obj.allTracks);
            tracks = obj.allTracks(idx);
        end    
        
        function AppendTracks(obj, tracks)
            if nargin > 1
                obj.allTracks = cat(2, obj.allTracks, tracks);
            end
        end
        
        function track = GetTrack(obj, id)
            try
                track = obj.allTracks(id);
            catch
                track = [];
            end
            
            if isempty(track) || track.id ~= id
                idx = arrayfun(@(x) x.id == id, obj.allTracks);
                track = obj.allTracks(idx);
            end
        end
    end
    
    methods(Access=protected)
        function  newTrack = createTrack(obj, centroid, bbox, timeIdx)
            %Create a new "track", with assiciated Kalman filter and
            %increment nextId (track counter)
            
            % Create a new track.
            newTrack = com.lib.ImageProc.KalmanTrack(obj.nextId, centroid, bbox, timeIdx);
            % Increment the next id.
            obj.nextId = obj.nextId + 1;
        end
        
        function initTracks(obj)
            obj.tracks = com.lib.ImageProc.KalmanTrack.empty;
            obj.allTracks = com.lib.ImageProc.KalmanTrack.empty;
        end        
        
        function detectObjects(obj, frame, img)
            [obj.mask, stats] = obj.detectorFun( frame, img);
            
            if ~isempty(stats)
                stats = struct2cell(stats);
                obj.bboxes = cell2mat(stats(1, :)');
                obj.centroids = cell2mat(stats(2, :)');
            else
                obj.centroids = [];
                obj.bboxes = [];
            end
        end
    
        function predictNewLocationsOfTracks(obj)
            for i = 1:length(obj.tracks)
                obj.tracks(i).Predict();
            end
        end
        
        function detectionToTrackAssignment(obj)
            nTracks = length(obj.tracks);
            nDetections = size(obj.centroids, 1);

            % Compute the cost of assigning each detection to each track.
            cost = zeros(nTracks, nDetections);
            for i = 1:nTracks
                cost(i, :) = obj.tracks(i).Distance(obj.centroids);
            end

            % Solve the assignment problem.
            %obj.CostOfNonAssignment = 5;
            [obj.assignments, obj.unassignedTracks, obj.unassignedDetections] = ...
                assignDetectionsToTracks (cost, obj.CostOfNonAssignment);
        end
    
        function updateAssignedTracks(obj, timeIdx)
            numAssignedTracks = size(obj.assignments, 1);
            for i = 1:numAssignedTracks
                ind = obj.assignments(i, 1);
                track = obj.tracks(ind);
                detectionIdx = obj.assignments(i, 2);
                centroid = obj.centroids(detectionIdx, :);

                % Correct the estimate of the object's location
                % using the new detection.
                track.Correct(centroid);

                % Replace predicted bounding box with detected
                % bounding box.
                track.bbox =  obj.bboxes(detectionIdx, :);
                
                % Update track's age, time index, centroid,...
                track.Update(timeIdx, centroid, false);
            end
        end
        
        function updateUnassignedTracks(obj, exclude, timeIdx)
            try
                szEx = fliplr(size(exclude));
                for i = 1:length(obj.unassignedTracks)
                    ind = obj.unassignedTracks(i);
                    track = obj.tracks(ind);

                    centroid = track.PredictedCentroid;
                    
                    roundedCentroid = min(max([1,1], round(centroid)), szEx);
                    
                    if exclude(roundedCentroid(:,2), roundedCentroid(:,1)) == 1
                        track.consecutiveInvisibleCount = obj.InvisibleForTooLong + 1;
                    else
                        track.Update(timeIdx, centroid, true);
                    end
                end
            catch ex
                %Centroid out of bounds
                track.consecutiveInvisibleCount = obj.InvisibleForTooLong + 1;
            end
        end
        
        function deleteLostTracks(obj)
            if isempty(obj.tracks)
                return;
            end
            
            ageThreshold = 2;

            % Compute the fraction of the track's age for which it was visible.
            ages = [obj.tracks(:).age];
            totalVisibleCounts = [obj.tracks(:).totalVisibleCount];
            visibility = totalVisibleCounts ./ ages;

            % Find the indices of 'lost' tracks.
            lostInds = (ages < ageThreshold & visibility < 0.5) | ...
                [obj.tracks(:).consecutiveInvisibleCount] >= obj.InvisibleForTooLong;

            % Delete lost tracks.
            obj.tracks = obj.tracks(~lostInds);
        end
        
        function createNewTracks(obj, timeIdx)
            obj.centroids = obj.centroids(obj.unassignedDetections, :);
            obj.bboxes = obj.bboxes(obj.unassignedDetections, :);

            for i = 1:size(obj.centroids, 1)
                %Create new track
                newTrack = obj.createTrack(obj.centroids(i,:), obj.bboxes(i, :), timeIdx);
                
                % Add it to the array of tracks.
                obj.tracks(end + 1) = newTrack;
                obj.allTracks(end + 1) = newTrack;
            end
        end
        
        function displayTrackingResults(obj, frame)
            % Convert the frame and the mask to uint8 RGB.
            frame = im2uint8(frame);
            
            obj.mask = uint8(repmat(obj.mask, [1, 1, 3])) .* 255;

            minVisibleCount = 1;
            if ~isempty(obj.tracks)

                % Noisy detections tend to result in short-lived tracks.
                % Only display tracks that have been visible for more than
                % a minimum number of frames.
                reliableTrackInds = ...
                    [obj.tracks(:).totalVisibleCount] >= minVisibleCount;
                reliableTracks = obj.tracks(reliableTrackInds);

                % Display the objects. If an object has not been detected
                % in this frame, display its predicted bounding box.
                if ~isempty(reliableTracks)
                    % Get bounding boxes.
                    obj.bboxes = cat(1, reliableTracks.bbox);

                    % Get ids.
                    ids = int32([reliableTracks(:).id]);

                    % Create labels for objects indicating the ones for
                    % which we display the predicted rather than the actual
                    % location.
                    labels = cellstr(int2str(ids'));
                    predictedTrackInds = ...
                        [reliableTracks(:).consecutiveInvisibleCount] > 0;
                    isPredicted = cell(size(labels));
                    isPredicted(predictedTrackInds) = {' predicted'};
                    labels = strcat(labels, isPredicted);

                    % Draw the objects on the frame.
                    frame = insertObjectAnnotation(frame, 'rectangle', ...
                        obj.bboxes, labels);

                    % Draw the objects on the mask.
                    obj.mask = insertObjectAnnotation(obj.mask, 'rectangle', ...
                        obj.bboxes, labels);
                end
            end

            try
                if isempty(obj.frameImg) || ~isvalid(obj.frameImg)
                    obj.frameImg = imagesc(obj.frameAxis, frame, [0,5]);
                    colormap(obj.frameAxis, "jet");
                else
                    obj.frameImg.CData = frame;
                end
                
                if isempty(obj.maskImg) || ~isvalid(obj.maskImg)
                    obj.maskImg = imagesc(obj.maskAxis, obj.mask);
                else
                    obj.maskImg.CData = obj.mask;
                end 
                
                pause(0.00001);
            catch
            end
        end
    end
end

