%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [V,D,A] = STEigenDecomp(S)
%UNTITLED Returns the eigenvectors V and eigenvalues D of a structure
%tensor image, computed with MSparkles' StructureTensor function.
%Returned eigenvalues are in descending order, s.t. d1 >= d2 >= ... >= dn.
%Corresponding eigenvectors are sorted accordingly.

    sz = size(S);
    nd = ndims(S);
    
    if nd == 3
        V = zeros([sz(1), sz(2), 4]);
        D = zeros([sz(1), sz(2), 2]);
        
        for i=1:sz(1)
            for j=1:sz(2)
                s = reshape(S(i,j,:),[2,2]);
                [v,d] = eig(s);
                [D(i,j,:), idx] = sort(diag(d), 'descend');
                v = v(:, idx);
                V(i,j,:) = v(:);
           end
           
        end
       
        if nargout > 2
            A = ((max(D, [], 3) - min(D, [], 3)) ./ sum(D, 3)) .^2;
        end
    elseif nd == 4
        
    end
    
end

