%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function S = StructureTensor(img, w)
%STRUCTURETENSOR Computes the structure tensor S of a single-channel (gray-scale) image img within a
%window of half-size w. This function automatically determines, if the image in %img is 2D or 3D, and
%computes the structure tensor accordingly.
%
%   img:    2D or 3D input image
%   w:      Half-size if the window to compute the structure tensor
%   S:      Matrix with structure tensors for each pixel in input image img
%
%   Weighting within the neighbourhood window is performed using a gaussian
%   kernel of window size 2*w+1, and sigma of window size / 3.
%
% 2D structure tensor:
%
%   | dx*dx    dx*dy |
%   | dy*dx    dy*dy |
%
%
% 3D structure tensor:
%
% | dx*dx    dx*dy    dx*dz |
% | dy*dx    dy*dy    dy*dz |
% | dz*dx    dz*dy    dz*dz |
%
% Structure tensors are stored explicitly for ease of use in subsequent
% steps. 

    sz = ndims(img);
    if ismatrix(img) 
        if sz == 2
            S = st2D(img, w);
        elseif sz == 3
            S = st3D(img, w);
        else
            error('StructureTensor: Input must be a 2D or 3D image.');
        end
    else
       error('StructureTensor: Input must be a 2D or 3D image.');
    end
end

function S = st2D(img, w)
    [dx, dy] = gradient(img);
    S = zeros([size(img), 4]);
    
    S(:,:,1) = dx .^ 2;
    S(:,:,2) = dx .* dy;
    S(:,:,2) = dx .* dy;
    S(:,:,3) = dy .^ 2;
    
    fSize = 2*w+1;
    sigma = fSize / 3;
    
    S = imgaussfilt(S, sigma, 'FilterSize', fSize);
end

function S = st3D(img, w)
    [dx, dy, dz] = gradient(img);
    S = zeros([size(img),9]);
    
    S(:,:,:,1)=dx .^ 2;
    S(:,:,:,2)=dx .* dy;
    S(:,:,:,3)=dx .* dz;
    
    S(:,:,:,4)=dx .* dy;
    S(:,:,:,5)=dy .^ 2;    
    S(:,:,:,6)=dy .* dz;
    
    S(:,:,:,7)=dx .* dz;
    S(:,:,:,8)=dy .* dz;
    S(:,:,:,9)=dz .^ 2;
    
    fSize = 2*w+1;
    sigma = fSize / 3;
    
    S = imgaussfilt(S, sigma, 'FilterSize', fSize);
end