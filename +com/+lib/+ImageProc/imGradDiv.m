%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function D = imGradDiv(img, s)
%IMJACOBIAN Computes the jacobian of the gradient image
%   img     A 2D or 3D scalar image
%   s       sigma for gaussian smoothing of the gradient image
%   J       The jacobian of J = d2/d^2(grad(img))
    
    
    [gx, gy] = gradient(img);
     gx = imgaussfilt(gx, s);
     gy = imgaussfilt(gy, s);
     
    
    [gxx, ~] = gradient(gx);
    [~, gyy] = gradient(gy);
    
    gxx = imgaussfilt(gxx);
    gyy = imgaussfilt(gyy);
    
    D = gxx + gyy;
end

function J = jacobian2D(img, s)
    J = zeros([ size(img), 4]);
    
    [gx, gy] = gradient(img);
     gx = imgaussfilt(gx, s);
     gy = imgaussfilt(gy, s);
    
    [J(:,:,1), J(:,:,2)] = gradient(gx);
    [J(:,:,3), J(:,:,4)] = gradient(gy);
    
    J(:,:,1) = imgaussfilt(J(:,:,1));
    J(:,:,2) = imgaussfilt(J(:,:,2));
    J(:,:,3) = imgaussfilt(J(:,:,3));
    J(:,:,4) = imgaussfilt(J(:,:,4));
end

function J = jacobian3D(img, s)
     J = zeros([ size(img), 9]);
    
    [gx, gy, gz] = gradient(img);
    gx = imgaussfilt(gx, s);
    gy = imgaussfilt(gy, s);
    gz = imgaussfilt(gz, s);
    
    [J(:,:,1), J(:,:,2), J(:,:,3)] = gradient(gx);
    [J(:,:,4), J(:,:,5), J(:,:,6)] = gradient(gy);
    [J(:,:,7), J(:,:,8), J(:,:,9)] = gradient(gz);
end

