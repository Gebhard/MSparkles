%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function C = imcriticalpoints(img, s)
%IMCRITICALPOINTS Summary of this function goes here
%   Detailed explanation goes here
    J = com.lib.ImageProc.imjacobian(img, s);
    
    switch ndims(img)
        case 2
            C = imcritical2D(J);
        case 3
            C = imcritical3D(J);
        otherwise
            error('IMCRITICALPOINTS: Image must be a 2D or 3D scalar image.');
    end
end

function C = imcritical2D(J)
    % J     Jacobian image, computed by imjacobian
    %Compute (complex) eigenvalues of jacobian
    % in 2D, this boils down to e1,2 = T/2 +- sqrt( (T/2)^2 -D ),
    % where T = trace(J), D = det(J);
    
    %Only look at points where gradient vanishes (magnitude -> 0)
    T = J(:,:,1) + J(:,:,4);
    D = J(:,:,1) * J(:,:,4) - J(:,:,2) * J(:,:,3);
    
    t2 = T./2;
    sq = sqrt( (t2).^2 - D );
    E1 = t2 + sq;
    E2 = t2 - sq;
    
    sz = size(J);
    C = zeros(sz(1), sz(2));
    
    C = real(E2);
%     idxRepell = imag(E1) == 0 & imag(E2) == 0 ;%& real(E1) > 0 & real(E2) > 0;
%     idxImag = imag(E1) ~= 0 & imag(E2) ~= 0 ;
%     %idxAttract = imag(E1) == 0 & imag(E2) == 0 & real(E1) < 0 & real(E2) < 0;
%     %idxSaddle = imag(E1) == 0 & imag(E2) == 0 & real(E1) < 0 & real(E2) > 0;
%     
%     C(idxRepell) = 1;
%     C(idxImag) = 2;
%     C(D==0) = 0;
end

function C = imcritical3D(J)
    error('IMCRITICALPOINTS: 3D images are not supported at the moment.');
end