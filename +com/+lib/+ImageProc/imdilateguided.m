%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function imOut = imdilateguided(img, labelimg)
%IMDILATEGUIDED Gradient guided "down-hill" dilation.
%   Dilation is performed along gradient direction iff gradient vector of center
%   element points towards the same major direction as at leas one gradient
%   vector of the neighbourhod defined by se. Further, the candidate
%   gradient vector must point downward, i.e. is is required that se(p) -
%   currpos >= 0.

    sz = size(img);    
    imOut = zeros(sz);
    
    img = padarray(img, [1,1], 'symmetric');
    labelimg = padarray(labelimg, [1,1], 'symmetric');
    
    [gx, gy] = gradient(img);    
    
    xnhd = [-1,0,1];
    ynhd = [-1,0,1];
    
    for i=1:sz(1)
        for j=1:sz(2)
            locnhoodx = xnhd + (j+1);
            locnhoody = ynhd + (i+1);
            
            dp =    gx(locnhoody, locnhoodx) .* gx(locnhoody(2), locnhoodx(2)) + ...
                    gy(locnhoody, locnhoodx) .* gy(locnhoody(2), locnhoodx(2));
            dp = dp > 0;
            
            ld = img(locnhoody, locnhoodx) - img( locnhoody(2), locnhoodx(2) );
            ld = ld >= 0;

            %get mask of acceptable values
            m = dp .* ld;
            dummy = labelimg(locnhoody, locnhoodx) .* m ;
            imOut(i,j) = max(dummy(:));
        end 
    end    
end

