%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function gradImg = imgradient(img, sigma, normalize)
%IMGRADIENT Computes the (smoothed) gradient of the input image img.
%   img     2D or 3D Input image to optaint the gradietn from
%   sigma   Sigma for gaussian to smooth the image gradient. If sigma is 0,
%           no smoothing is performed
%   
%   gradImg Output gradient image with extent [size(img), n], 
%           where n = {2,3}, depending on the dimensionality of the input
%           image. 

    if nargin < 3
        normalize = false;
    end
    
    if nargin < 2
        sigma = 0;
    end

    switch ndims(img)
        case 2
            gradImg = imageGradient2D(img, sigma, normalize);
        case 3
            gradImg = imageGradient3D(img, sigma, normalize);
        otherwise
            error("com.lib.ImageProc.imgradient: Inputimage mut be 2D or 3D scalar matrix.");
    end
end

function grad = imageGradient2D(img, sigma, normalize)
    grad = zeros([size(img), 2]);
    [grad(:,:,1), grad(:,:,2)] = gradient(img);
    
    if sigma > 0
        grad(:,:,1) = imgaussfilt(grad(:,:,1), sigma);
        grad(:,:,2) = imgaussfilt(grad(:,:,2), sigma);
    end
    
    if normalize
        n = sqrt(grad(:,:,1).^2 + grad(:,:,2).^2);
        grad = grad./n;
    end
end

function grad = imageGradient3D(img, sigma, normalize)
    grad = zeros([size(img), 3]);
    [grad(:,:,:,1), grad(:,:,:,2), grad(:,:,:,3)] = gradient(img);
    
    if sigma > 0
        grad(:,:,:,1) = imgaussfilt3(grad(:,:,:,1), sigma);
        grad(:,:,:,2) = imgaussfilt3(grad(:,:,:,2), sigma);
        grad(:,:,:,3) = imgaussfilt3(grad(:,:,:,3), sigma);
    end
    
    if normalize
        n = sqrt( grad(:,:,:,1).^2 + grad(:,:,:,2).^2 + grad(:,:,:,3).^2 );
        grad = grad./n;
    end
end


    
