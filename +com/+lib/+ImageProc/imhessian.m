%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [H, dx, dy] = imhessian(img)
%Computes the hessian H matrix of 2nd derivatives of a scalar image img.
%The output H has the same size as the image, but contains the derivatives
%in an additional dimension, in column order. For a 2D image, H is by 2x2,
%like this:
%
% | dxx dxy |
% | dyx dyy |
%
% First order derivatives are smoothed with a gaussian to obtain more
% stable results from 2nd order derivatives.

    H = zeros([size(img), 4]);

    [dx,dy] = gradient(img);
    dx = imgaussfilt(dx);
    dy = imgaussfilt(dy);
    
    [H(:,:,1), H(:,:,2)] = gradient(dx);
    [H(:,:,3), H(:,:,4)] = gradient(dy);
    
    H(:,:,1) = imgaussfilt(H(:,:,1));
    H(:,:,2) = imgaussfilt(H(:,:,2));
    H(:,:,3) = imgaussfilt(H(:,:,3));
    H(:,:,4) = imgaussfilt(H(:,:,4));
end

