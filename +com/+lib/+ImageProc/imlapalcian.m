%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [imOut, imGrad] = imlapalcian(img, sigma, normalize, invert)
    %IMLAPLACIAN computes the laplacian (divergence of the gradient) of the supplied image
    %   img         Input image, as 2D grayscale
    %   sigma       Optional parameter for gaussian smoothing of gradient image
    %   normalize   Boolean to decide if the laplacian is to be computed on
    %               the normalized gradient image.
    
    if nargin < 4
        invert = false;
    end
    
    if nargin < 3
        normalize = false;
    end
    
    if nargin < 2
        sigma = 0;
    end
    
    if invert
        scale = -1;
    else
        scale = 1;
    end

    imGrad = scale * com.lib.ImageProc.imgradient(img, sigma, normalize);
    
    szGrad = size(imGrad);
    
    switch szGrad(3)
        case 2
            imOut = divergence(imGrad(:,:,1), imGrad(:,:,2));
        case 3 
            imOut = divergence(imGrad(:,:,1), imGrad(:,:,2), imGrad(:,:,3));
        otherwise
            error("com.lib.ImageProc.imlapalcian: number of dimensions (" + szGrad(3) + ") is not supported");
    end
end

