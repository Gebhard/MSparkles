%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function signal = BandFilter(signal, Fs, bounds, type)
%BANDFILTER Applies a filter to a signal.
%   signal      1D signal vector in time domain
%   Fs          Sampling rate of the signal
%   bounds      Frequency bound(s) to be applies by the filter, 1 -OR- 2 element vector
%   type        Type of filter to be applied. Must be an element of com.lib.SigProc.BandFilterType
    
    signal      = fft(signal(:));                           % Transform to Fourier domain
    dF          = Fs / length( signal );                    % Freq resolution                
    f           = abs(-Fs/2:dF:Fs/2-dF)';                   % create frequency vector            
    
    switch(type)
        case com.lib.SigProc.BandFilterType.LoPass
            assert( numel(bounds) == 1 );
            BPF = f <= bounds(1);
        case com.lib.SigProc.BandFilterType.HiPass
            assert( numel(bounds) == 1 );
            BPF = f >= bounds(1);
        case com.lib.SigProc.BandFilterType.BandPass
            assert( numel(bounds) == 2 );
            BPF = (f < bounds(2)) & (f >= bounds(1));
        case com.lib.SigProc.BandFilterType.BandStop
            assert( numel(bounds) == 2 );
            BPF = ~((f < bounds(2)) & (f > bounds(1)));
        case com.lib.SigProc.BandFilterType.Notch
            assert( numel(bounds) == 1 );
            fRng = bounds(1) / (Fs/2); %Ratio of Notch frequency to nyquist limit
            BPF = ~((f < (bounds(1) + fRng)) & (f > (bounds(1) - fRng)));
        otherwise
            error("Filter type not recognized.");
    end
                                  
    filtered    = BPF .* fftshift( signal );                % apply filter mask to signal, centerd at 0
    signal      = ifft(ifftshift(filtered), 'symmetric');   % convert back
end

