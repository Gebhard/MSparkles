%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function frequencyBands = MultiBandPass(signal, Fs, bounds)
    %MULTIBANDPASS Performs multiple band-pas filters, and returns the separated bands.
    %   Signal in time domain
    %   FS sampling rate of the signal
    %   bounds Nx2 matrix, where each row contains the lower (inclusive) and upper (Exclusive) bound
    %   of a frequency band
    %   
    %   frequencyBands separated bands in the time domain as NxM matrix, where N is the number of frequency bands and M
    %   is the number of samples of the input signal.,
    
    szSignal    = size(signal);
    numBands    = size(bounds, 1);
    
    signal      = fft(signal);              % Transform each column to Fourier domain
    dF          = Fs / szSignal(1);         % Freq resolution                
    f           = abs(-Fs/2:dF:Fs/2-dF)';   % create frequency vector centered at 0
    
    frequencyBands = zeros(numBands, szSignal(2));    
    sigShift = abs(fftshift( signal, 1));   % Shift zero-frequency component to center of spectrum
    
    for ib=1:size(bounds,1)
        BPF = (f < bounds(ib, 2)) & (f >= bounds(ib, 1));
        filtered                = BPF .* sigShift;      %Band-pas filter 
        frequencyBands(ib,:)    = ifft(ifftshift(filtered), 'symmetric');   % convert back
    end
end

