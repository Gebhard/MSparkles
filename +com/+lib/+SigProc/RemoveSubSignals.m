%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

% Remove sub-peaks from an analyzed signal. A sub-peak is a peak, occuring during another, stronger
% peak.
%
%   peak        Detected peaks (as returned by findpeaks)
%   loc         Indices of the detected peaks (as returned by findpeaks)
%   intersect   Intersection points [start, end] of the horizontal line, representing the signal
%               duration with the signal curve.
%
%   validIdx    Indices of valid signal peaks (without indices of sub-peaks)

function validIdx = RemoveSubSignals(peak, loc, intersect)
    np = numel(peak);
    validIdx = 1:np;
    removePeaks = [];
    for j=1:np 
        for k=(j+1):np                        
            %% Is other peak higher?
            if peak(j) >= peak(k) && ...
                    (intersect(j,1) <= loc(k) && intersect(j,2) >= loc(k))
                removePeaks = cat(2,removePeaks, k);
            elseif peak(k) >= peak(j) && ...
                loc(j) >= intersect(k,1) && loc(j) <= intersect(k,2)
                removePeaks = cat(2,removePeaks, j);
            end
        end
    end

    if ~isempty(removePeaks)
        removePeaks = unique(removePeaks);
        validIdx(removePeaks) = [];
    end
end

