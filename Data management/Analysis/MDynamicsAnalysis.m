%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDynamicsAnalysis < com.CalciumAnalysis.Analysis.MCaAnalysis
    %MDYNAMICSANALYSIS Summary of this class goes here
    %   Detailed explanation goes here              
    
    methods
        function obj = MDynamicsAnalysis()
            obj = obj@com.CalciumAnalysis.Analysis.MCaAnalysis(com.Enum.AnalysisType.Dynamic);
            obj.AnalysisSettings = com.CalciumAnalysis.Settings.MCaDynamicRoiDetectionCfg;
        end
    end
    
    methods   
        function AnalyzeSignals(~)
            %Not (yet) implemented
        end        
        
        function result = IsIntersectable(~, ~)
            result = false;
        end
        
        function value = CreateAnalysisSettings(~)
            value = com.CalciumAnalysis.Settings.MCaDynamicRoiDetectionCfg;
        end
        
        function DeleteROI(obj, channelID, roiID)
            results = obj.AnalysisResults(channelID);
            results.DeleteROI(roiID);
        end
        
        function CC = AddROI(obj, channelID, ~)
            results = obj.AnalysisResults(channelID);            
            CC = results.ROIs;
        end
        
        function CC = MergeROI(obj, channelID, ~)
            results = obj.AnalysisResults(channelID);            
            CC = results.ROIs;
        end
        
        function CC = CutROI(obj, channelID, ~)
            results = obj.AnalysisResults(channelID);            
            CC = results.ROIs;
        end
        
        function resultset = CreateResultset(~, channelID)
            resultset = MDynamicsAnalysisResult(channelID);
        end
        
         function Edit(obj, hDataset, isNew  )
            com.CalciumAnalysis.UI.dlgDynamicRois(hDataset, obj, isNew);
        end
    end       
end

