%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MDynamicsAnalysisResult < com.CalciumAnalysis.Analysis.MAnalysisResult
    %MDYNAMICSANALYSISRESULT Summary of this class goes here
    %   Detailed explanation goes here

    properties (Transient, SetAccess = private)
        MaxArea = [];
        MaxF = [];
        MinF = [];
    end
    
    properties(Transient, Dependent)
        MaxDuration;
        MaxCaDist
        MaxAreaDist;
    end
    
    methods
        function obj = MDynamicsAnalysisResult(channelID)
            if nargin == 0
                channelID = [];
            end
            obj = obj@com.CalciumAnalysis.Analysis.MAnalysisResult(channelID);
        end
    end
    
    methods 
        function value = get.MaxDuration(obj)
            try
                value = max( horzcat(obj.Results.Duration{:}), [], 'all', 'omitnan');
            catch
                value = 0;
            end
        end
        
        function value = get.MaxCaDist(obj)
            try
                value = max(obj.Results.maxCaDist);
            catch
                try
                    N = obj.Results.NumObjects;
                    obj.Results.CaAcgMeanDists = cell(N,1 );
    
                    for k=1:N
                        meanCaDist = mean(obj.Results.SigTAvg{k}(:));
                        obj.Results.CaAcgMeanDists{k} = abs(obj.Results.SigTAvg{k}(:) - meanCaDist);
                        obj.Results.maxCaDist(k) = max( obj.Results.CaAcgMeanDists{k}(:) );
                    end
                    
                    value = max(obj.Results.maxCaDist);
                catch
                    value = 0;
                end
            end
        end
        
        function value = get.MaxAreaDist(obj)
            try
                value = max(obj.Results.maxADist);
            catch
                try
                    N = obj.Results.NumObjects;
                    obj.Results.AreaMeanDists = cell(N,1 );
    
                    for k=1:N
                        meanADist = mean(obj.Results.Areas{k}(:));
                        obj.Results.AreaMeanDists{k} = abs(obj.Results.Areas{k}(:) - meanADist);
                        obj.Results.maxADist(k) = max( obj.Results.AreaMeanDists{k}(:) );
                    end
                    value = max(obj.Results.maxADist);
                catch
                    value = 0;
                end
            end
        end        

        function value = get.MaxArea(obj)
            if isempty(obj.MaxArea) || obj.MaxArea == 0
                obj.MaxArea = max(cellfun(@max, obj.Results.Areas(:)));                       
            end 
            
            value = obj.MaxArea;
        end
        
        function set.MaxArea(obj, value)
            obj.MaxArea = value;
        end
        
        function value = get.MaxF(obj)
            if isempty(obj.MaxF)
                obj.MaxF = max(obj.Results.SigTAvg, [], 2, "omitnan");               
            end
            
            value = obj.MaxF;
        end
        
        function value = get.MinF(obj)
            if isempty(obj.MinF)
                obj.MinF = min(obj.Results.SigTAvg, [], 2, "omitnan");               
            end
            
            value = obj.MinF;
        end
    end
    
    methods
        function AnalyzeSignals(obj, hDataset)
            try
                if nargin < 2 || isempty(hDataset)
                    hDataset = obj.hParent.hDataset;
                end

                obj.Results = com.CalciumAnalysis.Analysis.MDynamicCaSignals();
                obj.Results.AnalyzeSignals(hDataset, obj.ROIs);
                obj.AnalyzeSynchronicity(); 
            
            catch ex
                com.common.Logging.MLogManager.Error("ROI / signal analysis failed.");
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function UpdateStats(obj)
            if (~isempty(obj.Results))
                obj.MinF = min(obj.Results.SigTAvg, [], 2, 'omitnan');
                obj.MaxF = max(obj.Results.SigTAvg, [], 2, 'omitnan');
            else
                obj.MinF = 0;
                obj.MaxF = 0;
            end
            
            obj.MaxArea = max(cellfun(@max, obj.Results.Areas(:)));
        end 
        
        function lbl = GetLabelMatrix(obj, channel, zPos, frame, RoiIDs)
            if isempty(zPos) || zPos == 0
                 lbl = obj.lblMatrix(:,:, channel, :, 1, frame);
            else
                lbl = obj.lblMatrix(:,:, channel, zPos, 1, frame);
            end
            
            if nargin == 5 && ~isempty(RoiIDs)
                lbl( ~ismember(lbl, RoiIDs) ) = 0;
            end
        end
        
        function OnExportExcel(obj, hXlsFile)
            obj.numSpecialExcelSheets = 2;
            OnExportExcel@com.CalciumAnalysis.Analysis.MAnalysisResult(obj, hXlsFile);
        end        
    end
    
    methods(Access=protected)
        function [syncMatrix, syncIdx] = GetSynchronicityMatrix(obj)
            %Get all active pixels and project them into one image.
            %Sync matrix then contains the unique indices of events active
            %at any time in the dataset
            %syncIdx is the synchronicity index, indicating how
            %synchronized [0..1] the activity is.
            
            try
                numFrames = obj.hParent.hDataset.MetaData.SamplesT;
                lblStack = obj.GetLabelMatrix(obj.ChannelID, [], 1:numFrames, []);
                lblIdx = find(max(lblStack, [], 6) > 1);

                offset =    obj.hParent.hDataset.MetaData.SamplesX * ...
                            obj.hParent.hDataset.MetaData.SamplesY * ...
                            obj.hParent.hDataset.MetaData.SamplesZ;
                
                syncIdx     = zeros(numel(lblIdx), numFrames);
                syncMatrix  = zeros(obj.RoiCount, numFrames, "logical");
                
                for ii=1:numFrames
                    idx = lblIdx + (ii-1)*offset;
                    candidates = lblStack(idx);
                    syncIdx(:,ii) = logical(candidates);
                    
                    u = unique(candidates(candidates > 0));
                    syncMatrix(u, ii) = 1;
                end
                
                syncIdx = sgolayfilt(mean(syncIdx), 3, 7);
                syncIdx(syncIdx < 0) = 0; %Remove artefacts from SGolay filter
            catch ex
                com.common.Logging.MLogManager.Error("Failed to create synchronicity matrix.");
                com.common.Logging.MLogManager.Exception(ex);
                syncMatrix = zeros( obj.hParent.hDataset.MetaData.SamplesX, ...
                                    obj.hParent.hDataset.MetaData.SamplesY, ...
                                    obj.hParent.hDataset.MetaData.SamplesZ);
            end
        end
        
        function OnSetResults(obj)
            obj.MaxArea = [];
            obj.MaxF = [];
            obj.MinF = [];
        end
        
        function OnSetROIs(obj)
            obj.lblMatrix = [];
            obj.morphLut = [];
        end
        
        function WriteSpecialTables(obj, hXlsFile, RowNames)
           %Write table for Peak-to-Peak times
            hXlsFile.Write(RowNames, 'Complexity', 'A2');
            hXlsFile.Write({'Complexity', 'Area complexity', 'Calcium complexity'}, 'Complexity', 'B1');
            hXlsFile.Write( obj.getMatrix( obj.Results.Complexity ), 'Complexity', 'B2');
            hXlsFile.Write( obj.getMatrix( obj.Results.ComplexityE ), 'Complexity', 'C2');
            hXlsFile.Write( obj.getMatrix( obj.Results.ComplexityC ), 'Complexity', 'D2');
            obj.UpdateExportWaitbar();
            
            %ToDo write stats table
            hDataset = obj.hParent.hParent.hParent;
            areaunit = hDataset.MetaData.DomainUnit + "�";
            hXlsFile.Write(RowNames, 'ROI-stats', 'A2');
            hXlsFile.Write({sprintf('Avg. area (%s)',areaunit), 'Area std.', 'Max. area', ...
                'Travelled distance', sprintf('Avg. speed (%s/sec)',areaunit)}, 'ROI-stats', 'B1');

            hXlsFile.Write([obj.Results.AvgArea, obj.Results.AvgAreaStd, obj.Results.MaxArea,...
                obj.Results.distTotal(:,1), obj.Results.AvgPropagationSpeed], 'ROI-stats', 'B2');
            
            obj.UpdateExportWaitbar();
        end
        
        function Traces = RenderRoiTrace(obj, hDataset, hRenderTarget, roiID, lineWidth, varargin)                        
            Traces = [];
            
            if obj.hParent.AnalysisSettings.ShowTracePlots            
                com.CalciumAnalysis.Rendering.Graph.MTracePlot(...
                    'Dataset', hDataset,...
                    'Resultset', obj,...
                    'PlotID', roiID,...
                    'Parent', hRenderTarget,...
                    'LineWidth', lineWidth, ...
                    'MarkerSize', 24,...
                    varargin{:});
            else
                %com.CalciumAnalysis.Rendering.RenderWavePlot(hRenderTarget, hDataset, obj.hParent , obj, roiID);
                com.CalciumAnalysis.Rendering.Graph.MWavePlot(...
                    'Dataset', hDataset,...
                    'Resultset', obj,...
                    'PlotID', roiID,...
                    'Parent', hRenderTarget,...
                    'Analysis', obj.hParent, ...
                    varargin{:});
                
                hGlobalSettings = MGlobalSettings.GetInstance();
                if ~isempty(hGlobalSettings) && hGlobalSettings.WavePlotsDisplayColorBar
                    if isempty(hRenderTarget.Colorbar)
                        colbar = colorbar( hRenderTarget );
                        ylabel(colbar, hDataset.NormalizationName);
                    end
                else
                    if ~isempty(hRenderTarget)
                        colorbar( hRenderTarget, 'off' );
                    end
                end
            end
        end
    end           
end

