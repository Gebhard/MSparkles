%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef (Abstract) MStaticAnalysis < com.CalciumAnalysis.Analysis.MCaAnalysis
    %MSTATICANALYSIS Summary of this class goes here
    %   Detailed explanation goes here    
    
    properties(Transient)
        GenerateRoiTracePlots;
    end
    
    methods
        function obj = MStaticAnalysis(analysisType)
            obj = obj@com.CalciumAnalysis.Analysis.MCaAnalysis(analysisType);
        end
    end          
    
    methods                
        function result = IsIntersectable(obj, hAnalysis)
            % Basic requirements for intersectability:
            % 1) hAnalysis it not intersectable with itself
            % 2) hAnalysis in not already in the list of intersections.
            
            result = hAnalysis ~= obj && ...
                ~any(strcmp(hAnalysis.ID, obj.arrIntersectIDs));                
        end
        
        function DeleteROI(obj, channelID, roiID)
            results = obj.AnalysisResults(channelID);
            results.DeleteROI(roiID);
        end
        
        function CC = AddROI(obj, channelID, roi)
            results = obj.AnalysisResults(channelID);
            CC = results.AddROI(roi);
        end
        
        function CC = MergeROI(obj, channelID, roi)
            results = obj.AnalysisResults(channelID);
            CC = results.MergeROI(roi);
        end
        
        function CC = CutROI(obj, channelID, roi)
            results = obj.AnalysisResults(channelID);
            CC = results.CutROI(roi);
        end        
        
        function resultset = CreateResultset(~, channelID)
            resultset = MStaticAnalysisResult(channelID);
        end
        
        function AnalyzeSignals(obj)
            arrayfun(@(x) x.AnalyzeSignals(), obj.AnalysisResults);
        end
    end    
end

