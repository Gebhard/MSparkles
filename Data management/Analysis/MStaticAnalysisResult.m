%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MStaticAnalysisResult < com.CalciumAnalysis.Analysis.MAnalysisResult
    %MSTATICANALYSISRESULT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties       
        MeanSignal = 0; %Mean signal over all ROIs
    end   
    
    methods
        function obj = MStaticAnalysisResult(channelID)
            obj = obj@com.CalciumAnalysis.Analysis.MAnalysisResult(channelID);
        end
    end
    
    methods
        function AnalyzeSignals(obj, hDataset)
            try
                if nargin < 2 || isempty(hDataset)
                    hDataset = obj.hParent.hDataset;
                end

                obj.Results = com.CalciumAnalysis.Analysis.MStaticCaSignals();
                obj.Results.AnalyzeSignals(hDataset, obj.ROIs);
                obj.AnalyzeSynchronicity(); 
                obj.AnalyzeEegSignalCorrelation();
            catch ex
                com.common.Logging.MLogManager.Error("ROI / signal analysis failed.");
                com.common.Logging.MLogManager.Exception(ex);
            end
        end
        
        function AnalyzeEegSignalCorrelation(obj)
            try
                hDataset = obj.hParent.hDataset;
                if ~isempty(obj.MeanSignal) && ~isempty(hDataset.EEGDataset)
                    hEegAnalysis = hDataset.EEGDataset.Analyses.CurrentAnalysis;
                    %Ensure we have the mean signal
                    obj.UpdateStats();

                    if ~isempty(hEegAnalysis)
                        
                        if hDataset.Settings.EegSync.CorrelationBand == com.EEG.Enum.WaveBand.avgPower
                            corrSig = hEegAnalysis.AnalysisResults.AvgSigPwr;
                            interval = 1; %seconds,epoch size of PSD computation
                        else
                            corrSig = hDataset.EEGDataset.GetWaveBand(hDataset.Settings.EegSync.CorrelationChannel,...
                                hDataset.Settings.EegSync.CorrelationBand);
                            interval = hDataset.EEGDataset.MetaData.SampleRate;
                        end

                        if ~isempty(corrSig)
                            %1 Compute the offset w.r.t the entire signal
                            caSig = obj.GetEegReferenceSignal(hEegAnalysis.AnalysisSettings.CorrRefSignal);

                            if size(corrSig,1) > 1
                                corrSig = mean(corrSig, 1, "omitnan");
                            end
                            
                            [absOffset, obj.EegMaxCorrelation] = MeasureSignalOffset(corrSig(:), caSig,...
                                interval, hDataset.MetaData.VolumeRate);

                            %2 Take synchronization offset into account
                            obj.EegSignalOffset = hDataset.Settings.EegSync.EegSyncStart - absOffset;
                        end
                    end
                end
            catch ex
                obj.EegSignalOffset = [];
                obj.EegMaxCorrelation = [];
                com.common.Logging.MLogManager.Error("EEG correlation analysis failed.");
                com.common.Logging.MLogManager.Exception(ex);
            end
        end      
        
        function sig = GetEegReferenceSignal(obj, CorrRefSignal)
            %Return the configured reference signal for correlation
            %analysis with EEG recording
            switch (CorrRefSignal)
                case com.EEG.Enum.CorrRefSig.SyncIdx
                    sig = obj.SynchronicityIndex(:);
                otherwise
                    sig = obj.MeanSignal(:);
            end
        end
        
        function lbl = GetLabelMatrix(obj, ~, ~, ~, RoiIDs)
            lbl = obj.lblMatrix;
            
            if nargin == 5 && ~isempty(RoiIDs)
                RoiIDs = cast(RoiIDs, class(lbl));
                lbl( ~ismember(lbl, RoiIDs) ) = 0;
            end
        end        
        
        function CC = AddROI(obj, roi)
            bw = createMask(roi);           

            if (isempty(obj.ROIs))
                obj.ROIs = bwconncomp(bw);
            else              
                if (isempty(obj.ROIs.PixelIdxList))
                    obj.ROIs.PixelIdxList = { find(bw>0) };
                else
                    obj.ROIs.PixelIdxList = [obj.ROIs.PixelIdxList , find(bw>0) ];
                end
                obj.ROIs.NumObjects = numel(obj.ROIs.PixelIdxList);
            end
            
            CC = obj.ROIs;
        end
        
        function CC = MergeROI(obj, roi)
            CC = obj.DrawRoi(roi, true);
        end
        
        function CC = CutROI(obj, roi)
            CC = obj.DrawRoi(roi, false);
        end
        
        function UpdateStats(obj)
            if (~isempty(obj.Results))
                obj.MeanSignal = mean(obj.Results.SigTAvg, 1, "omitnan");
            else
                obj.MeanSignal = 0;
            end
        end   
        
        function ResetResults(obj)
            ResetResults@com.CalciumAnalysis.Analysis.MAnalysisResult(obj);
            obj.SynchronicityIndex = [];          
        end
        
        function OnExportExcel(obj, hXlsFile)
            if obj.RoiCount > 0
                obj.numSpecialExcelSheets = 3;
                OnExportExcel@com.CalciumAnalysis.Analysis.MAnalysisResult(obj, hXlsFile);
            else
                com.common.Logging.MLogManager.Warning("No ROI present in analysis '"...
                    + obj.hParent.Name +"' (" + obj.hParent.AnalysisName + ") - Excel file not written!");
            end
        end
    end
    
     methods(Access=protected)
        function [syncMatrix, syncIdx] = GetSynchronicityMatrix(obj)
            %GETSYNCHRONICITYMARIX returns a binary 2D matrix where each row
            %represents a ROI and each column is a descrete time step. If a
            %cell in this matrix is covered (lies inbetween signal start and end)
            %by a signal that has a peak above the minimum classification threshold
            %it will be 1.
            [~, sigStartEnd, ~] = obj.GetClassifiedSignals();

            s = size(sigStartEnd);
            numT = s(2);
            numRois = s(1);

            syncMatrix = zeros(numRois , numT);
            syncIdx = zeros(1 , numT);
            
            try
                if ~isempty(syncMatrix)
                    [row, col] = find( ~isnan(sigStartEnd(:,:,1)) );
                    for ii=1:numel(row)
                        i = row(ii);
                        j= col(ii);
                        syncMatrix(i, round(sigStartEnd(i,j,1)):round(sigStartEnd(i,j,2)) ) = 1;
                    end

                    syncIdx = sgolayfilt(mean(syncMatrix), 3, 7);
                    syncIdx(syncIdx < 0) = 0; %Remove artefacts from SGolay filter
                end
            catch ex
                com.common.Logging.MLogManager.Error("Failed to create synchronicity matrix.");
                com.common.Logging.MLogManager.Exception(ex);
                syncMatrix = zeros(numRois , numT);
                syncIdx = zeros(1 , numT);
            end
        end
        
        function OnSetResults(obj)
            if (iscell(obj.Results))
                obj.Results = obj.Results{1};
            end
            
            %obj.UpdateStats();            
        end
        
        function WriteSpecialTables(obj, hXlsFile, RowNames)
            try
                %Write table for Peak-to-Peak times
                hXlsFile.Write(RowNames, 'Peak-to-Peak', 'A1');
                hXlsFile.Write( obj.getMatrix( obj.Results.PeakToPeakTime ), 'Peak-to-Peak', 'B1');
                obj.UpdateExportWaitbar();

                %Write table for Start-to-Start times
                hXlsFile.Write(RowNames, 'Start-to-Start', 'A1');
                hXlsFile.Write( obj.getMatrix( obj.Results.StartToStartTime ), 'Start-to-Start', 'B1');
                obj.UpdateExportWaitbar();

                %Write table for Inter-Signal times
                hXlsFile.Write(RowNames, 'Inter-Signal', 'A1');
                hXlsFile.Write( obj.getMatrix( obj.Results.InterSignalTime ), 'Inter-Signal', 'B1');
                obj.UpdateExportWaitbar();

                %Write table with ROI stats
                hDataset = obj.hParent.hParent.hParent;

                meanFreq = obj.Results.SigMeanFreq(:,1);
                stdFreq = obj.Results.SigMeanFreq(:,2);

                meanFreq(isnan(meanFreq)) = 0;
                stdFreq(isnan(stdFreq)) = 0;

                areaunit = hDataset.MetaData.DomainUnit + "²";

                sigTStd = obj.Results.SigTStd;
                sigTStd(isnan(sigTStd)) = 0;
                sigTStd = mean(sigTStd, 2);
                
                hXlsFile.Write(RowNames, 'ROI-stats', 'A2');
                hXlsFile.Write([sprintf("Area (%s)", areaunit), "# signals", "Mean duration (sec)", ...
                    "Duration Std (sec)", "Mean freq. (Hz)", "Freq. Std (Hz)", "Mean freq. (1/min)", ...
                    "Freq. Std (1/min)", "Mean signal std. dev."], "ROI-stats", "B1");
                hXlsFile.Write([obj.Results.AvgArea, obj.Results.SignalCount, obj.Results.AvgDuration,...
                    obj.Results.AvgDurationStd, meanFreq, stdFreq, meanFreq .* 60, stdFreq .* 60, sigTStd], 'ROI-stats', 'B2');
            catch
            end
        end
        
        function Traces = RenderRoiTrace(obj, hDataset, hRenderTarget, roiID, lineWidth, varargin)
                        
            hTracePlot = com.CalciumAnalysis.Rendering.Graph.MTracePlot(...
                'Dataset', hDataset,...
                'Resultset', obj,...
                'PlotID', roiID,...
                'Parent', hRenderTarget,...
                'LineWidth', lineWidth,...
                'MarkerSize', 24,...
                varargin{:});
            Traces = hTracePlot.Traces;
        end
     end
    
     methods(Access=private)
        function CC = DrawRoi(obj, roi, doMerge)
            bw = createMask(roi); 
            CCnew = bwconncomp(bw, 8);            
            obj.ROIs = IntersectRoiSets(obj.ROIs, CCnew, doMerge);
            CC = obj.ROIs;
        end                             
        
        function duplicateIdx = getDuplicateEntries(~, m1, m2)
            if ( ~isempty(m1) )
                if( ~isempty(m2) )
                    [~,duplicateIdx,~] = intersect(m1,m2);
                    return;
                end
            end

            duplicateIdx = [];
        end                
    end
end

