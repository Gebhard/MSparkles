function cmap = greenfireblue(m)
% FIREICE Black-Blue-LightCyan-Green-Yellow-White Colormap
%
%  GREENFIREBLUE(M) Creates a colormap with M colors
%
%   Inputs:
%       M - (optional) an integer specifying the number
%           of colors in the colormap. Default is 128.
%
%   Outputs:
%       CMAP - an Mx3 colormap matrix
%
%   Example:
%       imagesc(peaks(500))
%       colormap(greenfireblue), colorbar
%
%   Example:
%       imagesc(interp2(rand(10),2))
%       colormap(greenfireblue); colorbar
%
% See also: hot, jet, hsv, gray, copper, bone, cold, vivid, colors
%
% Author: Gebhard Stopper, adapted from Joseph Kirk
% Email: gebhard.stopper@uks.eu
% Release: 1.0
% Date: 03/01/2020
% Default Colormap Size
if ~nargin
    m = 128;
end

% Black-Blue-LightCyan-Green-Yellow-White
clrs = [0.04 0 0; 0.04 0 0; 0 0.31 0.67; 0.2 0.59 0.41;...
    0.53 1 0; 1 1 0.16; 1 1 1];

y = ([1  2   10    20    35    58    64] - 32.5) / 31.5 * 3;

if mod(m,2)
    delta = min(1,(numel(y)-1)/(m-1));
    half = (m-1)/2;
    yi = delta*(-half:half)';
else
    delta = min(1,(numel(y)-1)/m);
    half = m/2;
    yi = delta*nonzeros(-half:half);
end
cmap = interp2(1:3,y,clrs,1:3,yi);