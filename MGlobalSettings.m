%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MGlobalSettings < handle
    %MGLOBALSETTINGS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ProjectDir;
        CreateResultsDir logical;
        
        GenerateExcelFiles logical;
        GenerateHeatmapFiles logical;
        HeatampsIncludeSyncPlot logical;
        
        GenerateSummaryGraphs logical;
        GenerateCorrelationGraph logical;                
        
        GenerateRoiTraces logical;
        GenerateRoiTracesAuto logical;
        GenerateRoiTracesGrid logical;
        GenerateRoiTracesGlobal logical;
        GenerateRoiTracesManual logical;
        
        RoiTraceShowStd logical;
        RoiTraceShowRiseDecay logical;
        WavePlotsDisplayColorBar logical;
        
        GeneratePDFReport logical;
        reportIncludeRoiTraces logical;
        reportIncludeRoiTracesAuto logical;
        reportIncludeRoiTracesGrid logical;
        reportIncludeRoiTracesGlobal logical;
        reportIncludeRoiTracesManual logical;
        
        AutoImportImageJRois logical = true;
        
        %Messages
        AskBeforeExit logical;
        ShowRestWarnings logical;
        
        DefaultDetrendingType = com.CalciumAnalysis.Data.NormalizationMode.dFF0;
    end
    
    properties(Transient)
        LastPath = [];
    end
    
    properties
        ChannelColors;
        LineColor;
        MedianColor;
        ScatterColor;
        TrendLineColor;        
        ThresholdGroupColors;
    end
    
    properties(Constant)
        ColorLutNames = {  'Jet', 'Parula', 'HSV', 'GreenFireBlue', 'Hot', 'Cool', 'Spring', ...
            'Summer', 'Autumn', 'Winter', 'Gray', 'Bone', 'Copper', 'Pink', ...
            'Lines', 'Colorcube', 'Prism', 'Flag', 'White', 'RedGreen', 'RedYellowGreen', 'GreenGray'};
        
        RoiColorLutNames = ['MSparkles', MGlobalSettings.ColorLutNames];
        
        DefaultChannelColors =    [193/255, 5/255, 22/255;     %red
                            141/255, 183/255, 59/255;   %green
                            3/255, 127/255, 197/255;    %blue
                            220/255, 220/255, 220/255   %white
                            162/255, 25/255, 32/255;    %darker red
                            37/255, 148/255, 71/255;    %darker green
                            33/255, 79/255, 154/255     %darker blue
                            180/255, 180/255, 180/255;  %darker white
                            236/255, 185/255, 16/255    %yellow
                            220/255, 144/255, 22/255;   %orange
                            129/255, 32/255, 111/255];  %purple                            
        
        DefaultLineColor = [63/255, 99/255, 115/255]; %Dark blue        
        DefaultMedianColor = [48/255, 48/255, 48/255];
        DefaultScatterColor = [63/255, 99/255, 115/255]; %Dark blue
        DefaultTrendLineColor = [212/255, 13/255, 18/255];
        
        DefaultThresholdGroupColors = ColorThemes.Colorful();
    end    
    
    methods (Static)
        function instance = GetInstance()
           persistent Instance; 
           
           if isempty(Instance)
              Instance =  MGlobalSettings;
           end
           
           instance = Instance;           
        end
    end
    
    methods(Access=private)
        function obj = MGlobalSettings()
            try
                l = load(MGlobalSettings.GetSettingsSavePath(), '-mat');
                obj = l.settings;
            catch
                %No settinsg file exists => create new object with default
                %values
                obj.CreateResultsDir = true;
        
                obj.GenerateExcelFiles = true;
                obj.GenerateHeatmapFiles = true;
                obj.HeatampsIncludeSyncPlot = true;        
                obj.GenerateSummaryGraphs = true;
                obj.GenerateCorrelationGraph = true; 
                obj.GenerateRoiTraces = false;
                obj.GenerateRoiTracesAuto = false;
                obj.GenerateRoiTracesGrid = false;
                obj.GenerateRoiTracesGlobal = false;
                obj.GenerateRoiTracesManual = false;
                obj.RoiTraceShowStd = false;
                obj.RoiTraceShowRiseDecay = false;
                obj.WavePlotsDisplayColorBar = false;
                
                obj.GeneratePDFReport = true;
                obj.reportIncludeRoiTraces = true;
                obj.reportIncludeRoiTracesAuto = true;
                obj.reportIncludeRoiTracesGrid = false;
                obj.reportIncludeRoiTracesGlobal = false;
                obj.reportIncludeRoiTracesManual = true;
                obj.AutoImportImageJRois = true;
                
                obj.AskBeforeExit = true;
                obj.ShowRestWarnings = true;
                
                obj.ChannelColors = MGlobalSettings.DefaultChannelColors;
                obj.LineColor = MGlobalSettings.DefaultLineColor;
                obj.MedianColor = MGlobalSettings.DefaultMedianColor;
                obj.ScatterColor = MGlobalSettings.DefaultScatterColor;
                obj.TrendLineColor = MGlobalSettings.DefaultTrendLineColor;        
                obj.ThresholdGroupColors = MGlobalSettings.DefaultThresholdGroupColors;
            end
        end   
    end
    
    methods  
        function value = get.WavePlotsDisplayColorBar(obj)
            try
                if isempty(obj.WavePlotsDisplayColorBar)
                    obj.WavePlotsDisplayColorBar = false;
                end
                
                value = obj.WavePlotsDisplayColorBar;
            catch
                value = false;
            end
        end
        
        function value = get.RoiTraceShowRiseDecay(obj)
             try
                if isempty(obj.RoiTraceShowRiseDecay)
                    obj.RoiTraceShowRiseDecay = false;
                end
                
                value = obj.RoiTraceShowRiseDecay;
            catch
                value = false;
            end
        end
        
        function value = get.RoiTraceShowStd(obj)
             try
                if isempty(obj.RoiTraceShowStd)
                    obj.RoiTraceShowStd = false;
                end
                
                value = obj.RoiTraceShowStd;
            catch
                value = false;
            end
        end
        
        function value = get.AskBeforeExit(obj)
            try
                if isempty(obj.AskBeforeExit)
                    obj.AskBeforeExit = true;
                end
                
                value = obj.AskBeforeExit;
            catch
                value = true;
            end
        end
        
        function value = get.ShowRestWarnings(obj)
            try
                if isempty(obj.ShowRestWarnings)
                    obj.ShowRestWarnings = true;
                end
                value = obj.ShowRestWarnings;
            catch
                value = true;
            end
        end
        
        function value = get.HeatampsIncludeSyncPlot(obj)
            try
                value = obj.HeatampsIncludeSyncPlot;
            catch
                value = true;
            end
        end
        
        function value = get.GenerateCorrelationGraph(obj)
            try
                value = obj.GenerateCorrelationGraph;
            catch
                value = true;
            end
        end
        
        function value = get.GenerateSummaryGraphs(obj)
            try
                value = obj.GenerateSummaryGraphs;
            catch
                value = true;
            end
        end        
        
        function value = get.ProjectDir(obj)
           if isempty(obj.ProjectDir)
               obj.ProjectDir = MGlobalSettings.GetDefaultProjectDir();                          
           end
           
           value = obj.ProjectDir;
        end
        
        function value = get.ChannelColors(obj)
            if isempty(obj.ChannelColors)
                obj.ChannelColors = MGlobalSettings.DefaultChannelColors;
            end
            
            value = obj.ChannelColors;
        end
        
        function value = get.LineColor(obj)
            if isempty(obj.LineColor)
                obj.LineColor = MGlobalSettings.DefaultLineColor;
            end
            
            value = obj.LineColor;
        end
        
        function value = get.MedianColor(obj)
            if isempty(obj.MedianColor)
                obj.MedianColor = MGlobalSettings.DefaultMedianColor;
            end
            
            value = obj.MedianColor;
        end
        
        function value = get.ScatterColor(obj)
            if isempty(obj.ScatterColor)
                obj.ScatterColor = MGlobalSettings.DefaultScatterColor;
            end
            
            value = obj.ScatterColor;
        end
        
        function value = get.TrendLineColor(obj)
            if isempty(obj.TrendLineColor)
                obj.TrendLineColor = MGlobalSettings.DefaultTrendLineColor;
            end
            
            value = obj.TrendLineColor;
        end
               
        function value = get.ThresholdGroupColors(obj)
            if isempty(obj.ThresholdGroupColors)
                obj.ThresholdGroupColors = MGlobalSettings.DefaultThresholdGroupColors;
            end
            
            value = obj.ThresholdGroupColors;
        end                
                
        function logDir = GetLogDir(self)
            projDir = self.ProjectDir;
            logDir = fullfile(projDir, 'Logs');
            
            if (~isfolder(logDir))
                mkdir(logDir); 
            end 
        end
        
        function Save(settings)
           save(MGlobalSettings.GetSettingsSavePath(), 'settings');
        end        
    end
    
    methods(Access=private)
       
    end
    
    methods(Access=private, Static)                    
        function userDir = GetUserDir()
            %GETUSERDIR   return the user home directory.
            %   USERDIR = GETUSERDIR returns the user home directory using the registry
            %   on windows systems and using Java on non windows systems as a string
            %
            %   Example:
            %      getuserdir() returns on windows
            %           C:\Documents and Settings\MyName\Eigene Dateien

            if ispc
                userDir = winqueryreg('HKEY_CURRENT_USER',...
                    ['Software\Microsoft\Windows\CurrentVersion\' ...
                     'Explorer\Shell Folders'],'Personal');
            else
                userDir = char(java.lang.System.getProperty('user.home'));
            end
        end
         
        function dir = GetDefaultProjectDir()
            usrDir = MGlobalSettings.GetUserDir(); 
            dir = fullfile(usrDir, 'MSparkles Projects');
        end
        
        function path = GetSettingsSavePath()
            path = fullfile(userpath, 'MSparklesSettings.ini');
        end
    end
end

