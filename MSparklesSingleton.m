%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

classdef MSparklesSingleton < handle
    properties(Constant)
        MSparklesVersion = "1.8.19";
    end
    
    properties
        ProjectManager;
        AppWindow;

        hEegMainWnd;
        hCaMainWnd;
    end   
    
    properties(SetObservable=true)
        CurrentDataset;
        CurrentAnalysis;
        CurrentKey;         %Currently pressed keyboard key
    end
    
    methods
        function delete(obj)
            try 
                obj.ProjectManager.delete();
            catch
            end
            
            try
                obj.AppWindow.delete();
            catch
            end
        end
    end
    
    methods
        function value = get.CurrentDataset(obj)
            if ~isempty(obj.ProjectManager)
                value = obj.ProjectManager.CurrentDataset;
            else
                value = [];
            end
        end
        
        function value = get.CurrentAnalysis(obj)
            if ~isempty(obj.ProjectManager)
                value = obj.ProjectManager.CurrentAnalysis;
            else
                value = []; 
            end
        end
        
    end
    
    methods
        function CloseAppWindows(obj)
            %Just close all
            obj.CloseEegWindow();
            obj.CloseCalciumWindow();
            obj.CloseCellCounterWindow();
            
            pause( 0.5 );
        end
        
        function CloseEegWindow(obj)
            try
                delete( obj.hEegMainWnd );
            catch
            end
            
             obj.hEegMainWnd = [];
        end
        
        function CloseCalciumWindow(obj)
            try
                delete( obj.hCaMainWnd );
            catch
            end
            
             obj.hCaMainWnd = [];
        end
        
        function CloseCellCounterWindow(~)
        end
    end
    
    methods(Static)
        function value = GetInstance()
            try
                value = evalin('base', 'hMSparkles');
            catch
                value = [];
            end
        end
        
        function DeleteInstance()
            try
                evalin( 'base', 'clear hMSparkles' );
            catch
            end
        end               
    end
end

