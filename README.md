<h1>Introduction</h1>

![MSparkles mein window](help/images/MSparkles.png)

<p>
MSparkles is a MATLAB&#174;-based analysis tool, designed to analyze fluorescence fluctuations, caused by molecular signaling, such as intracellular Ca<sup>2+</sup> or Na<sup>+</sup> transients. MSparkles operates in a pixel-based manner and is agnostic to cell types and specific fluorophores. We have successfuly used MSparkles to analyze Ca<sup>2+</sup> signals (visualized with GCaMP3) <i>in vivo</i> and <i>ex vivo</i> in the murine brain and spinal cord on astrocytes, microglia and oligodendrocytes. We also used it to analyze ratiometric Na<sup>+</sup> signals in accute slices (visualized with SBFI-AM).
</p>
<p>
MSparkles is a fully graphical application, requiering no programming skills. It features an automated processing pipeline and provides direct visual feedback for easier parameter tuning. All results and generated graphs (and the raw data) are automatically exportedto a MS Excel&#174; file for further statistical analysis. In addition, MSparkles features integrated tools to export videos and customized graphs, ready for presentation.
</p>
<p>This version of MSparkles has been tested to run on Windows, Linux and (Intel) Mac systems.</p>

For more details, please have a look at our recent publications:

<p>
    <p>
    <a href="https://www.researchsquare.com/article/rs-2968234/v1">Novel algorithms for improved detection and analysis of fluorescent signal fluctuations (CURRENTLY UNDER REVIEW)</a><br>
        <i>Gebhard Stopper<sup>1</sup>, Laura C. Caudal<sup>1</sup>, Phillip Rieder<sup>1</sup>, Davide Gobbo<sup>1</sup>, Laura Stopper<sup>1</sup>, Lisa Felix<sup>2</sup>, Katharina Everaerts<sup>2</sup>, Xianshu Bai<sup>1</sup>, Christine R. Rose<sup>2</sup>, Anja Scheller<sup>1</sup>, Frank Kirchhoff<sup>1</sup></i>
        <br>
        <i><sup>1</sup> Department of Molecular Physiology, Center for Integrative Physiology and Molecular Medicine (CIPMM), University of Saarland, 66421 Homburg, <br>
        <sup>2</sup> Institute of Neurobiology, Faculty of Mathematics and Natural Sciences, Heinrich Heine University Düsseldorf, 40225 Düsseldorf
        </i>
    </p>
</p>
<p>
    <p>
        <a href="https://www.frontiersin.org/articles/10.3389/fnmol.2022.840948/full">Astrocytes and Microglia Exhibit Cell-Specific Ca2+ Signaling Dynamics in the Murine Spinal Cord</a><br>
        <i>
            Phillip Rieder<sup>1</sup>, Davide Gobbo<sup>1</sup>, Gebhard Stopper<sup>1</sup>, Anna Welle<sup>2</sup>, Elisa Damo<sup>1</sup>,<sup>1</sup>3, Frank Kirchhoff<sup>1</sup> and Anja Scheller<sup>1</sup>
        </i><br>
        <i>
            <sup>1</sup> Department of Molecular Physiology, Center for Integrative Physiology and Molecular Medicine (CIPMM), University of Saarland, Homburg, Germany <br>
            <sup>2</sup> Department of Genetics and Epigenetics, University of Saarland, Saarbrücken, Germany <br>
            <sup>3</sup>Institute of Pharmacology, Medical Faculty Heidelberg, Heidelberg University, Heidelberg, Germany
        </i>
    </p>
</p>

<h1>Setup</h1>
To setup MSparkles, checkut the code from this repository, or download the [MATLAB toolbox](MSparkles.mlappinstall "download") and install it to you MATLAB path. To run MSparkles, you need these additional toolboxes from the MATLAB file exchange:
<ul>
<li>[GUI Layout Toolbox](https://de.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox?s_tid=ta_fx_results)</li>
<li>[Widgets Toolbox 1.3.330](https://de.mathworks.com/matlabcentral/mlc-downloads/downloads/b0bebf59-856a-4068-9d9c-0ed8968ac9e6/099f0a4d-9837-4e5f-b3df-aa7d4ec9c9c9/packages/mltbx)</li>
</ul>

Moreover, you need these MATLAB toolboxes installed (and licenced):
<ul>
<li>Image processing toolbox</li>
<li>Computer vision toolbox</li>
<li>Signal processing toolbox</li>
<li>Wavelet toolbox</li>
<li>MATLAB report generator</li>
<li>Parallel computing toolbox</li>
<li>Spreadsheet link</li>
</ul>

<h1>Getting started</h1>
To get started with your analysis, just follow these 5 steps. For mor information and screenshots, please have a look at the <a href="https://glcdn.githack.com/Gebhard/MSparkles/-/raw/master/help/index.html">MSparkles help</a> and the <a href="https://glcdn.githack.com/Gebhard/MSparkles/-/raw/master/help/sites/HowToImport.html">How-To guide</a>.
<ol>
<li>Import your data.</li>
<li>Configure and run pre-processing.</li>
<li>Configure and run F<sub>0</sub> estimation</li>
<li>Configure and run ROI detection</li>
<li>Configure (if necessary) you signal analysis settings</li>
</ol>

After analysis, you results and some graphs are automatically exported. They will be in a <i>"results"</i>-folder, located inside you original data directory.

<h1>Acknowledgements</h1>
<h4>MSparkles is using the following 3rd-party software and resources.</h4>			  
<p>
<ul>
    <li><p><b>Icons</b> from <a href="https://icons8.de">icons8.de</a></p></li>
    <li><p><b>ScanImageTiffReader</b>, Licenceed under the <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache Licence 2.0</a>, Copyright &copy; Vidrio Technologies LLC.</p></li>
    <li><p><b>OWT SURE-LET</b>, downloadable from <a href="http://bigwww.epfl.ch/demo/suredenoising/matlab.html">bigwww.epfl.ch</a>, Copyright &copy; Florian Luisier, Thierry Blu and Michael Unser.<br>
    Reference:<br>
        <i>[1] F. Luisier, T. Blu, M. Unser, "A New SURE Approach to Image
        Denoising: Interscale Orthonormal Wavelet Thresholding," 
        IEEE Transactions on Image Processing, vol. 16, no. 3, pp. 593-606, 
            March 2007</i>
    </p></li>
    <li><b>decompose_kernel</b>, downloadable from <a href="https://de.mathworks.com/matlabcentral/fileexchange/28238-kernel-decomposition">MathWorks File Exchange</a>, Copyright &copy; Cris Luengo</li>
    <li><b>findjobj</b>, downloadable from <a href="https://de.mathworks.com/matlabcentral/fileexchange/14317-findjobj-find-java-handles-of-matlab-graphic-objects">MathWorks File Exchange</a>, Copyright &copy; by Yair M. Altman</li>
    <li><b>multiWaitbar</b>, downloadable from <a href="https://de.mathworks.com/matlabcentral/fileexchange/26589-multiwaitbar-label-varargin">MathWorks File Exchange</a>, By Ben Tordoff Copyright &copy; The MathWorks, Inc.</li>
    <li><b>ReadImageJROI</b>, licenced under <a href="https://www.gnu.org/licenses/gpl-3.0.de.html">GNU General Public License version 3</a>, downloadable from <a href="https://de.mathworks.com/matlabcentral/fileexchange/32479-readimagejroi">MathWorks File Exchange</a>, Copyright &copy; Dylan Muir</li>
</ul>
</p>           

