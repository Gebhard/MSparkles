function colors = Blue(numCols)
    if nargin==1
        step = 1/(numCols-1);
    else
        step = 1/4;
    end
    
    %cols = [159/255 196/255 224/255; 0/255 42/255 76/255];    %brightest; darkest    
    cols = [142/255 177/255 204/255; 0/255 45/255 80/255];    %brightest; darkest    
    colors = interp1( [1 2], cols, 1:step:2);
end

