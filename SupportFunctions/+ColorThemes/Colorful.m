function colors = Colorful()
%     colors = [  84/255,132/255,154/255;     %blue
%                 112/255,173/255,71/255;     %green
%                 204/255,0/255,3/255;        %red
%                 255/255,192/255,0/255;      %yellow
%                 158/255,94/255,155/255;     %purple
%                 149/255,182/255,197/255;    %brighter blue
%                 168/255,208/255,141/255;    %brighter green
%                 255/255,102/255,102/255;    %brighter red
%                 255/255,217/255,101/255;    %brighter yellow
%                 197/255,157/255,195/255];   %brighter purple
            
    colors = [  0/255,37/255,64/255;     %blue
                95/255,191/255,67/255;     %green
                245/255,24/255,54/255;        %red
                225/255,194/255,0/255;      %yellow
                97/255,24/255,70/255;     %purple
                17/255,74/255,114/255;    %brighter blue
                125/255,204/255,102/255;    %brighter green
                255/255,63/255,92/255;    %brighter red
                255/255,225/255,76/255;    %brighter yellow
                178/255,71/255,139/255];   %brighter purple
end

