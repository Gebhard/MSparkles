function colors = Red(numCols)
    if nargin==1 && numCols > 2
        step = 1/(numCols-1);
    else
        step = 1/4;
    end
      
    cols = [255/255 29/255 35/255; 69/255 0/255 3/255];    %brightest; darkest    
    colors = interp1( [1 2], cols, 1:step:2);
end

