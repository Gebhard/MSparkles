%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function S = CalcEntropy(src, maxLen)
%CALCENTROPY Summary of this function goes here
%   Detailed explanation goes here

    src = src(:);
        
    %src = src ./ max(src);
    
    %compute center 
    c1 = mean(src);
    %s1 = std(src);
    
    
    dist = zeros(maxLen, 1);
    %Per-vertex distance from center
    dist(1:numel(src)) = sqrt ( (src - c1) .^2 );

    %Get optimal histogram resolution
    %j = GetHistogramResolution(nDist);
    j = 5; %2^j bins
    
    %Compute histogram
    edges = 0:1/(2^j):1;
    N = histcounts(dist, edges);

    %Normalize (Probability density function)
    myPDF = N ./ sum(N(:));
    
    %compute entropy
    logPdf = log(myPDF);
    logPdf(myPDF == 0) = 0;

    S = -sum(myPDF .* logPdf );
    
    %Compensate for differences in number of elements
    %S = S * log10( numel(src) );
end

function J = GetHistogramResolution(dist)
    sz = size(dist);
    
    %Compute maximum histogram resolution (number of bins)
    %Ensure we have at least four bins, ant at max log2(N)/4, where N is the
    %number of input points ( distances )
    
    jMin = 2;
    jMax = round ( max(jMin, log2(sz(1) / 4 )) );
    
    %allocate space for quantization values
    w = zeros(jMax -1, 2);
    
    for j=jMin:jMax
        [S, e] = CalcCost(dist, j);
        w(j - jMin + 1, :) = [S, e];
    end
    
    %Normalize error
    w(:,2) = w(:,2) ./ max(w(:,2));
    
    %compute final cost
    fCost = sum(w, 2, "omitnan");
    
    %Get the minimal resolution with smallest cost
    J = max( min( find( fCost == min(fCost) ) + (jMin - 1) ), jMin);
end

function [Snorm, err] = CalcCost(dist, J)
    sz = size(dist);
    
    nBins = 2^J;
    [N, ~, bin] = histcounts(dist, nBins);
    
    %Compute entropy for this resolution
    %Normalize (Probability density function)
    pdf = N ./ sum(N(:));

    %compute entropy
    logPdf = log(pdf);
    logPdf(pdf == 0) = 0; %Ensure we get no crazy stuff if bins are empty...
    
    S = -sum( pdf .* logPdf );
        
    %compute Error
    [BinVal, ~, ix] = unique(bin);
    avgDist = accumarray(ix, dist, [nBins, 1], @mean);
    err =  sqrt( 1/sz(1) * sum((dist - avgDist(bin)) .^2));
    
    %compute final cost
    Snorm = S / log2(sz(1));
end

