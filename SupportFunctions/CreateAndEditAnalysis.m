%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function CreateAndEditAnalysis(hDataset, analysis)
    %CREATEANDEDITANALYSIS can create new MAnalysis object and edit
    %existing ones.
        % hDatasetNode  Handle of the parent dataset node in the tree
        % analysis      If analysis is a handle to a MAnalysis object that 
        %               already esists this object will be edited. If
        %               analysis is a member of com.Enum.AnalysisType instead, a
        %               new MAnalysis-object of that type will be created.        
    
    % Create new MAnalysis-object, if required
    if isa(analysis, 'com.Enum.AnalysisType')
        isNew = true;        
        
        %Create a new analysis to apply for all available channels
        hAnalysis = hDataset.Analyses.CreateAnalysis(analysis, 1:hDataset.MetaData.NumChannels );
    else
        isNew = false;
        
        if ischar(analysis) || isstring(analysis)
            hAnalysis = hDataset.Analyses.GetAnalysis(analysis);
        else
            hAnalysis = analysis;
        end
    end
    
    % Open the respective dialog for editing
    if ~isempty(hAnalysis)
        hAnalysis.Edit( hDataset, isNew );
        
        if hAnalysis.AnalysisType == com.Enum.AnalysisType.EEGSpikeTrainDetection
            UpdateEegWnd();
        else
            UpdateCaWnd();
        end
    end    
end

