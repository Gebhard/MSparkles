%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function hSection = CreateDataMgmtButtons(hasImportRoisBtn)
    import matlab.ui.internal.toolstrip.*    
    icondir = iconrootdir();
    
    hSection = Section('Data management');
    
    hCol1 = hSection.addColumn();
    hBtn = SplitButton('Add data', fullfile(icondir,'add_dataset_large.png'));
    hBtn.Description = 'Add one or more datasets.';
    hBtn.ButtonPushedFcn = @onBtnAddDataset;     % invoked when user clicks the main split-button part
    hBtn.DynamicPopupFcn = @CreateAddDataPopup; % invoked when user clicks the drop-down selector widget
    hCol1.add( hBtn );

    hCol2 = hSection.addColumn();
    hBtn = Button('Load', fullfile(icondir, 'opendataset.png'));
    hBtn.Description = 'Open the selected dataset.';
    hBtn.ButtonPushedFcn = @onBtnOpenDataset;
    hCol2.add( hBtn );

    hCol3 = hSection.addColumn();
    hBtn = Button('Create group', fullfile(icondir, 'add_folder.png'));
    hBtn.Description = 'Create a group that can contain multiple datasets.';
    hBtn.ButtonPushedFcn = @onBtnCreateGroup;
    hCol3.add( hBtn );
    
    if nargin > 0 && hasImportRoisBtn
        hBtn = Button('Import ROIs', fullfile(icondir, 'import.png'));
        hBtn.Description = 'Import ROIs from ImageJ.';
        hBtn.ButtonPushedFcn = @onBtnImportRois;
        hCol3.add(hBtn);
    end

    hBtn = Button('Delete', fullfile(icondir, 'delete.png'));
    hBtn.Description = 'Delete selected dataset or group.';
    hBtn.ButtonPushedFcn = @onBtnDelete;
    hCol3.add( hBtn );

    hCol4 = hSection.addColumn();
    hBtn = SplitButton('Open folder', fullfile(icondir, 'folder_large.png'));
    hBtn.Description = 'Open the folder containing the currently selected dataset in Windows Explorer.';
    hBtn.ButtonPushedFcn = @onBtnOpenFileLocation;
    hBtn.DynamicPopupFcn = @CreateOpenFilePopup; % invoked when user clicks the drop-down selector widget
    hCol4.add( hBtn );
end

function onBtnAddDataset(~,~)        
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan)
        parentNode = hProjMan.FindParentFolder(hProjMan.CurrentItem); 
        hProjMan.AddFiles(parentNode);
    end
end  

function hPopup = CreateAddDataPopup(~,~)
    import matlab.ui.internal.toolstrip.* 
    icondir = iconrootdir();

    hPopup = PopupList();

    % list header #1
    header = PopupListHeader('Add Dataset');
    hPopup.add(header);

     % list item #1
    item = ListItem('Add image sequence', fullfile(icondir, 'imageseq.png'));
    item.Description = 'Add an image sequence as dataset.';
    item.ShowDescription = true;
    item.ItemPushedFcn = @onBtnAddImageSequence;
    hPopup.add(item); 
    
     % list item #1
    item = ListItem('Attach EEG data', fullfile(icondir, 'eeg.png'));
    item.Description = 'Attach EEG data to the current dataset';
    item.ShowDescription = true;
    item.ItemPushedFcn = @onBtnAddEegData;
    hPopup.add(item);                           
end

function onBtnAddImageSequence(~,~)
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan)
        parentNode = hProjMan.FindParentFolder(hProjMan.CurrentItem); 
        hProjMan.AddImageSequence(parentNode);
    end
end

function onBtnAddEegData(~,~)
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan)
       hProjMan.AddEEGFile(hProjMan.CurrentItem);
    end
end

function onBtnOpenDataset(~,~)        
    hProjMan = com.Management.MProjectManager.GetInstance();
    hInst = MSparklesSingleton.GetInstance();

    if ~isempty(hProjMan) && ~isempty(hInst)
        %First delete old AppFigures
        hInst.CloseAppWindows();                
        hProjMan.LoadSelectedDataset();
    end
end

function onBtnCreateGroup(~,~)        
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan)
        hProjMan.CreateNewGroup(hProjMan.CurrentItem);
    end
end

function onBtnImportRois(~,~)
    projMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(projMan)
        if ~isempty(projMan.CurrentItem) && (projMan.CurrentItem.Value == com.Enum.TreeNodeTypes.DataSet)
            projMan.ImportRois();
        else
            errordlg('Please select a dataset to add ROIs', 'Cannot add ROIs');
        end
    end
end

function onBtnDelete(~, ~)  
    hProjMan = com.Management.MProjectManager.GetInstance();                   
    if ~isempty(hProjMan)                
        %hSelItem = hProjMan.SelectedNodes;

        msgBoxResult = questdlg({'Do you really want to delete the selected element(s)?';...
                 'This cannot be undone!'}, 'Delete all analyses?','Yes', 'No', 'No' );
        
        if (strcmpi(msgBoxResult, 'yes') == true)
            %for hNode=hSelItem                
            hProjMan.DeleteNode(hProjMan.SelectedNodes);
            %end
        end
    end
end   

function onBtnOpenFileLocation(~,~)
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan) 
        hNode = hProjMan.GetCurrentTreeNode();
        hProjMan.ShowInBrowser(hNode);
    end
end

function hPopup = CreateOpenFilePopup(~,~)
    import matlab.ui.internal.toolstrip.* 
    icondir = iconrootdir();

    hPopup = PopupList();

    % list header #1
    header = PopupListHeader('Open');
    hPopup.add(header);

     % list item #1
    item = ListItem('Show results folder', fullfile(icondir, 'results_folder.png'));
    item.Description = 'Open results folder of selected dataset in Windows Explorer.';
    item.ShowDescription = true;
    item.ItemPushedFcn =@onBtnOpenResultsFolder;
    hPopup.add(item);            
end

function onBtnOpenResultsFolder(~,~)
    hProjMan = com.Management.MProjectManager.GetInstance();
    if ~isempty(hProjMan)           
        hNode = hProjMan.GetCurrentTreeNode();
        hProjMan.OpenResultsFolder(hNode);
    end
end