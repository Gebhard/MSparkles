%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function EditDatasetSettings(hDatasetNode)
%EDITDATASETSETTINGS Summary of this function goes here
%   Detailed explanation goes here
    hDataset = hDatasetNode.Dataset;
        
    switch hDatasetNode.Type
        case com.Enum.TreeNodeTypes.DataSet
            com.CalciumAnalysis.UI.Settings(hDatasetNode);
            if (hDataset.IsLoaded)
                if hDataset.bRequiresReload == true
                    hDataset.LoadData();
                end

                UpdateCaWnd();
            end
        case com.Enum.TreeNodeTypes.EEGDataset
            hParent = hDatasetNode.GetParentDatasetNode();
            if ~isempty(hParent)
                com.EEG.UI.Settings(hParent.Dataset);
            end
    end        
end

