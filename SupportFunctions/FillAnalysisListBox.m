%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function FillAnalysisListBox(hListBox, analyses)
    hListBox.Items = {};
    hListBox.ItemsData = {};
    analysisList = {};
    
    if ~isempty(analyses)  
        if isa(analyses, "com.common.Analysis.AnalysisManagerRoot")   %Analysis manager
            analysisList = analyses.ValuesSorted;
        elseif isa(analyses, "com.CalciumAnalysis.Analysis.MCaAnalysis")    %Single analysis
            hListBox.Items =  {analyses.Name};
            hListBox.ItemsData =  {analyses.ID};
        elseif iscell(analyses)
            analysisList = analyses;
        end
        
        if ~isempty(analysisList)
            for i=1:numel( analysisList )
                analysis = analysisList{i};                    
                hListBox.Items =  [hListBox.Items, string(analysis.Name)];
                hListBox.ItemsData =  [hListBox.ItemsData, {analysis.ID}];
            end
        end
    end
end