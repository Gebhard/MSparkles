%--------------------------------------------------------------------------%
% Copyright � 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function gaussFilt = GetGaussKernel(sigma, N)    
    % Get 1D, 2D or 3D Gaussian weighting filter
    %   Computes normalized gaussian kernels in the specified dimensions
    %   2D case not tested yet.
    %   sigma = the sigma of the kernel to generate
    %   N = size vector, specifying the half sizes of the gauss kernel to be generated
    
    numDims = numel(N);

    if (numDims == 1)
        kSize = 2*N+1;
        %gaussFilt = zeros(1,1,L);
        %gausswin expects an alpha value, not sigma.
        %Relation of sigma and alpha is:  ? = (L � 1)/(2?)
%         alpha = (L-1)/2*sigma;        
%         gaussFilt = gausswin(L, alpha);
%         sumFilt = sum(gaussFilt);
%         if (sumFilt ~= 0)
%             gaussFilt  = gaussFilt/sumFilt;
%         end
        x = linspace(-N, N, kSize);
        gaussFilt = exp(-x .^ 2 / (2 * sigma ^ 2));
        gaussFilt = gaussFilt / sum (gaussFilt); % normalize
    elseif (numDims == 2)
        gaussFilt = fspecial('gaussian',2*N+1,sigma);
    else
        [x,y,z] = ndgrid(-N(1):N(1),-N(2):N(2),-N(3):N(3));
        arg = -(x.*x + y.*y + z.*z)/(2*sigma*sigma);
        gaussFilt = exp(arg);
        gaussFilt(gaussFilt<eps*max(gaussFilt(:))) = 0;
        sumFilt = sum(gaussFilt(:));
        if (sumFilt ~= 0)
            gaussFilt  = gaussFilt/sumFilt;
        end
    end
end