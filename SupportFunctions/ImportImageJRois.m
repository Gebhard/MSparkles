%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function ImportImageJRois(hDataset, ijRois)    
    if ~isempty(hDataset)
        hProjectManager = com.Management.MProjectManager.GetInstance();
        logger = com.common.Logging.MLogManager('');

        multiWaitbar('Importing ROIs', 'Color', 'g', 'Value', 0);

        if ~isempty(hProjectManager)
            %create dummy axes for ROI generation (needed by imroi and derived
            %classes)
            hMetadata = hDataset.MetaData;

            hAnalysis = MManualAnalysis;
            hAnalysis.Name = 'ImageJ ROIs';
            hAnalysis.AnalysisSettings = com.CalciumAnalysis.Settings.MCaManualRoiCfg;
            hDataset.Analyses.AddAnalysis(hAnalysis);                

            pos = [0, hMetadata.SamplesY, hMetadata.SamplesX, hMetadata.SamplesY];
            ax = axes('Visible', 'off', 'Position', pos, 'Units', 'pixels');
            image(zeros(hMetadata.SamplesY, hMetadata.SamplesX), 'Parent', ax);

            numRois = numel(ijRois);
            for i=1:numRois
                roi = ijRois{i};

                try
                    switch( lower(roi.strType) )
                        case  'rectangle'
                            pos = [roi.vnRectBounds(2), roi.vnRectBounds(1), roi.vnRectBounds(4) - roi.vnRectBounds(2), roi.vnRectBounds(3) - roi.vnRectBounds(1)];
                            newRoi = imrect(ax, pos);                    
                        case 'oval'
                            pos = [roi.vnRectBounds(2), roi.vnRectBounds(1), roi.vnRectBounds(4) - roi.vnRectBounds(2), roi.vnRectBounds(3) - roi.vnRectBounds(1)];
                            newRoi = imellipse(ax, pos);                    
                        case 'polygon'
                            newRoi = impoly(ax, roi.mnCoordinates);                    
                        case 'freehand'
                            newRoi = impoly(ax, roi.mnCoordinates);                     
                        case 'line'                        
                            pos = reshape(roi.vnLinePoints, [2 2]);
                            newRoi = imline(ax, pos);                    
                        otherwise
                            logger.LogWarning(sprintf('Encountered unsupported ROI type (%s)', roi.strType));
                            newRoi = [];                        
                    end

                    if ~isempty(newRoi)
                        %hProjectManager.AddRoiToDataSet(hDatasetNode, TreeNodeTypes.ManualAnalysis, newRoi);
                        hAnalysis.AddROI(1, newRoi);
                    end
                catch ex
                    logger.LogException(ex);
                end   

                multiWaitbar('Importing ROIs', 'Value', i/numRois);
            end

            hProjectManager.AddAnalyses(hDataset);

            multiWaitbar('Importing ROIs', 'close');
        end
    end
end

