%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function CC = IntersectRoiSets(CC,CCnew, doMerge)

    if ~isempty(CCnew) && ~isempty(CC)
        for r=1:CCnew.NumObjects
            newRoi = CCnew.PixelIdxList{r};

            intersectIdx = zeros(CC.NumObjects,1);

            for i=1:CC.NumObjects
                c = intersect(newRoi, CC.PixelIdxList{i});
                if (~isempty(c))
                    intersectIdx(i) = i;
                end
            end

            intersectIdx(intersectIdx == 0) = [];

            if ( ~isempty(intersectIdx))           
                if (doMerge)                        
                    idx = intersectIdx(1);

                    CC.PixelIdxList{idx} = unique(vertcat(CC.PixelIdxList{intersectIdx},newRoi));
                    intersectIdx(1) = [];
                    CC.PixelIdxList(intersectIdx) = [];
                    %CC.NumObjects = CC.NumObjects - numel(intersectIdx);            
                else 
                    CCtmp.Connectivity = CC.Connectivity;
                    CCtmp.ImageSize = CC.ImageSize;
                    CCtmp.NumObjects = 1;                

                    newROIs = {};

                    for i=1:numel(intersectIdx)
                        idx = intersectIdx(i);

                        CCtmp.PixelIdxList{1} = setdiff(CC.PixelIdxList{idx}, newRoi);
                        L = labelmatrix(CCtmp);
                        bw = L(:,:,1);
                        bw(bw>=1) = 1;
                        CCtmp = bwconncomp(bw);

                        newROIs{i} = {idx, CCtmp.PixelIdxList};                                   
                    end

                    for i=numel(newROIs):-1:1  
                        c = newROIs{i};
                        if (c{1} == 1)
                           CC.PixelIdxList = [c{2}, CC.PixelIdxList{c{1}+1:end}];
                        else
                           CC.PixelIdxList = [CC.PixelIdxList{1:c{1}-1}, c{2}, CC.PixelIdxList{c{1}+1:end}];
                        end
                    end

                    emptyIdx = cellfun(@isempty, CC.PixelIdxList);
                    CC.PixelIdxList( emptyIdx ) = [];                
                end

                CC.NumObjects = numel(CC.PixelIdxList);
            end     
        end
    end
end

