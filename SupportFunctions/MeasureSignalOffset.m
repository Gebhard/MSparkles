%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [offset, maxCorr] = MeasureSignalOffset(sig1, sig2, Fs1, Fs2)
    %MEASUREOFFSET searches for the closest match of sig2 with sampling rate
    %Fs2 within sig1 with sampling rate Fs1. It returns the temporal offset
    %in seconds of sig2 w.r.t the start of sig1.
    
    %rescale both signals to range [0..1]
    sig1 = rescale(sig1(:));
    sig2 = rescale(sig2(:));
    
    %Resample the signal with the lower sampling rate to get the highest
    %possible precission
    if Fs2 < Fs1
        [P1,Q1] = rat(Fs1/Fs2);         % Rational fraction approximation    
        sig2 = resample(sig2,P1,Q1);	% Change sampling rate by rational factor        
    elseif Fs1 < Fs2
        [P1,Q1] = rat(Fs2/Fs1);         % Rational fraction approximation    
        sig1 = resample(sig1,P1,Q1);	% Change sampling rate by rational factor
    end
    
    %Pad the shorter signal with trailling zeros to get normalized
    %correlation
    numSig1 = numel(sig1);
    numSig2 = numel(sig2);
    
    if numSig1 < numSig2
        sig1(end:numSig2) = 0;
    elseif numSig2 < numSig1
        sig2(end:numSig1) = 0;
    end
    
    %Compute cross correlation and offset
    [C,lag] = xcorr(sig1,sig2, 'normalized');
    [maxCorr,I] = max(abs(C));
    offset = lag(I) ./ max(Fs1, Fs2);
end