function hAxis = RenderEegPlot(hAxis, params) 
        hEegDataset = params.Dataset.EEGDataset;
        hAnalysis = [];

        com.EEG.Rendering.EEGTracePlot(hAxis, hEegDataset,...
                            hAnalysis, params.EegChan, params.EegSig, false, false);

        eegOffset = params.Dataset.Settings.EegSync.EegSyncStart;
        tEnd = eegOffset + params.Dataset.LastSampleTime;
        hAxis.XLim = [eegOffset, tEnd];
        TL = hAxis.XTickLabel;
        hAxis.XTick = hAxis.XTick + eegOffset;
        hAxis.XTickLabel = TL;
end