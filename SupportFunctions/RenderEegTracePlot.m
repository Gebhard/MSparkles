function hAxis = RenderEegTracePlot(hAxis, params)
    hEegDataset = params.Dataset.EEGDataset;
    hAnalysis = hEegDataset.Analyses.CurrentAnalysis;
    chan = params.Dataset.Settings.EegSync.DisplayEegChannel;
                    
    com.EEG.Rendering.EEGTracePlot(hAxis, hEegDataset,...
                        hAnalysis, chan);
                    
    eegOffset = params.Dataset.Settings.EegSync.EegSyncStart;
    tEnd = eegOffset + params.Dataset.LastSampleTime;
    hAxis.XLim = [eegOffset, tEnd];
end

