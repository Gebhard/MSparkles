%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

%% signal = Matrix to be compared against a ground truth (e.g. F)
%% truth = Ground truth matrix to be compared against (e.g. F0)
%% Both matrices have to be of the same size.
%% SSIM1D computes the structural dissimilarity index (DSSIM), which is a variant of the ordinary
%% structural similarity index (SSIM), along the third dimension (temporal axes of 2D+t calcium data).
%% DSSIM = (1 - SSIM) / 2

%% getGaussianKernel kreates a 1D, 2D, or 3D gaussian filter kernel, depending on the size vector N.
%% sigma = the sigma auf the gauss kernel to generate
%% N = The size of the gauss kernel. If na is a scalar, getGaussianKernel creates a 1D kernel.
%% if N is a 2D vector [x y] or a 3D vector [x y z]it creates a 2D kernel and a 3D kernel respetively.
%% Please note, that the kernel sizes speciefied by N are halfsizes, meaning the resinting kernel has a size of
%% [2*x+1 2*y+1 2*z+1] 

%this function is kept generic on purpose
function ssimmap  = SSIM1D( signal, truth )
    %Compute structural similarity for 1D signal against ground-truth signal    

    sigma = 2;    
    h = getGaussianKernel(sigma, [2,2,7]);   
    
    [k1, Kn, err] = decompose_kernel(h);
    
    %Gaussian weighted mean values as suggested in the paper
    
    %m_sig = imfilter(single(signal), h,'conv','replicate');    
    m_sig = imfilter(single(signal), k1{1},'conv','replicate');
    m_sig = imfilter(m_sig, k1{2},'conv','replicate');
    m_sig = imfilter(m_sig, k1{3},'conv','replicate');
    
    %m_tru = imfilter(single(truth), h,'conv','replicate');    
    m_tru = imfilter(single(truth), k1{1},'conv','replicate');
    m_tru = imfilter(m_tru, k1{2},'conv','replicate');
    m_tru = imfilter(m_tru, k1{3},'conv','replicate');
    
    m_sigtru = m_sig.*m_tru;
    m_sig = m_sig.^2;
    m_tru = m_tru.^2;
    
    %compute gauss weighted standard deviations

    %sigmax2 = imfilter(signal.^2, h,'conv','replicate') - m_sig; 
    sigmax2 = imfilter(signal.^2, k1{1},'conv','replicate');
    sigmax2 = imfilter(sigmax2, k1{2},'conv','replicate');
    sigmax2 = imfilter(sigmax2, k1{3},'conv','replicate') - m_sig;
    
    %sigmay2 = imfilter(truth.^2, h,'conv','replicate') - m_tru;      
    sigmay2 = imfilter(truth.^2, k1{1},'conv','replicate');
    sigmay2 = imfilter(sigmay2, k1{2},'conv','replicate');
    sigmay2 = imfilter(sigmay2, k1{3},'conv','replicate') - m_tru;
    
    %sigmaxy = imfilter(signal.*truth, h,'conv','replicate') - m_sigtru;
    sigmaxy = imfilter(signal.*truth, k1{1},'conv','replicate');
    sigmaxy = imfilter(sigmaxy, k1{2},'conv','replicate');
    sigmaxy = imfilter(sigmaxy, k1{3},'conv','replicate') - m_sigtru;
    
    exponents = [1,1,1];
    
    %%use actual dynamic range of input signal
    dynRng = max(signal(:)) - min(signal(:));        
    C = [(0.01*dynRng).^2 (0.03*dynRng).^2 ((0.03*dynRng).^2)/2];
    
    % General case: Equation 12 from [1] 
    % Luminance term
    if (exponents(1) > 0)
        num = 2*m_sigtru + C(1);
        den = m_sig + m_tru + C(1); 
        ssimmap = guardedDivideAndExponent(num,den,C(1),exponents(1));
    else 
        ssimmap = ones(size(A), 'like', A);
    end
    
    % Contrast term
    sigmaxsigmay = [];
    if (exponents(2) > 0)  
        sigmaxsigmay = sqrt(sigmax2.*sigmay2);
        num = 2*sigmaxsigmay + C(2);
        den = sigmax2 + sigmay2 + C(2); 
        ssimmap = ssimmap.*guardedDivideAndExponent(num,den,C(2),exponents(2));        
    end
    
    % Structure term
    if (exponents(3) > 0)
        num = sigmaxy + C(3);
        if isempty(sigmaxsigmay)
            sigmaxsigmay = sqrt(sigmax2.*sigmay2);
        end
        den = sigmaxsigmay + C(3); 
        ssimmap = ssimmap.*guardedDivideAndExponent(num,den,C(3),exponents(3));        
    end
  
    %Structural dissimilarity    
    ssimmap = real( (1 - ssimmap) / 2 );
end

function component = guardedDivideAndExponent(num, den, C, exponent)

if C > 0
    component = num./den;
else
    component = ones(size(num),'like',num);
    isDenNonZero = (den ~= 0);
    component(isDenNonZero) = num(isDenNonZero)./den(isDenNonZero);
end

if (exponent ~= 1)
    component = component.^exponent;
end

end

%% Computes normalized gaussian kernels in the specified dimensions
%% 2D case not tested yet.
%% sigma = the sigma of the kernel to generate
%% N = size vector, specifying the half sizes of the gauss kernel to be generated

function gaussFilt = getGaussianKernel(sigma, N)
% Get 1D, 2D or 3D Gaussian weighting filter
    numDims = numel(N);
    
    if (numDims == 1)
        gaussFilt = zeros(3,3, 2*N+1);
        gaussFilt(2,2,:) = gausswin( 2*N+1, sigma);
        sumFilt = sum(gaussFilt(2,2,:));
        if (sumFilt ~= 0)
         gaussFilt  = gaussFilt/sumFilt;
        end
    elseif (numDims == 2)
        gaussFilt = fspecial('gaussian',2*N+1,sigma);
    else
        [x,y,z] = ndgrid(-N(1):N(1),-N(2):N(2),-N(3):N(3));
        arg = -(x.*x + y.*y + z.*z)/(2*sigma*sigma);
        gaussFilt = exp(arg);
        gaussFilt(gaussFilt<eps*max(gaussFilt(:))) = 0;
        sumFilt = sum(gaussFilt(:));
        if (sumFilt ~= 0)
         gaussFilt  = gaussFilt/sumFilt;
        end
    end

end
