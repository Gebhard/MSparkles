%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function SaveGroupImage(hAnalysis, varargin)
    
    SaveGroupImage_version = "2.1.000";
    SaveGroupImage_date = "18.02.2019";
    
    p = inputParser;
    p.StructExpand    = true;  % if we allow parameter  structure expanding
    p.CaseSensitive   = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
    p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
    p.FunctionName    = "SaveGroupImage v." + SaveGroupImage_version +" (" + SaveGroupImage_date + ")"; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.
    
    addParameter(p, "OutPath",          [],     @(x) ischar(x) || isstring(x) || isempty(x));
    addParameter(p, "Resultset",        [],     @(x) isa(x, "com.CalciumAnalysis.Analysis.MAnalysisResult")); 
    addParameter(p, "ShowRoiNumbers",   true,   @(x) islogical(x));
    addParameter(p, "Dataset",          [],     @(x) isempty(x) || isa(x, "com.CalciumAnalysis.Data.MCaDataset")); 

    % input parsing
    parse(p,varargin{:});
    par = p.Results;  

    filePath = [];
    
    if (~isempty(par.OutPath))
        [fPath, ~, ~] = fileparts(par.OutPath);
        if isfolder(fPath)
            filePath = par.OutPath;
        end
    end
    
    if (isempty(filePath))
        if ~isempty(par.Dataset)
            [outpath,~,~] = fileparts(par.Dataset.OrgDatasetPath);
        else
            outpath = "";
        end
        
        [file,path] = uiputfile(["*.png","PNG";
                             "*.jpg","JPEG"], "Save image", outpath);
        filePath = [path file];         
    end
    
    if hAnalysis.AnalysisType == com.Enum.AnalysisType.Dynamic
        frames = 1:par.Dataset.MetaData.SamplesT;
    else
        frames = 1;
    end
    
    foundGroups = par.Resultset.RenderRoiMap(hAnalysis.AnalysisSettings, [], frames, 255, [], true, par.ShowRoiNumbers);
    imwrite(foundGroups, filePath);
end




