%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function [pathName, fileName] = SelectFile(strFilter, strTitle, searchfolder, IsSaveDlg)
%SELECTFILE Opens a uigetfile dialog and saves the path of the selected
%file to LastPath in MGlobalSettings
%   strFilter       Filter string to limit file formats
%   strTitle        Dialog title
%   searchfolder    Initial path to display when uigetfile dialog opens. If
%                   searchfolder, LastPath from MGlobalSettings will be
%                   used.
%
%   pathName        Path to the selected file
%   fileName        Name of the selected file. If uigetfile was closed with
%                   'Cancel', fileName is 0.
    if nargin < 4
       IsSaveDlg = false; 
    end

    if nargin < 3 || isempty(searchfolder)
        hGlobalSettings = MGlobalSettings.GetInstance();
        if ~isempty(hGlobalSettings)
            searchPath = hGlobalSettings.LastPath;
        end
    else
        searchPath = searchfolder;
    end
    
%     try
%         if ~isfolder(searchPath) && ~isfile(searchPath)
%             searchPath = char('');
%         end
%     catch %It's definetly not a folder (nor a file)
%         searchPath = char('');
%     end
            
    if IsSaveDlg
        [fileName, pathName] = uiputfile( strFilter, strTitle, searchPath);
    else
        [fileName, pathName] = uigetfile( strFilter, strTitle, searchPath, 'MultiSelect', 'off');
    end
    
    if ~isempty(fileName) && any(fileName ~= 0)
        hGlobalSettings.LastPath = pathName;        
    end
end