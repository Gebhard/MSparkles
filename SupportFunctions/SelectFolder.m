%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function pathName = SelectFolder(searchfolder, strTitle)
%SELECTFOLDER Opens a uigetdir dialog and saves the selected path to 
%LastPath in MGlobalSettings
%   searchfolder    Initial path of the folder select dialog. If
%                   searchfolder is empty, LastPath from MGlobalSettings
%                   will be used.
%   strTitle        Dialog title
%
%   pathName        Path to the selected file If uigetdir was closed with
%                   'Cancel', pathName is 0.

    if nargin < 2 || isempty(searchfolder)
        hGlobalSettings = MGlobalSettings.GetInstance();
        if ~isempty(hGlobalSettings)
            searchPath = hGlobalSettings.LastPath;
        end
    else
        searchPath = searchfolder;
    end
    
    try
        if ~isfolder(searchPath)
            searchPath = char('');
        end
    catch %It's definetly not a folder (nor a file)
        searchPath = char('');
    end
            
    pathName = uigetdir(searchPath, strTitle);
    
    if ~isempty(pathName) && any(pathName ~= 0)
        hGlobalSettings.LastPath = pathName;        
    end
end