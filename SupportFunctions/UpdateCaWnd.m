%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function UpdateCaWnd(windowName)
    %UPDATECAWND updates parts or all of MSparkles' main calcium analysis
    %window. 
    %   windowName  The name of the window component to be updated as
    %               string array. Accepted values: "All", "StackScroller",
    %               "GraphList", "DataExplorer".
    %               If windowName is empty, it will defaultto update the
    %               entire window.    
    
    if nargin == 0 || isempty(windowName)
        windowName = "All";
    end
           
    hInst = MSparklesSingleton.GetInstance();
    if ~isempty( hInst )
        hCaWnd = hInst.hCaMainWnd;
        if ~isempty(hCaWnd)
            for wnd =  windowName
                switch wnd
                    case "All"
                        hCaWnd.UpdateUI();
                    case "StackScroller"
                        hCaWnd.UpdateStackScroller();
                    case "GraphList"
                        hCaWnd.UpdateGraphList();
                    case "DataExplorer"
                        hCaWnd.UpdateDataExplorer();
                    otherwise
                        com.common.Logging.MLogManager.Warning(sprintf("UpdateCaWnd: Ignoring invalid option (%s)", wnd));
                end
            end
        end
    end
end
