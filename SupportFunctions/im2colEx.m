function b=im2colEx(varargin)
%IM2COL Rearrange image blocks into columns. Modified version to handle 3D
%images
%   B = IM2COL(A,[M N],'distinct') rearranges each distinct
%   M-by-N block in the image A into a column of B. IM2COL pads A
%   with zeros, if necessary, so its size is an integer multiple
%   of M-by-N. If A = [A11 A12; A21 A22], where each Aij is
%   M-by-N, then B = [A11(:) A21(:) A12(:) A22(:)].
%
%   B = IM2COL(A,[M N],'sliding') converts each sliding M-by-N
%   block of A into a column of B, with no zero padding. B has
%   M*N rows and will contain as many columns as there are M-by-N
%   neighborhoods in A. If the size of A is [MM NN], then the
%   size of B is (M*N)-by-((MM-M+1)*(NN-N+1). Each column of B
%   contains the neighborhoods of A reshaped as NHOOD(:), where
%   NHOOD is a matrix containing an M-by-N neighborhood of
%   A. IM2COL orders the columns of B so that they can be
%   reshaped to form a matrix in the normal way. For example,
%   suppose you use a function, such as SUM(B), that returns a
%   scalar for each column of B. You can directly store the
%   result in a matrix of size (MM-M+1)-by-(NN-N+1) using these
%   calls:
%
%        B = im2col(A,[M N],'sliding');
%        C = reshape(sum(B),MM-M+1,NN-N+1);
%
%   B = IM2COL(A,[M N]) uses the default block type of
%   'sliding'.
%
%   B = IM2COL(A,'indexed',...) processes A as an indexed image,
%   padding with zeros if the class of A is uint8 or uint16, or
%   ones if the class of A is double.
%
%   Class Support
%   -------------
%   The input image A can be numeric or logical. The output matrix
%   B is of the same class as the input image.
%
%   Example
%   -------
%   Calculate the local mean using a [2 2] neighborhood with zero padding.
%
%       A = reshape(linspace(0,1,16),[4 4])'
%       B = im2col(A,[2 2])
%       M = mean(B)
%       newA = col2im(M,[1 1],[3 3])
%
%   See also BLOCKPROC, COL2IM, COLFILT, NLFILTER.

%   Copyright 1993-2016 The MathWorks, Inc.

[a, block, kind, padval] = parse_inputs(varargin{:});

if strcmp(kind, 'distinct')
        
        nrows = block(1);
        ncols = block(2);
        nElementBlk = nrows*ncols;
    
        mpad = mod(size(a,1),nrows); if mpad>0, mpad = block(1)-mpad; end
        npad = mod(size(a,2),ncols); if npad>0, npad = block(2)-npad; end
         
        aPad = mkconstarray(class(a), padval, [size(a,1)+mpad size(a,2)+npad]);
        aPad(1:size(a,1),1:size(a,2)) = a;

        t1 = reshape(aPad,nrows,size(aPad,1)/nrows,[]);
        t2 = reshape(permute(t1,[1 3 2]),size(t1,1)*size(t1,3),[]);
        t3 =  permute(reshape(t2,nElementBlk,size(t2,1)/nElementBlk,[]),[1 3 2]);
        b = reshape(t3,nElementBlk,[]);
    
    
elseif strcmp(kind,'sliding')
    [ma,na,ka] = size(a);
    m = block(1); n = block(2); k = block(3);
    
    if any([ma na ka] < [m n k]) % if neighborhood is larger than image
        b = zeros(m*n*k,0);
        return
    end
    
    % Create Hankel-like indexing sub matrix.
    nc = ma-m+1; nn = na-n+1; nk = ka-k+1;
    cidx = (0:m-1)'; ridx = 1:nc;
    
    t = cidx(:,ones(nc,1)) + ridx(ones(m,1),:);    % Hankel Subscripts
    tt = zeros(m*n*k,nc);
    rows = 1:m;
    
    for ki=0:nk-1
        for i=0:n-1
            tt((ki*(n*m))+(i*m+rows),:) = (t+ma*i) + ki*(ma*na);
        end
    end
    
    ttt = zeros(m*n*k,nc*nn, nk);
    cols = 1:nc;
    
    for ki=1:nk
        for j=0:nn-1
            ttt(:,j*nc+cols, ki) = tt+ma*j + (ki-1)*(ma*na);
        end
    end
    
    % If a is a row vector, change it to a column vector. This change is
    % necessary when A is a row vector and [M N] = size(A).
    if ndims(a) == 2 && na > 1 && ma == 1
        a = a(:);
    end
    b = a(ttt);
    
else
    % We should never fall into this section of code.  This problem should
    % have been caught in input parsing.
    error(message('images:im2col:internalErrorUnknownBlockType', kind));
end

%%%
%%% Function parse_inputs
%%%
function [a, block, kind, padval] = parse_inputs(varargin)

narginchk(2,4);

switch nargin
    case 2
        if (strcmp(varargin{2},'indexed'))
            error(message('images:im2col:tooFewInputs'))
        else
            % IM2COL(A, [M N K])
            a = varargin{1};
            block = varargin{2};
            [~, sm] = size(block);
            
            if sm == 2 || ndims(a) == 2
                block(3) = 1;
            end
            
            kind = 'sliding';
            padval = 0;
        end
        
    case 3
        if (strcmp(varargin{2},'indexed'))
            % IM2COL(A, 'indexed', [M N])
            a = varargin{1};
            block = varargin{3};
            kind = 'sliding';
            padval = 1;
        else
            % IM2COL(A, [M N], 'kind')
            a = varargin{1};
            block = varargin{2};
            kind = validatestring(varargin{3},{'sliding','distinct'},mfilename,'kind',3);
            padval = 0;
        end
        
    case 4
        % IM2COL(A, 'indexed', [M N], 'kind')
        a = varargin{1};
        block = varargin{3};
        kind = validatestring(varargin{4},{'sliding','distinct'},mfilename,'kind',4);
        padval = 1;
        
end

if (isa(a,'uint8') || isa(a, 'uint16'))
    padval = 0;
end
