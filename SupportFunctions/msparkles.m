%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function J = msparkles(M)
%MSPARKLES returns an M-by-3 matrix containing the default MSparkles
%colormap.

    cols = [    0.2314    0.3490    0.6000;
                0.1294    0.4588    0.6078;
                0.8667    0.2941    0.2235;
                0.6863    0.0235    0.0235;
                0.1451    0.8275    0.4000;
                0.8941    0.2510    0.3725;
                     0    0.5176    1.0000;
                0.8863    0.5843    0.2784;
                0.1020    0.7176    0.9176;
                0.7412    0.0314    0.1098;
                0.8745    0.1255    0.1608;
                0.0353    0.7216    0.2431;
                0.9176    0.2980    0.5373;
                0.3333    0.6745    0.9333;
                     0    0.4667    0.7098;
                0.2784    0.6784    0.8706;
                0.8039    0.1255    0.1216;
                0.8549    0.3333    0.1843;
                     0    0.7647         0;
                1.0000         0    0.5176;
                     0    0.4667    0.7098;
                0.2980    0.4588    0.6392;
                0.9216    0.2863    0.1412;
                1.0000    0.4000         0;
                0.0078    0.7216    0.4588;
                0.9765    0.2824    0.4667;
                0.3137    0.6431    0.3490;
                     0    0.6863    0.9412;
                0.8314    0.8588    0.2471;
                1.0000    0.3412         0;
                1.0000    0.2000         0;
                     0    0.7059    0.5373;
                0.3647    0.7373    0.6196;
                     0    0.4941    0.8980;
                0.2549         0    0.5765;
                0.7255    0.1686    0.1529;
                0.9608    0.4902         0;
                0.8118    0.2353    0.2588;
                0.2275    0.6863    0.5216;
                1.0000    0.9882         0;
                0.9529    0.8510    0.1725;
                ];
    
    sz = size(cols);
    if (sz(1) >= M)
        J = cols(1:M,:);
    else
        numReps = ceil(M ./ sz(1));
        J = repmat(cols, numReps, 1);
        J = J(1:M, :);
    end
end

