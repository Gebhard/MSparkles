%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

function p = redyellowgreen(m)

if nargin < 1 || isempty(m)
    m = 256;
end

green =     [0, 0.5647, 0.4392];
yellow =    [0.8902, 0.7569, 0.2745];
red =       [0.9255, 0.2039, 0.3216];

% deal with small inputs
if m < 3
    if m == 0
        p = zeros(0,3);
    elseif m == 1
        p = zeros(1,3);
    else
        p = [green; red];
    end
    return
end


% create an appropriately long linearly interpolated chunk
interpCntr = ceil(m/2);
interpCol = (1/(interpCntr):1/(interpCntr):1)';

p = rescale( [interpCol*yellow + flipud(interpCol)*green; interpCol*red + flipud(interpCol)*yellow]);