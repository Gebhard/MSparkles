%--------------------------------------------------------------------------%
% Copyright © 2020 Gebhard Stopper, Center for integrative Physiology      %
% and molecular medicine (CIPMM), Medical faculty, Homburg,                %
% University of Saarland                                                   %
%                                                                          %
% E-mail: gebhard.stopper@uks.eu                                           %
% web: http://www.kirchhoff-lab.de/                                        %
%                                                                          %
% MSparkles is licensed under the Apache License, Version 2.0              %
% (the "License"); you may not use any files contained within the          %
% ScanImage release  except in compliance with the License.                %
% You may obtain a copy of the License at                                  %
% http://www.apache.org/licenses/LICENSE-2.0                               %
%                                                                          %
% Unless required by applicable law or agreed to in writing, software      %
% distributed under the License is distributed on an "AS IS" BASIS,        %
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. %
% See the License for the specific language governing permissions and      %
% limitations under the License.                                           %
%--------------------------------------------------------------------------%

  function val = wpercentile(d,w,pct)
    % calculates the weighted percentile along the first dimension
    % and the SEM according to SEM  = 1.58*IRQ/sqrt(size(d,1)) where IRQ is the
    % interquartile range (75% - 25% percentile)
    sz1 = size(d);
    % reshaping data
    N = prod(sz1(2:end));
    d = reshape(d,[sz1(1) N]);
    w = reshape(w,[sz1(1) N]);
    val = zeros([N 1]);
    for nn =1:N
      [~,indS] = sort(d(:,nn),1); % is most timeconsuming
      indS(~isfinite(d(indS,nn))) = [];
      indS(~isfinite(w(indS,nn))) = [];
      indS(w(indS,nn)==0) = [];
      if numel(unique(d(indS,nn)))>3 %sum(~isnan(unique(d(:,n))))>3 && sum(~isnan(unique(w(:,n))))>3
        % sorts the data along dim 1
        % calcs cumulative sum of weights for data which are ~NaN according data sorting
        cumd = cumsum(w(indS,nn));
        cumd = cumd / max(cumd(:));
        % search for first index above pct level
        indq2 = find(cumd>pct,1);
        % assuming cumd(ind-1)<=.5*max(cumd(:)) && cumd(ind)<cumd(ind+1)
        % calc x position of 50% cumd value by linear approcimation
        if indq2==1
          val(nn) = d(indS(indq2),nn);
        else
          try
            val(nn) = d(indS(indq2-1),nn)+(pct - cumd(indq2-1))/(cumd(indq2) - cumd(indq2-1))*( d(indS(indq2),nn)- d(indS(indq2-1),nn));
          catch
            stopHere
          end
        end
      else
        val(nn) = nanmedian(d(indS,nn));
      end
    end
  end